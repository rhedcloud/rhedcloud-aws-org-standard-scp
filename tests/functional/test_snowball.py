"""
=============
test_snowball
=============

Verify the ability to create a Snowball job in a non HIPAA account.

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Create a new role for Snowball having a trust relationship for
  importexport.amazonaws.com and read-only permission to S3
* Create a new S3 bucket
* Upload something into the new S3 bucket (because you can't export an empty
  bucket with Snowball)
* Create a Snowball job to export the new S3 bucket
* Describe the Snowball job
* Immediately cancel the Snowball job to avoid charges
* Verify that the Snowball job is Cancelled

${testcount:2}

"""

import pytest

from aws_test_functions import (
    new_bucket,
    new_policy,
    new_role,
    new_snowball_job,
)


@pytest.fixture(scope='module')
def snowball(session):
    print('session profile:', session.profile_name)
    return session.client('snowball')


@pytest.fixture(scope='module')
def bucket(session):
    s3 = session.resource('s3')
    with new_bucket('test-snowball', s3=s3) as b:
        # the S3 bucket requires data before it can be exported
        b.upload_file('./tests/resources/upload_file.rtf', 'upload.rtf',
                      ExtraArgs={'ServerSideEncryption': 'AES256'})

        yield b


@pytest.fixture(scope='module')
def role():
    stmt = [{
        'Effect': 'Allow',
        'Action': [
            's3:GetBucketLocation',
            's3:GetObject',
            's3:ListBucket',
        ],
        'Resource': 'arn:aws:s3:::*',
    }]

    with new_policy('test_snowball', stmt) as p:
        with new_role('test_snowball', 'importexport', p.arn) as r:
            yield r


@pytest.fixture(scope='module')
def setup_fixtures(role, bucket):
    yield


@pytest.mark.parametrize('snowball_type', ('STANDARD', 'EDGE'))
def test_snowball_access_permitted(role, bucket, snowball, snowball_type):
    """

    :testid: snowball-f-1-1, snowball-f-1-2
    """

    job_id = None

    print('creating snowball job', snowball_type)
    with new_snowball_job(
            role,
            bucket.name,
            snowball_type,
            snowball=snowball) as job_id:
        # describe the snowball job
        desc = snowball.describe_job(JobId=job_id)['JobMetadata']
        print('Described Snowball job: {}'.format(job_id))

        assert desc['JobType'] == 'EXPORT', 'Unexpected Snowball job type'
        assert desc['JobState'] == 'New', 'Unexpected Snowball job state'

    if job_id is not None:
        # describe the snowball job
        desc = snowball.describe_job(JobId=job_id)['JobMetadata']
        assert desc['JobState'] == 'Cancelled', 'Snowball job not cancelled'
