"""
===============
test_secretsmanager
===============

Verify access to use AWS SecretsManager

Plan:

* Attempt to create a secret
* Verify secret is given by list_secrets
* Verify secret is given by describe_secret
* Attempt to put a new value in the created secret
* Verify value is given by get_secret_value
* Attempt to update the secret
* Verify secret is successfully updated
* Attempt to delete the secret

${testcount:16}

"""

import pytest

from aws_test_functions import (
    create_secret,
    delete_secret,
    has_status,
    ignore_errors,
    in_setup_org,
    make_identifier,
    unexpected,
)


# This service is fully blocked
#pytestmark = [pytest.mark.scp_check('list_secrets')]

# Create a reusable functor to move the AWS account to the SETUP_ORG before
# attempting to invoke the requested function. This is necessary to ensure that
# we make the best effort to remove resources created during testing (since we
# should not be able to delete with the SCP in effect in the TEST_ORG).
cleaner = in_setup_org('Secrets Manager cleanup', one_way=True)


@pytest.fixture(scope='module')
def secretsmanager(session):
    """A shared handle to SecretsManager API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the SecretsManager API.

    """

    return session.client('secretsmanager')


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'secret_arn': 'not-created',
    }


def test_create_secret(shared_vars, stack):
    """Verify ability to create a secret

    :testid:

    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param contextlib.ExitStack stack:
        A context manager shared across all SecretsManager tests.

    """

    name = make_identifier("TestSecret")
    secret_arn = create_secret(name, "Testing SCP", "test secret string")
    assert secret_arn is not None, 'Secret failed to create'

    shared_vars['secret_arn'] = secret_arn
    stack.callback(
        ignore_errors(cleaner(delete_secret)),
        arn=secret_arn
    )


def test_list_secrets(secretsmanager, secret_arn):
    """Verify ability to list secrets

    :testid:

    :param SecretsManager.Client secretsmanager:
        A handle to the SecretsManager API.
    :param str secret_arn:
        The arn of the secret to delete.

    """

    res = secretsmanager.list_secrets()
    assert has_status(res, 200), unexpected(res)
    assert secret_arn is not None, 'List Secrets did not raise access denied, and the secret was not created'

    found = False
    for secret in res['SecretList']:
        if secret['ARN'] == secret_arn:
            found = True
            break

    assert found, 'Could not list the created secret {}'.format(unexpected(res))


def test_describe_secret(secretsmanager, secret_arn):
    """Verify ability to describe a secret

    :testid:

    :param SecretsManager.Client secretsmanager:
        A handle to the SecretsManager API.
    :param str secret_arn:
        The arn of the secret to delete.

    """

    res = secretsmanager.describe_secret(SecretId=secret_arn)
    assert has_status(res, 200), unexpected(res)
    assert 'ARN' in res, 'Could not describe the created secret {}'.format(unexpected(res))
    assert res['ARN'] == secret_arn, 'Unexpected ARN in describe secret {}'.format(unexpected(res))


def test_put_secret_value(secretsmanager, secret_arn):
    """Verify ability to put a secret value into a secret

    :testid:

    :param SecretsManager.Client secretsmanager:
        A handle to the SecretsManager API.
    :param str secret_arn:
        The arn of the secret to delete.

    """

    secret_value = 'updated secret string'
    res = secretsmanager.put_secret_value(SecretId=secret_arn,
                                          SecretString=secret_value)
    assert has_status(res, 200), unexpected(res)

    res = secretsmanager.get_secret_value(SecretId=secret_arn)
    assert has_status(res, 200), unexpected(res)
    assert 'SecretString' in res, 'Could not get the secret value {}'.format(unexpected(res))
    assert res['SecretString'] == secret_value, 'Secret value failed to update {}'.format(unexpected(res))


def test_update_secret(secretsmanager, secret_arn):
    """Verify ability to update a secret

    :testid:

    :param SecretsManager.Client secretsmanager:
        A handle to the SecretsManager API.
    :param str secret_arn:
        The arn of the secret to delete.

    """

    updated_desc = 'Updated Description'
    res = secretsmanager.update_secret(SecretId=secret_arn,
                                       Description=updated_desc)
    assert has_status(res, 200), unexpected(res)

    res = secretsmanager.describe_secret(SecretId=secret_arn)
    assert res['Description'] == updated_desc, 'Secret failed to update {}'.format(unexpected(res))


def test_delete_secret(secretsmanager, secret_arn, shared_vars):
    """Verify ability to delete a secret

    :testid:

    :param SecretsManager.Client secretsmanager:
        A handle to the SecretsManager API.
    :param str secret_arn:
        The arn of the secret to delete.

    """

    delete_secret(secret_arn)

    res = secretsmanager.describe_secret(SecretId=secret_arn)
    assert 'DeletedDate' in res, 'Could not delete the created secret {}'.format(unexpected(res))
    shared_vars['secret_arn'] = 'not-created'
