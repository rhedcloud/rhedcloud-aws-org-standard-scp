"""
========
test_swf
========

Verify access to use Amazon Simple Workflow Service (SWF).

Plan:

* Register a SWF domain
* Register a workflow type
* Register an activity type
* Start a workflow execution
* Wait for the workflow execution to end

${testcount:10}

"""

import pytest

from aws_test_functions import (
    ignore_errors,
    make_identifier,
    retry,
    unexpected,
)

# Treat errors about unknown resources as access denied errors (because they
# likely originate from an access denied error earlier in the suite).
ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden|UnknownResourceFault).*'


@pytest.fixture(scope='module')
def swf(session):
    """A shared handle to SWF API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the SWF API.

    """

    return session.client('swf')


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'domain': 'not-registered',
        'wf_type': 'not-registered',
        'activity_type': 'not-registered',
        'wf_id': 'not-created',
        'run_id': 'not-created',
    }


def test_register_domain(swf, stack, shared_vars):
    """Verify access to create register domains.

    :testid: swf-f-1-1

    :param SWF.Client swf:
        A handle to the SWF API.
    :param contextlib.ExitStack stack:
        A context manager shared across all SWF tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    domain = shared_vars['domain'] = make_identifier('test-domain')
    res = swf.register_domain(
        name=domain,
        workflowExecutionRetentionPeriodInDays='0',
    )
    assert res, unexpected(res)
    print('Registered domain: {}'.format(domain))

    stack.callback(
        ignore_errors(swf.deprecate_domain),
        name=domain,
    )


def test_register_workflow_type(swf, domain, stack, shared_vars):
    """Verify access to register workflow types.

    :testid: swf-f-1-2

    :param SWF.Client swf:
        A handle to the SWF API.
    :param str domain:
        Name of a registered SWF domain.
    :param contextlib.ExitStack stack:
        A context manager shared across all SWF tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = shared_vars['wf_type'] = make_identifier('test-wf-type')
    version = '1'
    res = swf.register_workflow_type(
        domain=domain,
        name=name,
        version=version,
    )
    assert res, unexpected(res)
    print('Registered workflow type: {}'.format(domain))

    stack.callback(
        ignore_errors(swf.deprecate_workflow_type),
        domain=domain,
        workflowType=dict(
            name=name,
            version=version,
        ),
    )


def test_register_activity_type(swf, domain, stack, shared_vars):
    """Verify access to register activity types.

    :testid: swf-f-1-3

    :param SWF.Client swf:
        A handle to the SWF API.
    :param str domain:
        Name of a registered SWF domain.
    :param contextlib.ExitStack stack:
        A context manager shared across all SWF tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = shared_vars['activity_type'] = make_identifier('test-act-type')
    version = '1'
    res = swf.register_activity_type(
        domain=domain,
        name=name,
        version=version,
    )
    assert res, unexpected(res)
    print('Registered activity type: {}'.format(domain))

    stack.callback(
        ignore_errors(swf.deprecate_activity_type),
        domain=domain,
        activityType=dict(
            name=name,
            version=version,
        ),
    )


def test_start_workflow_execution(swf, domain, wf_type, stack, shared_vars):
    """Verify access to start workflow executions.

    :testid: swf-f-1-4

    :param SWF.Client swf:
        A handle to the SWF API.
    :param str domain:
        Name of a registered SWF domain.
    :param str wf_type:
        Name of a registered SWF workflow type.
    :param contextlib.ExitStack stack:
        A context manager shared across all SWF tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    task_name = make_identifier('test-task')
    wf_id = shared_vars['wf_id'] = make_identifier('test-wf')
    res = swf.start_workflow_execution(
        domain=domain,
        workflowId=wf_id,
        workflowType=dict(
            name=wf_type,
            version='1',
        ),
        executionStartToCloseTimeout='10',
        taskList=dict(
            name=task_name,
        ),
        taskStartToCloseTimeout='5',
        childPolicy='TERMINATE',
    )
    assert 'runId' in res, unexpected(res)
    run_id = shared_vars['run_id'] = res['runId']
    print('Started workflow execution: {}'.format(run_id))


def test_describe_workflow_execution(swf, domain, wf_id, run_id):
    """Verify access to describe workflow executions.

    :testid: swf-f-1-5

    :param SWF.Client swf:
        A handle to the SWF API.
    :param str domain:
        Name of a registered SWF domain.
    :param str wf_id:
        ID of a SWF workflow.
    :param str run_id:
        ID of a SWF execution.

    """

    def _test(res):
        return 'executionInfo' in res and \
            res['executionInfo'].get('executionStatus') == 'CLOSED'

    res = retry(
        swf.describe_workflow_execution,
        kwargs=dict(
            domain=domain,
            execution=dict(
                workflowId=wf_id,
                runId=run_id,
            ),
        ),
        msg='Checking workflow execution',
        until=_test,
        pred=lambda ex: 'AccessDenied' not in str(ex),
        delay=30,
        show=True,
    )
    assert _test(res), unexpected(res)

    # the test description mentions 'COMPLETED', but we're checking for
    # 'TIMED_OUT' because nothing is really going to happen during this test
    assert res['executionInfo']['closeStatus'] == 'TIMED_OUT', unexpected(res)
