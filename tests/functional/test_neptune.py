"""
===============
test_neptune
===============

Verify access to use AWS Neptune DB

#FIXME: This test is an incredibly minimal smoke test to ensure
#FIXME: the client is producing acceptable results when blocked

Plan:

* Attempt to list clusters

${testcount:2}

"""

import pytest

from aws_test_functions import (
    has_status,
    unexpected
)


pytestmark = [
    # This service is fully blocked
    pytest.mark.scp_check('describe_db_clusters'),

    pytest.mark.xfail(reason="Neptune DB as far as I can tell can not be blocked through SCP")
]


@pytest.fixture(scope='module')
def neptune(session):
    """A shared handle to Neptune DB API.


    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Neptune DB API.

    """

    return session.client('neptune')


def test_describe_clusters(neptune):
    """Verify ability list neptune clusters

    :testid:

    :param Neptune.Client neptune:
        A handle to the Neptune DB API.

    """

    res = neptune.describe_db_clusters()
    assert has_status(res, 200), unexpected(res)
