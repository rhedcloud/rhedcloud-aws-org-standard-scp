"""
=================
test_codepipeline
=================

Verify users can utilize AWS CodePipeline functionality.

${testcount:5}

* Create a new test user with the RHEDcloudAdministratorRole.
* List pipelines
* Create a pipeline
* Update a pipeline
* Use get_pipeline
* Use get_pipeline_state
* Start a pipeline execution
* List pipeline executions
* Create a custom action type
* List action types
* Delete a custom action type

"""

import pytest

from aws_test_functions import (
    aws_client,
    get_or_create_role,
    has_status,
    ignore_errors,
    make_identifier,
    unexpected,
)

from tests.functional import test_codedeploy as cd

# This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('list_pipelines')]

pytest_plugins = [
    'tests.plugins.codedeploy',
]

# treat errors about nonexistent items as access denied errors (because they
# likely originate from an access denied error earlier in the suite).
ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden|Authorization|DoesNotExist).*'


@aws_client('codepipeline')
def delete_action_type(category, provider, version, *, codepipeline=None):
    """Delete a custom action type.

    :param str category:
        Category of the custom action type.
    :param str provider:
        Provider of the service used in the custom action.
    :param str version:
        Version of the custom action type to delete.

    :returns:
        A dictionary.

    """

    print('Deleting custom action type: {}'.format((category, provider, version)))

    return codepipeline.delete_custom_action_type(
        category=category,
        provider=provider,
        version=version,
    )


@pytest.fixture(scope='module')
def codepipeline(session):
    """A shared handle to CodePipeline APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the CodePipeline APIs.

    """

    return session.client('codepipeline')


@pytest.fixture(scope='module')
def role():
    """Create a new role for CodePipeline.

    :returns:
        An IAM.Role.

    """

    return get_or_create_role(
        'code_pipeline_service',
        'AWSCodePipelineFullAccess',
        service=['codedeploy', 'codepipeline'],
        service_role=True,
    )


@pytest.fixture(scope='module')
def pipeline_name():
    """Generate a random name for a pipeline.

    :returns:
        A string.

    """

    return make_identifier('TestCodePipeline')


@pytest.fixture(scope='module')
def action_type_info():
    """Return information about a custom action type.

    :returns:
        A dictionary.

    """

    return dict(
        category='Test',
        provider='CodeDeploy',
        version=make_identifier('type')[:9],
    )


@pytest.fixture(scope='module')
def pipeline_info(app_name, group_name, pipeline_name, role, codedeploy_bucket):
    """Return reusable information about a pipeline.

    :param str app_name:
        Name of the CodeDeploy application.
    :param str group_name:
        Name of the CodeDeploy deployment group.
    :param str pipeline_name:
        Name of the pipeline.
    :param IAM.Role role:
        Role for the pipeline to use when deploying.
    :param S3.Bucket codedeploy_bucket:
        Bucket where the pipeline assets reside.

    :returns:
        A dictionary.

    """

    return {
        'name': pipeline_name,
        'roleArn': role.arn,
        'artifactStore': {
            'type': 'S3',
            'location': 'test',
        },
        'stages': [dict(
            name='Source',
            actions=[dict(
                name='Source',
                actionTypeId=dict(
                    category='Source',
                    owner='AWS',
                    provider='S3',
                    version='1',
                ),
                configuration=dict(
                    PollForSourceChanges='false',
                    S3Bucket=codedeploy_bucket.name,
                    S3ObjectKey='SampleCodeDeployApp_Linux.zip',
                ),
                outputArtifacts=[{'name': app_name}],
            )],
        ), dict(
            name='Staging',
            actions=[dict(
                name='test-codepipeline',
                actionTypeId=dict(
                    category='Deploy',
                    owner='AWS',
                    provider='CodeDeploy',
                    version='1',
                ),
                configuration=dict(
                    ApplicationName=app_name,
                    DeploymentGroupName=group_name,
                ),
                inputArtifacts=[{'name': app_name}],
            )],
        )],
    }


@pytest.fixture(scope='module', autouse=True)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'deploy_id': None,
    }


def test_create_application(codedeploy, app_name, stack):
    """Create a CodeDeploy application.

    :testid: codepipeline-f-1-1

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodeDeploy tests.

    """

    cd.test_create_application(codedeploy, app_name, stack)


def test_create_deployment_group(codedeploy, app_name, group_name, codedeploy_role, stack):
    """Create a CodeDeploy deployment group.

    :testid: codepipeline-f-1-1

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.
    :param str group_name:
        Name of the CodeDeploy group.
    :param boto3.IAM.Role codedeploy_role:
        A Role permitted to run CodeDeploy tasks.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodePipeline tests.

    """

    cd.test_create_deployment_group(codedeploy, app_name, group_name, codedeploy_role, stack)


def test_create_deployment(codedeploy, app_name, group_name, revision_info, shared_vars):
    """Create a CodeDeploy deployment.

    :testid: codepipeline-f-1-1

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.
    :param str group_name:
        Name of the CodeDeploy group.
    :param dict revision_info:
        Information about the revision.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    cd.test_create_deployment(codedeploy, app_name, group_name, revision_info, shared_vars)


def test_create_pipeline(codepipeline, pipeline_name, pipeline_info, stack):
    """A customer should be able to create a pipeline.

    :testid: codepipeline-f-1-1

    :param CodePipeline.Client codepipeline:
        A handle to the CodeDeploy APIs.
    :param str pipeline_name:
        Name of the CodePipeline pipeline.
    :param dict pipeline_info:
        Pipeline configuration information.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodePipeline tests.

    """

    res = codepipeline.create_pipeline(
        pipeline=pipeline_info,
    )
    assert 'pipeline' in res, unexpected(res)
    print('Created pipeline: {}'.format(pipeline_name))

    stack.callback(
        ignore_errors(codepipeline.delete_pipeline),
        name=pipeline_name,
    )


def test_list_pipelines(codepipeline, pipeline_name):
    """A customer should be able to list pipelines.

    :testid: codepipeline-f-1-2

    :param CodePipeline.Client codepipeline:
        A handle to the CodeDeploy APIs.
    :param str pipeline_name:
        Name of the CodePipeline pipeline.

    """

    res = codepipeline.list_pipelines()
    assert 'pipelines' in res, unexpected(res)

    pipelines = res['pipelines']
    assert len(pipelines), unexpected(res)

    found = False
    for pipeline in pipelines:
        if pipeline['name'] == pipeline_name:
            found = True
            break

    assert found, 'pipeline not found'


def test_update_pipeline(codepipeline, pipeline_info):
    """Verify access to update pipelines.

    :testid: codepipeline-f-1-3

    :param CodePipeline.Client codepipeline:
        A handle to the CodeDeploy APIs.
    :param str pipeline_info:
        Information about the CodePipeline pipeline.

    """

    res = codepipeline.update_pipeline(
        pipeline=pipeline_info,
    )
    assert 'pipeline' in res, unexpected(res)


def test_get_pipeline(codepipeline, pipeline_name):
    """Verify access to describe pipelines.

    :testid: codepipeline-f-1-4

    :param CodePipeline.Client codepipeline:
        A handle to the CodeDeploy APIs.
    :param str pipeline_name:
        Name of the CodePipeline pipeline.

    """

    res = codepipeline.get_pipeline(
        name=pipeline_name,
    )
    assert 'pipeline' in res, unexpected(res)


def test_get_pipeline_state(codepipeline, pipeline_name):
    """Verify access to describe pipeline state.

    :testid: codepipeline-f-1-5

    :param CodePipeline.Client codepipeline:
        A handle to the CodeDeploy APIs.
    :param str pipeline_name:
        Name of the CodePipeline pipeline.

    """

    res = codepipeline.get_pipeline_state(
        name=pipeline_name,
    )
    assert 'pipelineName' in res and res['pipelineName'] == pipeline_name, \
        unexpected(res)
    assert 'stageStates' in res, unexpected(res)
    assert len(res['stageStates']) == 2, unexpected(res)


def test_start_pipeline_execution(codepipeline, pipeline_name):
    """Verify access to start a pipeline execution.

    :testid: codepipeline-f-1-6

    :param CodePipeline.Client codepipeline:
        A handle to the CodeDeploy APIs.
    :param str pipeline_name:
        Name of the CodePipeline pipeline.

    """

    res = codepipeline.start_pipeline_execution(
        name=pipeline_name,
    )
    assert 'pipelineExecutionId' in res, unexpected(res)


def test_list_pipeline_executions(codepipeline, pipeline_name):
    """Verify access to list pipeline executions.

    :testid: codepipeline-f-1-7

    :param CodePipeline.Client codepipeline:
        A handle to the CodeDeploy APIs.
    :param str pipeline_name:
        Name of the CodePipeline pipeline.

    """

    res = codepipeline.list_pipeline_executions(
        pipelineName=pipeline_name,
    )
    assert 'pipelineExecutionSummaries' in res, unexpected(res)


def test_create_custom_action_type(codepipeline, action_type_info, stack):
    """Verify access to create custom action types.

    :testid: codepipeline-f-1-8

    :param CodePipeline.Client codepipeline:
        A handle to the CodeDeploy APIs.
    :param dict action_type_info:
        Information about an action type.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodePipeline tests.

    """

    res = codepipeline.create_custom_action_type(
        inputArtifactDetails=dict(
            minimumCount=0,
            maximumCount=1,
        ),
        outputArtifactDetails=dict(
            minimumCount=0,
            maximumCount=1,
        ),
        configurationProperties=[dict(
            name='test',
            required=False,
            key=True,
            secret=False,
        )],
        **action_type_info,
    )
    assert 'actionType' in res, unexpected(res)

    stack.callback(
        ignore_errors(delete_action_type),
        codepipeline=codepipeline,
        **action_type_info,
    )


def test_list__action_types(codepipeline):
    """Verify access to list action types.

    :testid: codepipeline-f-1-9

    :param CodePipeline.Client codepipeline:
        A handle to the CodeDeploy APIs.

    """

    res = codepipeline.list_action_types()
    assert 'actionTypes' in res, unexpected(res)


def test_delete_custom_action_type(codepipeline, action_type_info):
    """Verify access to delete custom action types.

    :testid: codepipeline-f-1-10

    :param CodePipeline.Client codepipeline:
        A handle to the CodeDeploy APIs.
    :param dict action_type_info:
        Information about the action type to delete.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodePipeline tests.

    """

    res = delete_action_type(codepipeline=codepipeline, **action_type_info)
    assert has_status(res, 200), unexpected(res)
