"""
=============
test_cloudfront
=============

Verify access to AWS CloudFront.

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Attempt to create and delete a CloudFront Origin Access Identity
* Attempt to create and delete a CloudFront S3 distribution
* Attempt to access data uploaded to an S3 Bucket by way of a CloudFront distribution
* Attempt to list the CloudFront Distributions

${testcount:5}

"""

import pytest
from urllib.request import urlopen

from aws_test_functions import (
    has_status,
    new_bucket,
    new_distribution,
    new_origin_access_identity,
    unexpected
)

#This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('list_distributions')]


@pytest.fixture(scope='module')
def cloudfront(session):
    """A shared handle to CloudFront APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the CloudFront APIs.

    """

    return session.client('cloudfront')


@pytest.fixture(scope='module')
def bucket(session):
    """A handle to an S3 Bucket.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A boto3.S3.Bucket owned by the given user

    """

    s3 = session.resource('s3')
    client = session.client('s3')
    with new_bucket('cloudfrontbucket', s3=s3, ACL='public-read') as b:
        bucket_policy = """{
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "ReadAccess",
                    "Action": [
                      "s3:GetObject"
                    ],
                    "Effect": "Allow",
                    "Resource": "arn:aws:s3:::""" + b.name + """/*",
                    "Principal": "*"
                }
            ]
        }"""

        client.put_bucket_policy(Bucket=b.name, Policy=bucket_policy)
        b.upload_file('./tests/resources/TestCloudFront.txt', 'TestCloudFront.txt', ExtraArgs={'ServerSideEncryption': 'AES256'})
        yield b


def test_list_distributions(cloudfront):
    """Users should be able to query CloudFront Distributions.

    :testid: cf-f-1-2

    :param Cloudfront.Client cloudfront:
        A handle to the CloudFront APIs.

    """

    res = cloudfront.list_distributions()
    assert has_status(res, 200), unexpected(res)


# This test takes ~30 minutes
@pytest.mark.slowtest
def test_cloudfront_create_cdn(cloudfront, bucket):
    """Users should be able to create or query a CloudFront CDN.

    :testid: cf-f-1-1
    :testid: cf-f-1-2
    :testid: cf-f-1-3

    :param Cloudfront.Client cloudfront:
        A handle to the CloudFront APIs.
    :param S3.Bucket bucket:
        A bucket with TestCloudFront.txt in the root directory

    """

    with new_origin_access_identity('TestCFOAID') as cf_access_id:
        with new_distribution('TestDistribution', cf_access_id, bucket) as distribution_id:
            distribution_info = cloudfront.get_distribution(Id=distribution_id)['Distribution']
            assert distribution_info['Status'] == 'Deployed', 'CloudFront Distribution Failed to Deploy'

            url = 'http://' + distribution_info['DomainName'] + '/TestCloudFront.txt'
            open_response = urlopen(url)

            #One of the clients along the way adds CRLF to the text file - the replaces are to ensure the two have consistent line endings.
            download_file = open_response.read().decode('utf-8').replace('\r\n','\n')
            local_file = open('./tests/resources/TestCloudFront.txt', 'r').read().replace('\r\n','\n')
            assert download_file == local_file, 'CloudFront returned mismatched file upload from S3'

            response_status = open_response.status
            assert response_status == 200, 'Unexpected response from CloudFront distribution with reason {}'.format(open_response.reason)
