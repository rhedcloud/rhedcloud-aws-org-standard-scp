"""
===============
test_workspaces
===============

Verify that customers cannot launch or remove workspaces.

Plan:

* Create an IAM Policy that allows users to launch workspaces
* Attempt to launch a workspace.
* Create an IAM Policy that allows users to remove workspaces
* Attempt to remove a workspace.

${testcount:2}

"""

from contextlib import contextmanager

import pytest

from aws_test_functions import env, get_subnet_id
from aws_test_functions.workspaces import (
    delete_workspace,
    new_directory_id,
    new_workspace_id,
)


@pytest.fixture(scope='module')
def workspaces(session):
    """A shared handle to WorkSpaces APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the WorkSpaces APIs.

    """

    return session.client('workspaces')


@pytest.fixture(scope='module')
def subnets(vpc_id):
    """Return a list of subnet IDs where the directory will be created.

    :param str vpc_id:
        ID of a VPC.

    """

    return [
        get_subnet_id(vpc_id, subnet)
        for subnet in env.RHEDCLOUD_DEFAULT_VPC_ENDPOINT_SUBNETS
    ]


@pytest.fixture(scope='module')
def get_workspace_id(workspaces, session, user, vpc_id, subnets, stack, request):
    """Wrapper around a context manager that creates and automatically removes
    a WorkSpace.

    This is setup this way so the calls to create the resources actually take
    place within the test function instead of before the test function is
    executed. If the resources were created directly in the fixture, the
    AccessDenied errors would not be handled correctly. This gets around that
    problem.

    :param WorkSpaces.Client workspaces:
        A handle to the WorkSpaces APIs.
    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param IAM.User user:
        An IAM User resource to associate with the workspace.
    :param str vpc_id:
        ID of the VPC where the workspace will be created.
    :param list subnets:
        List of subnet IDs where the workspace will be created.
    :param contextlib.ExitStack stack:
        A context manager shared across all Workspaces tests.
    :param request:
        Pytest Request object.

    """

    ds = session.client('ds')
    request.workspace_id = None

    @contextmanager
    def wrapped():
        if request.workspace_id is None:
            did = stack.enter_context(new_directory_id(vpc_id, subnets, ds=ds))
            wid = stack.enter_context(new_workspace_id(did, user, workspaces=workspaces))
            request.workspace_id = wid

        yield request.workspace_id

    yield wrapped


@pytest.mark.raises_access_denied
def test_launch_workspace(workspaces, get_workspace_id):
    """Customers should not be able to launch WorkSpaces.

    :testid: workspaces-f-5-1

    :param WorkSpaces.Client workspaces:
        A handle to the WorkSpaces APIs.
    :param contextmanager get_workspace_id:
        A dependency on the fixture with automatically sets up and destroys a
        WorkSpace.

    """

    with get_workspace_id() as workspace_id:
        workspaces.start_workspaces(
            StartWorkspaceRequests=[dict(
                WorkspaceId=workspace_id,
            )],
        )
        assert False, 'Had permission to launch workspace'


@pytest.mark.raises_access_denied
def test_remove_workspace(workspaces, get_workspace_id):
    """Customers should not be able to remove WorkSpaces.

    :testid: workspaces-f-5-2

    :param WorkSpaces.Client workspaces:
        A handle to the WorkSpaces APIs.
    :param contextmanager get_workspace_id:
        A dependency on the fixture with automatically sets up and destroys a
        WorkSpace.

    """

    with get_workspace_id() as workspace_id:
        delete_workspace(workspace_id, workspaces=workspaces)
        assert False, 'Had permission to remove workspace'
