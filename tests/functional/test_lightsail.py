"""
=============
test_lightsail
=============

Verify that the lightsail service is not accessible

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Assert that blueprints cannot be accessed and lightsail instance creation is not accessible with a hard coded default blueprint
* Assert that a static ip address cannot be allocated

${testcount:2}

"""

import pytest

from aws_test_functions import ClientError


@pytest.fixture(scope='module')
def lightsail(session):
    """A shared handle to Lightsail APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Lightsail APIs.

    """

    return session.client('lightsail')


def test_instance_creation(lightsail):
    """Verify ability to create instances[is disabled by SCP].

    :testid: ls-f-1-1

    :param Lightsail.Client lightsail:
        A handle to the Lightsail APIs.

    """
    with pytest.raises(ClientError, match='AccessDenied'):
        lightsail.get_blueprints()
        assert False, 'Had permission to access Lightsail Blueprints'

    with pytest.raises(ClientError, match='AccessDenied'):
        lightsail.create_instances(
            instanceNames=['test_lightsail_instance'],
            availabilityZone='us-east-1a',
            blueprintId='nodejs_8_4_0',
            bundleId='micro_1_0')
        assert False, 'Had permission to create Lightsail instance'


@pytest.mark.raises_access_denied
def test_static_ip_allocation(lightsail):
    """Verify ability to create static IP[is disabled by SCP].

    :testid: ls-f-1-1

    :param Lightsail.Client lightsail:
        A handle to the Lightsail APIs.

    """
    lightsail.allocate_static_ip(staticIpName="TestStaticIPLightsail")
    lightsail.release_static_ip(staticIpName="TestStaticIPLightsail")
    assert False, 'Had permission to allocate static IPs through lightsail'
