'''
===========
test_elasticbeanstalk
===========
Users should be able to deploy and delete a simple web application using
AWS Elastic Beanstalk.

Plan:

* Create a simple web application and environment with Elastic Beanstalk
* Delete a simple web application and environment with Elastic Beanstalk

${testcount:4}

'''

import pytest

from aws_test_functions import get_uuid, retry
SLEEP_TIME = 15


pytestmark = [
    pytest.mark.slowtest,

    #This service is blocked in the HIPAA repository
    pytest.mark.scp_check('list_platform_versions')
]

@pytest.fixture(scope='module')
def elasticbeanstalk(session):
    return session.client('elasticbeanstalk')


def test_create_webapp(elasticbeanstalk):
    '''Create a simple web application with Elastic Beanstalk
    Delete a simple web application in Elastic Beanstalk

    :testid: beanstalk-f-1-1
    :testid: beanstalk-f-1-2

    '''
    test_name = 'TestEbWebAppDeleteMe{}'.format(get_uuid())
    version_label = 'v1.2.7-{}'.format(get_uuid())
    env_name = 'deleteMe-{}'.format(get_uuid())[0:40]

    elasticbeanstalk.create_application_version(
        ApplicationName=test_name,
        VersionLabel=version_label,
        AutoCreateApplication=True,
        Process=True,
    )
    response = elasticbeanstalk.create_environment(
        ApplicationName=test_name,
        CNAMEPrefix=test_name,
        VersionLabel=version_label,
        Description='Delete me',
        EnvironmentName=env_name,
        SolutionStackName='64bit Amazon Linux running Python',
    )

    response = retry(
        elasticbeanstalk.describe_environments,
        kwargs=dict(EnvironmentNames=[env_name]),
        msg='Checking ElasticBeanStalk status and health...',
        until=lambda s: s['Environments'][0]['Status'] == 'Ready' and s['Environments'][0]['Health'] == 'Green',
        delay=SLEEP_TIME,
        max_attempts=100
    )

    assert response['Environments'][0]['Status'] == 'Ready', \
    'Unexpected failure, last status: {}'.format(response['Environments'][0]['Status'])

    assert response['Environments'][0]['Health'] == 'Green', \
    'Unexpected failure, last health: {}'.format(response['Environments'][0]['Health'])

    elasticbeanstalk.delete_application(ApplicationName=test_name,
                                        TerminateEnvByForce=True)
    response = retry(
        elasticbeanstalk.describe_environments,
        kwargs=dict(EnvironmentNames=[env_name]),
        msg='Checking ElasticBeanStalk termination...',
        until=lambda s: s['Environments'][0]['Status'] == 'Terminated',
        delay=SLEEP_TIME,
        max_attempts=100
    )

    assert response['Environments'][0]['Status'] == 'Terminated', \
    'Unexpected failure, ElasticBeanStalk failed to terminate, last status: {}'.format(response['Environments'][0]['Status'])