"""
=============
test_trusted_advisor
=============

Verify that a user is able to use Trusted Advisor

Plan:

* Create a test user
* Create a Support client from user session
* Describe Trusted Advisor Checks, and get the check-id from the first one in the list
* Refresh the Trusted Advisor check using the check-id from above
* Describe the Trusted Advisor check using the check-id from above

${testcount:3}

"""

import pytest


def pytest_namespace():
    return {
        'trusted_advisor_check_id': None
    }


@pytest.fixture(scope='module')
def trusted_advisor(session):
    """A shared handle to AWS Support API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the AWS Support API.

    """
    return session.client('support')


@pytest.mark.premium_account_required
def test_describe_trusted_advisor_checks(trusted_advisor):
    """Describe Trusted Advisor Checks, and get the check-id from the first one in the list.

    :testid: trustedadvisor-f-1-1

    :param A handle to the Support API

    """
    trusted_advisor_checks_resp = trusted_advisor.describe_trusted_advisor_checks(
        language='en'
    )

    assert len(trusted_advisor_checks_resp['checks']) > 0, 'Trusted Advisor checks not found'
    pytest.trusted_advisor_check_id = trusted_advisor_checks_resp['checks'][0]['id']


@pytest.mark.premium_account_required
def test_refresh_trusted_advisor_check(trusted_advisor):
    """Refresh the Trusted Advisor check using the check-id set in test_describe_trusted_advisor_checks

    :testid: trustedadvisor-f-1-1

    :param A handle to the Support API

    """
    refresh_check_resp = trusted_advisor.refresh_trusted_advisor_check(
        checkId=pytest.trusted_advisor_check_id
    )

    assert len(refresh_check_resp['status']) > 0, 'Trusted Advisor status not found on refresh'


@pytest.mark.premium_account_required
def test_describe_trusted_advisor_check_result(trusted_advisor):
    """Describe the Trusted Advisor check using the check-id set in test_describe_trusted_advisor_checks

    :testid: trustedadvisor-f-1-1

    :param A handle to the Support API

    """
    trusted_advisor_check_resp = trusted_advisor.describe_trusted_advisor_check_result(
        checkId=pytest.trusted_advisor_check_id,
        language='en'
    )

    assert len(trusted_advisor_check_resp['result']) > 0, 'No result found for trusted advisor id' + pytest.trusted_advisor_check_id

