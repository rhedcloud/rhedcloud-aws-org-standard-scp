"""
========
test_efs
========

Verify access to AWS Elastic File System.

Plan:

* Attempt to create an EFS file system that is NOT encrypted
* Attempt to create an EFS file system that IS encrypted
* Attempt to mount an EFS volume on an on-premise server

${testcount:5}

"""

import pytest

from aws_test_functions.efs import (
    cleanup_file_systems,
    new_filesystem,
    new_mount_target,
)


pytestmark = [
    pytest.mark.scp_check('describe_file_systems'),
]


@pytest.fixture(scope='module')
def efs(session):
    """A shared handle to Elastic File System APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Elastic File System APIs.

    """

    return session.client('efs')


@pytest.fixture(scope='module', autouse=True)
def cleanup(efs):
    """Attempt to clean up any resources lingering from previous tests.

    :param EFS.Client efs:
        A handle to the Elastic File System APIs.

    """

    cleanup_file_systems(efs=efs)


@pytest.fixture(scope='module', autouse=True)
def shared_vars(efs, stack, cleanup):
    """Return a dictionary that is used to share information across test
    functions.

    :param EFS.Client efs:
        A handle to the Elastic File System APIs.
    :param in_correct_org:
        A dependency on the fixture that ensures the tests run in the
        appropriate Org Unit.
    :param cleanup:
        A dependency on the fixture that cleans up resources from previous EFS
        tests.

    """

    return {
        # for encrypted filesystem data
        True: {
            'filesystem_id': None,
        },

        # for non-encrypted filesystem data
        False: {
            'filesystem_id': None,
        },
    }


@pytest.fixture(scope='module', params=(False, True))
def encrypted(stack, request):
    return request.param


def test_create_filesystem(efs, encrypted, stack, shared_vars):
    """Verify ability to create encrypted and non-encrypted EFS
    filesystems.

    :testid: efs-f-1-1, efs-f-1-2

    :param EFS.Client efs:
        A handle to the Elastic File System APIs.
    :param bool encrypted:
        Whether to test encrypted or non-encrypted filesystems.
    :param contextlib.ExitStack stack:
        A context manager shared across all EFS tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    fs_id = stack.enter_context(new_filesystem(encrypted, efs=efs))
    assert fs_id is not None, 'Unexpected data: {}'.format(fs_id)

    shared_vars[encrypted]['filesystem_id'] = fs_id


def test_create_mount_target(efs, encrypted, internal_subnet_id, stack, shared_vars):
    """Verify ability to create encrypted and non-encrypted EFS
    filesystems.

    :testid: efs-f-1-1, efs-f-1-2

    :param EFS.Client efs:
        A handle to the Elastic File System APIs.
    :param bool encrypted:
        Whether to test encrypted or non-encrypted filesystems.
    :param str internal_subnet_id:
        The ID of a subnet in the type 1 VPC.
    :param contextlib.ExitStack stack:
        A context manager shared across all EFS tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    # get the ID of the filesystem created in the previous test or fall
    # back to a fake filesystem ID
    fs_id = shared_vars[encrypted]['filesystem_id'] or 'fs-1234abcd'

    target_id = stack.enter_context(
        new_mount_target(fs_id, internal_subnet_id, efs=efs),
    )
    assert target_id is not None, 'Unexpected data: {}'.format(target_id)


@pytest.mark.skip(reason='Direct Connect currently unavailable')
def test_mount_on_premise():
    """Verify inability to mount EFS file systems on an on-premise server.

    :testid: efs-f-2-1

    """

    assert False
