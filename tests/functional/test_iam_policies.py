"""
=================
test_iam_policies
=================

Customers should not have any IAM permissions to modify RHEDcloud IAM policies, but
they should be able to create and modify their own.

${testcount:5}

"""

import json

import pytest

from aws_test_functions import (
    create_policy,
    get_policy_arn,
    new_policy,
)

STMT = [{
    'Effect': 'Allow',
    'Action': [
        's3:GetBucketLocation',
        's3:GetObject',
        's3:ListBucket',
    ],
    'Resource': 'arn:aws:s3:::*',
}]


@pytest.fixture(scope='module')
def iam(session):
    return session.client('iam')


@pytest.fixture(scope='module')
def policy(session):
    with new_policy('test_iam_policies', STMT) as p:
        yield p


@pytest.fixture(scope='function')
def scp(iam, wait_for_scp, user):
    """Fixture that waits for the SCP to take effect before allowing a test to
    proceed.

    """

    wait_for_scp(iam.update_user, kwargs=dict(UserName=user.user_name, NewUserName=user.user_name), delay=30)


@pytest.fixture
def org_role_name(test_user_policies):
    """Retrieve the name of an Emory IAM Role which should already exist in
    the AWS account.

    :param iter test_user_policies:
        Names of IAM Policies that get applied to the test user. One will be
        Selected and stripped of the "Policy" suffix.

    :returns:
        A string.

    """

    for name in test_user_policies:
        if "/" in name and name.endswith("Policy"):
            return name[name.index("/") + 1:-6]


def test_create_delete_policy():
    """A customer should be able to create and delete IAM policies.

    :testid: iam-f-4-1, iam-f-4-2

    """

    with new_policy('test_iam_policies', STMT) as policy:
        assert 'test_iam_policies' in policy.policy_name, 'failed to create policy'


def test_modify_policy(policy, org_role_name):
    """A customer should be able to modify IAM policies.

    :testid: iam-f-4-3

    :param str org_role_name:
        Name of an existing IAM Policy.

    """

    assert policy.attach_role(RoleName=org_role_name), 'failed to update policy'


@pytest.mark.raises_access_denied
def test_create_rhedcloud_policy(session):
    """A customer should NOT be able to create new RHEDcloud IAM policies.

    :testid: iam-f-4-1, iam-f-4-2

    """

    create_policy('test_create_rhedcloud_policy', STMT, '/rhedcloud/', iam=session.resource('iam'))
    assert False, 'had permission to create RHEDcloud IAM policy'


@pytest.mark.raises_access_denied
def test_clone_rhedcloud_policy(session, iam, scp):
    """A customer should NOT be able to clone RHEDcloud IAM policies.

    :testid: iam-f-4-4

    """

    create_policy('RHEDcloudVpcFlowLogRolePolicy', STMT, '/rhedcloud/', iam=session.resource('iam'))
    assert False, 'had permission to clone RHEDcloud IAM policy'


@pytest.mark.raises_access_denied
def test_delete_rhedcloud_policy(iam, scp):
    """A customer should NOT be able to delete RHEDcloud IAM policies.

    :testid: iam-f-4-5

    """

    arn = get_policy_arn('RHEDcloudVpcFlowLogRolePolicy')
    iam.delete_policy(PolicyArn=arn)
    assert False, 'had permission to delete RHEDcloud IAM policy'


@pytest.mark.raises_access_denied
def test_modify_rhedcloud_policy(iam, scp):
    """A customer should NOT be able to modify RHEDcloud IAM policies.

    :testid: iam-f-4-6

    """

    arn = get_policy_arn('RHEDcloudVpcFlowLogRolePolicy')
    iam.create_policy_version(
        PolicyArn=arn,
        PolicyDocument=json.dumps({
            'Version': '2012-10-17',
            'Statement': STMT,
        }),
    )

    assert False, 'had permission to modify RHEDcloud IAM policy'
