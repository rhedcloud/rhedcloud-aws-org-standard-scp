import pytest


@pytest.fixture(scope='module')
def ecr(session):
    """A shared handle to ECR API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the ECR API.

    """

    return session.client('ecr')
