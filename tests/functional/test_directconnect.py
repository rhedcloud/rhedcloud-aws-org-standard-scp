"""
===========
test_directconnect
===========

Determine whether the user can use the AWS Direct Connect service.

Plan:

* Describe Direct Connect connection locations
* Create a Direct Connect connection

${testcount:4}

"""

from contextlib import contextmanager

import pytest

#This service is fully blocked
pytestmark = [pytest.mark.scp_check('describe_locations')]


@pytest.fixture(scope='module')
def directconnect(session):
    """A shared handle to DirectConnect APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the DirectConnect APIs.

    """

    return session.client('directconnect')


def test_describe_directconnect_locations(directconnect):
    """Attempt to describe direct connect locations.

    :testid: directconnect-f-1-1

    """

    res = directconnect.describe_locations()
    assert 'locations' in res, 'Unexpected response: {}'.format(res)


def test_create_delete_directconnect_connection(directconnect):
    """Create a direct connect connection and then delete it.

    :testid: directconect-f-1-2, directconnect-f-1-3

    """

    try:
        # testid: directconnect-f-1-2
        created = False
        location = 'TLXA1'
        bandwidth = '1Gbps'
        connectionName = 'RHEDcloudDirectConnectTest'

        res = directconnect.create_connection(location=location, bandwidth=bandwidth, connectionName=connectionName)
        
        if res['connectionId']:
            connectionId = res['connectionId']
            created = True

    finally:
        if created:
            # testid: directconnect-f-1-3
            print('Deleting Direct Connect connection: {}'.format(connectionName))
            directconnect.delete_connection(connectionId=connectionId)
