"""
========
test_ecr
========

Verify the ability to create and use an Elastic Container Registry(ECR)

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Create a new ECR repository with default settings.
* Get an authorization token using get_authorization_token.
* Using the Docker Python APIs, build a test image.
* Tag the test image with the proper endpoint of the created repository.
* Push the Docker image to the repository with the authorization credentials.
* Ensure that the image has successfully uploaded and available.

${testcount:4}

"""

from aws_test_functions import docker_to_ecr


pytest_plugins = ["tests.plugins.ecr"]


def test_ecr(ecr, stack):
    """Verify ability to use the ECR service

    :testid: ecr-f-1-1

    :param str docker_to_ecr:
        A dependency on a fixture that runs the test.

    """

    docker_to_ecr(stack, ecr=ecr)

    assert True, "Error pushing Image to ECR"
