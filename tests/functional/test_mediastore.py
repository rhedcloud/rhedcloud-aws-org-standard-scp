"""
==============
test_mediastore
==============

Verify users can utilize AWS MediaStore functionality.

${testcount:6}

* Attempt to create a MediaStore container
* Attempt to upload an object to the container
* Attempt to list the objects in the container
* Attempt to retrieve the uploaded object from the container
* Attempt to delete the uploaded object from the container
* Attempt to delete the container

"""

from botocore.exceptions import EndpointConnectionError
import pytest

from aws_test_functions import (
    aws_client,
    has_status,
    ignore_errors,
    new_mediastore_container,
    retry,
)

#This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('list_containers')]

# Bypass the "Access Denied" expectation for tests that require an endpoint_url
# NOTE: this might get more interesting if/when the blocks change
bypass_access_denied = pytest.mark.bypass('raises_access_denied')

# treat errors about nonexistent items as access denied errors (because they
# likely originate from an access denied error earlier in the suite).
ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden).*'


@pytest.fixture(scope='module')
def mediastore(session):
    """A shared handle to MediaStore API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the MediaStore API.

    """

    return session.client('mediastore')


@pytest.fixture
def mediastore_data(session, endpoint_url):
    """A shared handle to MediaStore Data API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the MediaStore Data API.

    """

    print('Creating MediaStore Data client using URL: {}'.format(endpoint_url))
    return session.client(
        'mediastore-data',
        endpoint_url=endpoint_url,
    )


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        # The name of the container
        'container_name': None,

        # The endpoint to access the container
        'endpoint_url': None,

        # The path of the uploaded object
        'data_path': None
    }


def test_create_container(mediastore, stack, shared_vars):
    """A customer should be able to create their own MediaStore containers

    :testid: mediastore-f-1-1

    :param MediaStore.Client mediastore:
        A handle to the MediaStore APIs.
    :param contextlib.ExitStack stack:
        A context manager shared across all MediaStore tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    with new_mediastore_container("TestContainer", delete=False) as container_name:
        res = mediastore.describe_container(ContainerName=container_name)
        shared_vars['endpoint_url'] = res['Container']['Endpoint']
        shared_vars['container_name'] = container_name

    stack.callback(
        ignore_errors(mediastore.delete_container),
        ContainerName=shared_vars['container_name'],
    )


@bypass_access_denied
def test_put_object(mediastore_data, endpoint_url, stack, shared_vars):
    """A customer should be able to upload data to a MediaStore container

    :testid: mediastore-f-1-2

    :param MediaStoreData.Client mediastore_data:
        A handle to the MediaStore Data APIs.
    :param str endpoint_url:
        An endpoint URL.
    :param contextlib.ExitStack stack:
        A context manager shared across all MediaStore tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    if not endpoint_url:
        print('Prior AccessDenied error means no endpoint_url is available')
        return

    with open('./tests/resources/big_buck_bunny.mp4', 'rb') as upload_file:
        data = upload_file.read()
        res = mediastore_data.put_object(Body=data, Path='/big_buck_bunny.mp4')
        assert has_status(res, 200), 'Unexpected response: {}'.format(res)
        shared_vars['data_path'] = '/big_buck_bunny.mp4'

    stack.callback(
        mediastore_data.delete_object,
        Path=shared_vars['data_path'],
    )


@bypass_access_denied
def test_get_object(mediastore_data, endpoint_url):
    """A customer should be able to retrieve data from a MediaStore container

    :testid: mediastore-f-1-3

    :param MediaStoreData.Client mediastore_data:
        A handle to the MediaStore Data APIs.
    :param str endpoint_url:
        An endpoint URL.

    """

    if not endpoint_url:
        print('Prior AccessDenied error means no endpoint_url is available')
        return

    with open('./tests/resources/big_buck_bunny.mp4', 'rb') as upload_file:
        res = retry(
            mediastore_data.list_items,
            kwargs=dict(
                Path='/',
            ),
            msg='Checking upload status',
            until=lambda r: len(r['Items']) > 0,
            # any exceptions should be raised immediately
            pred=lambda ex: False,
        )
        assert len(res['Items']), 'Object not uploaded, Unexpected response: {}'.format(res)

        res = mediastore_data.get_object(Path='/big_buck_bunny.mp4')
        assert has_status(res, 200), 'Unexpected response: {}'.format(res)
        local_data = upload_file.read()
        stored_data = res['Body'].read()
        assert local_data == stored_data, 'Downloaded data does not match local data'


@aws_client('mediastore')
def cleanup_containers(*, mediastore=None):
    """Attempt to remove any unused containers."""

    delete = ignore_errors(mediastore.delete_container)

    res = mediastore.list_containers()
    for container in res['Containers']:
        name = container['Name']
        print('Deleting container: {}'.format(name))
        delete(ContainerName=name)
