"""
==============
test_rds_mysql
==============

Plan:

* Create an instance with "Public accessibility" set to "No" and verify that it
  is not accessible outside of the VPC.
* Use Amazon RDS encryption using AWS Key Management Service to enable
  encryption at rest.
* Use combined CA bundle to encrypt data as it moves to and from the database.
* Verify MySQL is accessible through standard port.
* Verify that Standard Database User accounts do not have master account level
  privileges.

.. note::

    The tests for MySQL are identical to those for MariaDB, so we can easily
    reuse them.

.. note::

    These tests makes use of shared utilities in ``test/functional/rds/conftest.py``.

${testcount:4}

"""

import pytest

# the tests for MySQL are identical to the tests for MariaDB, so just import
# them here instead of duplicating them
from .test_rds_mariadb import *

ENGINE = 'mysql'


@pytest.fixture(scope='module')
def engine():
    return ENGINE
