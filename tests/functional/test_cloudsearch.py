"""
=============
test_cloudsearch
=============

Verify users cannot utilize AWS CloudSearch functionality. This is
a fully blocked service, and as such, aws_blocked_service.py functionality
is utilized for testing, as opposed to marking each test with pytest.mark.raises_access_denied

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Assert that a search domain cannot be created or destroyed
* Assert that domain names cannot be listed

${testcount:5}

"""

import pytest

#This service is fully blocked
pytestmark = [pytest.mark.scp_check('list_domain_names')]


@pytest.fixture(scope='module')
def cloudsearch(session):
    """A shared handle to CloudSearch APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the CloudSearch APIs.

    """
    return session.client('cloudsearch')


def test_create_domain(cloudsearch):
    """Verify restrictions to CloudSearch domain creation

    :testid: cs-f-1-1

    :param Cloudsearch.Client cloudsearch:
        A handle to the CloudSearch APIs.

    """

    create_response = {}
    try:
        create_response = cloudsearch.create_domain(DomainName='testcloudsearch')
        assert create_response['ResponseMetadata']['HTTPStatusCode'] == 200, 'Unexpected response: {}'.format(create_response)
    finally:
        if 'DomainStatus' in create_response and 'DomainName' in create_response['DomainStatus']:
            res = cloudsearch.delete_domain(DomainName=create_response['DomainStatus']['DomainName'])
            assert res['ResponseMetadata']['HTTPStatusCode'] == 200, 'Unexpected response: {}'.format(res)


def test_list_domain_names(cloudsearch):
    """Verify restrictions to CloudSearch domain listing

    :testid: cs-f-1-2

    :param Cloudsearch.Client cloudsearch:
        A handle to the CloudSearch APIs.

    """

    res = cloudsearch.list_domain_names()
    assert res['ResponseMetadata']['HTTPStatusCode'] == 200, 'Unexpected response: {}'.format(res)
