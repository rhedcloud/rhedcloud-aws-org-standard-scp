# RHEDcloud AWS Org Standard SCP

## Copyright

This software was created at Emory University. Please see NOTICE.txt for Emory's copyright and disclaimer notifications. This software was extended and modified by the RHEDcloud Foundation and is copyright 2019 by the RHEDcloud Foundation.

## License

This software is licensed under the Apache 2.0 license included in this distribution as LICENSE-2.0.txt

## Setup for local testing

* Create two profiles in your `~/.aws/credentials` file for the Serviceforge
  100 and Serviceforge 110 accounts:

```ini
[rhedcloud-serviceforge-100]
aws_access_key_id = YOUR ACCESS KEY ID
aws_secret_access_key = YOUR SECRET ACCESS KEY

[rhedcloud-serviceforge-110]
aws_access_key_id = YOUR ACCESS KEY ID
aws_secret_access_key = YOUR SECRET ACCESS KEY
```

* Make sure you don't have any values set in your environment for
  `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`:

```bash
unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
```

* Set your `AWS_PROFILE` environment variable to `rhedcloud-serviceforge-100` and your
  `AWS_DEFAULT_REGION` environment variable to `us-east-1`:

```bash
export AWS_PROFILE=rhedcloud-serviceforge-100
export AWS_DEFAULT_REGION=us-east-1
```

* Configure ``pytest`` to automatically move the Serviceforge 110 account
  between the `RHEDcloudCentralAdministration` and `RHEDcloudResearchAccounts`
  Organizational Units:

```bash
export RHEDCLOUD_AUTOMOVE_ACCOUNT=y
```

* Configure Redis so two instances of ``pytest`` running at the same time will
  not conflict with each other:

```bash
export REDIS_HOST=[get this from another tester]
export REDIS_PORT=[get this from another tester]
export REDIS_PASSWORD=[get this from another tester]
```

* Make sure you have all dependecies installed:

```bash
pip install -Ur requirements.txt
```

* Run tests:

```bash
make test
```

### Testing with a dedicated AWS account

If you have been provided access to an AWS account specifically for your tests,
there are a few additional steps that you will want to take. First, you will
need to setup a new profile in ``~/.aws/credentials`` using the information
provided to you to access the dedicated account (where ``XYZ`` is the account
number, such as ``100`` or ``110`` from previous steps):

```ini
[rhedcloud-serviceforge-XYZ]
aws_access_key_id = YOUR ACCESS KEY ID
aws_secret_access_key = YOUR SECRET ACCESS KEY
```

Next, you'll want to override the default vaules for two environment variables:

```bash
export RHEDCLOUD_ACCOUNT_NAME="Serviceforge XYZ"
export RHEDCLOUD_TEST_PROFILE=rhedcloud-serviceforge-XYZ
```

Once the appropriate CloudFormation stacks have been setup within the dedicated
AWS account, this should be enough for your local tests happily to run there.

### Creating stacks in dedicated AWS accounts

Once you have your AWS profile and environment variables setup for the new AWS
account, you must create the CloudFormation stacks in the account before
meaningful tests can be performed. Be sure to exchange any ``XYZ``'s below with
the appropriate AWS account number from above.

Make sure you have cloned the following repositories to the same parent
directory:

* ``rhedcloud-aws-pipeline-scripts``
* ``rhedcloud-aws-rs-account-cfn``
* ``rhedcloud-aws-vpc-type1-cfn``
* ``rhedcloud-aws-python-testutils``

Before the CloudFormation stacks can be created, the new AWS account must be
moved into the ``RHEDcloudCentralAdministration`` organizational unit. This is to
be sure the Standard Service Control Policy is not in effect. If the Standard
SCP is in effect, you will not have permission to create the necessary
resources.

```bash
AWS_PROFILE=rhedcloud-serviceforge-100 rhedcloud-aws-pipeline-scripts/org_move_account.py "Serviceforge XYZ" RHEDcloudCentralAdministration
```

Set some environment variables:

```bash
# explicitly use your new AWS profile so the stacks get created in the new AWS
# account instead of the 100 account
export AWS_PROFILE=rhedcloud-serviceforge-XYZ

export RHEDCLOUD_SETUP_ORG=EmoryCentralAdministration
export RHEDCLOUD_TEST_ORG=EmoryResearchAccounts
export CLOUDFORMATION_STACK_NAME=rhedcloud-aws-rs-account
export CLOUDTRAIL_BUCKET_NAME=serviceforge-aws-admin-XYZ-ct1
export CLOUDTRAIL_NAME=rhedcloud-aws-rs-account-Master
```

Create the rs-account stack:

```bash
cd rhedcloud-aws-rs-account-cfn
../rhedcloud-aws-pipeline-scripts/cfn_stack_create.py --parameter CloudTrailName=$CLOUDTRAIL_BUCKET_NAME --compact rhedcloud-aws-rs-account rhedcloud-aws-rs-account-cfn.json
cd ..
```

If the stack fails to create, just keep trying until there are no errors.

Finally, Create the type 1 VPC stack:

```bash
cd rhedcloud-aws-vpc-type1-cfn
../rhedcloud-aws-pipeline-scripts/cfn_stack_create.py --compact rhedcloud-aws-vpc-type1 rhedcloud-aws-vpc-type1-cfn.json
cd ..
```

If the stack fails to create, just keep trying until there are no errors.

At this point, it should finally be safe to run the tests in this repository.

## Managing environment variables

I highly recommend using a tool to manage all of the environment variables
required for any given project. My weapon of choice is [direnv](https://direnv.net).

Using `direnv`, you can simply place all of the `export` and `unset` lines in a
file called `.envrc` in the root of this repository then run `direnv allow` to
use it. Anytime you `cd` into this repository, those environment variables will
change based on what's in your `.envrc`. When you `cd` out of this repository,
the original environment variables are restored. You also need to run `direnv
allow` anytime you change the `.envrc` file.

If you choose to use `direnv` or a similar tool **DO NOT COMMIT** your `.envrc`
to Git. It should never leave your own computer.

Here's an example `.envrc`:

```bash
unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY

export AWS_DEFAULT_REGION=us-east-1
export AWS_PROFILE=rhedcloud-serviceforge-100
export RHEDCLOUD_AUTOMOVE_ACCOUNT=y

export REDIS_HOST=[get this from another tester]
export REDIS_PORT=[get this from another tester]
export REDIS_PASSWORD=[get this from another tester]

export PATH=../rhedcloud-aws-pipeline-scripts:$PATH
export PYTHONPATH=./tests:../rhedcloud-aws-python-testutils:$PYTHONPATH
```
