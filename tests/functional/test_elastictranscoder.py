"""
======================
test_elastictranscoder
======================

Verify access to use the Elastic Transcoder service.

Plan:

* Create an Elastic Transcoder Pipeline
* Create an Elastic Transcoder Job
* Check the status of the pipeline
* Check the status of the job
* Update the pipeline status
* List jobs in a pipeline
* Delete a pipeline

${testcount:7}

"""

from os.path import basename

import pytest

from aws_test_functions import (
    has_status,
    ignore_errors,
    make_identifier,
    new_bucket,
    new_policy,
    new_role,
    retry,
    unexpected,
)
from aws_test_functions.elastictranscoder import cleanup, delete_pipeline

# This service is blocked in the HIPAA org
pytestmark = [pytest.mark.scp_check('list_pipelines', client_fixture='elt')]

LOCAL_RESOURCE = './tests/resources/transcoder-sample.mp4'
INPUT_FILENAME = basename(LOCAL_RESOURCE)
OUTPUT_FILENAME = INPUT_FILENAME


@pytest.fixture(scope='module')
def elt(session):
    """A shared handle to Elastic Transcoder API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Elastic Transcoder API.

    """

    c = session.client('elastictranscoder')
    cleanup(elastictranscoder=c)

    yield c


@pytest.fixture(scope='module')
def role(stack):
    """Create a role for Elastic Trancoder to use when performing jobs.

    :param contextlib.ExitStack stack:
        A context manager shared across all Elastic Transcoder tests.

    :yields:
        An IAM Role resource.

    """

    stmt = [dict(
        Effect='Allow',
        Action=[
            "elastictranscoder:*",
            "cloudfront:*",
            "s3:List*",
            "s3:Put*",
            "s3:Get*",
            "s3:*MultipartUpload*",
            "iam:CreateRole",
            "iam:PutRolePolicy",
            "iam:GetRolePolicy",
            "iam:List*",
            "sns:CreateTopic",
            "sns:List*",
        ],
        Resource=['*'],
    )]

    p = stack.enter_context(new_policy('test_elastictranscoder', stmt))
    r = stack.enter_context(new_role('test_elastictranscoder', 'elastictranscoder', p.arn))

    yield r


@pytest.fixture(scope='module')
def buckets(stack):
    """Create an input and an output S3 bucket for Elastic Trancoder to use
    when performing jobs.

    :param contextlib.ExitStack stack:
        A context manager shared across all Elastic Transcoder tests.

    :yields:
        A 2-tuple with S3 Bucket resources for each bucket.

    """

    i = stack.enter_context(new_bucket('inputs', wait=False))
    o = stack.enter_context(new_bucket('outputs', wait=False))
    i.upload_file(
        LOCAL_RESOURCE,
        INPUT_FILENAME,
        ExtraArgs={'ServerSideEncryption': 'AES256'},
    )

    yield (i, o)


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'pipeline_id': '',
        'job_id': '',
    }


@pytest.mark.xfail(reason='Pretty inconsistent with AccessDenied errors')
def test_create_pipline(elt, buckets, role, stack, shared_vars):
    """Verify access to create an Elastic Transcoder pipeline.

    :testid: elastictranscoder-f-1-1

    :param ElasticTranscoder.Client elt:
        A handle to the Elastic Transcoder API.
    :param tuple(S3.Bucket) buckets:
        A 2-tuple with S3 Bucket resources for input and output buckets.
    :param IAM.Role role:
        IAM Role for Elastic Transcoder to use.
    :param contextlib.ExitStack stack:
        A context manager shared across all Elastic Transcoder tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    in_, out = buckets

    res = elt.create_pipeline(
        Name=make_identifier('transcoder')[:20],
        InputBucket=in_.name,
        OutputBucket=out.name,
        Role=role.arn,
    )
    assert has_status(res, 201), unexpected(res)
    assert 'Pipeline' in res, unexpected(res)

    pipeline_id = shared_vars['pipeline_id'] = res['Pipeline']['Id']
    print('Created pipeline: {}'.format(pipeline_id))

    stack.callback(
        ignore_errors(delete_pipeline),
        pipeline_id,
        elastictranscoder=elt,
    )


def test_create_job(elt, pipeline_id, stack, shared_vars):
    """Verify access to create an Elastic Transcoder job.

    :testid: elastictranscoder-f-1-2

    :param ElasticTranscoder.Client elt:
        A handle to the Elastic Transcoder API.
    :param str pipeline_id:
        ID of an Elastic Transcoder pipeline.
    :param contextlib.ExitStack stack:
        A context manager shared across all Elastic Transcoder tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = elt.create_job(
        PipelineId=pipeline_id,
        Inputs=[dict(
            Key=INPUT_FILENAME,
        )],
        Outputs=[dict(
            Key=OUTPUT_FILENAME,
            PresetId='1351620000001-100070',
        )],
    )
    assert has_status(res, 201), unexpected(res)
    assert 'Job' in res, unexpected(res)

    job_id = shared_vars['job_id'] = res['Job']['Id']
    print('Created job: {}'.format(job_id))

    stack.callback(
        ignore_errors(elt.cancel_job),
        Id=job_id,
    )


def test_read_pipeline(elt, pipeline_id):
    """Verify access to check the status of an Elastic Transcoder pipeline.

    :testid: elastictranscoder-f-1-3

    :param ElasticTranscoder.Client elt:
        A handle to the Elastic Transcoder API.
    :param str pipeline_id:
        ID of an Elastic Transcoder pipeline.

    """

    res = elt.read_pipeline(Id=pipeline_id)
    assert has_status(res, 200), unexpected(res)
    assert res['Pipeline']['Status'] == 'Active'


def test_read_job(elt, job_id):
    """Verify access to check the status of an Elastic Transcoder job.

    :testid: elastictranscoder-f-1-3

    :param ElasticTranscoder.Client elt:
        A handle to the Elastic Transcoder API.
    :param str job:
        ID of an Elastic Transcoder job.

    """

    res = elt.read_job(Id=job_id)
    assert has_status(res, 200), unexpected(res)
    assert 'Job' in res, unexpected(res)

    bad_values = ('canceled', 'error')
    assert res['Job'].get('Status', 'error').lower() not in bad_values


def test_update_pipeline_status(elt, pipeline_id):
    """Verify access to update the status of an Elastic Transcoder pipeline.

    :param ElasticTranscoder.Client elt:
        A handle to the Elastic Transcoder API.
    :param str pipeline_id:
        ID of an Elastic Transcoder pipeline.

    """

    res = elt.update_pipeline_status(
        Id=pipeline_id,
        Status='Paused',
    )
    assert has_status(res, 200), unexpected(res)


def test_list_jobs_by_pipeline(elt, pipeline_id):
    """Verify access to list jobs in an Elastic Transcoder pipeline.

    :param ElasticTranscoder.Client elt:
        A handle to the Elastic Transcoder API.
    :param str pipeline_id:
        ID of an Elastic Transcoder pipeline.

    """

    res = elt.list_jobs_by_pipeline(
        PipelineId=pipeline_id,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'Jobs' in res, unexpected(res)


def test_delete_pipeline(elt, pipeline_id):
    """Verify access to delete an Elastic Transcoder pipeline.

    :param ElasticTranscoder.Client elt:
        A handle to the Elastic Transcoder API.
    :param str pipeline_id:
        ID of an Elastic Transcoder pipeline.

    """

    retry(
        delete_pipeline,
        args=[pipeline_id],
        kwargs={'elastictranscoder': elt},
    )
