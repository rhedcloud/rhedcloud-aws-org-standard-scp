"""
========
test_dax
========

Verify access to use AWS DynamoDB Accelerator(DAX)

Plan:

* Attempt to create a DAX subnet group
* Attempt to create a DAX cluster
* Attempt to describe the available DAX clusters
* Ensure the created cluster reaches an 'available' state
* Attempt to delete the DAX cluster
* Attempt to delete the DAX subnet group

${testcount:12}

"""

import pytest

from aws_test_functions import (
    has_status,
    ignore_errors,
    in_setup_org,
    make_identifier,
    new_policy,
    new_role,
    new_security_group,
    retry,
    unexpected,
)

pytestmark = [
    # This test takes ~15 minutes
    pytest.mark.slowtest,

    # This service is fully blocked
    pytest.mark.scp_check('describe_clusters')
]

# Create a reusable functor to move the AWS account to the SETUP_ORG before
# attempting to invoke the requested function. This is necessary to ensure that
# we make the best effort to remove resources created during testing (since we
# should not be able to delete with the SCP in effect in the TEST_ORG).
cleaner = in_setup_org('DAX cleanup', one_way=True)


@pytest.fixture(scope='module')
def dax(session):
    """A shared handle to DAX API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the DAX API.

    """

    return session.client('dax')


@pytest.fixture(scope='module')
def ec2(session):
    """A shared handle to EC2 APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the EC2 APIs.

    """

    return session.client('ec2')


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'subnet_group_name': 'not-created',
        'cluster_name': 'not-created',
    }


@pytest.fixture(scope='module')
def role(stack):
    """Create a role for DAX to create clusters

    :param contextlib.ExitStack stack:
        A context manager shared across all DAX tests.

    :yields:
        An IAM Role resource.

    """

    stmt = [dict(
        Effect='Allow',
        Action=[
            "dynamodb:*",
        ],
        Resource=['*'],
    )]

    p = stack.enter_context(new_policy('test_dax', stmt))
    r = stack.enter_context(new_role('test_dax', 'dax', p.arn))

    yield r


@pytest.fixture(scope='module')
def security_group(ec2, vpc_id, stack):
    """Creates a Security Group in the given vpc allowing ingress on port 80 and 22

    :param str vpc_id:
        The AWS VPC ID to create the security group in
    :param contextlib.ExitStack stack:
        A context manager shared across all DAX tests

    :returns:
        A SecurityGroup.

    """

    sg = stack.enter_context(new_security_group(
        vpc_id,
        'test_dax_sg'
    ))

    ec2.authorize_security_group_ingress(
        GroupId=sg.group_id,
        IpProtocol='tcp',
        FromPort=8111,
        ToPort=8111,
        CidrIp='0.0.0.0/0',
    )

    yield sg


def test_create_subnet_group(dax, internal_subnet_id, shared_vars, stack):
    """Verify ability to create a subnet group for DAX

    :testid: dax-f-1-1

    :param DAX.Client dax:
        A handle to the DAX API.
    :param str internal_subnet_id:
        The ID of the subnet to use in the subnet group.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param contextlib.ExitStack stack:
        A context manager shared across all DAX tests

    """

    name = make_identifier('DaxTestSG')
    res = dax.create_subnet_group(SubnetGroupName=name,
                                  Description='Test SCP Subnet Group',
                                  SubnetIds=[internal_subnet_id])
    assert has_status(res, 200), unexpected(res)

    shared_vars['subnet_group_name'] = name
    stack.callback(
        ignore_errors(cleaner(dax.delete_subnet_group)),
        SubnetGroupName=name
    )


def test_create_cluster(dax, security_group, role, shared_vars, stack):
    """Verify ability to create a DAX cluster

    :testid: dax-f-1-2

    :param DAX.Client dax:
        A handle to the DAX API.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param contextlib.ExitStack stack:
        A context manager shared across all DAX tests

    """

    name = make_identifier('DaxCluster', max_length=20).replace('-', '0')
    res = dax.create_cluster(ClusterName=name,
                             NodeType='dax.t2.small',
                             ReplicationFactor=1,
                             SubnetGroupName=shared_vars['subnet_group_name'],
                             SecurityGroupIds=[security_group.id],
                             IamRoleArn=role.arn)
    assert has_status(res, 200), unexpected(res)

    shared_vars['cluster_name'] = name
    stack.callback(
        ignore_errors(cleaner(dax.delete_cluster)),
        ClusterName=name
    )


def test_describe_clusters(dax, cluster_name):
    """Verify ability to describe DAX clusters

    :testid: dax-f-1-3

    :param DAX.Client dax:
        A handle to the DAX API.
    :param str cluster_name:
        The name of a created or creating DAX cluster

    """

    arguments = None
    if cluster_name != 'not-created':
        arguments = dict(ClusterNames=[cluster_name])

    res = retry(
        dax.describe_clusters,
        kwargs=arguments,
        msg='Waiting for DAX cluster {} to finalize creation'.format(cluster_name),
        until=lambda r: r['Clusters'][0]['Status'] != 'creating',
        pred=lambda ex: False,
        delay=60,
        max_attempts=20
    )
    assert has_status(res, 200), unexpected(res)
    assert res['Clusters'][0]['Status'] == 'available', 'Failed to create cluster: {}'.format(res)


def test_delete_cluster(dax, cluster_name, shared_vars):
    """Verify ability to delete a DAX cluster

    :testid: dax-f-1-4

    :param DAX.Client dax:
        A handle to the DAX API.
    :param str cluster_name:
        The name of a created and available DAX cluster
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    def _test(res):
        return has_status(res, 200) and (
            'Clusters' not in res or
            not len(res['Clusters']) or
            'Status' not in res['Clusters'][0] or
            res['Clusters'][0]['Status'] != 'deleting'
        )

    # Delete the cluster and ensure that describe_cluster eventually
    # throws a ClusterNotFoundFault or no clusters are listed or
    # no status is listed for the cluster.
    res = dax.delete_cluster(ClusterName=cluster_name)
    assert has_status(res, 200), unexpected(res)

    deleted = False
    try:
        res = retry(
            dax.describe_clusters,
            kwargs=dict(ClusterNames=[cluster_name]),
            msg='Waiting for DAX cluster {} to finalize deletion'.format(cluster_name),
            until=lambda r: _test(r),
            pred=lambda ex: False,
            delay=60,
            max_attempts=20,
        )

        if _test(res):
            deleted = True
    except dax.exceptions.ClusterNotFoundFault:
        deleted = True

    assert deleted, 'Failed to delete Dax Cluster {}'.format(cluster_name)
    shared_vars['cluster_name'] = 'not-created'


def test_delete_subnet_group(dax, subnet_group_name, shared_vars):
    """Verify ability to delete a DAX subnet group

    :testid: dax-f-1-5

    :param DAX.Client dax:
        A handle to the DAX API.
    :param str subnet_group_name:
        The name of a created DAX subnet group
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = dax.delete_subnet_group(SubnetGroupName=subnet_group_name)
    assert has_status(res, 200), unexpected(res)

    shared_vars['subnet_group_name'] = 'not-created'
