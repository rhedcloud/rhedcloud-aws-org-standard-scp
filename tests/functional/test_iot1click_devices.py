"""
===============
test_iot1click_devices
===============

Verify access to use AWS IOT 1-Click Devices

#FIXME: This test is an incredibly minimal smoke test to ensure
#FIXME: the client is producing acceptable results when blocked 

Plan:

* Attempt to list available devices

${testcount:2}

"""

import pytest

from aws_test_functions import (
    has_status,
    unexpected
)


#This service is fully blocked
pytestmark = [pytest.mark.scp_check('list_devices', client_fixture='iot_devices')]


@pytest.fixture(scope='module')
def iot_devices(session):
    """A shared handle to Iot 1 Click Projects API.

    .. note::

    IOT Devices is unavailable in the default us-east-1 region!

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Iot 1 Click Devices Service API.

    """

    return session.client('iot1click-devices', region_name='us-west-2')


def test_list_devices(iot_devices):
    """Verify ability list devices

    :testid: 

    :param Iot1ClickDevicesService.Client iot_devices:
        A handle to the Iot 1 Click Devices Service API.

    """


    res = iot_devices.list_devices()
    assert has_status(res, 200), unexpected(res)
