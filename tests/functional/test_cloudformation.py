"""
===========
test_cloudformation
===========
A user should be able to create, update, and delete stacks

Plan:

* Create a stack
* Update a stack
* Delete a stack

${testcount:3}

"""

from contextlib import ExitStack

from botocore.exceptions import ClientError
import pytest


TEST_STACK_NAME = 'testcloudformation2'

pytestmark = pytest.mark.parametrize("stack_name", (
    pytest.param("rhedcloud{}".format(TEST_STACK_NAME), marks=pytest.mark.raises_access_denied),
    TEST_STACK_NAME,
))


@pytest.fixture(scope='module')
def cloudformation(session):
    return session.client('cloudformation')


@pytest.fixture(scope='module')
def template():
    return '''
Resources:
  HelloBucket:
    Type: AWS::S3::Bucket
    '''


@pytest.fixture(scope='module')
def template_modified(template):
    return template + '''
  HelloBucket2:
    Type: AWS::S3::Bucket
    '''


def test_create_stack(cloudformation, template, stack_name):
    """Create stack.

    :testid: cfn-f-1-1

    """

    # Clean up just in case
    response = cloudformation.delete_stack(StackName=stack_name)
    waiter = cloudformation.get_waiter('stack_delete_complete')
    waiter.wait(StackName=stack_name)

    # Using the CloudFormation client, create a stack from a simple
    # CloudFormation template.
    response = cloudformation.create_stack(StackName=stack_name,
                                           TemplateBody=template)
    assert 'StackId' in response
    waiter = cloudformation.get_waiter('stack_create_complete')
    waiter.wait(StackName=stack_name)

    # Using the CloudFormation client, describe the stack and look for a
    # status of CREATE_COMPLETE.
    response = cloudformation.describe_stacks(StackName=stack_name)

    assert response['Stacks'][0]['StackStatus'] == 'CREATE_COMPLETE', \
        'status is not CREATE_COMPLETE'


def test_update_stack(cloudformation, template_modified, stack_name):
    """Update stack.

    :testid: cfn-f-1-2

    """

    # Using a CloudFormation client and a modified CloudFormation template,
    # issue a stack update.
    response = cloudformation.update_stack(StackName=stack_name,
                                           TemplateBody=template_modified)
    waiter = cloudformation.get_waiter('stack_update_complete')
    waiter.wait(StackName=stack_name)

    # Using CloudFormation client, describe the stack and look for a status of
    # UPDATE_COMPLETE.
    response = cloudformation.describe_stacks(StackName=stack_name)
    assert response['Stacks'][0]['StackStatus'] == 'UPDATE_COMPLETE', \
        'status is not UPDATE_COMPLETE'


def test_delete_stack(cloudformation, stack_name):
    """Delete stack.

    :testid: cfn-f-1-3

    """

    with ExitStack() as stack:
        if stack_name.startswith("rhedcloud") and False:
            stack.enter_context(pytest.raises(ClientError, match=r"(AccessDenied|ValidationError)"))

        # Using the CloudFormation client, delete the stack.
        print("Deleting stack: {}".format(stack_name))
        res = cloudformation.delete_stack(StackName=stack_name)
        print("Delete stack result: {}".format(res))

    waiter = cloudformation.get_waiter('stack_delete_complete')
    waiter.wait(StackName=stack_name)

    with pytest.raises(ClientError, match="ValidationError"):
        # Verify the stack has been removed by describing the stack.
        print("Checking stack: {}".format(stack_name))
        cloudformation.describe_stacks(StackName=stack_name)
