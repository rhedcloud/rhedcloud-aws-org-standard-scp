"""
=========
test_xray
=========

Verify customers can access the AWS XRay APIs

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Attempt to call PutTelemetryRecords
* Attempt to call PutTraceSegments
* Attempt to call BatchGetTraces
* Attempt to call GetServiceGraph
* Attempt to call GetTraceGraph
* Attempt to call GetTraceSummaries

${testcount:12}

"""

from binascii import b2a_hex
from datetime import datetime
from os import urandom
import json
import time

import pytest

from aws_test_functions import has_status, retry, unexpected

pytestmark = pytest.mark.scp_check("get_encryption_config")


@pytest.fixture(scope="module")
def xray(session):
    """A shared handle to XRay APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the XRay APIs.

    """

    return session.client("xray")


@pytest.fixture(scope="module")
def time_range():
    end_time = time.time()
    start_time = end_time - 10
    hex_start = hex(int(start_time))[2:]

    yield {
        "end_time": end_time,
        "start_time": start_time,
        "hex_start": hex_start,
    }


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    return {
        "trace_id": "1-{:x}-0123456789abcdef01234567".format(
            int(datetime.now().timestamp()),
        ),
    }


def test_put_telemetry(xray):
    """Users should be able to upload Telemetry Records.

    :testid: xray-f-1-1

    :param XRay.Client xray:
        A handle to the xray APIs.

    """

    res = xray.put_telemetry_records(TelemetryRecords=[{"Timestamp": datetime.now()}])
    assert has_status(res, 200), unexpected(res)


def test_put_trace_segments(xray, time_range, shared_vars):
    """Users should be able to upload Trace Segments.

    :testid: xray-f-1-2

    :param XRay.Client xray:
        A handle to the xray APIs.

    """

    segment_id = b2a_hex(urandom(8)).decode("utf-8")
    trace_id = "-".join([
        "1",
        str(time_range["hex_start"]),
        b2a_hex(urandom(12)).decode("utf-8"),
    ])
    trace_configured = json.dumps({
        "name": "example.com",
        "id": segment_id,
        "start_time": time_range["start_time"],
        "trace_id": trace_id,
        "end_time": time_range["end_time"],
    })

    res = xray.put_trace_segments(TraceSegmentDocuments=[trace_configured])
    assert has_status(res, 200), unexpected(res)

    shared_vars["trace_id"] = trace_id


def test_batch_get_traces(xray, trace_id):
    """Users should be able to retrieve Traces.

    :testid: xray-f-2-1

    :param XRay.Client xray:
        A handle to the xray APIs.

    """

    res = xray.batch_get_traces(TraceIds=[trace_id])
    assert has_status(res, 200), unexpected(res)


def test_get_service_graph(xray, time_range):
    """Users should be able to retrieve Service Graphs.

    :testid: xray-f-2-2

    :param XRay.Client xray:
        A handle to the xray APIs.

    """

    start_date = datetime.fromtimestamp(time_range["start_time"])
    end_date = datetime.fromtimestamp(time_range["end_time"])

    res = xray.get_service_graph(StartTime=start_date, EndTime=end_date)
    assert has_status(res, 200), unexpected(res)


def test_get_trace_graph(xray, trace_id):
    """Users should be able to retrieve Trace Graphs.

    :testid: xray-f-2-3

    :param XRay.Client xray:
        A handle to the xray APIs.

    """

    res = retry(
        xray.get_trace_graph,
        kwargs=dict(
            TraceIds=[trace_id],
        ),
        pred=lambda exc: "not found" in str(exc),
        msg="Waiting for trace ID",
        max_attempts=5,
        delay=5,
    )
    assert has_status(res, 200), unexpected(res)


def test_get_trace_summaries(xray, time_range):
    """Users should be able to retrieve Trace Summaries.

    :testid: xray-f-2-4

    :param XRay.Client xray:
        A handle to the xray APIs.

    """

    start_date = datetime.fromtimestamp(time_range["start_time"])
    end_date = datetime.fromtimestamp(time_range["end_time"])

    res = xray.get_trace_summaries(StartTime=start_date, EndTime=end_date)
    assert has_status(res, 200), unexpected(res)
