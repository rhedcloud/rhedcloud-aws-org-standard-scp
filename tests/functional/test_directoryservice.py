"""
=====================
test_directoryservice
=====================

Verify that customers cannot use Microsoft AD.

Plan:

* Configure and verify setup of Microsoft AD
* Test domain join abilities
* Verify security group creation
* Verify access on specific ports

${testcount:4}

"""

import pytest

from aws_test_functions import env, get_subnet_id
from aws_test_functions.directoryservice import new_ad

# This service is fully blocked
pytestmark = [pytest.mark.scp_check('describe_directories', client_fixture='ds')]


@pytest.fixture(scope='module')
def ds(session):
    """A shared handle to Directory Service APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Directory Service APIs.

    """

    return session.client('ds')


@pytest.fixture(scope='module')
def subnets(vpc_id):
    """Return a list of subnet IDs where the directory will be created.

    :param str vpc_id:
        ID of a VPC.

    """

    return [
        get_subnet_id(vpc_id, subnet)
        for subnet in env.RHEDCLOUD_DEFAULT_VPC_ENDPOINT_SUBNETS
    ]


def test_create_ad(session, ds, vpc_id, subnets):
    """Customers should not be able to create Microsoft AD.

    :testid: ds-f-1-1

    """

    with new_ad('test.directoryservice.com', vpc_id, subnets, ds=ds) as ad:
        res = ds.describe_directories(DirectoryIds=[ad])
        assert 'DirectoryDescriptions' in res, 'Unexpected response: {}'.format(res)
        assert len(res['DirectoryDescriptions']), 'AD not found: {}'.format(res)


@pytest.mark.skip(reason='Service is blocked')
def test_join_domain(ds):
    """Customers should not be able to join EC2 instances to Microsoft AD.

    :testid: ds-f-1-2

    """

    assert False


@pytest.mark.skip(reason='Service is blocked')
def test_verify_security_group(ds):
    """Verify that the Domain Controller Security Group was created.

    :testid: ds-f-1-3

    """

    assert False


@pytest.mark.skip(reason='Service is blocked')
def test_port_access(ds):
    """Access domain controllers over specific ports.

    :testid: ds-f-1-4

    """

    assert False
