"""
=====================
test_alexaforbusiness
=====================

Verify access to use Alexa For Business

Plan:

* Attempt to create, search for, describe, update, and delete a profile
* Attempt to create, search for, describe, update, and delete a room
* Attempt to create, search for, describe, update, and delete a skill group
* Associate/disassociate a skill group with/from a room
* Attempt to create, search for, and delete a user

${testcount:20}

"""

import pytest

from aws_test_functions import (
    has_status,
    ignore_errors,
    make_identifier,
    retry,
    unexpected,
)
from aws_test_functions.alexaforbusiness import cleanup_users


ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden|Validation).*'

pytestmark = [
    pytest.mark.scp_check('list_skills', client_fixture='alexa'),
]


@pytest.fixture(scope='module')
def alexa(session):
    """A shared handle to Alexa for Business API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Alexa for Business API.

    """

    return session.client('alexaforbusiness')


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'profile_name': make_identifier('test-profile'),
        'profile_arn': '',
        'room_name': make_identifier('test-a4b-room'),
        'room_arn': '',
        'skill_group_name': make_identifier('test-a4b-skill-group'),
        'skill_group_arn': '',
        'user_id': make_identifier('test-alexa-user'),
        'user_arn': '',
    }


def test_create_profile(alexa, profile_name, stack, shared_vars):
    """Verify access to create profiles.

    :testid: alexaforbusiness-f-1-1, alexaforbusiness-f-2-1

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str profile_name:
        Name of an Alexa for Business Profile.
    :param contextlib.ExitStack stack:
        A context manager shared across all Alexa for Business tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = alexa.create_profile(
        ProfileName=profile_name,
        Timezone='US/Eastern',
        Address='201 Dowman Drive, Atlanta, GA 30322',
        DistanceUnit='IMPERIAL',
        TemperatureUnit='FAHRENHEIT',
        WakeWord='ALEXA',
    )
    assert has_status(res, 200), unexpected(res)
    assert 'ProfileArn' in res, unexpected(res)
    profile_arn = shared_vars['profile_arn'] = res['ProfileArn']
    print('Created Alexa for Business profile: {}'.format(profile_arn))

    stack.callback(
        ignore_errors(alexa.delete_profile),
        ProfileArn=profile_arn,
    )


def test_search_profiles(alexa, profile_arn):
    """Verify access to search profiles.

    :testid: alexaforbusiness-f-2-2

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str profile_arn:
        ARN of an Alexa for Business Profile.

    """

    def _test(res):
        return has_status(res, 200) and 'Profiles' in res and len(res['Profiles'])

    res = retry(
        alexa.search_profiles,
        until=_test,
        msg='Searching for profiles',
    )
    assert _test(res), unexpected(res)

    found = False
    for info in res['Profiles']:
        print('Found profile: {}'.format(info))
        if info['ProfileArn'] == profile_arn:
            found = True
            break

    assert found, 'Did not find profile: {}'.format(profile_arn)


def test_get_profile(alexa, profile_arn, profile_name):
    """Verify access to describe profiles.

    :testid: alexaforbusiness-f-2-3

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str profile_arn:
        ARN of an Alexa for Business Profile.
    :param str profile_name:
        Name of an Alexa for Business Profile.

    """

    res = alexa.get_profile(ProfileArn=profile_arn)
    assert has_status(res, 200), unexpected(res)
    assert 'Profile' in res, unexpected(res)

    assert res['Profile']['ProfileName'] == profile_name


def test_update_profile(alexa, profile_arn):
    """Verify access to update profiles.

    :testid: alexaforbusiness-f-2-4

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str profile_arn:
        ARN of an Alexa for Business Profile.

    """

    res = alexa.update_profile(
        ProfileArn=profile_arn,
        WakeWord='COMPUTER',
    )
    assert has_status(res, 200), unexpected(res)


def test_create_room(alexa, room_name, profile_arn, stack, shared_vars):
    """Verify access to create rooms.

    :testid: alexaforbusiness-f-1-2, alexaforbusiness-f-3-1

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str room_name:
        Name of the room to create.
    :param str profile_arn:
        ARN of an AlexaForBusiness profile.
    :param contextlib.ExitStack stack:
        A context manager shared across all Alexa for Business tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = alexa.create_room(
        RoomName=room_name,
        ProfileArn=profile_arn,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'RoomArn' in res, unexpected(res)

    room_arn = shared_vars['room_arn'] = res['RoomArn']
    print('Created room: {}'.format(room_arn))

    stack.callback(
        ignore_errors(alexa.delete_room),
        RoomArn=room_arn,
    )


def test_search_rooms(alexa, room_arn):
    """Verify access to search rooms.

    :testid: alexaforbusiness-f-3-2

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str room_arn:
        ARN of an AlexaForBusiness room.

    """

    def _test(res):
        return has_status(res, 200) and 'Rooms' in res and len(res['Rooms'])

    res = retry(
        alexa.search_rooms,
        until=_test,
        msg='Searching for rooms...',
    )
    assert _test(res), unexpected(res)

    found = False
    for info in res['Rooms']:
        if info['RoomArn'] == room_arn:
            found = True
            break

    assert found, 'Did not find room: {}'.format(room_arn)


def test_get_room(alexa, room_arn):
    """Verify access to describe rooms.

    :testid: alexaforbusiness-f-3-3

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str room_arn:
        ARN of an AlexaForBusiness room.

    """

    res = alexa.get_room(RoomArn=room_arn)
    assert has_status(res, 200), unexpected(res)
    assert 'Room' in res, unexpected(res)
    assert res['Room']['RoomArn'] == room_arn


def test_update_room(alexa, room_arn):
    """Verify access to update rooms.

    :testid: alexaforbusiness-f-3-4

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str room_arn:
        ARN of an AlexaForBusiness room.

    """

    res = alexa.update_room(
        RoomArn=room_arn,
        Description='new description',
    )
    assert has_status(res, 200), unexpected(res)


def test_create_skill_group(alexa, skill_group_name, stack, shared_vars):
    """Verify access to create skill groups.

    :testid: alexaforbusiness-f-1-3, alexaforbusiness-f-4-1

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str skill_group_name:
        Name of the room to create.
    :param contextlib.ExitStack stack:
        A context manager shared across all Alexa for Business tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = alexa.create_skill_group(
        SkillGroupName=skill_group_name,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'SkillGroupArn' in res, unexpected(res)

    skill_group_arn = shared_vars['skill_group_arn'] = res['SkillGroupArn']
    print('Created room: {}'.format(skill_group_arn))

    stack.callback(
        ignore_errors(alexa.delete_skill_group),
        SkillGroupArn=skill_group_arn,
    )


def test_search_skill_groups(alexa):
    """Verify access to search skill groups.

    :testid: alexaforbusiness-f-4-2

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.

    """

    def _test(res):
        return has_status(res, 200) and 'SkillGroups' in res and len(res['SkillGroups'])

    res = retry(
        alexa.search_skill_groups,
        until=_test,
        msg='Searching skill groups',
    )
    assert _test(res), unexpected(res)


def test_get_skill_group(alexa, skill_group_arn):
    """Verify access to describe skill groups.

    :testid: alexaforbusiness-f-4-3

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str skill_group_arn:
        ARN of an AlexaForBusiness skill group.

    """

    res = alexa.get_skill_group(SkillGroupArn=skill_group_arn)
    assert has_status(res, 200), unexpected(res)
    assert 'SkillGroup' in res, unexpected(res)
    assert res['SkillGroup']['SkillGroupArn'] == skill_group_arn


def test_update_skill_group(alexa, skill_group_arn):
    """Verify access to update skill groups.

    :testid: alexaforbusiness-f-4-4

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str skill_group_arn:
        ARN of an AlexaForBusiness skill group.

    """

    res = alexa.update_skill_group(
        SkillGroupArn=skill_group_arn,
        Description='new description',
    )
    assert has_status(res, 200), unexpected(res)


def test_associate_skill_group_with_room(alexa, skill_group_arn, room_arn, stack):
    """Verify access to create skill groups.

    :testid: alexaforbusiness-f-1-4, alexaforbusiness-f-4-5

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str skill_group_arn:
        ARN of a skill group.
    :param str room_arn:
        ARN of a room.
    :param contextlib.ExitStack stack:
        A context manager shared across all Alexa for Business tests.

    """

    res = alexa.associate_skill_group_with_room(
        SkillGroupArn=skill_group_arn,
        RoomArn=room_arn,
    )
    assert has_status(res, 200), unexpected(res)
    print('Associated skill group {} with room {}'.format(skill_group_arn, room_arn))

    stack.callback(
        ignore_errors(alexa.disassociate_skill_group_from_room),
        SkillGroupArn=skill_group_arn,
        RoomArn=room_arn,
    )


def test_disassociate_skill_group_from_room(alexa, skill_group_arn, room_arn):
    """Verify access to disassociate skill groups from rooms.

    :testid: alexaforbusiness-f-4-6

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str skill_group_arn:
        ARN of a skill group.
    :param str room_arn:
        ARN of a room.

    """

    res = alexa.disassociate_skill_group_from_room(
        SkillGroupArn=skill_group_arn,
        RoomArn=room_arn,
    )
    assert has_status(res, 200), unexpected(res)
    print('Disassociated skill group {} from room {}'.format(skill_group_arn, room_arn))


def test_delete_skill_group(alexa, skill_group_arn):
    """Verify access to delete skill groups.

    :testid: alexaforbusiness-f-4-7

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str skill_group_arn:
        ARN of a skill group.

    """

    res = alexa.delete_skill_group(SkillGroupArn=skill_group_arn)
    assert has_status(res, 200), unexpected(res)


def test_delete_room(alexa, room_arn):
    """Verify access to delete skill groups.

    :testid: alexaforbusiness-f-3-5

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str room_arn:
        ARN of an AlexaForBusiness room.

    """

    res = alexa.delete_room(RoomArn=room_arn)
    assert has_status(res, 200), unexpected(res)


def test_delete_profile(alexa, profile_arn):
    """Verify access to delete skill groups.

    :testid: alexaforbusiness-f-2-5

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str profile_arn:
        ARN of an Alexa for Business Profile.

    """

    res = alexa.delete_profile(ProfileArn=profile_arn)
    assert has_status(res, 200), unexpected(res)


def test_create_user(alexa, user_id, stack, shared_vars):
    """Verify access to create users.

    :testid: alexaforbusiness-f-5-1

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str user_id:
        ARN for an Alexa for Business user.
    :param contextlib.ExitStack stack:
        A context manager shared across all Alexa for Business tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = alexa.create_user(
        UserId=user_id,
        FirstName='Demo',
        LastName='User',
    )
    assert has_status(res, 200), unexpected(res)
    assert 'UserArn' in res, unexpected(res)
    user_arn = shared_vars['user_arn'] = res['UserArn']

    print('Created user: {}'.format(user_arn))
    print('User Info: {}'.format(res))

    stack.callback(
        ignore_errors(cleanup_users),
        alexa=alexa,
    )


def test_search_users(alexa, user_arn):
    """Verify access to search users.

    :testid: alexaforbusiness-f-5-2

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.
    :param str user_arn:
        ARN for an Alexa for Business user.

    """

    def _test(res):
        users = res.get('Users', [])
        user_arns = [u['UserArn'] for u in users]
        return has_status(res, 200) and user_arn in user_arns

    res = retry(
        alexa.search_users,
        until=_test,
        msg='Searching Alexa users...',
    )
    assert _test(res), unexpected(res)


def test_delete_user(alexa):
    """Verify access to delete users.

    Note that this issues a call to cleanup all users because we require the
    user's enrollment ID, which is only available via the ``search_users()``
    function. If we're calling that, we might as well remove all users anyway.

    :testid: alexaforbusiness-f-5-3

    :param AlexaForBusiness.Client alexa:
        A handle to the Alexa for Business API.

    """

    cleanup_users(alexa=alexa)
