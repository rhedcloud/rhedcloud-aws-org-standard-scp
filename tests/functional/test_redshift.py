"""
=============
test_redshift
=============

A customer can launch a RedShift cluster

Plan:

* Create test user
* Create an IAM role
* Launch RedShift
* Be sure to put it in the customer VPC - not the Default VPC
* Check cluster status

* db-0-RedShift Launch and check a RedShift cluster

${testcount:1}

"""

import pytest

from aws_test_functions import (
    catch,
    get_uuid,
    retry,
)


@pytest.fixture(scope='module')
def redshift(session):
    """A shared handle to AWS RedShift API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the AWS RedShift API.

    """
    return session.client('redshift')


def test_create_cluster(redshift, internal_subnet_id):
    """Create a RedShift cluster.

    :testid: db-0-RedShift

    :param boto3.client A handle to the AWS RedShift API
    :param string The AWS subnet ID of the vpc

    """

    identifier = '{}-{}'.format('redshift', get_uuid())
    status_check = 'creating'

    group_response = redshift.create_cluster_subnet_group(
        ClusterSubnetGroupName=identifier,
        Description=identifier,
        SubnetIds=[internal_subnet_id]
    )

    assert 'ClusterSubnetGroup' in group_response, 'unable to create ' \
        'subnet group'

    create_response = redshift.create_cluster(
        DBName='db-0-redshift-cluster',
        ClusterIdentifier=identifier,
        NodeType='dc1.large',
        MasterUsername=identifier,
        MasterUserPassword='db-0-RedShift',
        ClusterType='single-node',
        ClusterSubnetGroupName=identifier,
        PubliclyAccessible=False
    )

    assert 'Cluster' in create_response, 'cluster not created'

    status = retry(
        get_cluster_status,
        args=(redshift, identifier,),
        msg='Checking cluster status',
        until=lambda r: r == status_check,
        delay=10,
    )

    assert status == status_check, 'cluster status is not ' + status_check


def get_cluster_status(redshift, cluster_identifier):
    """Return status of a cluster given it's ClusterIdentifier

    :param boto3.client A handle to the AWS RedShift API
    :param string cluster identifier

    :returns string cluster status

    """

    describe_response = redshift.describe_clusters(
        ClusterIdentifier=cluster_identifier
    )

    return describe_response['Clusters'][0]['ClusterStatus']


@pytest.fixture(scope='module', autouse=True)
def cleanup(redshift):
    """Delete any available clusters and their subnet groups

    :param boto3.client A handle to the AWS RedShift API

    """

    yield

    cleanup_clusters(redshift)
    cleanup_cluster_groups(redshift)


@catch(Exception, msg='Failed to delete cluster: {ex}')
def cleanup_clusters(redshift, *, wait=False):
    """Delete any available clusters

    :param Redshift.Client redshift:
        A handle to the AWS RedShift API
    :param bool wait: (optional)
        Whether to wait for each cluster to be deleted.

    """
    describe_response = redshift.describe_clusters()

    # delete clusters
    for cluster in describe_response['Clusters']:
        cluster_identifier = cluster['ClusterIdentifier']

        print('Deleting cluster: {}'.format(cluster_identifier))
        redshift.delete_cluster(
            ClusterIdentifier=cluster_identifier,
            SkipFinalClusterSnapshot=True
        )

        if wait:
            waiter = redshift.get_waiter('cluster_deleted')
            waiter.wait(
                ClusterIdentifier=cluster_identifier,
            )


@catch(Exception, msg='Failed to delete cluster subnet group: {ex}')
def cleanup_cluster_groups(redshift):
    """Delete any available cluster subnet groups

    :param boto3.client A handle to the AWS RedShift API

    """
    describe_response = redshift.describe_cluster_subnet_groups()

    for subnet_group in describe_response['ClusterSubnetGroups']:
        redshift.delete_cluster_subnet_group(
            ClusterSubnetGroupName=subnet_group['ClusterSubnetGroupName']
        )
