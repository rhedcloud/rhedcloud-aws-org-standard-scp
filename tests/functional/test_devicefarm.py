"""
===============
test_devicefarm
===============

Verify access to AWS Device Farm

Plan:

* Create a project
* Create an app
* Upload an APK for the app
* Create a device pool for testing the app
* Schedule a run to test the app
* Wait for the scheduled run to complete

${testcount:12}

"""

from subprocess import run
import json
import os

import pytest

from aws_test_functions import (
    in_setup_org,
    make_identifier,
    retry,
)

pytestmark = [
    # These tests require about 6 minutes on average, so they're marked as slow
    # tests. Most of the time is spent waiting for the device farm to boot up, run
    # the tests, and wrap things up.
    pytest.mark.slowtest,

    # This service is blocked in the HIPAA configuration so we ensure the scp is
    # in effect
    pytest.mark.scp_check('list_projects')
]

# Create a reusable functor to move the AWS account to the SETUP_ORG before
# attempting to invoke the requested function. This is necessary to ensure that
# we make the best effort to remove resources created during testing (since we
# should not be able to delete with the SCP in effect in the TEST_ORG).
cleaner = in_setup_org('Device Farm cleanup', one_way=True)


@pytest.fixture(scope='module')
def devicefarm(session):
    """A shared handle to Device Farm API.

    .. note::

        Device Farm is unavailable in the default us-east-1 region!

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Device Farm API.

    """

    return session.client('devicefarm', region_name='us-west-2')


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    fake_arn = 'arn:aws:devicefarm:us-west-2:111111111111:project:abcdef12-3456-7890-abcd-ef1234567890'

    return {
        'project_arn': fake_arn,
        'app_arn': fake_arn,
        'app_url': 'not-created',
        'pool_arn': fake_arn,
        'run_arn': fake_arn,
    }


@pytest.fixture(scope='module')
def apk():
    """Return a dictionary with information about the APK to upload and test."""

    path = './tests/resources/RHEDcloud Mobile_v4_apkpure.com.apk'
    return dict(
        name=os.path.basename(path),
        path=path,
    )


def test_create_project(devicefarm, stack, shared_vars):
    """Verify access to create a project.

    :testid: devicefarm-f-1-1

    :param DeviceFarm.Client devicefarm:
        A handle to the Device Farm API.
    :param contextlib.ExitStack stack:
        A context manager shared across all Device Farm tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-devicefarm-proj')
    res = devicefarm.create_project(
        name=name,
    )
    assert 'project' in res, 'Unexpected response: {}'.format(res)
    project_arn = shared_vars['project_arn'] = res['project']['arn']
    print('Created project: {}'.format(project_arn))

    stack.callback(
        cleaner(devicefarm.delete_project),
        arn=project_arn,
    )


def test_create_app(devicefarm, project_arn, apk, stack, shared_vars):
    """Verify access to create an app.

    :testid: devicefarm-f-1-1

    :param DeviceFarm.Client devicefarm:
        A handle to the Device Farm API.
    :param str project_arn:
        The ARN of the project created to test a mobile application.
    :param dict apk:
        Information about the APK to test.
    :param contextlib.ExitStack stack:
        A context manager shared across all Device Farm tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = devicefarm.create_upload(
        projectArn=project_arn,
        name=apk['name'],
        type='ANDROID_APP',
    )
    assert 'upload' in res, 'Unexpected response: {}'.format(res)

    upload = res['upload']
    app_arn = shared_vars['app_arn'] = upload['arn']
    shared_vars['app_url'] = upload['url']
    print('Created app: {}'.format(project_arn))

    stack.callback(
        cleaner(devicefarm.delete_upload),
        arn=app_arn,
    )


def test_upload_app(devicefarm, apk, app_arn, app_url):
    """Verify access to upload an APK to test.

    :testid: devicefarm-f-1-1

    :param DeviceFarm.Client devicefarm:
        A handle to the Device Farm API.
    :param dict apk:
        Information about the APK to test.
    :param str app_arn:
        The ARN of the app created to test a mobile application.
    :param str app_url:
        The pre-signed URL used to upload the APK to test.

    """

    # don't bother uploading the APK in the TEST_ORG (where we won't have a
    # valid URL in the fixture)
    if app_url.startswith('https'):
        # I got tired of getting HTTP 403 errors when trying to upload via
        # Python, so I took the easy way out ;)
        res = run(['curl', '-T', apk['path'], app_url])
        assert res.returncode == 0, 'Failed to upload APK: {}'.format(res)

    desired = 'SUCCEEDED'
    res = retry(
        devicefarm.get_upload,
        kwargs=dict(
            arn=app_arn,
        ),
        msg='Checking upload status',
        until=lambda r: r['upload']['status'] == desired,

        # any exceptions should be raised immediately
        pred=lambda ex: False,
    )
    assert res['upload']['status'] == desired, 'Failed to upload APK: {}'.format(res)


def test_create_device_pool(devicefarm, project_arn, stack, shared_vars):
    """Verify access to create a device pool.

    :testid: devicefarm-f-1-1

    :param DeviceFarm.Client devicefarm:
        A handle to the Device Farm API.
    :param str project_arn:
        The ARN of the project created to test a mobile application.
    :param contextlib.ExitStack stack:
        A context manager shared across all Device Farm tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-devicefarm-pool')
    res = devicefarm.create_device_pool(
        projectArn=project_arn,
        name=name,
        rules=[dict(
            attribute='ARN',
            operator='IN',
            value=json.dumps([
                'arn:aws:devicefarm:us-west-2::device:B3754B5D1B5A4F0281CD6FCDD0709C62'
            ]),
        )],
    )
    assert 'devicePool' in res, 'Unexpected response: {}'.format(res)
    pool_arn = shared_vars['pool_arn'] = res['devicePool']['arn']
    print('Created device pool: {}'.format(project_arn))

    stack.callback(
        cleaner(devicefarm.delete_device_pool),
        arn=pool_arn,
    )


def test_schedule_run(devicefarm, project_arn, app_arn, pool_arn, stack, shared_vars):
    """Verify access to schedule a test of the uploaded APK.

    :testid: devicefarm-f-1-2

    :param DeviceFarm.Client devicefarm:
        A handle to the Device Farm API.
    :param str project_arn:
        The ARN of the project created to test a mobile application.
    :param str app_arn:
        The ARN of the app to test.
    :param str pool_arn:
        The ARN of the device pool to use when testing the app.
    :param contextlib.ExitStack stack:
        A context manager shared across all Device Farm tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = devicefarm.schedule_run(
        projectArn=project_arn,
        appArn=app_arn,
        devicePoolArn=pool_arn,
        test=dict(
            type='BUILTIN_FUZZ'
        )
    )
    assert 'run' in res, 'Unexpected response: {}'.format(res)
    run_arn = shared_vars['run_arn'] = res['run']['arn']
    print('Created run: {}'.format(run_arn))

    stack.callback(
        cleaner(devicefarm.delete_run),
        arn=run_arn,
    )


def test_get_run(devicefarm, run_arn, stack, shared_vars):
    """Verify access to check whether a test has completed.

    :testid: devicefarm-f-1-3

    :param DeviceFarm.Client devicefarm:
        A handle to the Device Farm API.
    :param str run_arn:
        The ARN of the scheduled run to test the application.
    :param contextlib.ExitStack stack:
        A context manager shared across all Device Farm tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    desired = 'COMPLETED'

    res = retry(
        devicefarm.get_run,
        kwargs=dict(
            arn=run_arn,
        ),
        msg='Checking run status',
        delay=60,
        until=lambda r: r['run']['status'] == desired,

        # any exceptions should be raised immediately
        pred=lambda ex: False,
    )
    assert res['run']['status'] == desired, 'Run did not complete: {}'.format(res)
