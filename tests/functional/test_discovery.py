"""
==============
test_discovery
==============

Verify access to use AWS Application Discovery Service.

Plan:

* Create an application
* List applications
* Describe an application
* Rename an application
* Delete an application
* Get a discovery summary

${testcount:5}

"""

import pytest

from aws_test_functions import (
    has_status,
    ignore_errors,
    make_identifier,
    unexpected,
)

ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden|InvalidParameter).*'

# This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('describe_agents')]


@pytest.fixture(scope='module')
def discovery(session):
    """A shared handle to Application Discovery Service API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Application Discovery Service API.

    """

    return session.client('discovery', region_name='us-west-2')


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'config_id': 'not-created',
    }


def test_create_application(discovery, stack, shared_vars):
    """Verify access to create applications.

    :testid: discovery-f-1-1

    :param ApplicationDiscoveryService.Client discovery:
        A handle to the Application Discovery Service API.
    :param contextlib.ExitStack stack:
        A context manager shared across all Application Discovery Service
        tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-applications')
    res = discovery.create_application(
        name=name,
    )
    assert 'configurationId' in res, unexpected(res)
    config_id = shared_vars['config_id'] = res['configurationId']
    print('Created application: {}'.format(config_id))

    stack.callback(
        ignore_errors(test_delete_application),
        discovery, config_id,
    )


def test_list_configurations(discovery):
    """Verify access to list application configurations.

    :testid: discovery-f-1-2

    :param ApplicationDiscoveryService.Client discovery:
        A handle to the Application Discovery Service API.

    """

    res = discovery.list_configurations(
        configurationType='APPLICATION',
    )
    assert has_status(res, 200) and 'configurations' in res, unexpected(res)

    servers = res['configurations']
    assert len(servers), unexpected(res)


def test_describe_configurations(discovery, config_id):
    """Verify access to describe application configurations.

    :testid: discovery-f-1-3

    :param ApplicationDiscoveryService.Client discovery:
        A handle to the Application Discovery Service API.
    :param str config_id:
        ID of an application.

    """

    res = discovery.describe_configurations(
        configurationIds=[config_id],
    )
    assert has_status(res, 200) and 'configurations' in res, unexpected(res)


def test_update_application(discovery, config_id):
    """Verify access to update an application configuration.

    :testid: discovery-f-1-4

    :param ApplicationDiscoveryService.Client discovery:
        A handle to the Application Discovery Service API.
    :param str config_id:
        ID of an application.

    """

    res = discovery.update_application(
        configurationId=config_id,
        name='new name',
        description='New description',
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_application(discovery, config_id):
    """Verify access to delete application configurations.

    :testid: discovery-f-1-5

    :param ApplicationDiscoveryService.Client discovery:
        A handle to the Application Discovery Service API.
    :param str config_id:
        ID of an application.

    """

    print('Deleting application: {}'.format(config_id))
    res = discovery.delete_applications(
        configurationIds=[config_id],
    )
    assert has_status(res, 200), unexpected(res)


def test_get_discovery_summary(discovery):
    """Verify access to get the discovery summary.

    :testid: discovery-f-1-6

    :param ApplicationDiscoveryService.Client discovery:
        A handle to the Application Discovery Service API.

    """

    res = discovery.get_discovery_summary()
    assert has_status(res, 200) and 'agentSummary' in res, unexpected(res)
