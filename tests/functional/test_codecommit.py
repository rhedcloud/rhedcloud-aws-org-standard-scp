"""
===============
test_codecommit
===============

Verify access to AWS CodeCommit functionality.

${testcount:13}

* Create a new test user with the RHEDcloudAdministratorRole.
* Create a repository
* List repositories
* Describe a repository
* Update a repository
* Create a branch in a repository
* Set the default branch in a repository
* List branches in a repository
* Describe a branch in a repository
* Delete a branch in a repository
* List triggers in a repository
* Create a new trigger on a repository
* Delete a repository

"""

import pytest

from aws_test_functions import (
    has_status,
    ignore_errors,
    make_identifier,
    unexpected,
)


@pytest.fixture(scope='module')
def codecommit(session):
    """A shared handle to CodeCommit API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the CodeCommit API.

    """

    return session.client('codecommit')


@pytest.fixture(scope='module', autouse=True)
def topic_arn(session):
    """Create an SNS topic for a repository trigger.

    Note that the SNS service was fully blocked when this was added, so we
    treat this portion a bit different than may be necessary if/when the SNS
    service is unblocked.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    """

    sns = session.client('sns')

    name = make_identifier('test-codecommit')
    arn = None

    try:
        res = sns.create_topic(Name=name)

        assert has_status(res, 200), unexpected(res)
        assert 'TopicArn' in res, unexpected(res)

        arn = res['TopicArn']
        print('Created SNS topic: {}'.format(arn))

        yield arn
    finally:
        if arn is not None:
            print('Deleting SNS topic: {}'.format(arn))
            ignore_errors(sns.delete_topic)(TopicArn=arn)


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'repo_name': make_identifier('TestRepo'),
        'commit_id': '',
    }


def test_create_repository(codecommit, repo_name, stack):
    """Verify access to create CodeCommit repositories.

    :testid: codecommit-f-1-1

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodeCommit tests.

    """

    res = codecommit.create_repository(
        repositoryName=repo_name,
    )
    assert has_status(res, 200), unexpected(res)

    print('Created repository: {}'.format(repo_name))

    stack.callback(
        ignore_errors(codecommit.delete_repository),
        repositoryName=repo_name,
    )


def test_list_repositories(codecommit, repo_name):
    """Verify access to list the CodeCommit repositories.

    :testid: codecommit-f-1-2

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.

    """

    res = codecommit.list_repositories()
    assert has_status(res, 200), unexpected(res)

    found = False
    for repo in res['repositories']:
        if repo['repositoryName'] == repo_name:
            found = True
            break

    assert found, 'Did not find repo: {}'.format(repo_name)


def test_get_repository(codecommit, repo_name):
    """Verify access to describe CodeCommit repositories.

    :testid: codecommit-f-1-3

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.

    """

    res = codecommit.get_repository(
        repositoryName=repo_name,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'repositoryMetadata' in res, unexpected(res)


def test_update_repository_description(codecommit, repo_name):
    """Verify access to update CodeCommit repository descriptions.

    :testid: codecommit-f-1-4

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.

    """

    res = codecommit.update_repository_description(
        repositoryName=repo_name,
        repositoryDescription='testing testing',
    )
    assert has_status(res, 200), unexpected(res)


def test_put_file(codecommit, repo_name, shared_vars):
    """Verify access to add/update files in CodeCommit repositories.

    :testid: codecommit-f-2-1

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.

    """

    res = codecommit.put_file(
        repositoryName=repo_name,
        branchName='master',
        fileContent=b'hello world',
        filePath='hi.txt',
    )
    assert has_status(res, 200), unexpected(res)
    assert 'commitId' in res, unexpected(res)

    shared_vars['commit_id'] = res['commitId']


def test_create_branch(codecommit, repo_name, commit_id):
    """Verify access to create branches in CodeCommit repositories.

    :testid: codecommit-f-2-1

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.

    """

    res = codecommit.create_branch(
        repositoryName=repo_name,
        branchName='develop',
        commitId=commit_id,
    )
    assert has_status(res, 200), unexpected(res)


def test_update_default_branch(codecommit, repo_name):
    """Verify access to set the default branch in CodeCommit repositories.

    :testid: codecommit-f-2-2

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.

    """

    res = codecommit.update_default_branch(
        repositoryName=repo_name,
        defaultBranchName='develop',
    )
    assert has_status(res, 200), unexpected(res)


def test_list_branches(codecommit, repo_name):
    """Verify access to list branches in CodeCommit repositories.

    :testid: codecommit-f-2-3

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.

    """

    res = codecommit.list_branches(
        repositoryName=repo_name,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'branches' in res, unexpected(res)
    assert len(res['branches']) == 2, unexpected(res)


def test_get_branch(codecommit, repo_name):
    """Verify access to describe branches in CodeCommit repositories.

    :testid: codecommit-f-2-4

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.

    """

    res = codecommit.get_branch(
        repositoryName=repo_name,
        branchName='develop',
    )
    assert has_status(res, 200), unexpected(res)
    assert 'branch' in res, unexpected(res)


def test_delete_branch(codecommit, repo_name):
    """Verify access to delete branches in CodeCommit repositories.

    :testid: codecommit-f-2-5

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.

    """

    res = codecommit.delete_branch(
        repositoryName=repo_name,
        branchName='master',
    )
    assert has_status(res, 200), unexpected(res)
    assert 'deletedBranch' in res, unexpected(res)


def test_get_repository_triggers(codecommit, repo_name):
    """Verify access to get triggers on CodeCommit repositories.

    :testid: codecommit-f-3-1

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.

    """

    res = codecommit.get_repository_triggers(
        repositoryName=repo_name,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'triggers' in res, unexpected(res)


def test_put_repository_triggers(codecommit, repo_name, topic_arn):
    """Verify access to create triggers on CodeCommit repositories.

    :testid: codecommit-f-3-2

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.
    :param str topic_arn:
        ARN of an SNS topic.

    """

    res = codecommit.put_repository_triggers(
        repositoryName=repo_name,
        triggers=[dict(
            name='foo',
            destinationArn=topic_arn,
            branches=[],  # must be present
            events=['all'],
        )]
    )
    assert has_status(res, 200), unexpected(res)
    assert 'configurationId' in res, unexpected(res)


def test_delete_repository(codecommit, repo_name):
    """Verify access to delete CodeCommit repositories.

    :testid: codecommit-f-1-5

    :param CodeCommit.Client codecommit:
        A handle to the CodeCommit API.
    :param str repo_name:
        Name of a CodeCommit repository.

    """

    res = codecommit.delete_repository(
        repositoryName=repo_name,
    )
    assert has_status(res, 200), unexpected(res)
