"""
=======
test_s3
=======

Verify that:

* Customers should be able to create S3 buckets that are not publicly available.
* Customers should be able to create S3 buckets that are encrypted.

${testcount:4}

"""

import pytest

from aws_test_functions import (
    find_objects_in_bucket,
    new_bucket,
)


@pytest.fixture(scope='module')
def s3(session):
    return session.client('s3')


@pytest.fixture(scope='module')
def s3res(session):
    return session.resource('s3')


@pytest.mark.parametrize('public', (False, True))
def test_public_bucket(s3, s3res, public):
    """Create an S3 bucket with different "public" settings.

    :testid: s3-f-1-1, s3-f-1-2

    """

    created = False
    acl = 'public-read' if public else 'private'
    print('Creating bucket with ACL={}'.format(acl))
    with new_bucket('test-s3', ACL=acl, s3=s3res) as bucket:
        created = True

        # upload a file (write operations)
        dest = '{}-upload.rtf'.format(acl)
        print('Uploading to {}...'.format(dest))
        bucket.upload_file('./tests/resources/upload_file.rtf', dest, ExtraArgs={'ServerSideEncryption': 'AES256'})

        # make sure we can find the uploaded file (read operations)
        print('Verifying {}...'.format(dest))
        found = find_objects_in_bucket(bucket.name, dest, s3=s3)
        assert found.get(dest), 'file was not uploaded'

    assert created, 'failed to create bucket with ACL={}'.format(acl)


@pytest.mark.parametrize('encrypted', (True, False))
def test_encrypted_bucket(s3, s3res, encrypted, stack):
    """Create an S3 bucket with different "encrypted" settings.

    :testid: s3-f-2-1, s3-f-2-2

    """

    bucket = stack.enter_context(new_bucket("test-s3", s3=s3res))

    if encrypted:
        s3.put_bucket_encryption(
            Bucket=bucket.name,
            ServerSideEncryptionConfiguration={
                'Rules': [{
                    'ApplyServerSideEncryptionByDefault': {
                        'SSEAlgorithm': 'AES256',
                    },
                }],
            },
        )

    dest = 'upload-{}.rtf'.format(encrypted)
    print('Uploading to {}...'.format(dest))
    bucket.upload_file('./tests/resources/upload_file.rtf', dest, ExtraArgs={'ServerSideEncryption': 'AES256'})

    # make sure the uploaded file is encrypted
    print('Verifying {}...'.format(dest))
    res = s3.head_object(
        Bucket=bucket.name,
        Key=dest,
    )

    sse = res.get('ServerSideEncryption')
    if encrypted:
        assert sse == 'AES256', 'file is unexpectedly not encrypted'
    else:
        if sse is not None:
            pytest.xfail("SCP has been modified such that non-encrypted uploads are blocked. This should be discussed")
