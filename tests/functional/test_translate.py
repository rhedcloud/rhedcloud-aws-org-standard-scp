"""
===========
test_translate
===========
A user should be able to use the Translate service.

Plan:

* Attempt to make a TranslateText API call

${testcount:1}

"""

from botocore.exceptions import ClientError
import pytest

# Use a custom regular expression when handling "access denied" errors that
# accounts for resources that do not get created because of other "access
# denied" errors.
# AWS checks for the presence of the job before checking permissions (!!!)
ACCESS_DENIED_NEEDLE = r'(AccessDenied|NotAuthorized)'

#This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('translate_text', kwargs=dict(Text='Hello World',
                                                                  SourceLanguageCode='en',
                                                                  TargetLanguageCode='de'))]


@pytest.fixture(scope='module')
def translate(session):
    """A shared handle to Translate APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Translate APIs.

    """

    return session.client('translate')


def test_translate(translate):
    '''Translate 'Hello World' into German

    :testid: translate-f-1-1

    '''
    resp = translate.translate_text(Text='Hello World',
                                    SourceLanguageCode='en',
                                    TargetLanguageCode='de')

    assert 'TranslatedText' in resp, \
        'Expected "TranslatedText" key to exist in response'
    assert resp['TranslatedText'] == 'Hallo Welt', \
        'Expected a different value as translation'
