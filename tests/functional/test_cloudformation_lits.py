"""
===========
test_cloudformation_lits
===========
A user should not be able to update a CloudFormation stack created by LITS

Plan:

* Try to update an existing LITS stack
* Try to use CAPABILITY_NAMED_IAM and CAPABILITY_IAM to create a root user

${testcount:2}

"""

from copy import deepcopy
import json

import pytest

LITS_STACK_NAME = "rhedcloud-aws-vpc-type1"


@pytest.fixture(scope="module")
def cloudformation(session):
    return session.client("cloudformation")


@pytest.mark.raises_access_denied
def test_update_lits_stack(cloudformation):
    """Update LITS stack.

    :testid: cfn-f-2-1

    """

    original_template = cloudformation.get_template(StackName=LITS_STACK_NAME)
    original_template = original_template["TemplateBody"]
    new_template = deepcopy(original_template)

    # Add an S3 bucket
    test_bucket_name = "TestBucketDeleteMe5395eb1bc72bd2eba7d7c779d4a6323e52c82327404aee3dbd817d31"
    new_template["Resources"][test_bucket_name] = dict(Type="AWS::S3::Bucket")

    # Should raise here
    cloudformation.update_stack(
        StackName=LITS_STACK_NAME,
        TemplateBody=json.dumps(new_template),
    )
    # Should not reach here, cancel
    cloudformation.cancel_update_stack(StackName=LITS_STACK_NAME)


@pytest.mark.raises_access_denied
def test_update_lits_stack_with_capabilities_argument(cloudformation):
    """Nefarious update to the LITS stack should fail.

    :testid: cfn-f-2-2

    """
    original_template = cloudformation.get_template(StackName=LITS_STACK_NAME)
    original_template = original_template["TemplateBody"]
    new_template = deepcopy(original_template)

    # Try to add a root user
    test_username = "TestUserDeleteMe"
    new_template["Resources"][test_username] = dict(
        Type="AWS::IAM::User",
        Properties=dict(
            LoginProfile=dict(
                Password="CredentialRedactedAndRotated",
                PasswordResetRequired=True,
            ),
            Policies=[
                dict(
                    PolicyName="fullaccess",
                    PolicyDocument=dict(
                        Version="2012-10-17",
                        Statement=[dict(
                            Effect="Allow",
                            Action="*",
                            Resource="*",
                        )],
                    ),
                ),
            ],
        ),
    )
    # Should raise here
    cloudformation.update_stack(
        StackName=LITS_STACK_NAME,
        TemplateBody=json.dumps(new_template),
        Capabilities=[
            "CAPABILITY_IAM",
            "CAPABILITY_NAMED_IAM",
        ],
    )
    # Should not reach here, cancel the update
    cloudformation.cancel_update_stack(StackName=LITS_STACK_NAME)
