import pytest

from aws_test_functions import (
    new_policy,
    new_role,
    new_security_group,
)

pytest_plugins = [
    "tests.plugins.ec2",
]


@pytest.fixture(scope="module")
def ecs(session):
    """A shared handle to ECS API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the ECS API.

    """

    return session.client("ecs")


@pytest.fixture(scope="module")
def security_group(ec2, vpc_id, stack):
    """Creates a Security Group in the given vpc allowing ingress on port 80

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        The AWS VPC ID to create the security group in
    :param contextlib.ExitStack stack:
        A context manager shared across all ECS tests.

    :returns:
        A SecurityGroup.

    """

    sg = stack.enter_context(new_security_group(vpc_id, "test_ecs", ec2=ec2))
    ec2.authorize_security_group_ingress(
        GroupId=sg.group_id,
        IpProtocol="tcp",
        FromPort=80,
        ToPort=80,
        CidrIp="0.0.0.0/0",
    )

    yield sg


@pytest.fixture(scope="module")
def ecs_task_role(stack):
    """Create a new role for ECS Tasks.

    :param contextlib.ExitStack stack:
        A context manager shared across all ECS tests.

    :returns:
        An IAM.Role.

    """

    stmt = [
        {
            "Effect": "Allow",
            "Action": [
                "ecs:CreateCluster",
                "ecs:DeregisterContainerInstance",
                "ecs:DiscoverPollEndpoint",
                "ecs:Poll",
                "ecs:RegisterContainerInstance",
                "ecs:StartTelemetrySession",
                "ecs:Submit*",
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
            ],
            "Resource": "*",
        }
    ]

    p = stack.enter_context(new_policy("ecs_task_policy", stmt))
    r = stack.enter_context(new_role("ecs_task_role", "ec2", p.arn))

    yield r
