"""
==============
test_inspector
==============

This is a fully blocked service. Attempts to list, describe,
create, update, delete Inspector related services

inspector-f-1
Customers should be able to make "list" calls within the AWS Inspector service

inspector-f-2
Customers should be able to make "get" calls within the AWS Inspector service

inspector-f-3
Customers should be able to make "put" calls within the AWS Inspector service

${testcount:20}

"""

import time

import pytest

from aws_test_functions import (
    has_status,
    ignore_errors,
    make_identifier,
    new_ec2_instance,
    retry,
    unexpected,
)

INSPECTOR_AMI = 'ami-0d71c6dd9557531ff'


@pytest.fixture(scope='module')
def inspector(session):
    """
    A shared handle to Amazon Inspector

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to Amazon Inspector

    """

    return session.client('inspector')


@pytest.fixture(scope='module')
def resource_groups(session):
    """
    A shared handle to Amazon Resource Groups

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to Amazon Resource Groups

    """

    return session.client('resource-groups')


@pytest.fixture(scope='module')
def sns(session):
    """
    A shared handle to Amazon Simple Notification Service

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to Amazon SNS

    """

    return session.client('sns')


@pytest.fixture(scope='module', autouse=True)
def ec2_instance(stack):
    """Create an EC2 instance with the proper tags for an Inspector assessment.
    """

    instance = stack.enter_context(new_ec2_instance(
        'inspector_instance',
        ami=INSPECTOR_AMI
    ))
    instance.wait_until_running()

    yield instance


@pytest.fixture(scope='module', autouse=True)
def shared_vars():
    """
    Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'target_arn': 'not-created',
        'target_name': make_identifier('target'),
        'template_arn': 'not-created',
        'resource_group_arn': 'not-created',
        'run_arn': 'not-created',
        'topic_arn': 'not-created'
    }


@pytest.mark.bypass('raises_access_denied')
def test_create_topic_arn(sns, stack, shared_vars):
    """Create an SNS topic for an Inspector subscription.

    :param SNS.Client sns:
        A handle to the SNS API.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-inspector-sns')
    arn = None

    res = sns.create_topic(Name=name)
    assert 'TopicArn' in res, unexpected(res)
    assert has_status(res, 200), unexpected(res)

    arn = res['TopicArn']

    # Programmatically finding the Inspector Account ID eludes me
    # Here we allow anyone the ability to publish to this topic
    res = sns.add_permission(
        TopicArn=arn,
        Label='label',
        AWSAccountId=['*'],
        ActionName=[
            "Publish"
        ],
    )
    assert has_status(res, 200), unexpected(res)

    print('Created SNS topic: {}'.format(arn))
    shared_vars['topic_arn'] = arn

    stack.callback(
        ignore_errors(sns.delete_topic),
        TopicArn=arn,
    )


def test_list_templates(inspector):
    """
    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-1-1
    Make API call "ListAssessmentTemplates"

    """

    list_response = inspector.list_assessment_templates()
    assert 'assessmentTemplateArns' in list_response, \
        'Unable to list assessment templates'


def test_list_targets(inspector):
    """
    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-1-2
    Make API call "ListAssessmentTargets"

    """

    list_response = inspector.list_assessment_targets()
    assert 'assessmentTargetArns' in list_response, \
        'Unable to list assessment targets'


def test_list_runs(inspector):
    """
    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-1-3
    Make API call "ListAssessmentRuns"

    """

    list_response = inspector.list_assessment_runs()
    assert 'assessmentRunArns' in list_response, \
        'Unable to list assessment runs'


def test_list_findings(inspector):
    """
    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-1-4
    Make API call "ListFindings"

    """

    list_response = inspector.list_findings()
    assert 'findingArns' in list_response, \
        'Unable to list findings'


def test_create_group(inspector, ec2_instance, shared_vars):
    """
    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-3-1
    Make API call "CreateResourceGroup"

    """

    ec2_tags = ec2_instance.tags
    resource_tags = []
    for tag in ec2_tags:
        resource_tags.append(dict(
            key=tag['Key'],
            value=tag['Value'],
        ))

    create_response = inspector.create_resource_group(
        resourceGroupTags=resource_tags
    )

    assert 'resourceGroupArn' in create_response, 'Unable to create group'
    shared_vars['resource_group_arn'] = create_response['resourceGroupArn']


def test_describe_groups(inspector, shared_vars):
    """
    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-2-1
    Make API call "DescribeResourceGroups"

    """

    describe_response = inspector.describe_resource_groups(
        resourceGroupArns=[shared_vars['resource_group_arn']]
    )

    assert len(describe_response['resourceGroups']) != 0, \
        'Unable to describe resource group'


def test_targets(inspector, shared_vars):
    """
    Create assessment target and verify that it is able to
    be described via describe_assessment_targets

    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-2-2
    Make API call "DescribeAssessmentTargets"

    :testid: inspector-f-3-3
    Make API call "CreateAssessmentTarget"

    """

    target_arn = None

    create_response = inspector.create_assessment_target(
        assessmentTargetName=shared_vars['target_name'],
        resourceGroupArn=shared_vars['resource_group_arn']
    )

    assert 'assessmentTargetArn' in create_response, \
        'Unable to create assessment target'

    target_arn = create_response['assessmentTargetArn']
    shared_vars['target_arn'] = target_arn

    describe_response = inspector.describe_assessment_targets(
        assessmentTargetArns=[target_arn]
    )

    assert len(describe_response['assessmentTargets']) != 0, \
        'No assessment targets to describe'


def test_templates(inspector, shared_vars):
    """
    Create an assessment template and verify that it is able to
    be described via describe_assessment_templates

    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-2-3
    Make API call "DescribeAssessmentTemplates"

    :testid: inspector-f-3-6
    Make API call "CreateAssessmentTemplate"

    """

    template_arn = None
    rules_response = inspector.list_rules_packages(maxResults=1)

    create_response = inspector.create_assessment_template(
        assessmentTargetArn=shared_vars['target_arn'],
        assessmentTemplateName=make_identifier('target'),
        durationInSeconds=180,
        rulesPackageArns=rules_response['rulesPackageArns']
    )

    assert 'assessmentTemplateArn' in create_response, \
        'Unable to create assessment template'

    template_arn = create_response['assessmentTemplateArn']
    shared_vars['template_arn'] = template_arn

    describe_response = inspector.describe_assessment_templates(
        assessmentTemplateArns=[template_arn]
    )

    assert len(describe_response['assessmentTemplates']) != 0, \
        'No assessment templates to describe'


# This test takes ~4 minutes
@pytest.mark.slowtest
def test_assessment_run(inspector, shared_vars):
    """
    Start assessment run and verify that it is able to
    be described via describe_assessment_runs

    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-2-4
    Make API call "DescribeAssessmentRuns"

    :testid: inspector-f-3-8
    Make API call "StartAssessmentRun"

    :testid: inspector-f-3-9
    Make API call "StopAssessmentRun"

    """

    run_arn = None
    run_name = make_identifier('inspector-assessment')
    start_response = retry(
        inspector.start_assessment_run,
        kwargs=dict(
            assessmentTemplateArn=shared_vars['template_arn'],
            assessmentRunName=run_name,
        ),
        msg='Attempting to start assessment run...',
        pred=lambda ex: 'InvalidInputException' in str(ex),
        delay=20,
        max_attempts=20,
    )

    assert 'assessmentRunArn' in start_response, \
        'Unable to start assessment run'

    run_arn = start_response['assessmentRunArn']
    shared_vars['run_arn'] = run_arn

    describe_response = retry(
        inspector.describe_assessment_runs,
        kwargs=dict(assessmentRunArns=[run_arn]),
        msg='Checking Assessment start status {}...'.format(run_name),
        until=lambda s: s['assessmentRuns'][0]['name'] == run_name and s['assessmentRuns'][0]['state'] == 'COLLECTING_DATA',
        pred=lambda ex: False,
        delay=20,
    )

    assert len(describe_response['assessmentRuns']) != 0, \
        'No assessment runs to describe'

    # Allow the agent some time to collect some data
    time.sleep(120)

    res = inspector.stop_assessment_run(
        assessmentRunArn=run_arn,
        stopAction='START_EVALUATION'
    )
    assert has_status(res, 200), unexpected(res)

    describe_response = retry(
        inspector.describe_assessment_runs,
        kwargs=dict(assessmentRunArns=[run_arn]),
        msg='Checking Assessment run stop status {}...'.format(run_name),
        until=lambda s: s['assessmentRuns'][0]['name'] == run_name and s['assessmentRuns'][0]['state'] in ('FAILED', 'ERROR', 'COMPLETED', 'COMPLETED_WITH_ERRORS', 'CANCELED'),
        pred=lambda ex: False,
        delay=20,
    )

    assert describe_response['assessmentRuns'][0]['state'] == 'COMPLETED', \
        'Assessment run failed to complete properly with response: {}'.format(describe_response)


@pytest.mark.xfail(reason='This test requires test_assessment_run, which is marked slow. If it does not run, this will fail')
def test_describe_findings(inspector):
    """
    Describe findings

    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-2-5
    Make API call "DescribeFindings"

    """

    list_response = inspector.list_findings()
    assert len(list_response['findingArns']) != 0, \
        'No findings to describe'


# NOTE: resource_groups API is blocked.
# NOTE: Further there does not seem to be an API for bulk resource group management, or individual deletion,
# NOTE: through Inspector(AWS official or otherwise such as Boto or Java).
# NOTE: Even in the AWS Console, there seem to be no artifacts of the resource group created
@pytest.mark.skip(reason='See note in code, resource group management is limited')
def test_delete_group(resource_groups, shared_vars):
    """
    Delete resource group

    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-3-2
    Make API call "DeleteResourceGroup"

    """

    groups = []
    list_response = resource_groups.list_groups()
    groups = list_response['Groups']

    assert len(groups) != 0, 'No resource groups to delete'

    for group in groups:
        res = resource_groups.delete_group(
            GroupName=group['Name']
        )
        assert has_status(res, 200), unexpected(res)


def test_update_target(inspector, shared_vars):
    """
    Update assessment target

    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-3-5
    Make API call "UpdateAssessmentTarget"

    """

    update_response = inspector.update_assessment_target(
        assessmentTargetArn=shared_vars['target_arn'],
        assessmentTargetName=shared_vars['target_name'],
        resourceGroupArn=shared_vars['resource_group_arn']
    )
    assert has_status(update_response, 200), unexpected(update_response)


def test_subscribe(inspector, sns, shared_vars):
    """
    Subscribe to assessment run started event

    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-3-10
    Make API call "SubscribeToEvent"

    """

    subscribe_response = inspector.subscribe_to_event(
        resourceArn=shared_vars['template_arn'],
        event='ASSESSMENT_RUN_STARTED',
        topicArn=shared_vars['topic_arn']
    )
    assert has_status(subscribe_response, 200), unexpected(subscribe_response)


def test_unsubscribe(inspector, shared_vars):
    """
    Unsubscribe to assessment run started event

    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-3-11
    Make API call "UnsubscribeFromEvent"

    """

    unsubscribe_response = inspector.unsubscribe_from_event(
        resourceArn=shared_vars['template_arn'],
        event='ASSESSMENT_RUN_STARTED',
        topicArn=shared_vars['topic_arn']
    )
    assert has_status(unsubscribe_response, 200), unexpected(unsubscribe_response)


def test_delete_target(inspector):
    """
    Delete assessment targets

    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-3-4
    Make API call "DeleteAssessmentTarget"

    """

    target_arns = []
    list_response = inspector.list_assessment_targets()
    target_arns = list_response['assessmentTargetArns']

    assert len(target_arns) != 0, 'No targets to delete'

    for target in target_arns:
        res = inspector.delete_assessment_target(
            assessmentTargetArn=target
        )
        assert has_status(res, 200), unexpected(res)


def test_delete_templates(inspector, shared_vars):
    """
    Delete assessment templates

    :param boto3.client A handle to the AWS Inspector API

    :testid: inspector-f-3-7
    Make API call "DeleteAssessmentTemplate"

    """

    list_response = inspector.list_assessment_templates()
    templates = list_response['assessmentTemplateArns']

    for template in templates:
        print('deleting %s' % (template['arn']))
        res = inspector.delete_assessment_template(
            assessmentTemplateArn=template['arn']
        )
        assert has_status(res, 200), unexpected(res)
