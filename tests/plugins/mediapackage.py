"""
=====================
MediaPackage Fixtures
=====================

This module contains fixtures for MediaPackage tests and tests for related
services (like MediaLive).

"""

import pytest


@pytest.fixture(scope='module')
def mediapackage(session):
    """A shared handle to the MediaPackage API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the MediaPackage API.

    """

    return session.client('mediapackage')
