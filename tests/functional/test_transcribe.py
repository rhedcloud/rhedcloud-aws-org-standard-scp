"""
===========
test_transcribe
===========
A user should not be able to use the Transcribe service.

Plan:

* Attempt to start a job to transcribe speech to text and allow it to complete

${testcount:2}

"""

import json

import pytest

from aws_test_functions import (
    new_bucket,
    new_transcription_job,
)


# Use a custom regular expression when handling "access denied" errors that
# accounts for resources that do not get created because of other "access
# denied" errors.
# AWS checks for the presence of the job before checking permissions (!!!)
ACCESS_DENIED_NEEDLE = r'(AccessDenied|NotAuthorized)'

# This service is fully blocked
pytestmark = [pytest.mark.scp_check('list_transcription_jobs', kwargs=dict(Status='IN_PROGRESS'))]


@pytest.fixture(scope='module')
def transcribe(session):
    """A shared handle to Transcribe APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Transcribe APIs.

    """

    return session.client('transcribe')


@pytest.fixture(scope='module')
def sample_file_uri(session):
    s3 = session.resource('s3')
    key = 'transcribe-sample.wav'
    trust_policy_stmt = [
        dict(
            Effect='Allow',
            Principal=dict(
                Service='transcribe.amazonaws.com',
            ),
            Action='s3:GetObject',
        ),
    ]

    with new_bucket('transcribe-bucket', s3=s3) as b:
        b.upload_file(
            './tests/resources/transcribe-sample.wav',
            key,
            ExtraArgs={'ServerSideEncryption': 'AES256'},
        )
        trust_policy_stmt[0]['Resource'] = "arn:aws:s3:::{}/*".format(b.name)
        doc = json.dumps({
            'Version': '2012-10-17',
            'Statement': trust_policy_stmt,
        }, indent=2)
        session.client('s3').put_bucket_policy(
            Bucket=b.name,
            Policy=doc,
        )
        yield 'https://s3.amazonaws.com/{}/{}'.format(b.name, key)


def test_transcribe(sample_file_uri, transcribe):
    '''Start a job to transcribe speech to text

    :testid: transcribe-f-1-1
    :testid: transcribe-f-1-2

    '''

    with new_transcription_job(
        'transcription_test_job',
        sample_file_uri,
        'wav',
        'en-US',
        True,
    ) as job_name:
        assert job_name
