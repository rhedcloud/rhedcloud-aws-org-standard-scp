"""
===============
test_simpledb
===============

Verify access to use AWS SimpleDB

Plan:

* Attempt to create a domain
* Attempt to list the available domains and verify the created one is returned
* Attempt to put a value in the domain
* Attempt to retrive the value from the domain
* Attempt to delete the value from the domain
* Attempt to delete the domain

${testcount:12}

"""

import pytest

from aws_test_functions import (
    aws_client,
    catch,
    has_status,
    ignore_errors,
    in_setup_org,
    make_identifier,
    retry,
    unexpected,
)

ACCESS_DENIED_NEEDLE = r".*(AccessDenied|AuthorizationFailure).*"

# This service is fully blocked
pytestmark = [pytest.mark.scp_check("list_domains", needle="AuthorizationFailure")]

# Create a reusable functor to move the AWS account to the SETUP_ORG before
# attempting to invoke the requested function. This is necessary to ensure that
# we make the best effort to remove resources created during testing (since we
# should not be able to delete with the SCP in effect in the TEST_ORG).
cleaner = in_setup_org("SimpleDB cleanup", one_way=True)


@pytest.fixture(scope="module")
def simpledb(session):
    """A shared handle to SimpleDB API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the SimpleDB API.

    """

    return session.client("sdb")


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        "domain_name": "not-created",
        "item_name": "not-created",
    }


def test_create_domain(simpledb, shared_vars, stack):
    """Verify ability to create a domain

    :testid:

    :param SimpleDB.Client simpledb:
        A handle to the SimpleDB API.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param contextlib.ExitStack stack:
        A context manager shared across all SimpleDB tests.

    """

    name = make_identifier("TestSimpleDBDomain")
    res = simpledb.create_domain(
        DomainName=name,
    )
    assert has_status(res, 200), unexpected(res)

    shared_vars["domain_name"] = name
    stack.callback(
        ignore_errors(cleaner(simpledb.delete_domain)),
        DomainName=name,
    )


def test_list_domains(simpledb, domain_name):
    """Verify ability to list domains

    :testid:

    :param SimpleDB.Client simpledb:
        A handle to the SimpleDB API.
    :param str domain_name:
        The name of the domain created for SimpleDB

    """

    res = simpledb.list_domains()
    assert has_status(res, 200), unexpected(res)
    domains = res["DomainNames"]

    found = False
    for name in domains:
        if name == domain_name:
            found = True
            break
    assert found, "Did not find domain {} in list_domains: {}".format(
        domain_name, domains
    )


def test_put_attributes(simpledb, domain_name, shared_vars):
    """Verify ability to add items and attributes to a domain

    :testid:

    :param SimpleDB.Client simpledb:
        A handle to the SimpleDB API.
    :param str domain_name:
        The name of the domain created for SimpleDB.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier("TestSimpleDBItem")
    res = simpledb.put_attributes(
        DomainName=domain_name,
        ItemName=name,
        Attributes=[
            dict(Name="Test_Attribute_1", Value="Test_Value_1"),
            dict(Name="Test_Attribute_2", Value="Test_Value_2"),
        ],
    )
    assert has_status(res, 200), unexpected(res)
    shared_vars["item_name"] = name
    print("Created item: {}".format(name))


def test_get_attributes(simpledb, domain_name, item_name):
    """Verify ability to get items and attributes from a domain

    :testid:

    :param SimpleDB.Client simpledb:
        A handle to the SimpleDB API.
    :param str domain_name:
        The name of the domain created for SimpleDB
    :param str item_name:
        The name of the item created in the SimpleDB domain

    """

    res = retry(
        simpledb.get_attributes,
        kwargs=dict(
            DomainName=domain_name,
            ItemName=item_name,
        ),
        until=lambda res: len(res.get("Attributes", [])) == 2,
        max_attempts=3,
    )
    assert has_status(res, 200), unexpected(res)

    assert (len(res["Attributes"]) == 2), "Unexpected attribute list after put: {}".format(res)

    for attr in res["Attributes"]:
        assert attr["Name"] in ("Test_Attribute_1", "Test_Attribute_2")
        assert attr["Value"] in ("Test_Value_1", "Test_Value_2")


def test_delete_attributes(simpledb, domain_name, item_name):
    """Verify ability to delete attributes from a domain

    :testid:

    :param SimpleDB.Client simpledb:
        A handle to the SimpleDB API.
    :param str domain_name:
        The name of the domain created for SimpleDB
    :param str item_name:
        The name of the item created in the SimpleDB domain

    """

    res = simpledb.delete_attributes(
        DomainName=domain_name,
        ItemName=item_name,
        Attributes=[
            dict(Name="Test_Attribute_1", Value="Test_Value_1"),
            dict(Name="Test_Attribute_2", Value="Test_Value_2"),
        ],
    )
    assert has_status(res, 200), unexpected(res)

    res = retry(
        simpledb.get_attributes,
        kwargs=dict(
            DomainName=domain_name,
            ItemName=item_name,
        ),
        msg="Checking delete status",
        until=lambda r: "Attributes" not in r or len(r["Attributes"]) == 0,
    )
    assert has_status(res, 200), unexpected(res)
    assert (
        "Attributes" not in res or len(res["Attributes"]) == 0
    ), "Unexpected attribute list after delete: {}".format(res)


def test_delete_domain(simpledb, shared_vars):
    """Verify ability to delete a domain

    :testid:

    :param SimpleDB.Client simpledb:
        A handle to the SimpleDB API.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = simpledb.delete_domain(
        DomainName=shared_vars["domain_name"],
    )
    assert has_status(res, 200), unexpected(res)
    shared_vars["domain_name"] = "not-created"


@catch(Exception, msg="Failed to delete domain: {ex}")
@aws_client("sdb")
def cleanup_domains(*, sdb=None):
    """Attempt to remove any unused domains."""

    res = sdb.list_domains()
    if "DomainNames" in res:
        for name in res["DomainNames"]:
            print("Deleting domain: {}".format(name))
            sdb.delete_domain(DomainName=name)
