#!/usr/bin/env python3
"""
=====================
Test Resource Cleanup
=====================

The purpose of this script is to remove any resources that tend to linger
around after tests complete, especially when the existence of those resources
causes the test pipeline to fail.

"""

import boto3

from aws_test_functions.appstream import cleanup_image_builders
from aws_test_functions.plugin import dict_fixture
from aws_test_functions import (
    datapipeline,
    elastictranscoder,
    opsworks,
    secretsmanager,
    waf,
)
from tests.functional import (
    test_cloud9,
    test_cloudhsm,
    test_ecs,
    test_guardduty,
    test_iot,
    test_kinesisvideo,
    test_redshift,
    test_medialive,
    test_mediapackage,
    test_mediastore,
    test_simpledb,
)
from tests.functional.rds.conftest import cleanup as cleanup_rds_resources


def cleanup_cloud9():
    """Attempt to clean up any resources left behind by CloudHSM tests."""

    print("Deleting any Cloud9 environments...")

    test_cloud9.cleanup_cloud9()


def cleanup_cloudhsm():
    """Attempt to clean up any resources left behind by CloudHSM tests."""

    print("Deleting any CloudHSM clusters...")

    test_cloudhsm.cleanup_clusters()


def cleanup_datapipeline():
    """Attempt to clean up any resources left behind by MediaStore tests."""

    print("Deleting any Data Pipeline pipelines...")
    datapipeline.cleanup_pipelines()


def cleanup_ecs():
    """Attempt to clean up any resources left behind by ECS tests."""

    print("Deleting any ECS clusters...")
    test_ecs.cleanup_clusters()


def cleanup_guardduty():
    """Attempt to clean up any resources left behind by GuardDuty tests."""

    print("Deleting any GuardDuty detectors...")
    test_guardduty.cleanup_detectors()


def cleanup_iot():
    """Attempt to clean up any resources left behind by IoT tests."""

    print("Deleting any IoT resources...")
    test_iot.cleanup_thing_types()


def cleanup_kinesis_video():
    """Attempt to clean up any resources left behind by KinesisVideo tests."""

    print("Deleting any Kinesis Video streams...")
    test_kinesisvideo.cleanup_streams()


def cleanup_medialive():
    """Attempt to clean up any resources left behind by MediaLive tests."""

    print("Deleting any MediaLive channels...")
    test_medialive.cleanup_channels()


def cleanup_mediapackage():
    """Attempt to clean up any resources left behind by MediaPackage tests."""

    print("Deleting any MediaPackage channels...")
    test_mediapackage.cleanup()


def cleanup_mediastore():
    """Attempt to clean up any resources left behind by MediaStore tests."""

    print("Deleting any MediaStore containers...")
    test_mediastore.cleanup_containers()


def cleanup_redshift():
    """Attempt to clean up any resources left behind by Redshift tests."""

    print("Deleting any Redshift clusters...")

    redshift = boto3.client("redshift")
    test_redshift.cleanup_clusters(redshift, wait=True)


def cleanup_rds():
    """Attempt to clean up any resources left behind by RDS tests."""

    print("Deleting any RDS resources...")

    rds = boto3.client("rds")
    cleanup_rds_resources(rds)


def cleanup_secretsmanager():
    """Attempt to clean up any resources left behind by MediaStore tests."""

    print("Deleting any SecretsManager secrets..")
    secretsmanager.cleanup_secrets()


def cleanup_simpledb():
    """Attempt to clean up any resources left behind by MediaPackage tests."""

    print("Deleting any SimpleDB Domains...")
    test_simpledb.cleanup_domains()


def main():

    cleanup_image_builders()
    cleanup_cloud9()
    cleanup_cloudhsm()
    cleanup_datapipeline()
    cleanup_ecs()
    elastictranscoder.cleanup()
    cleanup_guardduty()
    cleanup_iot()
    cleanup_kinesis_video()
    cleanup_mediapackage()
    cleanup_mediastore()
    opsworks.cleanup()
    cleanup_rds()
    cleanup_redshift()
    cleanup_secretsmanager()
    cleanup_simpledb()
    waf.cleanup_waf()


if __name__ == "__main__":
    main()
