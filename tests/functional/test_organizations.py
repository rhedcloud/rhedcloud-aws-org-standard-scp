"""
==================
test_organizations
==================

Verify that:

* Customers cannot make any Organizations API calls
* Customers cannot make "Leave Organization" calls to break from an
  RHEDcloud-created Organization.

${testcount:4}

"""

import pytest


@pytest.fixture(scope='module')
def orgs(session):
    return session.client('organizations')


@pytest.mark.raises_access_denied
def test_describe_org(orgs):
    """Customers should NOT be able to describe Organizations.

    :testid: organization-f-1-1

    """

    orgs.describe_organization()
    assert False, 'had permission to describe Organization'


@pytest.mark.raises_access_denied
def test_create_org(orgs):
    """Customers should NOT be able to create Organizations.

    :testid: organization-f-1-1

    """

    orgs.create_organization()
    assert False, 'had permission to create Organization'


@pytest.mark.raises_access_denied
def test_describe_create_account_status(orgs):
    """Customers should NOT be able to describe create account status.

    :testid: organization-f-1-1

    """

    orgs.describe_create_account_status(CreateAccountRequestId='car-abcd1234')
    assert False, 'had permission to describe create account status'


@pytest.mark.raises_access_denied
def test_leave_org(orgs):
    """Customers should NOT be able to leave an RHEDcloud-created organization.

    :testid: organization-f-1-2

    """

    orgs.leave_organization()
    assert False, 'had permission to leave Organization'
