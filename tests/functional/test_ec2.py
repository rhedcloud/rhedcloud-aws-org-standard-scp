"""
=============
test_ec2
=============

Users should be to launch, reboot, stop, re-size, and restart ec2 instances in
the public and private subnets

Plan:

* ec2-f-1-1 User should be able to launch an ec2 instance into the public
and private subnets
* ec2-f-1-2 User should not be able to launch an ec2 instance into the
management subnets
* ec2-f-1-3 User should be able to reboot an instance
* ec2-f-1-4 User should be able to stop an instance
* ec2-f-1-5 User should be able to re-size an instance
* ec2-f-1-6 User should be able to start an instance
* ec2-f-1-7 User should be able to terminate an instance

* ec2-f-2-1 User should be able to add an EBS storage volume to an instance
* ec2-f-2-2 User should be able to add a network interface to an instance

* ec2-f-3-1 User should be able to create a security group
* ec2-f-3-2 User should be able to modify security group
* ec2-f-3-4 User should be able to delete security group

${testcount:12}

"""

import pytest
import time

from aws_test_functions import (
    ClientError,
    get_subnet_id,
    has_status,
    new_ec2_instance,
    unexpected,
)

positive_envs = [
    'Public Subnet 1',
    'Public Subnet 2',
    'Private Subnet 1',
    'Private Subnet 2',
]

negative_envs = [
    'MGMT1 - LITS ONLY',
    'MGMT2 - LITS ONLY',
]

pytestmark = pytest.mark.slowtest


@pytest.fixture(scope='module')
def ec2(session):
    """A shared handle to AWS EC2 API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the AWS EC2 API.

    """
    return session.client('ec2')


@pytest.fixture(scope='module')
def instance(session, vpc_id):
    env = positive_envs[1]
    subnet_id = get_subnet_id(vpc_id, env)

    with new_ec2_instance(env, session=session, SubnetId=subnet_id) as inst:
        yield inst


@pytest.fixture(scope='module', autouse=True)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        # check to run further security group tests
        'security_group_created': False,
        'security_group_deleted': False
    }


@pytest.mark.parametrize('env', positive_envs)
def test_launch_instances(ec2, vpc_id, session, env):
    """User should be able to launch an ec2 instance into the public and
    private subnets

    :testid: ec2-f-1-1

    :param boto3.client A handle to the AWS EC2 API
    :param string The AWS subnet ID of the vpc
    :param boto3.session.Session A session belonging to an authenticated
    IAM User.
    :param string current element of negative_env[]

    """

    with new_ec2_instance(
        env,
        session=session,
        SubnetId=get_subnet_id(vpc_id, env)
    ):
        time.sleep(5)
        assert True, 'Could not create instance for ' + env


@pytest.mark.raises(ClientError, match='UnauthorizedOperation')
@pytest.mark.parametrize('env', negative_envs)
def test_launch_mgmt_instances(ec2, vpc_id, session, env):
    """User should not be able to launch an ec2 instance into the management
    subnets

    :testid: ec2-f-1-2

    :param boto3.client A handle to the AWS EC2 API
    :param string The AWS subnet ID of the vpc
    :param boto3.session.Session A session belonging to an authenticated
    IAM User.
    :param string current element of negative_env[]

    """

    with new_ec2_instance(
        env,
        session=session,
        SubnetId=get_subnet_id(vpc_id, env)
    ):
        time.sleep(5)
        assert False, 'Could create instance for ' + env


def test_reboot_instance(ec2, instance):
    """User should be able to reboot an instance

    :testid: ec2-f-1-3

    :param boto3.client A handle to the AWS EC2 API
    :param string The AWS subnet ID of the vpc
    :param boto3.session.Session A session belonging to an authenticated
    IAM User.

    """

    instance.wait_until_running()

    reboot_response = ec2.reboot_instances(
        InstanceIds=[instance.instance_id]
    )

    assert len(reboot_response['ResponseMetadata']) > 0, 'Unable to reboot ' \
        'instance: {}'.format(reboot_response)


def test_stop_instance(ec2, instance):
    """User should be able to stop an instance

    :testid: ec2-f-1-4

    :param boto3.client A handle to the AWS EC2 API
    :param string The AWS subnet ID of the vpc
    :param boto3.session.Session A session belonging to an authenticated
    IAM User.

    """

    instance.wait_until_running()

    stop_response = ec2.stop_instances(
        InstanceIds=[instance.instance_id]
    )

    assert len(stop_response['StoppingInstances']) > 0, 'Unable to stop ' \
        'instance: {}'.format(stop_response)


def test_resize_instance(ec2, instance):
    """User should be able to resize an instance

    :testid: ec2-f-1-5

    :param boto3.client A handle to the AWS EC2 API
    :param string The AWS subnet ID of the vpc
    :param boto3.session.Session A session belonging to an authenticated
    IAM User.

    """

    instance.wait_until_stopped()

    resize_response = ec2.modify_instance_attribute(
        InstanceId=instance.instance_id,
        InstanceType={
            'Value': 't2.small'
        }
    )

    assert resize_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to resize instance: {}'.format(resize_response)


def test_start_instance(ec2, instance):
    """User should be able to start an instance

    :testid: ec2-f-1-6

    :param boto3.client A handle to the AWS EC2 API
    :param string The AWS subnet ID of the vpc
    :param boto3.session.Session A session belonging to an authenticated
    IAM User.

    """

    start_response = ec2.start_instances(
        InstanceIds=[instance.instance_id]
    )

    assert len(start_response['StartingInstances']) > 0, \
        'Unable to start instance: {}'.format(start_response)


def test_terminate_instance(ec2, instance):
    """User should be able to terminate an instance

    :testid: ec2-f-1-7

    :param boto3.client A handle to the AWS EC2 API
    :param string The AWS subnet ID of the vpc
    :param boto3.session.Session A session belonging to an authenticated
    IAM User.

    """

    instance.wait_until_running()

    terminate_response = ec2.terminate_instances(
        InstanceIds=[instance.instance_id]
    )

    assert len(terminate_response['TerminatingInstances']) > 0, \
        'Unable to terminate instance: {}'.format(terminate_response)


def test_create_storage_volume(ec2, vpc_id, session):
    """User should be able to create an EBS storage volume and attach it to
    an instance


    :testid: ec2-f-2-1
    :testid: ec2-f-2-2

    :param boto3.client A handle to the AWS EC2 API
    :param string The AWS subnet ID of the vpc
    :param boto3.session.Session A session belonging to an authenticated
    IAM User.

    """

    env = positive_envs[2]

    with new_ec2_instance(
        env,
        session=session,
        SubnetId=get_subnet_id(vpc_id, env)
    ) as instance:

        instance.wait_until_running()

        avail_zones = ec2.describe_availability_zones()

        assert len(avail_zones['AvailabilityZones']) > 0, \
            'No accessibility zones available'

        volume = ec2.create_volume(
            AvailabilityZone=avail_zones['AvailabilityZones'][0]['ZoneName'],
            Size=10
        )

        # ec2-f-2-1 assert
        assert volume['ResponseMetadata']['HTTPStatusCode'] == 200, \
            'Unable to create volume'

        time.sleep(10)

        storage_response = ec2.attach_volume(
            Device='xvdh',
            InstanceId=instance.instance_id,
            VolumeId=volume['VolumeId']
        )

        # ec2-f-2-2 asset
        assert storage_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
            'Unable to attach volume'


def test_create_security_group(ec2, shared_vars):
    """User should be able to create a security group

    :testid: ec2-f-3-1

    :param boto3.client A handle to the AWS EC2 API
    :param dict shared_vars: dictionary shared between test functions

    """

    describe_response = ec2.describe_security_groups()
    assert describe_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to list security groups: {}'.format(describe_response)

    create_response = ec2.create_security_group(
        Description='ec2-f-3-1 description',
        GroupName='ec2-f-3'
    )

    assert create_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to create security group: {}'.format(create_response)

    shared_vars['security_group_created'] = True


def test_modify_security_group(ec2, shared_vars):
    """User should be able to modify a security group

    :testid: ec2-f-3-2

    :param boto3.client A handle to the AWS EC2 API
    :param dict shared_vars: dictionary shared between test functions

    """
    assert shared_vars['security_group_created'], 'security group not created'

    describe_response = ec2.describe_security_groups(
        GroupNames=['ec2-f-3']
    )

    modify_response = ec2.authorize_security_group_ingress(
        GroupId=describe_response['SecurityGroups'][0]['GroupId'],
        SourceSecurityGroupName='ec2-f-3'
    )

    assert modify_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to modify security group'


def test_delete_security_group(ec2, shared_vars):
    """User should be able to delete a security group

    :testid: ec2-f-3-4

    :param boto3.client A handle to the AWS EC2 API
    :param dict shared_vars: dictionary shared between test functions

    """
    assert shared_vars['security_group_created'], 'security group not created'

    ec2.describe_security_groups(
        GroupNames=['ec2-f-3']
    )

    delete_response = delete_security_group(ec2)

    assert delete_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to delete security group'

    shared_vars['security_group_deleted'] = True


@pytest.mark.raises_access_denied(match='UnauthorizedOperation')
def test_create_default_vpc(ec2):
    """Verify access to create a default VPC.

    :param EC2.Client ec2:
        A handle to the AWS EC2 API.

    """

    res = ec2.create_default_vpc(DryRun=True)
    assert has_status(res, 200), unexpected(res)
    assert 'Vpc' in res, unexpected(res)


@pytest.fixture(scope='module', autouse=True)
def cleanup(ec2, shared_vars):
    """Delete security group if it still exists

    """

    yield

    # this will be executed after all test functions in this module complete
    if shared_vars['security_group_created'] \
            and not shared_vars['security_group_deleted']:
        delete_security_group(ec2)


def delete_security_group(ec2):
    """General function to delete security group

    :param boto3.client A handle to the AWS EC2 API

    """

    describe_response = ec2.describe_security_groups(
        GroupNames=['ec2-f-3']
    )

    delete_response = ec2.delete_security_group(
        GroupId=describe_response['SecurityGroups'][0]['GroupId']
    )

    return delete_response
