"""
============
test_support
============

A user should be able to:

  support-f-1-1 - describe AWS Support services
  support-f-1-2 - describe AWS Support severity levels
  support-f-2-1 - create an AWS Support case
  support-f-2-2 - describe AWS Support cases
  support-f-3-1 - add AWS Support case communications to a case
  support-f-3-2 - describe AWS Support case communications
  support-f-4-1 - create an AWS Support case attachment set
  support-f-4-2 - add attachment set to AWS Support case
  support-f-4-3 - describe AWS Support case attachment
  support-f-5-1 - resolve any open AWS Support cases.

${testcount:10}

"""

import pytest


pytestmark = [
    pytest.mark.premium_account_required,
]


skip_because_metrics = pytest.mark.skip(
    reason='AWS requested we stop running these test because it impacts their metrics negatively',
)


@pytest.fixture(scope='module')
def support(session):
    """A shared handle to Support APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Support APIs.

    """

    return session.client('support')


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """
    Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'case_id': '',
        'service': '',
        'severity_level': '',
        'attachment_set_id': '',
        'attachment_id': '',
    }


def test_describe_services(support, shared_vars):
    """Attempt to describe AWS Support services

    :testid: support-f-1-1

    :param Support.Client api:
        A handle to the AWS Support APIs.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    describe_response = support.describe_services()

    services = describe_response['services']
    assert len(services) > 0, 'No support services found'
    shared_vars['service'] = services[0]


def test_describe_severity_levels(support, shared_vars):
    """Attempt to describe AWS Support severity levels

    :testid: support-f-1-2

    :param Support.Client api:
        A handle to the AWS Support APIs.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    describe_response = support.describe_severity_levels()

    severity_levels = describe_response['severityLevels']
    assert len(severity_levels) > 0, 'No support services found'
    shared_vars['severity_level'] = severity_levels[0]


@skip_because_metrics
def test_create_case(support, service, severity_level, shared_vars):
    """Attempt to get create an AWS Support case

    :testid: support-f-2-1

    :param Support.Client api:
        A handle to the AWS Support APIs.
    :param dict service:
    :param dict severity_level:
    :param dict shared_vars:
        dictionary shared between test functions

    """

    category = service['categories'][0]

    create_response = support.create_case(
        subject='test subject',
        communicationBody='test comm body',
        serviceCode=service['code'],
        categoryCode=category['code'],
        severityCode=severity_level['code'],
    )

    assert create_response['caseId'] is not None, \
        'Unable to create Support case'

    shared_vars['case_id'] = create_response['caseId']


@skip_because_metrics
def test_describe_case(support):
    """Attempt to get describe AWS Support cases

    :testid: support-f-2-2

    :param Support.Client api:
        A handle to the AWS Support APIs.

    """

    describe_response = support.describe_cases()

    assert len(describe_response['cases']) > 0, \
        'Unable to retrieve Support cases'


@skip_because_metrics
def test_add_communication(support, case_id):
    """Attempt to add a communication to an AWS Support case

    :testid: support-f-3-1

    :param Support.Client api:
        A handle to the AWS Support APIs.
    :param str case_id:
        Support Case ID.

    """

    add_response = support.add_communication_to_case(
        caseId=case_id,
        communicationBody='test body',
    )

    assert add_response['result'] is True, 'Unable to add communication'


def test_add_attachment(support, shared_vars):
    """Attempt to create an attachment set for an AWS Support case

    :testid: support-f-4-1

    :param Support.Client api:
        A handle to the AWS Support APIs.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    add_response = support.add_attachments_to_set(
        attachments=[{
            'fileName': 'test.png',
            'data': b'\xe8\x03',
        }],
    )

    attachment_set_id = add_response['attachmentSetId']
    assert attachment_set_id is not None, 'Unable to add attachment'
    shared_vars['attachment_set_id'] = attachment_set_id


@skip_because_metrics
def test_add_attachment_to_case(support, case_id, attachment_set_id):
    """Attempt to add an attachment set to an AWS Support case

    :testid: support-f-4-2

    :param Support.Client api:
        A handle to the AWS Support APIs.
    :param str case_id:
        Support Case ID.
    :param str attachment_set_id:
        Support Case attachment set ID.

    """

    add_response = support.add_communication_to_case(
        caseId=case_id,
        communicationBody='test add attachment',
        attachmentSetId=attachment_set_id,
    )

    assert add_response['result'] is True, \
        'Unable to add attachment set to case'


@skip_because_metrics
def test_describe_communication(support, case_id, shared_vars):
    """Attempt to describe AWS Support communications

    :testid: support-f-3-2

    :param Support.Client api:
        A handle to the AWS Support APIs.
    :param str case_id:
        Support Case ID.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    describe_response = support.describe_communications(
        caseId=case_id,
    )

    communications = describe_response['communications']
    assert len(communications) > 0, \
        'Unable to retrieve communications'

    for comm in communications:
        attachmentSet = comm['attachmentSet']

        if (len(attachmentSet) > 0):
            shared_vars['attachment_id'] = attachmentSet[0]['attachmentId']


@skip_because_metrics
def test_describe_attachments(support, attachment_id):
    """Attempt to describe attachments in a given attachment set

    :testid: support-f-4-3

    :param Support.Client api:
        A handle to the AWS Support APIs.
    :param str attachment_id:
        ID of an attachment.

    """

    describe_response = support.describe_attachment(
        attachmentId=attachment_id,
    )

    assert describe_response['attachment'] is not None, \
        'Unable to describe attachment'


@skip_because_metrics
def test_resolve_case(support):
    """Attempt to resolve open AWS Support cases

    :testid: support-f-5-1

    :param Support.Client api:
        A handle to the AWS Support APIs.

    """

    describe_response = support.describe_cases()

    for case in describe_response['cases']:
        resolve_response = support.resolve_case(
            caseId=case['caseId'],
        )

        assert resolve_response['finalCaseStatus'] == 'resolved', \
            'Unable to resolve case'
