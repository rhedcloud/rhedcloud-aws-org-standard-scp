"""
========
test_emr
========

Verify that customers can create and use Elastic Map Reduce Clusters.

Plan:

* Create a new EMR_DefaultRole
* Create a new EMR_EC2_DefaultRole
* Create an EMR Cluster in a private subnet
* Attempt to create an EMR Cluster in a management subnet

${testcount:2}

"""

from contextlib import ExitStack

import pytest

from aws_test_functions.emr import new_emr_cluster
from aws_test_functions.env import RHEDCLOUD_PRIVATE_SUBNET, RHEDCLOUD_MGMT_SUBNET
from aws_test_functions import (
    ClientError,
    get_subnet_id,
    new_instance_profile,
    new_role,
)


@pytest.fixture(scope='module')
def emr(session):
    """A shared handle to EMR APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the EMR APIs.

    """

    return session.client('emr')


@pytest.fixture(scope='module')
def iam(session):
    """A shared handle to IAM Service Resource APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the IAM Service Resource APIs.

    """

    return session.resource('iam')


@pytest.fixture(scope='module')
def emr_default_role(iam):
    """Create a new IAM Role for EMR. This resource is automatically cleaned up
    after the tests finish, regardless of outcome.

    :param IAM.ServiceResource iam:
        A handle to the IAM Service Resource APIs.

    :yields:
        An IAM Role resource.

    """

    arn = 'arn:aws:iam::aws:policy/service-role/AmazonElasticMapReduceRole'
    with new_role('test_emr', 'elasticmapreduce', arn, iam=iam) as r:
        yield r


@pytest.fixture(scope='module')
def emr_ec2_default_role(emr_default_role):
    """Create a new IAM Instance Profile for use in the EMR cluster. This
    resource is automatically cleaned up after the tests finish, regardless of
    outcome.

    :param IAM.Role emr_default_role:
        An IAM Role resource.

    :yields:
        An IAM InstanceProfile resource.

    """

    with new_instance_profile('test_emr', role=emr_default_role) as p:
        yield p


@pytest.fixture(scope='module')
def envs(vpc_id):
    """Setup parameters for each environment that must be tested.

    :param str vpc_id:
        ID of a VPC.

    :returns:
        A dictionary.

    """

    return {
        'private': {
            'name': 'test_emr_private',
            'subnet_id': get_subnet_id(vpc_id, RHEDCLOUD_PRIVATE_SUBNET),
            'expected_outcome': True,
        },
        'mgmt': {
            'name': 'test_emr_mgmt',
            'subnet_id': get_subnet_id(vpc_id, RHEDCLOUD_MGMT_SUBNET),
            'expected_outcome': False,
        },
    }


# @pytest.fixture(scope='module', params=['private', 'mgmt'])
@pytest.fixture(scope='module', params=['private'])
def env(envs, request):
    """Return parameters for the specific environment to be tested.

    :param dict envs:
        A mapping of environments to be tested and their respective test
        parameters.

    :returns:
        A dictionary.

    """

    return envs[request.param]


def test_create_cluster(emr, emr_default_role, emr_ec2_default_role, env):
    """A user should be able to create EMR Clusters in private subnets, but not
    management subnets.

    :testid: emr-f-1-2, emr-f-1-4

    :param EMR.Client emr:
        A handle to the EMR APIs.
    :param str name:
        Name of the cluster to create.
    :param IAM.Role role:
        IAM Role to use in the cluster.
    :param IAM.InstanceProfile profile:
        IAM Instance Profile to use in the cluster.
    :param str subnet_id:
        ID of the subnet where the cluster should reside.

    """

    with ExitStack() as stack:
        params = (
            env['name'],
            emr_default_role,
            emr_ec2_default_role,
            env['subnet_id'],
        )

        if not env['expected_outcome']:
            stack.enter_context(pytest.raises(ClientError, match='UnauthorizedOperation'))

        found = False
        job_flow_id = stack.enter_context(new_emr_cluster(*params, emr=emr))
        res = emr.list_clusters()
        assert 'Clusters' in res, 'Unexpected response: {}'.format(res)

        for cluster in res['Clusters']:
            print('Found cluster: {}'.format(cluster['Name']))
            if cluster['Id'] == job_flow_id:
                found = True

        assert found, 'New cluster not found: {}'.format(job_flow_id)
