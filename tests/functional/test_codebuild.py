"""
==============
test_codebuild
==============

Verify users cannot utilize AWS CodeBuild functionality. Although this is
a fully blocked service, aws_blocked_service.py functionality is  not utilized
for testing - A user permission caused issue even within the setup organization
and further testing in the setup organization is not currently being pursued.

${testcount:4}

* Create a new test user with the RHEDcloudAdministratorRole.
* Attempt to create a project
* Attempt to start a project build
* Attempt to check the status of a project build
* Attempt to delete a project

"""

import pytest

from aws_test_functions import (
    ignore_errors,
    new_bucket,
    new_policy,
    new_role,
    unexpected,
)

ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden|ResourceNotFound).*'


@pytest.fixture(scope='module')
def codebuild(session):
    """A shared handle to CodeBuild APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the CodeBuild APIs.

    """

    return session.client('codebuild')


@pytest.fixture(scope='module')
def input_bucket(session):
    """A handle to an S3 Bucket.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User

    :returns:
        A boto3.S3.Bucket owned by the given user

    """

    s3 = session.resource('s3')
    with new_bucket('codebuild-input-bucket', s3=s3) as b:
        b.upload_file('./tests/resources/MessageUtil.zip', 'MessageUtil.zip', ExtraArgs={'ServerSideEncryption': 'AES256'})
        yield b


@pytest.fixture(scope='module')
def output_bucket(session):
    """A handle to an S3 Bucket.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User with the myjob.sh batch script uploaded.

    :returns:
        A boto3.S3.Bucket owned by the given user

    """

    s3 = session.resource('s3')
    with new_bucket('codebuild-output-bucket', s3=s3) as b:
        yield b


@pytest.fixture(scope='module')
def build_service_role():
    """Create a new role for CodeBuild.

    :returns:
        An IAM.Role.

    """

    stmt = [{
        "Sid": "CloudWatchLogsPolicy",
        "Effect": "Allow",
        "Action": [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
        ],
        "Resource": [
            "*"
        ]
    }, {
        "Sid": "CodeCommitPolicy",
        "Effect": "Allow",
        "Action": [
            "codecommit:GitPull"
        ],
        "Resource": [
            "*"
        ]
    }, {
        "Sid": "S3GetObjectPolicy",
        "Effect": "Allow",
        "Action": [
            "s3:GetObject",
            "s3:GetObjectVersion"
        ],
        "Resource": [
            "*"
        ]
    }, {
        "Sid": "S3PutObjectPolicy",
        "Effect": "Allow",
        "Action": [
            "s3:PutObject"
        ],
        "Resource": [
            "*"
        ]
    }]

    with new_policy('code_build_service', stmt) as p:
        with new_role('code_build_service', 'codebuild', p.arn) as r:
            yield r


def test_create_project(codebuild, input_bucket, output_bucket, build_service_role, stack):
    """A customer should be NOT able to create a CodeBuild project

    :testid: codebuild-f-1-1

    :param CodeBuild.Client codebuild:
        A handle to the CodeBuild APIs.
    :param S3.Bucket input_bucket:
        S3 bucket containing input data.
    :param S3.Bucket output_bucket:
        S3 bucket for output data.
    :param boto3.IAM.Role build_service_role:
        A Role permitted to run CodeBuild tasks.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodeBuild tests.

    """

    project_path = input_bucket.name + '/MessageUtil.zip'
    res = codebuild.create_project(
        name='TestCodeBuildProject',
        source={
            'type': 'S3',
            'location': project_path,
        },
        artifacts={
            'type': 'S3',
            'location': output_bucket.name,
        },
        environment={
            'type': 'LINUX_CONTAINER',
            'image': 'aws/codebuild/java:openjdk-8',
            'computeType': 'BUILD_GENERAL1_SMALL',
        },
        serviceRole=build_service_role.arn,
    )
    assert 'project' in res and 'arn' in res['project'], unexpected(res)
    print('Created project: {}'.format(res['project']['arn']))

    stack.callback(
        ignore_errors(test_delete_project),
        codebuild,
    )


def test_build_project(codebuild):
    """A customer should be NOT able to start a build of a CodeBuild project

    :testid: codebuild-f-1-2

    :param CodeBuild.Client codebuild:
        A handle to the CodeBuild APIs.

    """

    codebuild.start_build(projectName='TestCodeBuildProject')


def test_status_check(codebuild):
    """A customer should be NOT able to check the status of a CodeBuild project

    :testid: codebuild-f-1-3

    :param CodeBuild.Client codebuild:
        A handle to the CodeBuild APIs.

    """

    build_id_list = codebuild.list_builds()['ids']
    build_id = None
    if len(build_id_list):
        build_id = build_id_list[0]
        codebuild.batch_get_builds(ids=[build_id])


def test_stop_build(codebuild):
    """A customer should be NOT able to stop the build of a CodeBuild project

    :testid: codebuild-f-1-4

    :param CodeBuild.Client codebuild:
        A handle to the CodeBuild APIs.

    """

    build_id_list = codebuild.list_builds()['ids']
    build_id = None
    if len(build_id_list):
        build_id = build_id_list[0]
        codebuild.stop_build(id=build_id)


def test_delete_project(codebuild):
    """A customer should be NOT able to delete a CodeBuild project

    :testid: codebuild-f-1-5

    :param CodeBuild.Client codebuild:
        A handle to the CodeBuild APIs.

    """

    codebuild.delete_project(name='TestCodeBuildProject')
