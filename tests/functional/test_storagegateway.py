"""
===================
test_storagegateway
===================

Verify the ability to to use Amazon Storage Gateway

Plan:

* Create a new AWS Storage Gateway EC2 instance in the default VPC while in the
  RHEDcloudAccountAdministrationOrg org unit
* Attempt to activate a File Gateway Storage Gateway while in the
  RHEDcloudAccountAdministrationOrg org unit, which should work
* Attempt to activate a File Gateway Storage Gateway while in the
  RHEDcloudResearchAccounts org unit, which should NOT work

${testcount:2}

"""

from urllib.parse import parse_qs, urlparse
import time

import pytest

from aws_test_functions import (
    get_uuid,
    new_ec2_instance,
    new_security_group,
    retry,
)

#This service is fully blocked
pytestmark = [pytest.mark.scp_check('list_gateways')]

# AWS Storage Gateway AMI ID
# See the table of AMI IDs for storage gateways in each supported region:
# https://docs.aws.amazon.com/storagegateway/latest/userguide/ec2-gateway-file.html
AMI_ID = 'ami-0c38bb1899ad0b177'


@pytest.fixture(scope='module')
def storagegateway(session):
    """A shared handle to Storage Gateway APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Storage Gateway APIs.

    """

    return session.client('storagegateway')


@pytest.fixture(scope='module')
def security_group(session, external_runner):
    """Allow inbound traffic on TCP port 80.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param CommandRunner external_runner:
        A handle to the script runner in the same subnet as the Storage
        Gateway.

    :yields:
        An EC2 SecurityGroup resource.

    """

    ec2 = session.client('ec2')
    vpc_id = external_runner.instance.vpc_id

    with new_security_group(vpc_id, 'test_storagegateway', ec2=ec2) as sg:
        ec2.authorize_security_group_ingress(
            GroupId=sg.group_id,
            IpProtocol='tcp',
            FromPort=80,
            ToPort=80,
            CidrIp='0.0.0.0/0',
        )

        yield sg


@pytest.fixture(scope='module')
def instance(session, external_runner, security_group):
    """Create a new AWS Storage Gateway EC2 instance.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param CommandRunner external_runner:
        A handle to the script runner in the same subnet as the Storage
        Gateway.
    :param EC2.SecurityGroup security_group:
        The security group resource to apply to the created instance.

    :yields:
        An EC2 Instance resource.

    """

    params = dict(
        # the smallest, cheapest size usable with the AWS Storage Gateway AMI
        size='m4.xlarge',
        ami=AMI_ID,
        SubnetId=external_runner.instance.subnet_id,
        SecurityGroupIds=[
            security_group.group_id,
        ],
    )

    with new_ec2_instance('test_storagegateway', session=session, **params) as inst:
        print('Waiting for Storage Gateway EC2 Instance {} to be ready...'.format(inst.instance_id))
        inst.wait_until_running()

        # wait a bit longer for the appliance software to be fully accessible
        time.sleep(90)

        yield inst


def get_activation_key(runner, instance):
    """Return the activation key for the Storage Gateway appliance with the
    specified IP address.

    :param CommandRunner runner:
        A handle to the script runner in the same subnet as the Storage
        Gateway.
    :param EC2.Instance instance:
        An EC2 Instance resource for the Storage Gateway instance to query.

    :returns:
        A string.

    """

    # make sure we have the latest information about the instance
    instance.reload()
    ip_addr = instance.public_ip_address

    output = runner('curl -vSsfIm 10 http://{}/?activationRegion=us-east-1'.format(ip_addr))
    for line in output.splitlines():
        if not line.startswith('Location:'):
            continue

        # extract the activation key from the redirect URL provided by the
        # gateway
        url = urlparse(line.split(' ')[1])
        qs = parse_qs(url.query)
        print('Storage Gateway activation info: {}'.format(qs))

        return qs['activationKey'][0]

    assert False, 'Failed to find activation key: {}'.format(output)


@pytest.fixture(scope='module')
def activation_key(external_runner, instance):
    """Return the activation key for the Storage Gateway appliance with the
    specified IP address.

    :param CommandRunner runner:
        A handle to the script runner in the same subnet as the Storage
        Gateway.
    :param EC2.Instance instance:
        An EC2 Instance resource for the Storage Gateway instance to query.

    :returns:
        A string.

    """

    return retry(
        get_activation_key,
        args=(external_runner, instance),
        msg='Getting activation key from {}'.format(instance.instance_id),
        delay=10,
    )


@pytest.mark.slowtest
def test_activation(storagegateway, instance, activation_key):
    """Verify ability to activate a File Gateway.

    As a fully blocked service, this test should raise an AccessDenied error
    when executed in the RHEDcloudResearchAccounts org unit.

    :testid: storagegateway-f-1-1

    :param StorageGateway.Client storagegateway:
        A handle to the Storage Gateway APIs.
    :param EC2.Instance instance:
        An EC2 Instance resource for the Storage Gateway instance to query.
    :param str activation_key:
        The key required to activate the specified Storage Gateway instance.

    """

    gateway_name = 'test_storagegateway-{}'.format(get_uuid())
    params = dict(
        ActivationKey=activation_key,
        GatewayName=gateway_name,
        GatewayTimezone='GMT-5:00',
        GatewayRegion='us-east-1',
        GatewayType='FILE_S3',
    )

    print('Activating Gateway: {}'.format(params))
    res = storagegateway.activate_gateway(**params)
    assert 'GatewayARN' in res, 'Unexpected response: {}'.format(res)
    arn = res['GatewayARN']

    # if we make it this far, we need to clean up after ourselves
    print('Deleting Gateway: {}'.format(arn))
    storagegateway.delete_gateway(
        GatewayARN=arn,
    )
