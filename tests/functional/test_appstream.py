"""
==============
test_appstream
==============

Verify access to use AWS AppStream

Plan:

* Attempt to create, describe, update, start, stop, and delete fleets
* Attempt to associate and disassociate fleets and stacks
* Attempt to create, describe, update, and delete stacks
* Attempt to create, describe, start, stop, and delete image builders
* Attempt to describe, copy, and delete images

${testcount:22}

"""

import os

import pytest

from aws_test_functions import (
    get_or_create_role,
    has_status,
    ignore_errors,
    in_setup_org,
    make_identifier,
    unexpected,
)
from aws_test_functions.appstream import (
    cleanup_fleets,
    cleanup_image_builders,
    cleanup_stacks,
    delete_fleet,
    delete_image,
    delete_image_builder,
    delete_stack,
    stop_image_builder,
    _wait_for_state,
)

pytestmark = [
    pytest.mark.slowtest,

    # This service is blocked in the HIPAA repository
    pytest.mark.scp_check('describe_stacks'),
]

# Create a reusable functor to move the AWS account to the SETUP_ORG before
# attempting to invoke the requested function. This is necessary to ensure that
# we make the best effort to remove resources created during testing (since we
# should not be able to delete with the SCP in effect in the TEST_ORG).
cleaner = in_setup_org('AppStream cleanup', one_way=True)


# A lot of the steps in these tests require several minutes to complete. This
# can be a real bother when trying to iterate quickly, so there are some
# settings here that we can adjust depending on what kind of mood we're in.
MAX_ATTEMPTS = 30
RETRY_DELAY = 120
FASTER_ITERATIONS = bool(os.getenv('FASTER_ITERATIONS'))
if FASTER_ITERATIONS:
    # we don't want to wait around for a while, so adjust behavior
    pytestmark.remove(pytest.mark.slowtest)
    MAX_ATTEMPTS = 2
    RETRY_DELAY = 30


@pytest.fixture(scope='module')
def appstream(session):
    """A shared handle to AppStream API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the AppStream API.

    """

    return session.client('appstream')


@pytest.fixture(scope='module', autouse=True)
def role(session):
    """Get or create a role for AppStream to do magic.

    This Role does not get removed automatically because it's required for
    AppStream to work when tests are not running.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        An IAM Role resource.

    """

    iam = session.resource('iam')

    return get_or_create_role(
        'AmazonAppStreamServiceAccess',
        'arn:aws:iam::aws:policy/service-role/AmazonAppStreamServiceAccess',
        'AdministratorAccess',
        service='appstream',
        iam=iam,
    )


@pytest.fixture(scope='module', autouse=True)
def cleanup(appstream, role):
    """Attempt to cleanup any unused resources created by prior tests.

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param IAM.Role role:
        A dependency on the fixture that makes sure we have a Service Role to
        work with for AppStream.

    """

    print('Cleaning up previous tests...')
    cleanup_stacks(appstream=appstream)
    cleanup_image_builders(appstream=appstream)
    cleanup_fleets(appstream=appstream)

    yield


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'image': 'Base-Image-Builder-05-02-2018',
        'instance_type': 'stream.standard.medium',

        'fleet_name': make_identifier('test-fleet'),
        'builder_name': make_identifier('test-builder'),
        'copied_image_name': make_identifier('test-copy'),
        'stack_name': make_identifier('test-stack'),
    }


def test_create_fleet(appstream, fleet_name, image, instance_type, stack):
    """Verify access to create fleets.

    :testid: appstream-f-1-1

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str fleet_name:
        Name for the fleet.
    :param str image:
        Name of the image to use for the fleet.
    :param str instance_type:
        Type of instance to use for the fleet.
    :param contextlib.ExitStack stack:
        A context manager shared across all AppStream tests.

    """

    res = appstream.create_fleet(
        Name=fleet_name,
        ImageName=image,
        InstanceType=instance_type,
        ComputeCapacity=dict(
            DesiredInstances=1,
        ),
    )
    assert 'Fleet' in res, unexpected(res)
    print('Created fleet: {}'.format(fleet_name))

    stack.callback(
        cleaner(ignore_errors(delete_fleet)),
        fleet_name,
        appstream=appstream,
    )


def test_describe_fleets(appstream, stack):
    """Verify access to describe fleets.

    :testid: appstream-f-1-2

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param contextlib.ExitStack stack:
        A context manager shared across all AppStream tests.

    """

    res = appstream.describe_fleets()
    assert 'Fleets' in res, unexpected(res)


def test_update_fleet(appstream, fleet_name):
    """Verify access to update fleets.

    :testid: appstream-f-1-3

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str fleet_name:
        Name of the fleet to update.

    """

    res = appstream.update_fleet(
        Name=fleet_name,
        ComputeCapacity=dict(
            DesiredInstances=2,
        ),
    )
    assert 'Fleet' in res, unexpected(res)
    assert res['Fleet']['ComputeCapacityStatus']['Desired'] == 2, 'Unexpected capacity: {}'.format(res)


def test_create_stack(appstream, stack_name, stack):
    """Verify access to create stacks.

    :testid: appstream-f-2-1

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str stack_name:
        Name of the stack.
    :param contextlib.ExitStack stack:
        A context manager shared across all AppStream tests.

    """

    res = appstream.create_stack(
        Name=stack_name,
    )
    assert 'Stack' in res, unexpected(res)
    print('Created stack: {}'.format(stack_name))

    stack.callback(
        cleaner(ignore_errors(delete_stack)),
        stack_name,
        appstream=appstream,
    )


def test_describe_stacks(appstream, stack):
    """Verify access to describe stacks.

    :testid: appstream-f-2-2

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param contextlib.ExitStack stack:
        A context manager shared across all AppStream tests.

    """

    res = appstream.describe_stacks()
    assert 'Stacks' in res, unexpected(res)


def test_update_stack(appstream, stack_name):
    """Verify access to update stacks.

    :testid: appstream-f-2-3

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str stack_name:
        Name of the stack to update.

    """

    new_descr = 'test test test'
    res = appstream.update_stack(
        Name=stack_name,
        Description=new_descr,
    )
    assert 'Stack' in res, unexpected(res)
    assert res['Stack']['Description'] == new_descr, 'Description did not change: {}'.format(res)


def test_associate_fleet(appstream, fleet_name, stack_name):
    """Verify access to associate fleets with stacks.

    :testid: appstream-f-1-4

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str fleet_name:
        Name of a fleet.
    :param str stack_name:
        Name of a stack.

    """

    res = appstream.associate_fleet(
        FleetName=fleet_name,
        StackName=stack_name,
    )
    assert has_status(res, 200), unexpected(res)


def test_list_associated_stacks(appstream, fleet_name):
    """Verify access to list stacks that are associated with a fleet.

    :testid: appstream-f-1-5

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str fleet_name:
        Name of a fleet.

    """

    res = appstream.list_associated_stacks(
        FleetName=fleet_name,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'Names' in res and len(res['Names']) == 1, unexpected(res)


def test_list_associated_fleets(appstream, stack_name):
    """Verify access to list fleets that are associated with a stack.

    :testid: appstream-f-2-4

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str stack_name:
        Name of a stack.

    """

    res = appstream.list_associated_fleets(
        StackName=stack_name,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'Names' in res and len(res['Names']) == 1, unexpected(res)


def test_start_fleet(appstream, fleet_name, stack):
    """Verify access to start a fleet.

    :testid: appstream-f-1-6

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str fleet_name:
        Name of a fleet.
    :param str stack_name:
        Name of a stack.

    """

    res = appstream.start_fleet(
        Name=fleet_name,
    )
    assert has_status(res, 200), unexpected(res)

    stack.callback(
        cleaner(ignore_errors(appstream.stop_fleet)),
        Name=fleet_name,
    )


def test_stop_fleet(appstream, fleet_name):
    """Verify access to stop a fleet.

    :testid: appstream-f-1-7

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str fleet_name:
        Name of a fleet.

    """

    res = appstream.stop_fleet(
        Name=fleet_name,
    )
    assert has_status(res, 200), unexpected(res)

    res = _wait_for_state(
        appstream.describe_fleets, fleet_name,
        'Fleets', 'STOPPED',
        delay=RETRY_DELAY,
        max_attempts=MAX_ATTEMPTS,
    )
    assert has_status(res, 200), unexpected(res)


def test_disassociate_fleet(appstream, fleet_name, stack_name):
    """Verify access to stop a fleet.

    :testid: appstream-f-1-8

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str fleet_name:
        Name of a fleet.
    :param str stack_name:
        Name of a stack.

    """

    res = appstream.disassociate_fleet(
        FleetName=fleet_name,
        StackName=stack_name,
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_stack(appstream, stack_name):
    """Verify access to delete stacks.

    :testid: appstream-f-2-5

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str stack_name:
        Name of the stack to delete.

    """

    delete_stack(stack_name, appstream=appstream)


def test_delete_fleet(appstream, fleet_name):
    """Verify access to delete fleets.

    :testid: appstream-f-1-9

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str fleet_name:
        Name of the fleet to delete.

    """

    delete_fleet(fleet_name, appstream=appstream)


def test_create_image_builder(appstream, builder_name, image, instance_type, stack):
    """Verify access to create image builders.

    :testid: appstream-f-3-1

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str builder_name:
        Name of a builder.
    :param str image:
        Name of the image to use for the image builder.
    :param str instance_type:
        Type of instance to use for the image builder.
    :param contextlib.ExitStack stack:
        A context manager shared across all AppStream tests.

    """

    res = appstream.create_image_builder(
        Name=builder_name,
        ImageName=image,
        InstanceType=instance_type,
    )
    assert 'ImageBuilder' in res, unexpected(res)
    print('Created image builder: {}'.format(builder_name))

    stack.callback(
        cleaner(ignore_errors(appstream.delete_image_builder)),
        Name=builder_name,
    )


def test_describe_image_builders(appstream):
    """Verify access to describe image builders.

    :testid: appstream-f-3-2

    :param AppStream.Client appstream:
        A handle to the AppStream API.

    """

    res = appstream.describe_image_builders()
    assert has_status(res, 200), unexpected(res)
    assert 'ImageBuilders' in res and len(res['ImageBuilders']), unexpected(res)


def test_start_image_builder(appstream, builder_name, stack):
    """Verify access to start image builders.

    :testid: appstream-f-3-3

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str builder_name:
        Name of a builder.
    :param str stack_name:
        Name of a stack.

    """

    res = appstream.start_image_builder(
        Name=builder_name,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'ImageBuilder' in res, unexpected(res)

    res = _wait_for_state(
        appstream.describe_image_builders, builder_name,
        'ImageBuilders', 'RUNNING',
        delay=RETRY_DELAY,
        max_attempts=MAX_ATTEMPTS,
    )
    assert has_status(res, 200), unexpected(res)

    stack.callback(
        cleaner(ignore_errors(appstream.stop_image_builder)),
        Name=builder_name,
    )


def test_stop_image_builder(appstream, builder_name):
    """Verify access to stop image builders.

    :testid: appstream-f-3-4

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str builder_name:
        Name of a builder.

    """

    res = stop_image_builder(builder_name, appstream=appstream)
    assert has_status(res, 200), unexpected(res)
    assert 'ImageBuilders' in res, unexpected(res)


def test_delete_image_builder(appstream, builder_name):
    """Verify access to delete image builders.

    :testid: appstream-f-3-5

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str builder_name:
        Name of a builder.

    """

    res = delete_image_builder(builder_name, max_attempts=MAX_ATTEMPTS, appstream=appstream)
    assert has_status(res, 200), unexpected(res)
    assert 'ImageBuilder' in res, unexpected(res)


def test_describe_images(appstream, stack):
    """Verify access to describe images.

    :testid: appstream-f-4-1

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param contextlib.ExitStack stack:
        A context manager shared across all AppStream tests.

    """

    res = appstream.describe_images()
    assert 'Images' in res, unexpected(res)


@pytest.mark.skip(reason='Successfully creating images eludes me')
def test_copy_image(appstream, copied_image_name, image, stack):
    """Verify access to create images.

    .. note::

        A more correct approach to create an image would probably be to use the
        created image builder to produce a new custom image. I haven't been
        able to get that working though. As a short-term approach, I tried
        copying an existing image, but that didn't work either.

    :testid: appstream-f-4-2

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str copied_image_name:
        Name to give to the copied image.
    :param str image:
        Name of the image to copy.
    :param contextlib.ExitStack stack:
        A context manager shared across all AppStream tests.

    """

    res = appstream.copy_image(
        SourceImageName=image,
        DestinationImageName=copied_image_name,
        DestinationRegion='us-east-1',
    )
    assert 'DestinationImageName' in res, unexpected(res)
    print('Copied image {} to {}'.format(image, copied_image_name))

    stack.callback(
        cleaner(ignore_errors(delete_image)),
        copied_image_name,
        appstream=appstream,
    )


@pytest.mark.skip(reason='Successfully creating images eludes me')
def test_delete_image(appstream, copied_image_name):
    """Verify access to delete images.

    :testid: appstream-f-4-3

    :param AppStream.Client appstream:
        A handle to the AppStream API.
    :param str copied_image_name:
        Name of the image to delete.

    """

    res = delete_image(copied_image_name, appstream=appstream)
    assert 'Image' in res, unexpected(res)


# expect some tests to fail when we're iterating faster
if FASTER_ITERATIONS:
    xfail_slow = pytest.mark.xfail(reason='Takes a long time')

    test_stop_fleet = xfail_slow(test_stop_fleet)
    test_delete_fleet = xfail_slow(test_delete_fleet)
    test_start_image_builder = xfail_slow(test_start_image_builder)
    test_stop_image_builder = xfail_slow(test_stop_image_builder)
    test_delete_image_builder = xfail_slow(test_delete_image_builder)
