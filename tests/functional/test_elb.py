"""
========
test_elb
========

Verify the ability to use AWS Elastic Load Balancers.

Plan:

* Create two web servers, one in each Public subnet
* Edit security groups to allow HTTP traffic from anywhere
* Create an Internet-facing load balancer with the target being the two web servers.
* Attempt to connect to the provided DNS name from outside the VPC. This should fail.
* Attempt to connect to the provided DNS name from within the VPC. This should succeed.
* Stop one web server, then verify that the load balancer still has one healthy target.
* Stop the second web server, then verify that the load balancer has no healthy targets.
* Start one web server, wait for it to be ready, then verify that the load balancer has one
  healthy target again.

The above steps should be used for each of the following load balancer types:

* network load balancer
* application load balancer

${testcount:7}

"""

from contextlib import ExitStack
import time

import pytest

from aws_test_functions import (
    create_health_checker,
    env,
    get_default_security_group,
    get_subnet_id,
    in_test_org,
    move_to_setup_org,
    new_ec2_instance,
    new_elb,
    new_internet_gateway,
    new_security_group,
    wait_for_elb_state,
)


# see https://aws.amazon.com/marketplace/fulfillment?productId=f5774628-e459-457a-b058-3b513caefdee&ref_=dtl_psb_continue
NGINX_AMI = 'ami-0f894e5eb0982a455'
ELB_TYPES = ('application', 'network', 'classic')

SCRIPT = r"""#!/bin/bash

curl -SsfIm 10 http://{elb_dns_name}/
"""


@pytest.fixture(scope='module')
def ec2(session):
    return session.client('ec2')


@pytest.fixture(scope='module')
def subnets(ec2, vpc_id):
    return [
        get_subnet_id(vpc_id, subnet, ec2=ec2)
        for subnet in env.RHEDCLOUD_DEFAULT_VPC_ENDPOINT_SUBNETS
    ]


@pytest.fixture(scope='module')
def igw(vpc_id):
    """Create a temporary Internet Gateway for the classic ELBs"""

    move_to_setup_org('IGW setup')
    with new_internet_gateway(vpc_id):
        with in_test_org():
            yield


@pytest.fixture(scope='module')
def security_group(ec2, vpc_id):
    """Create a new security group that allows HTTP traffic from anywhere."""

    with new_security_group(vpc_id, 'test_elb', ec2=ec2) as sg:
        ec2.authorize_security_group_ingress(
            GroupId=sg.group_id,
            IpProtocol='tcp',
            FromPort=80,
            ToPort=80,
            CidrIp='0.0.0.0/0',
        )

        yield sg


@pytest.fixture(scope='module')
def default_sg(ec2, vpc_id):
    return get_default_security_group(vpc_id, ec2=ec2)['GroupId']


@pytest.fixture(scope='module')
def servers(session, default_sg, security_group, vpc_id, subnets):
    """Create an EC2 instance running a webserver in each subnet."""

    with ExitStack() as stack:
        servers = []
        for idx, subnet in enumerate(subnets):
            servers.append(stack.enter_context(
                new_ec2_instance(
                    'test_elb_{}'.format(idx),
                    ami=NGINX_AMI,
                    session=session,
                    SubnetId=subnet,
                    SecurityGroupIds=[
                        default_sg,
                        security_group.group_id,
                    ],
                )
            ))

        # wait for instances to be in state "running"
        while not all(s.state['Name'] == 'running' for s in servers):
            print('waiting for all servers to be in "running" state...')
            time.sleep(5)
            for s in servers:
                s.reload()

        yield servers


@pytest.fixture(scope='class')
def load_balancers(session, servers, default_sg, security_group, igw, script_runner_instance_profile):
    """Create all load balancers up front.

    This helps reduce total test time because each load balancer requires
    approximately the same amount of time to finish provisioning. When all load
    balancers are created at the same time, generally only the first test takes
    significantly longer than the others, because it must wait for the load
    balancer to be active.

    """

    elb = session.client('elb')

    with ExitStack() as stack:
        elbs = dict()
        for scheme in ('internet-facing', 'internal'):
            for elb_type in ELB_TYPES:
                key = '{}-{}'.format(scheme, elb_type)
                lb = elbs[key] = stack.enter_context(new_elb(elb_type, scheme, servers, session=session))

                if elb_type == 'classic':
                    elb.apply_security_groups_to_load_balancer(
                        LoadBalancerName=lb['LoadBalancerName'],
                        SecurityGroups=[
                            default_sg,
                            security_group.group_id,
                        ],
                    )

        yield elbs


@pytest.fixture(scope='module')
def validate_elb(session, setup_runners):
    """Validate ELB connectivity by attempting to connect to it either from
    within or outside of the Type 1 VPC.

    """

    def wrapped(mode, lb, **kwargs):
        # the ELB must be active before it makes sense to create the EC2
        # instance to hit the ELB's DNS name
        wait_for_elb_state(lb, 'active', session=session)

        script = SCRIPT.format(elb_dns_name=lb['DNSName'])

        return setup_runners[mode](script)

    return wrapped


@pytest.mark.slowtest
class TestELB:

    def test_internet_facing_application_elb(self, load_balancers, validate_elb):
        """VMs should not be accessible via the Internet using an application ELB.

        :testid: elb-f-1-1

        """

        lb = load_balancers['internet-facing-application']
        output = validate_elb('external', lb)
        assert 'Connection timed out' in output, 'Unexpected output: {}'.format(output)

    def test_internal_application_elb(self, load_balancers, validate_elb, subnets):
        """VMs should be accessible from within the VPC using an application ELB.

        :testid: elb-f-1-2

        """

        lb = load_balancers['internal-application']
        output = validate_elb('internal', lb)
        assert output.startswith('HTTP/1.1 200 OK'), 'Unexpected output: {}'.format(output)

    def test_internet_facing_network_elb(self, load_balancers, validate_elb):
        """VMs should not be accessible via the Internet using a network ELB.

        :testid: elb-f-2-1

        """

        lb = load_balancers['internet-facing-network']
        output = validate_elb('external', lb)
        assert 'Connection timed out' in output, 'Unexpected output: {}'.format(output)

    def test_internal_network_elb(self, load_balancers, validate_elb, subnets):
        """VMs should be accessible from within the VPC using a network ELB.

        :testid: elb-f-2-2

        """

        lb = load_balancers['internal-network']
        output = validate_elb('internal', lb)
        assert output.startswith('HTTP/1.1 200 OK'), 'Unexpected output: {}'.format(output)

    def test_internet_facing_classic_elb(self, load_balancers, validate_elb):
        """VMs should not be accessible via the Internet using a classic ELB.

        :testid: elb-f-3-1

        """

        lb = load_balancers['internet-facing-classic']
        output = validate_elb('external', lb)
        assert 'Connection timed out' in output, 'Unexpected output: {}'.format(output)

    def test_internal_classic_elb(self, load_balancers, validate_elb, subnets):
        """VMs should be accessible from within the VPC using a classic ELB.

        :testid: elb-f-3-2

        """

        lb = load_balancers['internal-classic']
        output = validate_elb('internal', lb)
        assert output.startswith('HTTP/1.1 200 OK'), 'Unexpected output: {}'.format(output)

    def test_elb_functionality(self, session, load_balancers, servers):
        """An application ELB should still appear healthy when one target is lost.

        Normally I would use ``@pytest.mark.parametrize`` to handle each ELB type individually,
        but this test requires so much time that it makes more sense to check the state of both
        ELB types at the same time.

        :testid: elb-f-1-3, elb-f-2-3

        """

        elb_types = ('application', 'network')
        elbv2 = session.client('elbv2')
        checkers = {}
        check_delay = 0
        for elb_type in elb_types:
            lb = load_balancers['internal-{}'.format(elb_type)]
            wait_for_elb_state(lb, 'active', session=session)

            tg_res = elbv2.describe_target_groups(LoadBalancerArn=lb['LoadBalancerArn'])
            assert 'TargetGroups' in tg_res, 'Unexpected response: {}'.format(tg_res)
            tg = tg_res['TargetGroups'][0]
            check_delay = max(
                check_delay,
                tg['HealthCheckIntervalSeconds'] * tg['UnhealthyThresholdCount'] + 5
            )

            checkers[elb_type] = create_health_checker(tg, servers)

        # make sure both servers are healthy before we proceed
        for attempt in range(12):
            print('Waiting for both servers to be healthy... (attempt #{})'.format(attempt + 1))
            retry = False
            for elb_type in elb_types:
                try:
                    checkers[elb_type]('healthy', 'healthy')
                except AssertionError:
                    retry = True
                    time.sleep(5)
                    break

            if not retry:
                break

        def check_health(*expected, multiplier=1):
            """Closure to reduce duplication.

            :param int multiplier: (optional)
                When starting an EC2 instance up again, initial health checks require more
                time. The multiplier can be used to extend the delay before checking instance
                health.

            """

            print('Waiting for health checks...')
            time.sleep(check_delay * multiplier)
            for elb_type in elb_types:
                checkers[elb_type](*expected)

        print('Stopping server 1')
        servers[0].stop()
        servers[0].wait_until_stopped()
        check_health('unused', 'healthy')

        print('Stopping server 2')
        servers[1].stop()
        servers[1].wait_until_stopped()
        check_health('unused', 'unused')

        print('Starting server 1')
        servers[0].start()
        servers[0].wait_until_running()
        check_health('healthy', 'unused', multiplier=3)

        print('Starting server 2')
        servers[1].start()
        servers[1].wait_until_running()
        check_health('healthy', 'healthy', multiplier=3)
