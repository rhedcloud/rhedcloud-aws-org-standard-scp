"""
==============
test_mobilehub
==============

Verify access to AWS Mobile Hub

Plan:

* Attempt to create a project
* Attempt to list projects
* Attempt to describe a project
* Attempt to export a project
* Attempt to update a project
* Attempt to delete a project

${testcount:6}

"""

import io

import pytest

from aws_test_functions import (
    aws_client,
    catch,
    get_or_create_role,
    get_uri_contents,
    make_identifier,
    retry,
)

pytestmark = [
    pytest.mark.scp_check(
        'list_projects',
        client_fixture='mobile',
        needle='UnauthorizedException',
    ),
]


@pytest.fixture(scope='module')
def mobile(session):
    """A shared handle to Mobile API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Mobile API.

    """

    return session.client('mobile')


@pytest.fixture(scope='module')
def role(session):
    """Get or create a role for Mobile Hub to do magic.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :yields:
        An IAM Role resource.

    """

    iam = session.resource('iam')

    return get_or_create_role(
        'MobileHub_Service_Role',
        'arn:aws:iam::aws:policy/service-role/AWSMobileHub_ServiceUseOnly',
        service='mobilehub',
        iam=iam,
    )


@pytest.fixture(scope='module', autouse=True)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        # ID of a project
        'project_id': None,

        # project configuration from exporting a project
        'project_config': None,
    }


@pytest.fixture(scope='function')
def project_id(shared_vars):
    return shared_vars['project_id'] or '4dd96434-480c-471a-bae7-6eebe0225287'


@pytest.fixture(scope='function')
def project_config(shared_vars):
    return shared_vars['project_config'] or b'{}'


@catch(Exception, msg='Failed to delete project: {ex}')
@aws_client('mobile')
def delete_project(project_id, *, mobile=None):
    """Delete a project.

    :param str project_id:
        The ID of the project to delete.

    """

    print('Deleting project: {}'.format(project_id))
    res = mobile.delete_project(
        projectId=project_id,
    )
    assert 'deletedResources' in res, 'Unexpected response: {}'.format(res)


def test_create_project(mobile, role, stack, shared_vars):
    """Verify access to create a project.

    :testid: mobile-f-1-1

    :param Mobile.Client opsworks:
        A handle to the Mobile Hub API.
    :param IAM.Role role:
        A dependency on the fixture that configures the AWS account to be able
        to use Mobile Hub.
    :param contextlib.ExitStack stack:
        A context manager shared across all OpsWorks tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-mobilehub')

    res = retry(
        mobile.create_project,
        kwargs=dict(
            name=name,
            region='us-east-1',
        ),
        msg='Creating project',
        show=True,
        pred=lambda ex: 'ValidationException' in str(ex)
    )
    assert 'details' in res and 'projectId' in res['details'], \
        'Unexpected response: {}'.format(res)

    project_id = shared_vars['project_id'] = res['details']['projectId']
    print('Created project: {}'.format(project_id))

    stack.callback(
        delete_project,
        project_id,
        mobile=mobile,
    )


def test_list_projects(mobile, project_id):
    """Verify access to list projects.

    :testid: mobile-f-1-2

    :param Mobile.Client opsworks:
        A handle to the Mobile Hub API.
    :param str project_id:
        The ID of a Mobile Hub Project.

    """

    found = False

    print('Listing projects:')
    res = mobile.list_projects()
    assert 'projects' in res, 'Unexpected response: {}'.format(res)
    assert len(res['projects']), 'No projects found: {}'.format(res)

    for project in res['projects']:
        if project['projectId'] == project_id:
            found = True
            break

    assert found, 'Project {} not found!\n{}'.format(project_id, res)


def test_describe_project(mobile, project_id):
    """Verify access to describe a project.

    :testid: mobile-f-1-2

    :param Mobile.Client opsworks:
        A handle to the Mobile Hub API.
    :param str project_id:
        The ID of a Mobile Hub Project.

    """

    print('Describing project: {}'.format(project_id))
    res = mobile.describe_project(
        projectId=project_id,
    )

    assert 'details' in res and 'projectId' in res['details'], \
        'Unexpected response: {}'.format(res)
    assert res['details']['projectId'] == project_id, \
        'Project ID did not match: {}'.format(res)


def test_export_project(mobile, project_id, shared_vars):
    """Verify access to export a project, which is a prerequisite for updating
    a project.

    :testid: mobile-f-1-3

    :param Mobile.Client opsworks:
        A handle to the Mobile Hub API.
    :param str project_id:
        The ID of a Mobile Hub Project.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    print('Exporting project: {}'.format(project_id))
    res = mobile.export_project(
        projectId=project_id,
    )
    assert 'downloadUrl' in res, 'Unexpected response: {}'.format(res)

    shared_vars['project_config'] = get_uri_contents(res['downloadUrl'],
                                                     binary=True)


def test_update_project(mobile, project_id, project_config):
    """Verify access to update a project.

    :testid: mobile-f-1-3

    :param Mobile.Client opsworks:
        A handle to the Mobile Hub API.
    :param str project_id:
        The ID of a Mobile Hub Project.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    print('Updating project: {}'.format(project_id))
    res = mobile.update_project(
        projectId=project_id,
        contents=io.BytesIO(project_config),
    )
    assert 'details' in res and 'projectId' in res['details'], \
        'Unexpected response: {}'.format(res)


def test_delete_project(mobile, project_id):
    """Verify access to delete a project.

    :testid: mobile-f-1-4

    :param Mobile.Client opsworks:
        A handle to the Mobile Hub API.
    :param str project_id:
        The ID of a Mobile Hub Project.

    """

    print('Deleting project: {}'.format(project_id))
    res = mobile.delete_project(
        projectId=project_id,
    )
    assert 'deletedResources' in res, 'Unexpected response: {}'.format(res)
