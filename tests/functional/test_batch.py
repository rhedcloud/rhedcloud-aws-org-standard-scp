"""
==========
test_batch
==========

Verify the ability to create and run batch jobs

Plan:

* Create a new test user with the RHEDcloudAdministratorRole

${testcount:7}

"""

import pytest

from aws_test_functions import (
    aws_client,
    dict_to_filters,
    ignore_errors,
    new_batch_compute_environment,
    new_batch_job_definition,
    new_batch_job_queue,
    new_bucket,
    new_instance_profile,
    new_policy,
    new_role,
    new_security_group,
    retry,
    unexpected,
)

from tests.functional.test_ecr import docker_to_ecr


# this test can take ~5-10 minutes
pytestmark = pytest.mark.slowtest


@pytest.fixture(scope="module")
def ec2(session):
    """A shared handle to EC2 APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the EC2 APIs.

    """

    return session.client("ec2")


@pytest.fixture(scope="module")
def batch(session):
    """A shared handle to AWS Batch APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Batch APIs.

    """

    return session.client("batch")


@pytest.fixture(scope="module")
def ecr(session):
    """A shared handle to Elastic Container Registry APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the ECR APIs.

    """

    return session.client("ecr")


@pytest.fixture(scope="module")
def s3(session):
    """A shared handle to S3 APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the S3 APIs.

    """

    return session.client("s3")


@pytest.fixture(scope="module")
def setup_fixtures():
    """Attempt to cleanup artifacts from prior test runs."""

    cleanup_batch()

    yield


@pytest.fixture(scope="module")
def service_role(stack):
    """Create a new role with necessary permissions for AWS Batch Service.

    :param contextlib.ExitStack stack:
        A context manager shared across all Batch tests.

    :returns:
        An IAM.Role.

    """

    stmt = [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeAccountAttributes",
                "ec2:DescribeInstances",
                "ec2:DescribeSubnets",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeKeyPairs",
                "ec2:DescribeImages",
                "ec2:DescribeImageAttribute",
                "ec2:DescribeSpotFleetInstances",
                "ec2:DescribeSpotFleetRequests",
                "ec2:DescribeSpotPriceHistory",
                "ec2:RequestSpotFleet",
                "ec2:CancelSpotFleetRequests",
                "ec2:ModifySpotFleetRequest",
                "ec2:TerminateInstances",
                "autoscaling:DescribeAccountLimits",
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeLaunchConfigurations",
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:CreateLaunchConfiguration",
                "autoscaling:CreateAutoScalingGroup",
                "autoscaling:UpdateAutoScalingGroup",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:DeleteLaunchConfiguration",
                "autoscaling:DeleteAutoScalingGroup",
                "autoscaling:CreateOrUpdateTags",
                "autoscaling:SuspendProcesses",
                "autoscaling:PutNotificationConfiguration",
                "autoscaling:TerminateInstanceInAutoScalingGroup",
                "ecs:DescribeClusters",
                "ecs:DescribeContainerInstances",
                "ecs:DescribeTaskDefinitions",
                "ecs:DescribeTasks",
                "ecs:ListClusters",
                "ecs:ListContainerInstances",
                "ecs:ListTaskDefinitionFamilies",
                "ecs:ListTaskDefinitions",
                "ecs:ListTasks",
                "ecs:CreateCluster",
                "ecs:DeleteCluster",
                "ecs:RegisterTaskDefinition",
                "ecs:DeregisterTaskDefinition",
                "ecs:RunTask",
                "ecs:StartTask",
                "ecs:StopTask",
                "ecs:UpdateContainerAgent",
                "ecs:DeregisterContainerInstance",
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "logs:DescribeLogGroups",
                "iam:GetInstanceProfile",
                "iam:PassRole",
                "s3:GetBucketLocation",
                "s3:GetObject",
                "s3:ListBucket",
            ],
            "Resource": "*",
        }
    ]

    p = stack.enter_context(new_policy("batch_service_policy", stmt))
    r = stack.enter_context(new_role("batch_service_role", "batch", p.arn))

    yield r


@pytest.fixture(scope="module")
def ecs_instance_profile(stack):
    """Create a new instance profile with necessary permissions for batch
    compute environment ECS management.

    :param contextlib.ExitStack stack:
        A context manager shared across all Batch tests.

    :returns:
        An IAM.InstanceProfile.

    """

    stmt = [
        {
            "Effect": "Allow",
            "Action": [
                "autoscaling:Describe*",
                "autoscaling:UpdateAutoScalingGroup",
                "cloudformation:CreateStack",
                "cloudformation:DeleteStack",
                "cloudformation:DescribeStack*",
                "cloudformation:UpdateStack",
                "cloudwatch:GetMetricStatistics",
                "ec2:Describe*",
                "elasticloadbalancing:*",
                "ecr:*",
                "ecs:*",
                "events:DescribeRule",
                "events:DeleteRule",
                "events:ListRuleNamesByTarget",
                "events:ListTargetsByRule",
                "events:PutRule",
                "events:PutTargets",
                "events:RemoveTargets",
                "iam:ListInstanceProfiles",
                "iam:ListRoles",
                "iam:PassRole",
                "logs:*",
            ],
            "Resource": "*",
        }
    ]

    p = stack.enter_context(new_policy("ecs_policy", stmt))
    r = stack.enter_context(new_role("ecs_role", "ec2", p.arn))
    ip = stack.enter_context(new_instance_profile("ecs_instance_profile", role=r))

    yield ip


@pytest.fixture(scope="module")
def job_role(stack):
    """Create a new role with necessary permissions for a batch job.

    :param contextlib.ExitStack stack:
        A context manager shared across all Batch tests.

    :returns:
        An IAM.InstanceProfile.

    """

    stmt = [
        {
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "s3:GetBucketLocation",
                "s3:GetObject",
                "s3:ListBucket",
            ],
            "Resource": "*",
        }
    ]

    p = stack.enter_context(new_policy("batch_job_policy", stmt))
    r = stack.enter_context(new_role("batch_job_role", "ecs-tasks", p.arn))

    yield r


@pytest.fixture(scope="module")
def bucket(session):
    """A handle to an S3 Bucket.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User with the myjob.sh batch script uploaded.

    :returns:
        A boto3.S3.Bucket owned by the given user

    """

    s3 = session.resource("s3")
    with new_bucket("batch-script-bucket", s3=s3) as b:
        b.upload_file(
            "./tests/resources/fetch-and-run/myjob.sh",
            "myjob.sh",
            ExtraArgs={"ServerSideEncryption": "AES256"},
        )
        yield b


@pytest.fixture(scope="module")
def security_group(ec2, vpc_id, stack):
    """Creates a Security Group in the given vpc allowing ingress on port 80 and 22

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        The AWS VPC ID to create the security group in
    :param contextlib.ExitStack stack:
        A context manager shared across all Batch tests.

    :returns:
        A SecurityGroup.

    """

    vpc_resp = ec2.describe_vpcs(Filters=dict_to_filters({"isDefault": "true"}))
    test_vpc_id = vpc_resp["Vpcs"][0]["VpcId"]

    # return get_default_security_group(vpc_id)['GroupId']
    sg = stack.enter_context(new_security_group(test_vpc_id, "test_batch", ec2=ec2))

    ec2.authorize_security_group_ingress(
        GroupId=sg.group_id,
        IpProtocol="tcp",
        FromPort=80,
        ToPort=80,
        CidrIp="0.0.0.0/0",
    )

    ec2.authorize_security_group_ingress(
        GroupId=sg.group_id,
        IpProtocol="tcp",
        FromPort=22,
        ToPort=22,
        CidrIp="0.0.0.0/0",
    )

    yield sg


@aws_client("batch")
def get_job_status(job_id, *, batch=None):
    """Retrieve the status of the specified Batch Job.

    :param str job_id:
        ID of a Batch Job.

    :returns:
        A string.

    """

    wait_response = batch.describe_jobs(jobs=[job_id])
    status = wait_response["jobs"][0]["status"]

    print("Job {} status: {}".format(job_id, status))
    return status


@aws_client("batch")
@aws_client("ec2")
def cleanup_batch(*, batch=None, ec2=None):
    """Attempt to clean up artifacts from previous test runs."""

    cleanup_security_groups(ec2=ec2)


@aws_client("ec2")
def cleanup_security_groups(*, ec2=None):
    """Attempt to clean up security groups from previous test runs."""

    res = ec2.describe_security_groups()
    batch_sgs = [
        sg for sg in res["SecurityGroups"] if sg["GroupName"].startswith("test_batch")
    ]

    for sg in batch_sgs:
        group_id = sg["GroupId"]
        print("Deleting security group: {}".format(group_id))
        ignore_errors(ec2.delete_security_group)(GroupId=group_id)


def test_batch(
    ecr,
    session,
    internal_subnet_id,
    security_group,
    service_role,
    job_role,
    ecs_instance_profile,
    bucket,
    batch,
    s3,
    ec2,
    stack,
):
    """Create a batch compute environment, job queue, and job definition, then
    run a batch job on the environment.

    :testid: batch-f-1-1

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param str internal_subnet_id:
        The ID of the subnet where the compute environment will be deployed.
    :param boto3.EC2.SecurityGroup security_group:
        A SecurityGroup for the compute environment instances.
    :param boto3.IAM.Role service_role:
        A Role for the compute environment service.
    :param boto3.IAM.Role job_role:
        A Role for running the batch job.
    :param boto3.IAM.InstanceProfile ecs_instance_profile:
        An InstanceProfile for the compute environment instances.
    :param S3.Bucket bucket:
        A handle to an S3 Bucket owned by the user.
    :param Batch.Client batch:
        A handle to the Batch APIs.
    :param S3.Client s3:
        A handle to the S3 APIs.
    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param contextlib.ExitStack stack:
        A context manager shared across all Batch tests.

    """

    vpc_resp = ec2.describe_vpcs(Filters=dict_to_filters({"isDefault": "true"}))
    # print(vpc_resp)
    test_vpc_id = vpc_resp["Vpcs"][0]["VpcId"]
    subnet_resp = ec2.describe_subnets(Filters=dict_to_filters({"vpc-id": test_vpc_id}))
    # print(subnet_resp)
    test_id = subnet_resp["Subnets"][0]["SubnetId"]

    ce_name = stack.enter_context(
        new_batch_compute_environment(
            prefix="test_batch_ce",
            subnets=[test_id],
            security_group_ids=[security_group.id],
            service_role=service_role,
            ecs_instance_profile=ecs_instance_profile,
        )
    )

    wait_response = batch.describe_compute_environments(computeEnvironments=[ce_name])
    assert (
        len(wait_response["computeEnvironments"]) > 0
    ), "Failed to retrieve Compute Environment status"
    while wait_response["computeEnvironments"][0]["status"] == "CREATING":
        wait_response = batch.describe_compute_environments(
            computeEnvironments=[ce_name]
        )

    queue_name = stack.enter_context(
        new_batch_job_queue(prefix="test_batch_queue", compute_environment_name=ce_name)
    )
    wait_response = batch.describe_job_queues(jobQueues=[queue_name])
    assert len(wait_response["jobQueues"]) > 0, "Failed to retrieve Job Queue status"
    while wait_response["jobQueues"][0]["status"] == "CREATING":
        wait_response = batch.describe_job_queues(jobQueues=[queue_name])

    def_name = stack.enter_context(
        new_batch_job_definition(
            prefix="test_batch_job",
            repo_name=docker_to_ecr(stack, ecr=ecr),
            job_role=job_role,
        )
    )

    s3_url = "s3://{}/myjob.sh".format(bucket.name)
    params = dict(
        jobName="test_job",
        jobQueue=queue_name,
        jobDefinition=def_name,
        containerOverrides={
            # 'command': ['echo', 'hello'],
            "environment": [
                {"name": "BATCH_FILE_TYPE", "value": "script"},
                {"name": "BATCH_FILE_S3_URL", "value": s3_url},
            ]
        },
    )

    submit_response = batch.submit_job(**params)
    assert "jobId" in submit_response, unexpected(submit_response)
    job_id = submit_response["jobId"]

    status = retry(
        get_job_status,
        args=[job_id],
        kwargs={"batch": batch},
        msg="Checking job {} status".format(job_id),
        until=lambda s: s in ("SUCCEEDED", "FAILED"),
        delay=60,
        max_attempts=15,
    )

    assert status == "SUCCEEDED", unexpected(status)
