import os

import pytest

from aws_test_functions import has_status, ignore_errors, unexpected

# Use a custom regular expression when handling "access denied" errors that
# accounts for resources that do not get created because of other "access
# denied" errors.
# AWS checks for the presence of the job before checking permissions (!!!)
ACCESS_DENIED_NEEDLE = r'(AccessDenied|NotAuthorized)'

pytestmark = [pytest.mark.scp_check('get_tag_keys')]


@pytest.fixture(scope="module")
def tag(session):
    """A shared handle to the Resource Groups Tagging API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Resource Groups Tagging API.

    """

    return session.client("resourcegroupstaggingapi")


@pytest.fixture(scope="module")
def resource_arn(user):
    """Return the name of a resource that should already exist in the AWS
    account.

    :param IAM.User user:
        The current test User resource.

    :returns:
        A string.

    """

    return "arn:aws:s3:::{}".format(os.getenv("CLOUDTRAIL_BUCKET_NAME"))


@pytest.fixture(scope="module")
def resource_tag():
    """Return the name of a tag to use for testing.

    :returns:
        A string.

    """

    return "bitbucket_pipeline"


def test_tag_resources(tag, resource_arn, resource_tag, stack):
    """Verify access to tag resources.

    :param Tag.Client tag:
        A handle to the Resource Groups Tagging API.
    :param str resource_arn:
        ARN of a resource to tag.
    :param str resource_tag:
        Name of a tag to apply to a resource.
    :param contextlib.ExitStack stack:
        A context manager shared across all tests in this module.

    """

    res = tag.tag_resources(
        ResourceARNList=[
            resource_arn,
        ],
        Tags={
            resource_tag: "true",
        },
    )
    assert has_status(res, 200), unexpected(res)

    stack.callback(
        ignore_errors(test_untag_resources),
        tag,
        resource_arn,
        resource_tag,
    )


def test_get_resources(tag):
    """Verify access to list tagged resources.

    :param Tag.Client tag:
        A handle to the Resource Groups Tagging API.

    """

    res = tag.get_resources()
    assert has_status(res, 200), unexpected(res)

    assert len(res["ResourceTagMappingList"]) >= 1


def test_get_tag_keys(tag):
    """Verify access to list tags that are applied to resources.

    :param Tag.Client tag:
        A handle to the Resource Groups Tagging API.

    """

    res = tag.get_tag_keys()
    assert has_status(res, 200), unexpected(res)


def test_get_tag_values(tag):
    """Verify access to list values that are associated with tags applied to
    resources.

    :param Tag.Client tag:
        A handle to the Resource Groups Tagging API.

    """

    res = tag.get_tag_values(Key="key")
    assert has_status(res, 200), unexpected(res)


def test_untag_resources(tag, resource_arn, resource_tag):
    """Verify access to untag resources.

    :param Tag.Client tag:
        A handle to the Resource Groups Tagging API.
    :param str resource_arn:
        ARN of a resource to tag.
    :param str resource_tag:
        Name of a tag to apply to a resource.

    """

    res = tag.untag_resources(
        ResourceARNList=[
            resource_arn,
        ],
        TagKeys=[
            resource_tag,
        ],
    )
    assert has_status(res, 200), unexpected(res)
