"""
==============
test_iam_roles
==============

Verify that customers should not have any IAM permissions to modify RHEDcloud IAM
roles. But customers should be able to create and modify roles of their own.

${testcount:8}

"""

import json
import os

import pytest

from aws_test_functions import (
    attach_policy,
    create_role,
    debug,
    delete_role,
    get_arn,
    get_policy_arn,
    new_role,
)


@pytest.fixture(scope='module')
def iam(session):
    return session.resource('iam')


@pytest.fixture(scope='module')
def role(session):
    """Create a role while in the RHEDcloudAccountAdministrationOrg OU"""

    iam = session.resource('iam')
    with new_role('test_iam_roles', 'ec2', iam=iam) as r:
        yield r


@pytest.fixture(scope='session')
def rhedcloud_iam_role_name():
    """Provide access to an RHEDcloud-created role"""

    return os.getenv('RHEDCLOUD_IAM_ROLE_NAME') or 'RHEDcloudAuditorRole'


@pytest.fixture(scope='module')
def rhedcloud_role(session, rhedcloud_iam_role_name):
    """Provide access to an RHEDcloud-created role"""

    iam = session.resource('iam')
    yield iam.Role(rhedcloud_iam_role_name)


@pytest.fixture(scope='function')
def scp(session, wait_for_scp, user):
    """Fixture that waits for the SCP to take effect before allowing a test to
    proceed.

    """

    iam = session.client('iam')
    wait_for_scp(iam.update_user, kwargs=dict(UserName=user.user_name, NewUserName=user.user_name), delay=30)


def test_create_delete_role(iam):
    """A customer should be able to create and delete their own roles.

    :testid: iam-f-2-1, iam-f-2-2

    """

    with new_role('test_iam_roles', 'ec2', iam=iam) as r:
        assert r.role_name is not None


@pytest.mark.raises_access_denied
def test_update_role(iam, rhedcloud_iam_role_name):
    """Verify access to update IAM roles.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str rhedcloud_iam_role_name:
        Name of an IAM role.

    """

    debug("Updating role: {}".format(rhedcloud_iam_role_name))
    res = iam.meta.client.update_role(
        RoleName=rhedcloud_iam_role_name,
        Description="string",
    )
    assert has_status(res, 200), unexpected(res)


def test_modify_role(role):
    """A customer should be able to modify their own roles.

    :testid: iam-f-2-3

    """

    with attach_policy(role, 'AdministratorAccess'):
        assert any([
            'AdministratorAccess' == p.policy_name
            for p in role.attached_policies.all()
        ]), 'failed to attach policy to role'


@pytest.mark.raises_access_denied
def test_attach_role_policy(iam, rhedcloud_iam_role_name, stack):
    """Verify access to attach IAM policies to IAM roles.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str rhedcloud_iam_role_name:
        Name of an IAM role.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    policy_arn = get_policy_arn("RHEDcloudAdministratorRolePolicy")

    res = iam.meta.client.attach_role_policy(
        RoleName=rhedcloud_iam_role_name,
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Attached policy {} to role {}".format(policy_arn, rhedcloud_iam_role_name))
    stack.callback(
        ignore_errors(test_detach_role_policy),
        iam,
        rhedcloud_iam_role_name,
        policy_arn,
    )


@pytest.mark.raises_access_denied
def test_detach_role_policy(iam, rhedcloud_iam_role_name):
    """Verify access to detach IAM policies from IAM roles.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str rhedcloud_iam_role_name:
        Name of an IAM role.

    """

    policy_arn = get_policy_arn("RHEDcloudAdministratorRolePolicy")

    debug("Detaching policy {} from role {}".format(policy_arn, rhedcloud_iam_role_name))
    res = iam.meta.client.detach_role_policy(
        RoleName=rhedcloud_iam_role_name,
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.raises_access_denied
def test_create_rhedcloud_role(iam, rhedcloud_iam_role_name, stack):
    """Verify access to create IAM roles.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str rhedcloud_iam_role_name:
        Name of the role to create.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = iam.create_role(
        RoleName=rhedcloud_iam_role_name,
        Path="/rhedcloud/",
        AssumeRolePolicyDocument=json.dumps({
            "Version": "2012-10-17",
            "Statement": [{
                "Effect": "Allow",
                "Principal": {
                    "Service": ["lambda.amazonaws.com"],
                },
                "Action": "sts:AssumeRole"
            }],
        }),
        Description=rhedcloud_iam_role_name,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Created role: {}".format(rhedcloud_iam_role_name))
    stack.callback(
        ignore_errors(delete_role),
        iam,
        rhedcloud_iam_role_name,
    )


@pytest.mark.raises_access_denied
def test_clone_rhedcloud_role(iam, scp, rhedcloud_iam_role_name):
    """A customer should be NOT able to create their own role with the same
    name as an RHEDcloud role.

    :testid: iam-f-2-4

    """

    create_role(rhedcloud_iam_role_name, rhedcloud_role=True, iam=iam)
    assert False, 'had permission to create {}'.format(rhedcloud_iam_role_name)


@pytest.mark.raises_access_denied
def test_delete_rhedcloud_role(iam, scp, rhedcloud_iam_role_name):
    """A customer should be NOT able to delete RHEDcloud roles.

    :testid: iam-f-2-5

    """

    delete_role(role_name=rhedcloud_iam_role_name, iam=iam)
    assert False, 'had permission to delete {}'.format(rhedcloud_iam_role_name)


@pytest.mark.raises_access_denied
def test_modify_rhedcloud_role(rhedcloud_role, scp, rhedcloud_iam_role_name):
    """A customer should be NOT able to modify RHEDcloud roles.

    :testid: iam-f-2-6

    """

    with attach_policy(rhedcloud_role, 'AdministratorAccess'):
        assert False, 'had permission to modify {}'.format(rhedcloud_iam_role_name)


def test_modify_role_trust_policy(session, role):
    """A customer should be able to modify the trust policy for their own
    roles.

    :testid: iam-f-2-7

    """

    iam = session.client('iam')
    res = iam.update_assume_role_policy(
        RoleName=role.role_name,
        PolicyDocument=json.dumps({
            "Version": "2012-10-17",
            "Statement": [{
                "Effect": "Allow",
                "Principal": {
                    "Service": "vpc-flow-logs.amazonaws.com",
                },
                "Action": "sts:AssumeRole"
            }]
        }),
    )

    assert res['ResponseMetadata']['HTTPStatusCode'] == 200, 'failed to modify trust policy for customer role: {}'.format(res)


@pytest.mark.raises_access_denied
def test_modify_rhedcloud_role_trust_policy(session, scp, rhedcloud_iam_role_name):
    """A customer should be NOT able to modify the trust policy for RHEDcloud
    roles.

    :testid: iam-f-2-8

    """

    iam = session.client('iam')
    iam.update_assume_role_policy(
        RoleName=rhedcloud_iam_role_name,
        PolicyDocument=json.dumps({
            'Version': '2012-10-17',
            'Statement': [{
                'Effect': 'Allow',
                'Action': 'sts.AssumeRole',
                'Principal': {
                    'Service': 'importexport.amazonaws.com',
                },
            }],
        }),
    )
    assert False, 'had permission to modify {} trust policy'.format(rhedcloud_iam_role_name)


@pytest.mark.skip(reason='unclear how to move forward')
def test_new_role_saml_policy(user, session, role):
    """A customer should be NOT able to create a new role with full access and
    call restricted AWS APIs.

    :testid: iam-f-2-9

    """

    iam = session.client('iam')
    res = iam.update_assume_role_policy(
        RoleName=role.role_name,
        PolicyDocument=json.dumps({
            "Version": "2012-10-17",
            "Statement": [{
                "Effect": "Allow",
                "Principal": {
                    "Federated": get_arn('saml-provider', 'RHEDcloud_Prod_IDP'),
                },
                "Action": "sts:AssumeRoleWithSAML",
                "Condition": {
                    "StringEquals": {
                        "saml:iss": "https://login.rhedcloud.edu/idp/shibboleth",
                        "saml:aud": "https://signin.aws.amazon.com/saml"
                    }
                }
            }]
        }),
    )

    assert res['ResponseMetadata']['HTTPStatusCode'] == 200, 'failed to modify trust policy for customer role: {}'.format(res)

    with attach_policy(user, role.role_name):
        assert False, "I don't know what I'm doing"
