"""
================
tests.rds.common
================

Test functions that are shared across most/all RDS engines.

"""


def test_publicly_accessible(database, external_output, query_timeout_needle):
    """Make sure the RDS instance is NOT accessible externally.

    :testid: db-rds-1-*

    """

    assert 'PubliclyAccessible' in database, 'Unexpected response: {}'.format(database)
    assert not database['PubliclyAccessible'], 'Database is publicly accessible'

    assert query_timeout_needle in external_output, 'Query did not time out:\n{}'.format(external_output)


def test_encryption_at_rest(database):
    """Make sure RDS has encryption enabled.

    :testid: db-rds-2a-*

    """

    assert 'StorageEncrypted' in database, 'Unexpected response: {}'.format(database)
    assert database['StorageEncrypted'], 'Database is not encrypted at rest'


def test_encryption_in_motion(internal_output, query_timeout_needle):
    """Connect to RDS instance on the standard port with encryption in motion.

    :testid: db-rds-2b-*, db-rds-3-*

    """

    assert query_timeout_needle not in internal_output, 'Query timed out:\n{}'.format(internal_output)


def test_account_privileges(internal_output):
    """Standard database user accounts should not have master account level privileges.

    :testid: db-rds-3-*, db-rds-4-*

    """

    assert False, "not implemented"
