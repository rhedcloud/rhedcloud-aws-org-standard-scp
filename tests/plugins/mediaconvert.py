"""
===================
MediaConvert Fixtures
===================

This module contains fixtures for MediaConvert tests and tests for related
services (like MediaLive and MediaPackage).

"""

import pytest
from aws_test_functions import (
    new_bucket,
    new_role,
    new_policy
)

@pytest.fixture(scope='session')
def mediaconvert(session):
    """A shared handle to MediaConvert APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the MediaConvert APIs.

    """

    return session.client('mediaconvert')


@pytest.fixture(scope='session')
def convert_bucket(session):
    """A handle to an S3 Bucket containing big_buck_bunny.mp4..

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User with the myjob.sh batch script uploaded.

    :returns:
        A boto3.S3.Bucket owned by the given user

    """

    s3 = session.resource('s3')
    with new_bucket('mediabucket', s3=s3, ACL='public-read-write') as b:
        b.upload_file('./tests/resources/big_buck_bunny.mp4',
                      'big_buck_bunny.mp4',
                      ExtraArgs={'ServerSideEncryption': 'AES256'})
        yield b


@pytest.fixture(scope='session')
def backup_bucket(session):
    """A handle to an S3 Bucket containing big_buck_bunny.mp4..

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User with the myjob.sh batch script uploaded.

    :returns:
        A boto3.S3.Bucket owned by the given user

    """

    s3 = session.resource('s3')
    with new_bucket('backup_bucket', s3=s3, ACL='public-read-write') as b:
        b.upload_file('./tests/resources/big_buck_bunny.mp4',
                      'big_buck_bunny.mp4',
                      ExtraArgs={'ServerSideEncryption': 'AES256'})
        yield b


@pytest.fixture(scope='module')
def mediaconvert_role(stack):
    """Create a new role with necessary permissions for AWS MediaConvert.

    :param contextlib.ExitStack stack:
        A context manager shared across all Batch tests.

    :returns:
        An IAM.Role.

    """

    stmt = [{
        "Effect": "Allow",
        "Action": [
            "execute-api:Invoke",
            "s3:*",
        ],
        "Resource": "*"
    }]

    p = stack.enter_context(new_policy('mediaconvert', stmt))
    r = stack.enter_context(new_role('mediaconvert', 'mediaconvert', p.arn))

    yield r