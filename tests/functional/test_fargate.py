"""
============
test_fargate
============

Verify the ability to create and use Fargate API calls.

Plan:

* Create an ECS cluster
* Create a service
* List services
* Describe a service
* Update a service
* Delete a service

${testcount:6}

"""

import pytest

from aws_test_functions import ecs as ecs_lib, has_status, ignore_errors, make_identifier, unexpected

from tests.functional import test_ecs

pytest_plugins = ["tests.plugins.ecs"]


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    return {
        "cluster_name": "",
        "task_def_name": "",
        "service_name": make_identifier("test-ecs-service"),
    }


def test_setup(ecs, ecs_task_role, stack, shared_vars):
    """Create a new ECS cluster and task definition.

    :testid: fargate-f-1-1

    :param ECS.Client ecs:
        A handle to the ECS API.
    :param boto3.IAM.Role ecs_task_role:
        A Role permitted to run ECS tasks.
    :param contextlib.ExitStack stack:
        A context manager shared across all ECS tests.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    test_ecs.test_create_cluster(ecs, stack, shared_vars)
    test_ecs.test_create_task_definition(ecs, ecs_task_role, stack, shared_vars)


def test_create_service(
    ecs, service_name, cluster_name, task_def_name, security_group,
    internal_subnet_id, stack,
):
    """Verify access to create a new ECS service.

    :testid: fargate-f-1-2

    :param ECS.Client ecs:
        A handle to the ECS API.
    :param str service_name:
        Name of an ECS service.
    :param str cluster_name:
        Name of an ECS cluster.
    :param str task_def_name:
        Name of a task definition.
    :param EC2.SecurityGroup security_group:
        A Security Group resource.
    :param str internal_subnet_id:
        ID of an internal subnet.
    :param contextlib.ExitStack stack:
        A context manager shared across all ECS tests.

    """

    ecs_lib.create_service(
        service_name,
        cluster_name,
        task_def_name,
        security_group.group_id,
        internal_subnet_id,
        ecs=ecs,
    )

    stack.callback(
        ignore_errors(ecs_lib.delete_service),
        service_name,
        cluster_name,
        ecs=ecs,
    )


def test_list_services(ecs, cluster_name):
    """Verify access to list ECS services.

    :testid: fargate-f-1-3

    :param ECS.Client ecs:
        A handle to the ECS API.
    :param str cluster_name:
        Name of an ECS cluster.

    """

    res = ecs.list_services(
        cluster=cluster_name,
    )
    assert has_status(res, 200), unexpected(res)


def test_describe_services(ecs, cluster_name, service_name):
    """Verify access to describe ECS services.

    :testid: fargate-f-1-4

    :param ECS.Client ecs:
        A handle to the ECS API.
    :param str cluster_name:
        Name of an ECS cluster.
    :param str service_name:
        Name of an ECS service.

    """

    res = ecs.describe_services(
        cluster=cluster_name,
        services=[service_name],
    )
    assert has_status(res, 200), unexpected(res)


def test_update_service(ecs, cluster_name, service_name):
    """Verify access to update ECS services.

    :testid: fargate-f-1-5

    :param ECS.Client ecs:
        A handle to the ECS API.
    :param str cluster_name:
        Name of an ECS cluster.
    :param str service_name:
        Name of an ECS service.

    """

    res = ecs.update_service(
        cluster=cluster_name,
        service=service_name,
        desiredCount=1,
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_service(ecs, service_name, cluster_name):
    """Verify access to delete ECS services.

    :testid: fargate-f-1-6

    :param ECS.Client ecs:
        A handle to the ECS API.
    :param str service_name:
        Name of an ECS service.
    :param str cluster_name:
        Name of an ECS cluster.

    """

    ecs_lib.delete_service(
        service_name,
        cluster_name,
        ecs=ecs,
    )
