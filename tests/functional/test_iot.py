"""
========
test_iot
========

Verify access to AWS IoT APIs.

Plan:

* Create, describe, and delete a certificate
* Create, describe, and delete a thing type
* Create, describe, and delete a thing
* Create, describe, and cancel a job
* Create, describe, and delete a policy
* Create, describe, and delete a topic rule

${testcount:24}

"""

import json

import pytest

from aws_test_functions import (
    ignore_errors,
    make_identifier,
    new_bucket,
    new_role,
    unexpected,
)
from aws_test_functions.iot import (
    cancel_job,
    cleanup_thing_types,
    delete_certificate,
    delete_policy,
    delete_thing,
    delete_thing_type,
    delete_topic_rule,
    new_certificate,
    new_thing,
)

# Use a custom regular expression when handling "access denied" errors that
# accounts for resources that do not get created because of other "access
# denied" errors.
# AWS checks for the presence of the job before checking permissions (!!!)
ACCESS_DENIED_NEEDLE = r'(AccessDenied|ResourceNotFound)'

# This service is fully blocked in the HIPAA SCP
pytestmark = [pytest.mark.scp_check('list_certificates', delay=60)]


@pytest.fixture(scope='module')
def iot(session):
    """A shared handle to IoT API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the IoT API.

    """

    return session.client('iot')


@pytest.fixture(scope='module', autouse=True)
def do_cleanup(iot):
    """Cleanup resources from previous tests.

    :param IoT.Client iot:
        A handle to the IoT API.

    """

    cleanup_thing_types(iot=iot)


@pytest.fixture(scope='module')
def bucket(session, stack):
    """Create an S3 bucket for use with IoT data.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param contextlib.ExitStack stack:
        A context manager shared across all IoT tests.

    :yields:
        An S3 Bucket resource.

    """

    s3 = session.resource('s3')
    b = stack.enter_context(new_bucket('test-iot', s3=s3))

    yield b


@pytest.fixture(scope='module')
def role(session, stack):
    """Create an IAM Role to use with IoT jobs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param contextlib.ExitStack stack:
        A context manager shared across all IoT tests.

    :yields:
        An IAM Role resource.

    """

    iam = session.resource('iam')
    r = stack.enter_context(new_role(
        'test-iot',
        'iot',
        'AWSIoTFullAccess',
        'AmazonS3FullAccess',
        iam=iam,
    ))

    yield r


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'cert_id': 'abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789',
        'thing_name': 'not-created',
        'thing_arn': 'arn:aws:iot:us-east-1:111111111111:thing/not-created',
        'thing_type_name': 'not-created',
        'job_id': 'not-created',
        'policy_name': 'not-created',
        'rule_name': 'not_created',
    }


def test_list_certificates(iot):
    """Verify access to list certificates.

    :testid: iot-f-1-1

    :param IoT.Client iot:
        A handle to the IoT API.

    """

    res = iot.list_certificates()
    assert 'certificates' in res, unexpected(res)


@pytest.mark.xfail(reason='Picky with failures')
def test_list_jobs(iot):
    """Verify access to list jobs.

    :testid: iot-f-1-2

    :param IoT.Client iot:
        A handle to the IoT API.

    """

    res = iot.list_jobs()
    assert 'jobs' in res, unexpected(res)


def test_list_policies(iot):
    """Verify access to list policies.

    :testid: iot-f-1-3

    :param IoT.Client iot:
        A handle to the IoT API.

    """

    res = iot.list_policies()
    assert 'policies' in res, unexpected(res)


def test_list_things(iot):
    """Verify access to list things.

    :testid: iot-f-1-4

    :param IoT.Client iot:
        A handle to the IoT API.

    """

    res = iot.list_things()
    assert 'things' in res, unexpected(res)


def test_create_certificate(iot, stack, shared_vars):
    """Verify access to create certificates.

    :testid: iot-f-3-1

    :param IoT.Client iot:
        A handle to the IoT API.
    :param contextlib.ExitStack stack:
        A context manager shared across all IoT tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    cert = stack.enter_context(new_certificate())
    shared_vars['cert_id'] = cert['certificateId']


def test_describe_certificate(iot, cert_id):
    """Verify access to describe certificates.

    :testid: iot-f-2-1

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str cert_id:
        ID of a certificate.

    """

    res = iot.describe_certificate(certificateId=cert_id)
    assert 'certificateDescription' in res, unexpected(res)


def test_update_certificate(iot, cert_id):
    """Verify access to update certificates.

    :testid: iot-f-3-9

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str cert_id:
        ID of a certificate.

    """

    iot.update_certificate(
        certificateId=cert_id,
        newStatus='ACTIVE',
    )


def test_create_thing_type(iot, stack, shared_vars):
    """Verify access to create thing types.

    :param IoT.Client iot:
        A handle to the IoT API.
    :param contextlib.ExitStack stack:
        A context manager shared across all IoT tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-thing-type')
    res = iot.create_thing_type(
        thingTypeName=name,
    )
    assert 'thingTypeName' in res, unexpected(res)

    print('Created thing type: {}'.format(name))
    shared_vars['thing_type_name'] = name

    stack.callback(
        ignore_errors(delete_thing_type),
        name,
        iot=iot,
    )


def test_describe_thing_type(iot, thing_type_name):
    """Verify access to describe thing types.

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str thing_type_name:
        Name of a thing type.

    """

    res = iot.describe_thing_type(thingTypeName=thing_type_name)
    assert 'thingTypeArn' in res, unexpected(res)


def test_create_thing(iot, stack, shared_vars):
    """Verify access to create things.

    :testid: iot-f-3-4

    :param IoT.Client iot:
        A handle to the IoT API.
    :param contextlib.ExitStack stack:
        A context manager shared across all IoT tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    thing = stack.enter_context(new_thing('test-thing', iot=iot))
    assert 'thingName' in thing, unexpected(thing)
    shared_vars['thing_arn'] = thing['thingArn']
    shared_vars['thing_name'] = thing['thingName']


def test_describe_thing(iot, thing_name):
    """Verify access to describe things.

    :testid: iot-f-2-5

    :param IoT.Client iot:
        A handle to the IoT API.
    :param string thing_name:
        Name of a thing.

    """

    res = iot.describe_thing(thingName=thing_name)
    assert 'defaultClientId' in res, unexpected(res)


def test_update_thing(iot, thing_name, thing_type_name):
    """Verify access to update things.

    :testid: iot-f-3-10

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str thing_name:
        Name of a thing.
    :param str thing_type_name:
        Name of a thing type.

    """

    iot.update_thing(
        thingName=thing_name,
        thingTypeName=thing_type_name,
    )


def test_create_job(iot, role, thing_arn, stack, shared_vars):
    """Verify access to create jobs.

    :testid: iot-f-3-2

    :param IoT.Client iot:
        A handle to the IoT API.
    :param IAM.Role role:
        An IAM Role resource to use when creating the job.
    :param str thing_arn:
        ARN of a thing.
    :param contextlib.ExitStack stack:
        A context manager shared across all IoT tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    job_id = make_identifier('test-job')
    res = iot.create_job(
        jobId=job_id,
        targets=[thing_arn],
        document=json.dumps({
            'operation': 'systemStatus',
        }),
        presignedUrlConfig=dict(
            roleArn=role.arn,
        ),
    )
    assert 'jobId' in res, unexpected(res)

    shared_vars['job_id'] = res['jobId']
    print('Created job: {}'.format(job_id))

    stack.callback(
        ignore_errors(cancel_job),
        job_id,
        iot=iot,
    )


def test_describe_job(iot, job_id):
    """Verify access to describe jobs.

    :testid: iot-f-2-2

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str job_id:
        ID of a job.

    """

    res = iot.describe_job(jobId=job_id)
    assert 'job' in res, unexpected(res)


def test_create_policy(iot, stack, shared_vars):
    """Verify access to create policies.

    :testid: iot-f-4-1

    :param IoT.Client iot:
        A handle to the IoT API.
    :param contextlib.ExitStack stack:
        A context manager shared across all IoT tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    """

    name = make_identifier('test-iot-policy')
    res = iot.create_policy(
        policyName=name,
        policyDocument=json.dumps({
            "Version": "2012-10-17",
            "Statement": [{
                "Effect": "Allow",
                "Action": "iot:Connect",
                "Resource": "*",
            }]
        }),
    )
    assert 'policyArn' in res, unexpected(res)

    print('Created policy: {}'.format(name))
    shared_vars['policy_name'] = name

    stack.callback(
        ignore_errors(delete_policy),
        name,
        iot=iot,
    )


def test_get_policy(iot, policy_name):
    """Verify access to describe policies.

    :testid: iot-f-2-3

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str policy_name:
        Name of a policy.

    """

    res = iot.get_policy(policyName=policy_name)
    assert 'policyArn' in res, unexpected(res)


def test_create_topic_rule(iot, bucket, role, stack, shared_vars):
    """Verify access to create topic rules.

    :testid: iot-f-3-3

    :param IoT.Client iot:
        A handle to the IoT API.
    :param S3.Bucket bucket:
        An S3 Bucket resource to use with the rule.
    :param IAM.Role role:
        An IAM Role resource to use when creating the rule.
    :param contextlib.ExitStack stack:
        A context manager shared across all IoT tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    """

    name = shared_vars['rule_name'] = make_identifier('test-iot-rule').replace('-', '_')
    key = make_identifier('test-iot-key')

    iot.create_topic_rule(
        ruleName=name,
        topicRulePayload=dict(
            sql="SELECT * FROM 'iot/test'",
            ruleDisabled=True,
            actions=[dict(
                s3=dict(
                    roleArn=role.arn,
                    bucketName=bucket.name,
                    key=key,
                )
            )],
        )
    )
    print('Created topic rule: {}'.format(name))

    stack.callback(
        ignore_errors(delete_topic_rule),
        name,
        iot=iot,
    )


def test_get_topic_rule(iot, rule_name):
    """Verify access to describe topic rules.

    :testid: iot-f-2-4

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str rule_name:
        Name of a topic rule.

    """

    res = iot.get_topic_rule(ruleName=rule_name)
    assert 'ruleArn' in res, unexpected(res)


def test_delete_topic_rule(iot, rule_name):
    """Verify access to delete topic rules.

    :testid: iot-f-3-7

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str rule_name:
        Name of a topic rule.

    """

    delete_topic_rule(rule_name, iot=iot)


def test_delete_policy(iot, policy_name):
    """Verify access to delete policies.

    :testid: iot-f-4-2

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str policy_name:
        Name of a policy.

    """

    delete_policy(policy_name, iot=iot)


def test_cancel_job(iot, job_id):
    """Verify access to cancel jobs.

    :testid: iot-f-3-6

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str job_id:
        ID of a job.

    """

    res = cancel_job(job_id, iot=iot)
    assert 'jobId' in res, unexpected(res)


def test_delete_thing(iot, thing_name):
    """Verify access to delete things.

    :testid: iot-f-3-8

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str thing_name:
        Name of a thing.

    """

    delete_thing(thing_name, iot=iot)


def test_delete_thing_type(iot, thing_type_name):
    """Verify access to delete thing types.

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str thing_type_name:
        Name of a thing type.

    """

    delete_thing_type(thing_type_name, iot=iot)


def test_delete_certificate(iot, cert_id):
    """Verify access to delete certificates.

    :testid: iot-f-3-5

    :param IoT.Client iot:
        A handle to the IoT API.
    :param str cert_id:
        ID of a certificate.

    """

    delete_certificate(cert_id, iot=iot)
