"""
===============
test_connect
===============

Verify access to use AWS Connect.

#FIXME: This test is an incredibly minimal smoke test to ensure
#FIXME: the client is producing acceptable results when blocked 

Plan:

* Attempt to stop a non existent contact

${testcount:2}

"""

import pytest

from aws_test_functions import make_identifier


CONTACT_ID = make_identifier('test-fake-contact')
INSTANCE_ID = make_identifier('test-fake-instance')

#This service is fully blocked
pytestmark = [pytest.mark.scp_check('stop_contact', kwargs=dict(ContactId=CONTACT_ID,
                                                                InstanceId=INSTANCE_ID))]


@pytest.fixture(scope='module')
def connect(session):
    """A shared handle to Connect API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Connect API.

    """

    return session.client('connect')


def test_stop_contact(connect):
    """Verify ability to stop a contact

    :testid: connect-f-1-1 

    :param Connect.Client connect:
        A handle to the Connect API.

    """

    try:
        res = connect.stop_contact(ContactId=CONTACT_ID,
                                   InstanceId=INSTANCE_ID)
    except connect.exceptions.ContactNotFoundException:
        pass