"""
========
conftest
========

Fixtures shared across tests for several RDS engines.

"""

from itertools import count

import pytest

from aws_test_functions import (
    cleanup_aurora_clusters,
    cleanup_db_parameter_groups,
    cleanup_db_subnet_groups,
    cleanup_option_groups,
    get_uuid,
    new_db_instance_async,
)

# RDS engines to test, with the respective values representing the order the
# engine should be tested. This value is based on the average amount of time it
# takes to provision an RDS instance with a given engine. Instances that finish
# provisioning faster get tested sooner.
ENGINES = {
    'mariadb': 10,
    'mysql': 20,
    'postgres': 30,
    'docdb': 35,
    'oracle-ee': 40,
    'sqlserver-web': 50,
    'aurora': 60,
    'aurora-mysql': 60,
    'aurora-postgresql': 70,
}


class TestSorter:

    def __init__(self):
        # help preserve test function ordering within test modules
        self._idx = count()

        # flag to indicate when the first RDS test has been encountered
        self._sorted_rds_tests = False

    def __call__(self, item):
        """Determine the relative position of ``item`` in the list of test
        functions.

        :param item:
            A test function detected by pytest.

        :returns:
            A 2-tuple containing the relative position of ``item`` in the
            containing list and a second integer indicating the original
            position in the list.

        """

        # assume the test will keep its position in the list
        key = 0

        if not is_rds_test(item):
            # if we've already hit RDS tests in the list, push remaining
            # non-RDS tests to the bottom of the list.
            if self._sorted_rds_tests:
                key = 1000
        else:
            # assume that all RDS tests are grouped together, and that any tests
            # NOT for RDS from this point forward will come after all RDS tests.
            self._sorted_rds_tests = True

            # return the value from the ENGINES dictionary that indicates how long
            # the RDS engine takes to provision, relatively speaking.
            key = ENGINES[item.module.ENGINE]

        return key, next(self._idx)


def is_rds_test(item):
    """Simple, reusable test to see if a test looks like an RDS test."""

    return callable(getattr(item.module, 'engine', None))


@pytest.hookimpl(trylast=True)
def pytest_itemcollected(item):
    """Automatically mark all RDS tests as slow tests"""

    if is_rds_test(item):
        item.add_marker('slowtest')


@pytest.hookimpl(trylast=True)
def pytest_collection_modifyitems(items, config):
    """Exclude RDS engines that are not targeted by this run of pytest.

    Also sort the RDS test functions so the faster-to-provision RDS engines
    get tested sooner. This gives pytest something to do while the RDS engines
    that take more time to provision finish getting set up.

    .. note::

        This works on the assumption that the name of the database engine being
        tested is in returned by the ``engine`` fixture in each test module.

    """

    global ENGINES

    # see if we are skipping slowtests (like RDS)
    if 'not slowtest' in config.getoption('markexpr'):
        # explicitly remove any RDS tests
        items[:] = [i for i in items if not is_rds_test(i)]
        return

    # find all targeted engines
    engines = {item.module.ENGINE for item in items if is_rds_test(item)}

    # remove engines that are not targeted
    for engine in set(ENGINES) ^ engines:
        ENGINES.pop(engine)

    # overwrite the list of test functions with the sorted list
    items[:] = sorted(items, key=TestSorter())


@pytest.fixture(scope='session')
def global_rds(global_session):
    """Session-wide RDS client"""

    return global_session.client('rds')


@pytest.fixture(scope='module')
def rds(session):
    """Module-wide RDS client"""

    return session.client('rds')


def cleanup(global_rds):
    """Remove any unused resources from previous RDS tests."""

    cleanup_db_subnet_groups(rds=global_rds)
    cleanup_option_groups(rds=global_rds)
    cleanup_db_parameter_groups(rds=global_rds)
    cleanup_aurora_clusters(rds=global_rds)


@pytest.fixture(scope='session')
def cleanup_rds(global_rds):
    cleanup(global_rds)


@pytest.fixture(scope='session')
def prepare_oracle_database(global_rds, cleanup_rds):
    """Setup an Option Group to enable Native Network Encryption (NNE) for Oracle.

    Beware that other option groups must be cleaned up BEFORE this is called.

    """

    engine = 'oracle-ee'
    if engine not in ENGINES:
        # don't bother setting anything up for Oracle if it's not going to be tested
        return {}

    name = 'test-rds-oracle-options-{}'.format(get_uuid())
    res = global_rds.create_option_group(
        OptionGroupName=name,
        OptionGroupDescription='Enable Native Network Encryption',
        EngineName=engine,
        MajorEngineVersion='19',
    )
    assert 'OptionGroup' in res, 'Failed to create Oracle option group:\n{}'.format(res)
    print('Created option group for Oracle: {}'.format(name))

    # db-rds-2b-oracle: enable Network Native Encryption
    res = global_rds.modify_option_group(
        OptionGroupName=name,
        OptionsToInclude=[{
            "OptionName": "NATIVE_NETWORK_ENCRYPTION",
            "OptionSettings": [{
                "Name": "SQLNET.ENCRYPTION_TYPES_SERVER",
                "Value": "AES256",
            }, {
                "Name": "SQLNET.CRYPTO_CHECKSUM_TYPES_SERVER",
                "Value": "SHA1",
            }, {
                "Name": "SQLNET.ENCRYPTION_SERVER",
                "Value": "REQUIRED",
            }, {
                "Name": "SQLNET.CRYPTO_CHECKSUM_SERVER",
                "Value": "REQUIRED",
            }],
        }],
        ApplyImmediately=True,
    )
    assert 'OptionGroup' in res, 'Failed to require NNE in Oracle option group:\n{}'.format(res)

    return {
        'OptionGroupName': name,
        'DBInstanceClass': 'db.m4.large',
    }


@pytest.fixture(scope='session')
def prepare_sqlserver_database(global_rds, cleanup_rds):
    """Setup an Parameter Group to force SSL for SQL Server.

    Beware that other option groups must be cleaned up BEFORE this is called.

    """

    engine = 'sqlserver-web'
    if engine not in ENGINES:
        # don't bother setting anything up for SQL Server if it's not going to be tested
        return {}

    name = 'test-rds-sqlserver-params-{}'.format(get_uuid())

    res = global_rds.create_db_parameter_group(
        DBParameterGroupName=name,
        DBParameterGroupFamily='sqlserver-web-14.0',
        Description=name,
    )
    assert 'DBParameterGroup' in res, 'Failed to create SQL Server parameter group:\n{}'.format(res)

    # db-rds-2b-sqlserver: force SSL
    res = global_rds.modify_db_parameter_group(
        DBParameterGroupName=name,
        Parameters=[{
            "ParameterName": "rds.force_ssl",
            "ParameterValue": "1",

            # we cannot use "immediate" because "rds.force_ssl" is a static parameter
            "ApplyMethod": "pending-reboot",
        }]
    )
    assert 'DBParameterGroupName' in res, 'Failed to force SSL in SQL Server parameter group:\n{}'.format(res)

    return {
        'DBParameterGroupName': name,
    }


@pytest.fixture(scope='session', autouse=True)
def engine_parameters(prepare_oracle_database, prepare_sqlserver_database):
    """Gather all additional parameters for needy RDS engines."""

    return {
        # Enable Native Network Encryption for Oracle
        'oracle-ee': prepare_oracle_database,

        # Force SSL for SQL Server
        'sqlserver-web': prepare_sqlserver_database,
    }


@pytest.fixture(scope='session')
def databases(cleanup_rds, global_rds, vpc_id, setup_runners, engine_parameters):
    """Create all RDS instances (that will be tested) up front so there's less
    waiting around for each instance to be available.

    """

    try:
        dbs = {}
        for engine in ENGINES:
            if engine == "docdb":
                # DocumentDB is handled differently than most RDS stuff
                continue

            db_name = 'test-rds-{}'.format(engine)
            params = engine_parameters.get(engine) or {}
            db = new_db_instance_async(db_name, engine, vpc_id, rds=global_rds, **params)
            dbs[engine] = db

        yield dbs
    finally:
        for db in dbs.values():
            db.cleanup()


# -----------------------------------------------------------------------------
#                           MODULE-SCOPED FIXTURES
# -----------------------------------------------------------------------------


@pytest.fixture(scope='module')
def db_name(engine):
    """Reusable name for RDS instance and output logs."""

    return 'test-rds-{}'.format(engine)


@pytest.fixture(scope='module')
def database(databases, engine):
    """A handle to the specific RDS instance being tested."""

    return databases[engine]


@pytest.fixture(scope='module')
def script(database, raw_script):
    """Replace information in each engine's script."""

    # wait for the RDS instance to be available so we have access to
    # information, such as the instance endpoint
    database.wait()

    return raw_script.format(
        hostname=database['Endpoint']['Address'],
        username=database['MasterUsername'],
        password=database['MasterUserPassword'],
    ).encode()


@pytest.fixture(scope='module')
def query_reached_needle(engine):
    """Needle to search for that indicates the script got to the point of
    executing queries (installed all dependencies, etc).

    """

    return "select now()"


@pytest.fixture(scope='module')
def query_timeout_needle(engine):
    """Needle to search for that indicates the script encountered a timeout
    error when trying to connect to an RDS instance.

    """

    assert False, '"query_timeout_needle" fixture not implemented for {}'.format(engine)


@pytest.fixture(scope='module')
def internal_output(internal_runner, script, query_reached_needle):
    """Return the output from the internal EC2 instance's run of the script.

    A basic sanity test is performed (to make sure we got to the point of
    actually querying the RDS instance) before the output is returned.

    """

    output = internal_runner(script)
    check_query_executed(query_reached_needle, output)

    return output


@pytest.fixture(scope='module')
def external_output(external_runner, script, query_reached_needle):
    """Return the output from the external EC2 instance's run of the script.

    A basic sanity test is performed (to make sure we got to the point of
    actually querying the RDS instance) before the output is returned.

    """

    output = external_runner(script)
    check_query_executed(query_reached_needle, output)

    return output


def check_query_executed(needle, output):
    """Basic sanity test, which checks that the script executed by an EC2
    instance got far enough to actually query the RDS instance."""

    assert needle in output, "Script failed before attempting to connect to database:\n{}".format(output)
