from zipfile import ZipFile, ZipInfo
import io

import pytest

from aws_test_functions import (
    make_identifier,
    new_policy,
    new_role,
)


@pytest.fixture(scope='module')
def _lambda(session):
    """A shared handle to Lambda API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Lambda API.

    """

    return session.client('lambda')


@pytest.fixture(scope='module')
def lambda_role():
    """Create a new role with necessary permissions for AWS Lambda.

    :returns:
        An IAM.Role.

    """

    stmt = [{
        'Effect': 'Allow',
        'Action': [
            'lambda:CreateFunction',
            'lambda:DeleteFunction',
            'lambda:Invoke',
            'lambda:ListFunctions',
        ],
        'Resource': 'arn:aws:iam:::*',
    }]

    with new_policy('test_lambda', stmt) as p:
        with new_role('test_lambda', 'lambda', p.arn) as r:
            yield r


@pytest.fixture(scope='module')
def lambda_func_name():
    """Create a random function name.

    :returns:
        A string.

    """

    return make_identifier('HelloLambda')


@pytest.fixture(scope='module')
def lambda_code():
    """Return the contents of a file containing code for a Lambda function.

    :returns:
        A string.

    """

    with open('./tests/resources/HelloLambda.py') as fh:
        return fh.read()


@pytest.fixture(scope='module')
def lambda_zip_bytes(lambda_code):
    """Return the contents of a Zip file with the Lambda function code.

    :param str lambda_code:
        The code for the Lambda function.

    :returns:
        A bytestring.

    """

    out = io.BytesIO()
    with ZipFile(out, 'w') as fh:
        # give full access to included file
        zinfo = ZipInfo('HelloLambda.py')
        zinfo.external_attr = 0o777 << 16

        fh.writestr(zinfo, lambda_code)

    return out.getvalue()
