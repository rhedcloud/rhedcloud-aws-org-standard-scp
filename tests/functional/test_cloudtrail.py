"""
===============
test_cloudtrail
===============

Verify restrictions to RHEDcloud managed CloudTrail Trails and verify
functionality for customer managed CloudTrail Trails

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Assert that the RHEDcloud Managed CloudTrail Trail cannot be updated or
  deleted
* Assert that the RHEDcloud Managed CloudTrail S3 bucket can be read
* Assert that the RHEDcloud Managed CloudTrail S3 bucket cannot be modified or
  deleted, not can the contents
* Assert that a user can create, modify, and delete a self managed CloudTrail
  trail
* Assert that a user cannot send a customer managed CloudTrail trail to  an
  RHEDcloud managed CloudTrail bucket

${testcount:5}

"""

import pytest

from aws_test_functions import (
    ClientError,
    get_account_number,
    new_bucket,
)


@pytest.fixture(scope='module')
def bucket(user, session):
    """A handle to an S3 Bucket.

    :param boto3.IAM.User user:
        An authenticated IAM User.
    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A boto3.S3.Bucket owned by the given user

    """
    s3 = session.resource('s3')
    client = session.client('s3')
    with new_bucket('testcloudtrail', s3=s3, ACL="bucket-owner-full-control") as b:
        bucket_policy = """{
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "AWSCloudTrailAclCheck",
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "cloudtrail.amazonaws.com"
                    },
                    "Action": "s3:GetBucketAcl",
                    "Resource": "arn:aws:s3:::""" + b.name + """"
                },
                {
                    "Sid": "AWSCloudTrailWrite",
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "cloudtrail.amazonaws.com"
                    },
                    "Action": "s3:PutObject",
                    "Resource": "arn:aws:s3:::""" + b.name + "/AWSLogs/" + get_account_number() + """/*",
                    "Condition": {
                        "StringEquals": {
                            "s3:x-amz-acl": "bucket-owner-full-control"
                        }
                    }
                }
            ]
        }"""

        client.put_bucket_policy(Bucket=b.name,
                                 Policy=bucket_policy)
        yield b


@pytest.fixture(scope='module')
def cloudtrail(session):
    """A shared handle to CloudTrail APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the CloudTrail APIs.

    """
    return session.client('cloudtrail')


def test_rhedcloud_managed_restrictions(cloudtrail):
    """Verify restrictions to RHEDcloud managed CloudTrail Trail to all user roles.

    :testid: cloudtrail-f-1-1
    :testid: cloudtrail-f-1-2
    :testid: cloudtrail-f-1-3

    :param Cloudtrail.Client cloudtrail:
        A handle to the CloudTrail APIs.

    """

    rhedcloud_trail_name = "rhedcloud-aws-rs-account-Master"
    trail_response = cloudtrail.describe_trails(trailNameList=[rhedcloud_trail_name])
    assert trail_response['ResponseMetadata']['HTTPStatusCode'] == 200, 'Unexpected status code on DescribeTrails'
    trails = trail_response['trailList']
    if len(trails) > 0:
        with pytest.raises(ClientError) as ex1:
            cloudtrail.update_trail(Name=rhedcloud_trail_name, EnableLogFileValidation=False)
            assert False, 'Had permission to update RHEDcloud managed CloudTrail'
        assert 'AccessDenied' in str(ex1), 'Unexpected ClientError: {}'.format(ex1)

        with pytest.raises(ClientError) as ex2:
            cloudtrail.delete_trail(Name=rhedcloud_trail_name)
            cloudtrail.create_trail(
                Name=rhedcloud_trail_name,
                S3BucketName="serviceforge-aws-admin-110-ct1",
                IncludeGlobalServiceEvents=True,
                IsMultiRegionTrail=True,
                HomeRegion='us-east-1',
                EnableLogFileValidation=True)
            assert False, 'Had permission to delete RHEDcloud managed CloudTrail'
        assert 'AccessDenied' in str(ex2), 'Unexpected ClientError: {}'.format(ex2)


@pytest.mark.skip(reason='Having trouble with customer access to the rs-account-master bucket')
def test_customer_managed_restrictions(cloudtrail, bucket):
    """Verify functionality to user roles for customer managed CloudTrail Trails.

    :testid: cloudtrail-f-2-1
    :testid: cloudtrail-f-2-2

    :param Cloudtrail.Client cloudtrail:
        A handle to the CloudTrail APIs.
    :param S3.Bucket bucket:
        A handle to an S3 Bucket owned by the user.

    """

    create_response = {}
    try:
        create_response = cloudtrail.create_trail(
            Name="TestCustomerCloudTrail",
            S3BucketName=bucket.name,
            EnableLogFileValidation=False)

        assert create_response['ResponseMetadata']['HTTPStatusCode'] == 200, 'Failed to create trail'

        update_response = cloudtrail.update_trail(
            Name="TestCustomerCloudTrail",
            EnableLogFileValidation=True)

        assert update_response['LogFileValidationEnabled'], 'Failed to update trail'

        with pytest.raises(ClientError) as ex:
            cloudtrail.update_trail(Name="TestCustomerCloudTrail", S3BucketName='serviceforge-aws-admin-110-ct1')
            assert False, 'Had permission to use RHEDcloud managed S3Bucket with customer managed CloudTrail'
        assert 'AccessDenied' in str(ex), 'Unexpected ClientError: {}'.format(ex)
    except Exception:
        raise
    finally:
        if 'Name' in create_response:
            cloudtrail.delete_trail(Name=create_response['Name'])
