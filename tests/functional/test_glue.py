"""
=========
test_glue
=========

Verify that customers can use AWS Glue to read in data, transform it, and write
the results back out.

Plan:

* Create an S3 bucket and populate it with sample data
* Create a target S3 bucket
* Read data from source bucket
* Transform read data
* Write transformed data to target bucket

${testcount:3}

"""

from glob import glob
from os.path import basename
import io
import time

import pytest

from aws_test_functions import (
    get_uuid,
    new_bucket,
    new_role,
    retry,
)

#This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('get_connections')]

@pytest.fixture(scope='module')
def glue(session):
    """A shared handle to Glue APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Glue APIs.

    """

    return session.client('glue')


@pytest.fixture(scope='module')
def role():
    """Create a new IAM Role for use with Glue with correct policies for Glue.

    :yields:
        An IAM Role.

    """

    policies = (
        'arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole',
        'AmazonS3FullAccess',
    )

    with new_role('AWSGlueServiceRole-test', 'glue', *policies, Path='/service-role/') as r:
        yield r


@pytest.fixture(scope='module')
def s3(session):
    """A shared handle to S3 Service Resource APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the S3 Service Resource APIs.

    """

    return session.resource('s3')


@pytest.fixture(scope='module')
def target_bucket(s3):
    """Create a new S3 bucket to hold target data.

    :param S3.ServiceResource s3:
        A handle to the S3 Service Resource APIs.

    :yields:
        An S3 Bucket resource.

    """

    with new_bucket('aws-glue-target', s3=s3) as b:
        yield b


@pytest.fixture(scope='module')
def source_bucket(s3, db, target_bucket):
    """Create a new S3 bucket to hold source data.

    :param S3.ServiceResource s3:
        A handle to the S3 Service Resource APIs.
    :param str db:
        Name of the Glue Database to populate.
    :param S3.Bucket target_bucket:
        An S3 Bucket resource where results will be uploaded.

    :yields:
        An S3 Bucket resource.

    """

    with new_bucket('aws-glue-source', s3=s3) as b:
        # upload the script that will be executed by the job
        with open('./tests/resources/glue_join_and_rationalize.py') as fh:
            buf = io.BytesIO(fh.read().format(
                db=db,
                target_bucket=target_bucket,
            ).encode())
            b.upload_fileobj(buf, 'glue_join_and_rationalize.py', ExtraArgs={'ServerSideEncryption': 'AES256'})

        # populate bucket
        for filename in glob('./tests/resources/legislators/*.json'):
            print('Uploading source data: {}'.format(filename))
            b.upload_file(filename, basename(filename), ExtraArgs={'ServerSideEncryption': 'AES256'})

        yield b


@pytest.fixture(scope='module')
def db(glue):
    """Create a new AWS Glue Database to hold crawled data. The database is
    automatically removed when tests finish.

    :param Glue.Client glue:
        A handle to the Glue APIs.

    :yields:
        A string: the name of the created database.

    """

    name = 'test_glue-{}'.format(get_uuid())
    created = False

    try:
        glue.create_database(
            DatabaseInput=dict(
                Name=name,
            ),
        )
        created = True
        print('Created database: {}'.format(name))

        yield name
    finally:
        if created:
            print('Deleting database: {}'.format(name))
            glue.delete_database(Name=name)


@pytest.fixture(scope='module')
def crawler(glue, role, source_bucket, db):
    """Create a new AWS Glue crawler to read and parse data. The crawler is
    automatically removed when tests finish.

    :param Glue.Client glue:
        A handle to the Glue APIs.
    :param IAM.Role role:
        The IAM Role to use when crawling data.
    :param S3.Bucket source_bucket:
        The bucket where source data resides.
    :param str db:
        Name of the Glue Database where crawled data will be stored.

    :yields:
        A string: the name of the created crawler.

    """

    name = 'test_glue-{}'.format(get_uuid())
    created = False

    params = dict(
        Name=name,
        Role=role.arn,
        DatabaseName=db,
        Targets=dict(
            S3Targets=[dict(
                Path='s3://{}/'.format(source_bucket.name),
            )],
        ),
    )

    try:
        retry(
            glue.create_crawler,
            kwargs=params,
            msg='Creating crawler',
        )
        created = True
        print('Created crawler: {}'.format(name))

        yield name
    finally:
        if created:
            print('Deleting crawler: {}'.format(name))
            glue.delete_crawler(Name=name)


@pytest.fixture(scope='module')
def job(glue, source_bucket, role):
    """Create a new AWS Glue Job to transform the crawled data. The crawler is
    automatically removed when tests finish.

    :param Glue.Client glue:
        A handle to the Glue APIs.
    :param IAM.Role role:
        The IAM Role to use when crawling data.
    :param S3.Bucket source_bucket:
        The bucket where source data resides.
    :param str db:
        Name of the Glue Database where crawled data will be stored.

    :yields:
        A string: the name of the created crawler.

    """

    name = 'test_glue-{}'.format(get_uuid())
    script_path = 's3://{}/glue_join_and_rationalize.py'.format(source_bucket.name)
    created = False

    try:
        res = glue.create_job(
            Name=name,
            Role=role.arn,
            Command=dict(
                Name='glueetl',
                ScriptLocation=script_path,
            ),
        )
        assert 'Name' in res, 'Unexpected response: {}'.format(res)
        assert res['Name'] == name
        created = True
        print('Created job: {}'.format(name))

        yield name
    finally:
        if created:
            print('Deleting job: {}'.format(name))
            glue.delete_job(JobName=name)


@pytest.mark.slowtest
def test_read_data(glue, crawler):
    """Customers should be able to read data from a source S3 bucket using Glue
    APIs.

    :testid: glue-f-1-1

    :param Glue.Client glue:
        A handle to the Glue APIs.
    :param str crawler:
        Name of the Glue Crawler to check.

    """

    ready = False
    glue.start_crawler(Name=crawler)

    for attempt in range(30):
        print('Checking crawler status (attempt #{})'.format(attempt + 1))
        res = glue.get_crawler(Name=crawler)
        assert 'Crawler' in res, 'Unexpected response: {}'.format(res)
        info = res['Crawler']

        if info['State'] == 'READY':
            ready = True
            break

        time.sleep(15)

    assert ready, 'Crawler failed to return to READY state: {}'.format(info)


@pytest.mark.slowtest
def test_transform_fields(glue, job):
    """Customers should be able to create jobs to transform data using Glue
    APIs.

    :testid: glue-f-1-1

    :param Glue.Client glue:
        A handle to the Glue APIs.
    :param str job:
        Name of the Glue Job to run.

    """

    res = glue.start_job_run(JobName=job)
    assert 'JobRunId' in res, 'Unexpected response: {}'.format(res)

    success = False
    for attempt in range(60):
        print('Checking job state (attempt #{})'.format(attempt + 1))
        res = glue.get_job_runs(JobName=job)
        assert 'JobRuns' in res and len(res['JobRuns']), 'Unexpected response: {}'.format(res)
        info = res['JobRuns'][0]
        state = info['JobRunState']
        is_running = state in ('STARTING', 'RUNNING', 'STOPPING')

        if not is_running:
            success = state == 'SUCCEEDED'
            break

        time.sleep(30)

    assert success, 'Job did not enter SUCCEEDED state: {}'.format(info)


@pytest.mark.slowtest
def test_data_exists(target_bucket):
    """Customers should be able to store Glue Job results in S3 buckets.

    :testid: glue-f-1-1

    :param S3.Bucket target_bucket:
        The S3 bucket expected to hold the Glue Job results.

    """

    parquet_count = 0
    keys = [obj.key for obj in target_bucket.objects.all()]

    for key in keys:
        if key.endswith('.parquet'):
            parquet_count += 1

    assert parquet_count == 4, 'Did not find expected files in target bucket: {}'.format(keys)
