"""
============
test_appsync
============

Verify access to use AWS AppSync

Plan:

* Attempt to create a GraphQL API

${testcount:28}

"""

import pytest

from aws_test_functions import (
    make_identifier,
    get_or_create_role,
    has_status,
    ignore_errors,
    unexpected,
    retry,
)

pytestmark = [
    pytest.mark.scp_check('list_graphql_apis'),
]


graphql_fail = pytest.mark.xfail(reason='GraphQL is weird')


SCHEMA = b"""
schema {
    query: Query
    mutation: Mutation
}

type Query {
    # Get a single value of type 'Post' by primary key.
    singlePost(id: ID!): Post
}

type Mutation {
    # Put a single value of type 'Post'.
    # If an item exists it's updated. If it does not it's created.
    putPost(id: ID!, title: String!): Post
}

type Post {
    id: ID!
    title: String!
}
"""


@pytest.fixture(scope='module')
def appsync(session):
    """A shared handle to AppSync API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the AppSync API.

    """

    return session.client('appsync')


@pytest.fixture(scope='module')
def role():
    """Create a new role for AppSync.

    :returns:
        An IAM.Role.

    """

    return get_or_create_role(
        'appsync-service-role',
        'AWSAppSyncAdministrator',
        service=['appsync'],
        service_role=True,
    )


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    return {
        'api_name': make_identifier('test-appsync'),
        'api_id': '',
        'ds_name': make_identifier('test-appsync').replace('-', '_'),
        'ds_arn': '',
        'api_key': '',
    }


@pytest.fixture
def data_source_config(api_id, ds_name, role):
    """Return common configuration values for a data source.

    :param str api_id:
        ID of a GraphQL API.
    :param str ds_name:
        Name of the data source.
    :param IAM.Role role:
        Service Role resource.

    :returns:
        A dictionary.

    """

    return dict(
        apiId=api_id,
        name=ds_name,
        type='AMAZON_DYNAMODB',
        serviceRoleArn=role.arn,
        dynamodbConfig=dict(
            tableName='data',
            awsRegion='us-east-1',
        ),
    )


@pytest.fixture
def resolver_config(api_id, ds_name):
    """Return common configuration values for a resolver.

    :param str api_id:
        ID of a GraphQL API.
    :param str ds_name:
        Name of the data source.

    :returns:
        A dictionary.

    """

    return dict(
        apiId=api_id,
        typeName='Post',
        fieldName='title',
        dataSourceName=ds_name,
        requestMappingTemplate='''
            {
              "version": "2017-02-28",
              "operation": "PutItem",
              "key": {
                "id": $util.dynamodb.toDynamoDBJson($ctx.args.input.id),
              },
              "attributeValues": $util.dynamodb.toMapValuesJson($ctx.args.input),
              "condition": {
                "expression": "attribute_not_exists(#id)",
                "expressionNames": {
                  "#id": "id",
                },
              },
            }
        ''',
        responseMappingTemplate='''
            $util.toJson($context.result)
        ''',
    )


def test_create_graphql_api(appsync, api_name, stack, shared_vars):
    """Verify access to create GraphQL APIs.

    :testid: appsync-f-1-1, appsync-f-2-1

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param contextlib.ExitStack stack:
        A context manager shared across all AppSync tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = appsync.create_graphql_api(
        name=api_name,
        authenticationType='API_KEY',
    )
    assert 'graphqlApi' in res, unexpected(res)
    api_id = shared_vars['api_id'] = res['graphqlApi']['apiId']
    print('Created GraphQL API: {}'.format(api_id))
    print('Created GraphQL API: {}'.format(res))

    stack.callback(
        ignore_errors(test_delete_graphql_api),
        appsync, api_id,
    )


def test_list_graphql_apis(appsync):
    """Verify access to list GraphQL APIs.

    :testid: appsync-f-2-2

    :param AppSync.Client appsync:
        A handle to the AppSync API.

    """

    res = appsync.list_graphql_apis()
    assert has_status(res, 200), unexpected(res)
    assert 'graphqlApis' in res and res['graphqlApis'], unexpected(res)


def test_get_graphql_api(appsync, api_id, api_name):
    """Verify access to describe GraphQL APIs.

    :testid: appsync-f-2-3

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param str api_id:
        Name of a GraphQL API.

    """

    res = appsync.get_graphql_api(
        apiId=api_id,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'graphqlApi' in res and res['graphqlApi']['name'] == api_name, \
        unexpected(res)


def test_update_graphql_api(appsync, api_id, api_name):
    """Verify access to update GraphQL APIs.

    :testid: appsync-f-2-4

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param str api_id:
        Name of a GraphQL API.

    """

    res = appsync.update_graphql_api(
        apiId=api_id,
        name='Updated {}'.format(api_name),
        authenticationType='API_KEY',
    )
    assert has_status(res, 200), unexpected(res)


def test_start_schema_creation(appsync, api_id):
    """Verify access to create new schemas.

    :testid: appsync-f-1-2, appsync-f-3-1

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.

    """

    res = appsync.start_schema_creation(
        apiId=api_id,
        definition=SCHEMA,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'status' in res, unexpected(res)


def test_get_schema_creation_status(appsync, api_id):
    """Verify access to check schema creation status.

    :testid: appsync-f-3-2

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.

    """

    def _test(res):
        return \
            has_status(res, 200) and \
            'status' in res and \
            res['status'] in ('ACTIVE', 'SUCCESS')

    res = retry(
        appsync.get_schema_creation_status,
        kwargs=dict(
            apiId=api_id,
        ),
        until=_test,
        msg='Checking schema creation status',
    )
    assert _test(res), unexpected(res)


def test_get_introspection_schema(appsync, api_id):
    """Verify access to get the introspection schema.

    :testid: appsync-f-3-3

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.

    """

    res = appsync.get_introspection_schema(
        apiId=api_id,
        format='JSON',
    )
    assert has_status(res, 200), unexpected(res)
    assert 'schema' in res, unexpected(res)


def test_create_data_source(appsync, data_source_config, stack, shared_vars):
    """Verify access to create new data sources.

    :testid: appsync-f-1-3, appsync-f-4-1

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param dict data_source_config:
        Configuration for a data source.
    :param contextlib.ExitStack stack:
        A context manager shared across all AppSync tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = appsync.create_data_source(**data_source_config)
    assert has_status(res, 200), unexpected(res)
    assert 'dataSource' in res, unexpected(res)

    ds_arn = shared_vars['ds_arn'] = res['dataSource']['dataSourceArn']
    print('Created data source: {}'.format(ds_arn))

    stack.callback(
        ignore_errors(test_delete_data_source),
        appsync,
        data_source_config['apiId'],
        data_source_config['name'],
    )


def test_list_data_sources(appsync, api_id):
    """Verify access to list data sources.

    :testid: appsync-f-4-2

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.

    """

    res = appsync.list_data_sources(
        apiId=api_id,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'dataSources' in res and res['dataSources'], unexpected(res)


def test_get_data_source(appsync, api_id, ds_name, ds_arn):
    """Verify access to describe data sources.

    :testid: appsync-f-4-3

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param str ds_name:
        Name of a data source.
    :param str ds_arn:
        ARN of the data source.

    """

    res = appsync.get_data_source(
        apiId=api_id,
        name=ds_name,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'dataSource' in res and res['dataSource']['dataSourceArn'] == ds_arn, \
        unexpected(res)


def test_update_data_source(appsync, data_source_config):
    """Verify access to update data sources.

    :testid: appsync-f-4-4

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param dict data_source_config:
        Configuration for a data source.

    """

    res = appsync.update_data_source(
        description='Updated description',
        **data_source_config,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'dataSource' in res, unexpected(res)


def test_create_resolver(appsync, api_id, resolver_config, stack):
    """Verify access to create new resolvers.

    :testid: appsync-f-1-4, appsync-f-5-1

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param dict resolver_config:
        Configuration for a resolver.
    :param contextlib.ExitStack stack:
        A context manager shared across all AppSync tests.

    """

    res = appsync.create_resolver(**resolver_config)
    assert has_status(res, 200), unexpected(res)
    assert 'resolver' in res, unexpected(res)

    stack.callback(
        ignore_errors(test_delete_resolver),
        appsync, api_id,
    )


def test_list_resolvers(appsync, api_id):
    """Verify access to list resolvers.

    :testid: appsync-f-5-2

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.

    """

    res = appsync.list_resolvers(
        apiId=api_id,
        typeName='Post',
    )
    assert has_status(res, 200), unexpected(res)
    assert 'resolvers' in res, unexpected(res)


def test_get_resolver(appsync, api_id):
    """Verify access to describe resolvers.

    :testid: appsync-f-5-3

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.

    """

    res = appsync.get_resolver(
        apiId=api_id,
        typeName='Post',
        fieldName='title',
    )
    assert has_status(res, 200), unexpected(res)
    assert 'resolver' in res, unexpected(res)


def test_update_resolver(appsync, resolver_config):
    """Verify access to update resolvers.

    :testid: appsync-f-5-4

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param dict resolver_config:
        Configuration for a resolver.

    """

    key = 'requestMappingTemplate'
    resolver_config[key] = resolver_config[key].replace('#id', '#title')

    res = appsync.update_resolver(**resolver_config)
    assert has_status(res, 200), unexpected(res)
    assert 'resolver' in res, unexpected(res)


def test_create_api_key(appsync, api_id, stack, shared_vars):
    """Verify access to create API keys.

    :testid: appsync-f-6-1

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param contextlib.ExitStack stack:
        A context manager shared across all AppSync tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = appsync.create_api_key(
        apiId=api_id,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'apiKey' in res, unexpected(res)

    api_key = shared_vars['api_key'] = res['apiKey']['id']
    print('Created API key: {}'.format(api_key))
    print('Created API key: {}'.format(res))

    stack.callback(
        ignore_errors(test_delete_api_key),
        appsync, api_id, api_key,
    )


def test_list_api_keys(appsync, api_id):
    """Verify access to list API keys.

    :testid: appsync-f-6-2

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.

    """

    res = appsync.list_api_keys(
        apiId=api_id,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'apiKeys' in res and res['apiKeys'], unexpected(res)


def test_update_api_key(appsync, api_id, api_key):
    """Verify access to update API keys.

    :testid: appsync-f-6-3

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param str api_key:
        ID of a GraphQL API key.

    """

    res = appsync.update_api_key(
        apiId=api_id,
        id=api_key,
        description='new description',
    )
    assert has_status(res, 200), unexpected(res)


@graphql_fail
def test_create_request(appsync, api_id, api_key):
    """Verify access to create objects using a GraphQL API.

    :testid: appsync-f-7-1, appsync-f-1-5

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param str api_key:
        ID of a GraphQL API key.

    """

    assert False


@graphql_fail
def test_retrieve_request(appsync, api_id, api_key):
    """Verify access to retrieve objects using a GraphQL API.

    :testid: appsync-f-7-2, appsync-f-1-5

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param str api_key:
        ID of a GraphQL API key.

    """

    assert False


@graphql_fail
def test_update_request(appsync, api_id, api_key):
    """Verify access to update objects using a GraphQL API.

    :testid: appsync-f-7-3, appsync-f-1-5

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param str api_key:
        ID of a GraphQL API key.

    """

    assert False


@graphql_fail
def test_delete_request(appsync, api_id, api_key):
    """Verify access to update objects using a GraphQL API.

    :testid: appsync-f-7-4, appsync-f-1-5

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param str api_key:
        ID of a GraphQL API key.

    """

    assert False


@graphql_fail
def test_list_request(appsync):
    """Verify access to list objects using a GraphQL API.

    :testid: appsync-f-7-5, appsync-f-1-5

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param str api_key:
        ID of a GraphQL API key.

    """

    assert False


@graphql_fail
def test_query_request(appsync):
    """Verify access to query objects using a GraphQL API.

    :testid: appsync-f-7-6, appsync-f-1-5

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param str api_key:
        ID of a GraphQL API key.

    """

    assert False


def test_delete_api_key(appsync, api_id, api_key):
    """Verify access to delete API keys.

    :testid: appsync-f-6-4

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param str api_key:
        ID of a GraphQL API key.

    """

    print('Deleting API key: {}'.format(api_key))
    res = appsync.delete_api_key(
        apiId=api_id,
        id=api_key,
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_resolver(appsync, api_id):
    """Verify access to delete resolvers.

    :testid: appsync-f-5-5

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.

    """

    print('Deleting resolver for API: {}'.format(api_id))
    res = appsync.delete_resolver(
        apiId=api_id,
        typeName='Post',
        fieldName='title',
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_data_source(appsync, api_id, ds_name):
    """Verify access to delete data sources.

    :testid: appsync-f-4-5

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.
    :param str ds_name:
        Name of a data source.

    """

    print('Deleting data source: {}'.format(ds_name))
    res = appsync.delete_data_source(
        apiId=api_id,
        name=ds_name,
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_graphql_api(appsync, api_id):
    """Verify access to delete GraphQL APIs.

    :testid: appsync-f-2-5

    :param AppSync.Client appsync:
        A handle to the AppSync API.
    :param str api_id:
        ID of a GraphQL API.

    """

    print('Deleting GraphQL API: {}'.format(api_id))
    res = appsync.delete_graphql_api(
        apiId=api_id,
    )
    assert has_status(res, 200), unexpected(res)
