"""
===============
test_apigateway
===============

Verify restrictions to the AWS API Gateway API. This is a fully blocked
service,and as such, aws_blocked_service.py functionality is utilized
for testing, as opposed to marking each test with
pytest.mark.raises_access_denied

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Attempt to create a REST API
* Attempt to get a REST API resource
* Attempt to delete a REST API

${testcount:41}

"""

import json

import pytest

from aws_test_functions import (
    has_status,
    ignore_errors,
    make_identifier,
    unexpected,
)

pytestmark = [pytest.mark.scp_check("get_domain_names")]


@pytest.fixture(scope='module')
def apigateway(session):
    """A shared handle to API Gateway APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the API Gateway APIs.

    """

    return session.client('apigateway')


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """
    Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'api_id': '',
        'api_key': '',
        'model_name': '',
        'deployment_id': '',
        'resource_id': '',
        'doc_part_id': '',
        'doc_version': '',
        'stage_name': 'TestStage'
    }


def test_create_api(apigateway, stack, shared_vars):
    """Attempt to create a REST API

    :testid: apigateway-f-1-1

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param contextlib.ExitStack stack:
        A context manager shared across all API Gateway tests.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    res = apigateway.create_rest_api(
        name='TestRESTAPI',
    )
    assert has_status(res, 201), unexpected(res)

    api_id = shared_vars['api_id'] = res['id']
    print("Created REST API: {}".format(api_id))

    stack.callback(
        ignore_errors(apigateway.delete_rest_api),
        restApiId=api_id,
    )


def test_get_apis(apigateway):
    """Attempt to get available rest apis

    :testid: apigateway-f-1-4

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.

    """

    res = apigateway.get_rest_apis()
    assert len(res['items']) > 0


def test_get_api(apigateway, api_id):
    """Attempt to get specific rest api

    :testid: apigateway-f-1-5

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API

    """

    res = apigateway.get_rest_api(
        restApiId=api_id,
    )

    assert res is not None, 'Unable to get specific rest api'


def test_create_resource(apigateway, api_id, shared_vars):
    """Attempt to retrieve list of resources and
    create a REST API resource

    :testid: apigateway-f-6-1

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param dict shared_vars:
        dictionary shared between test functions

    """

    get_response = apigateway.get_resources(
        restApiId=api_id,
    )
    root = get_response['items'][0]

    res = apigateway.create_resource(
        restApiId=api_id,
        parentId=root['id'],
        pathPart='test'
    )
    assert has_status(res, 201), 'Unable to create resource: {}'.format(res)
    shared_vars['resource_id'] = res['id']


def test_create_apikey(apigateway, shared_vars):
    """Attempt to create a REST API resource

    :testid: apigateway-f-2-1

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.

    """

    res = apigateway.create_api_key(
        name=make_identifier('key')
    )
    assert has_status(res, 201), 'Unable to create api key'

    shared_vars['api_key'] = res['id']


def test_update_apikey(apigateway, api_key):
    """Attempt to update an API key

    :testid: apigateway-f-2-2

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_key:
        An API key.

    """

    res = apigateway.update_api_key(
        apiKey=api_key,
        patchOperations=[{
            'op': 'replace',
            'path': '/name',
            'value': 'value'
        }]
    )
    assert has_status(res, 200), unexpected(res)


def test_get_apikeys(apigateway):
    """ Attempt to retrieve list of api keys

    :testid: apigateway-f-2-4

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.

    """

    res = apigateway.get_api_keys()

    assert len(res['items']) > 0


def test_get_apikey(apigateway, api_key):
    """ Attempt to retrieve list of api keys

    :testid: apigateway-f-2-5

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.

    """

    res = apigateway.get_api_key(
        apiKey=api_key,
    )

    assert has_status(res, 200), 'Unable to get api key'


def test_delete_apikey(apigateway, api_key):
    """ Attempt to delete an API key

    :testid: apigateway-f-2-3

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_key:
        An API key.

    """

    res = apigateway.delete_api_key(
        apiKey=api_key,
    )

    assert has_status(res, 202), 'Unable to delete api key: {}'.format(res)


def test_create_model(apigateway, api_id, shared_vars):
    """Attempt to create an API model

    :testid: apigateway-f-3-1

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param dict shared_vars:
        dictionary shared between test functions

    """

    res = apigateway.create_model(
        restApiId=api_id,
        name='testmodel',
        schema=json.dumps({
            "$schema": "http://json-schema.org/draft-04/schema#",
            "title": "login",
            "type": "object",
            "properties": {
                "error_code": {
                    "type": "integer",
                },
                "error_message": {
                    "type": "string",
                },
            },
        }),
        contentType='application/json'
    )
    assert has_status(res, 201), 'Unable to create model: {}'.format(res)

    shared_vars['model_name'] = res['name']


def test_update_model(apigateway, api_id, model_name):
    """Attempt to update an API model

    :testid: apigateway-f-3-2

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str model_name:
        A model name.

    """

    res = apigateway.update_model(
        restApiId=api_id,
        modelName=model_name,
        patchOperations=[{
            'op': 'replace',
            'path': '/description',
            'value': ''
        }]
    )

    assert has_status(res, 200), 'Unable to update model'


def test_get_models(apigateway, api_id):
    """Attempt to retrieve list of API models

    :testid: apigateway-f-3-4

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API

    """

    res = apigateway.get_models(
        restApiId=api_id,
    )

    assert len(res['items']) > 0


def test_get_model(apigateway, api_id, model_name):
    """Attempt to get an API model

    :testid: apigateway-f-3-5

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str model_name:
        A model name.

    """

    res = apigateway.get_model(
        restApiId=api_id,
        modelName=model_name,
    )

    assert has_status(res, 200), 'Unable to get model'


def test_delete_model(apigateway, api_id, model_name):
    """Attempt to delete an API model

    :testid: apigateway-f-3-3

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str model_name:
        A model name

    """

    res = apigateway.delete_model(
        restApiId=api_id,
        modelName=model_name,
    )

    assert has_status(res, 202), 'Unable to delete model: {}'.format(res)


def test_create_documentation_part(apigateway, api_id, shared_vars):
    """Attempt to create, retrieve, update, delete a REST API documentation
    part.

    :testid: apigateway-f-4-1

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param dict shared_vars:
        dictionary shared between test functions

    """

    res = apigateway.create_documentation_part(
        restApiId=api_id,
        location={
            'type': 'API'
        },
        properties='{"description": "Test Description"}'
    )
    assert has_status(res, 201), 'Unable to create documentation part: {}'.format(res)

    shared_vars['doc_part_id'] = res['id']


def test_get_documentation_part(apigateway, api_id, doc_part_id):
    """Attempt to get REST API documentation part

    :testid: apigateway-f-4-5

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str doc_part_id:
        ID of a documentation part

    """

    res = apigateway.get_documentation_part(
        restApiId=api_id,
        documentationPartId=doc_part_id,
    )

    assert has_status(res, 200), 'Unable to get documentation part'


def test_get_documentation_parts(apigateway, api_id):
    """Attempt to get list of REST API documentation parts

    :testid: apigateway-f-4-4

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API

    """

    res = apigateway.get_documentation_parts(
        restApiId=api_id,
    )

    assert len(res['items']) > 0


def test_update_documentation_part(apigateway, api_id, doc_part_id):
    """Attempt to update REST API documentation part

    :testid: apigateway-f-4-2

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str doc_part_id:
        ID of a documentation part

    """

    res = apigateway.update_documentation_part(
        restApiId=api_id,
        documentationPartId=doc_part_id,
        patchOperations=[{
            'op': 'replace',
            'path': '/properties',
            'value': '{"description": "Updated Description"}'
        }]
    )

    assert has_status(res, 200), 'Unable to create documentation part'


def test_put_method(apigateway, api_id, resource_id):
    """Attempt to create a REST API method

    :testid: apigateway-f-9-1

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str resource_id:
        ID of a resource

    """

    res = apigateway.put_method(
        restApiId=api_id,
        resourceId=resource_id,
        httpMethod='GET',
        authorizationType='NONE'
    )

    assert has_status(res, 201), 'Unable to put method: {}'.format(res)


def test_update_method(apigateway, api_id, resource_id):
    """Attempt to update a REST API method

    :testid: apigateway-f-9-2

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str resource_id:
        ID of a resource

    """

    res = apigateway.update_method(
        restApiId=api_id,
        resourceId=resource_id,
        httpMethod='GET',
        patchOperations=[{
            'op': 'replace',
            'path': '/apiKeyRequired',
            'value': 'false'
        }]
    )

    assert has_status(res, 200), 'Unable to update method'


def test_get_method(apigateway, api_id, resource_id):
    """Attempt to get a REST API method

    :testid: apigateway-f-9-4

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str resource_id:
        ID of a resource

    """

    res = apigateway.get_method(
        restApiId=api_id,
        resourceId=resource_id,
        httpMethod='GET'
    )

    assert has_status(res, 200), 'Unable to update method'


def test_put_integration(apigateway, api_id, resource_id):
    """Attempt to put a REST API integration

    :testid: apigateway-f-8-1

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str resource_id:
        ID of a resource

    """

    res = apigateway.put_integration(
        restApiId=api_id,
        resourceId=resource_id,
        httpMethod='GET',
        type='MOCK'
    )

    assert has_status(res, 201), 'Unable to put integration: {}'.format(res)


def test_update_integration(apigateway, api_id, resource_id):
    """Attempt to update a REST API integration

    :testid: apigateway-f-8-2

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str resource_id:
        ID of a resource

    """

    res = apigateway.update_integration(
        restApiId=api_id,
        resourceId=resource_id,
        httpMethod='GET',
        patchOperations=[{
            'op': 'replace',
            'path': '/httpMethod',
            'value': 'GET'
        }]
    )

    assert has_status(res, 200), 'Unable to update integration'


def test_get_integration(apigateway, api_id, resource_id):
    """Attempt to get a REST API integration

    :testid: apigateway-f-8-4

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API.
    :param str resource_id:
        ID of a resouce.

    """

    res = apigateway.get_integration(
        restApiId=api_id,
        resourceId=resource_id,
        httpMethod='GET'
    )

    assert has_status(res, 200), 'Unable to get integration'


def test_create_deployment(apigateway, api_id, stage_name, stack, shared_vars):
    """Attempt to create a REST API deployment

    :testid: apigateway-f-5-1

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str stage_name:
        Name of a stage.
    :param contextlib.ExitStack stack:
        A context manager shared across all API Gateway tests.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    res = apigateway.create_deployment(
        restApiId=api_id,
        stageName=stage_name,
    )
    assert has_status(res, 201), 'Unable to create deployment'

    deployment_id = shared_vars['deployment_id'] = res['id']

    stack.callback(
        ignore_errors(test_delete_deployment),
        apigateway,
        api_id,
        deployment_id,
    )


def test_update_deployment(apigateway, api_id, deployment_id):
    """ Attempt to update a REST API deployment

    :testid: apigateway-f-5-2

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str deployment_id:
        ID of a deployment

    """

    res = apigateway.update_deployment(
        restApiId=api_id,
        deploymentId=deployment_id,
        patchOperations=[{
            'op': 'replace',
            'path': '/description',
            'value': 'Test'
        }]
    )

    assert has_status(res, 200), 'Unable to update deployment'


def test_get_deployments(apigateway, api_id):
    """ Attempt to retrieve list of API deployments

    :testid: apigateway-f-5-4

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API

    """

    res = apigateway.get_deployments(
        restApiId=api_id,
    )

    assert len(res['items']) > 0


def test_get_deployment(apigateway, api_id, deployment_id):
    """ Attempt to retrieve list of API deployments

    :testid: apigateway-f-5-5

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str deployment_id:
        ID of a deployment

    """

    res = apigateway.get_deployment(
        restApiId=api_id,
        deploymentId=deployment_id,
    )

    assert has_status(res, 200), 'Unable to get deployment'


def test_update_resource(apigateway, api_id, resource_id):
    """Attempt to create a REST API resource

    :testid: apigateway-f-6-2

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str resource_id:
        ID of a resource

    """

    res = apigateway.update_resource(
        restApiId=api_id,
        resourceId=resource_id,
        patchOperations=[{
            'op': 'replace',
            'path': '/pathPart',
            'value': 'updated'
        }]
    )

    assert has_status(res, 200), unexpected(res)


def test_get_resource(apigateway, api_id, resource_id):
    """Attempt to get a specific REST API resource

    :testid: apigateway-f-6-5

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str resource_id:
        ID of a resource

    """

    res = apigateway.get_resource(
        restApiId=api_id,
        resourceId=resource_id,
    )

    assert has_status(res, 200), 'Unable to get resource'


def test_create_documentation_version(apigateway, api_id, shared_vars):
    """Attempt to create an API documentation version

    :testid: apigateway-f-7-1

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param dict shared_vars:
        dictionary shared between test functions

    """

    res = apigateway.create_documentation_version(
        restApiId=api_id,
        documentationVersion='0.2'
    )
    assert has_status(res, 201), 'Unable to create documentation version'

    shared_vars['doc_version'] = res['version']


def test_update_documentation_version(apigateway, api_id, doc_version):
    """Attempt to create an API documentation version

    :testid: apigateway-f-7-2

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str doc_version:
        Documentation version

    """

    res = apigateway.update_documentation_version(
        restApiId=api_id,
        documentationVersion=doc_version,
        patchOperations=[{
            'op': 'replace',
            'path': '/description',
            'value': 'Test'
        }]
    )

    assert has_status(res, 200), 'Unable to update documentation version'


def test_get_documentation_version(apigateway, api_id, doc_version):
    """Attempt to get an API documentation version

    :testid: apigateway-f-7-5

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str doc_version:
        Documentation version

    """

    res = apigateway.get_documentation_version(
        restApiId=api_id,
        documentationVersion=doc_version,
    )

    assert has_status(res, 200), 'Unable to get documentation version'


def test_get_documentation_versions(apigateway, api_id):
    """Attempt to get list of API documentation version

    :testid: apigateway-f-7-4

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API

    """

    res = apigateway.get_documentation_versions(
        restApiId=api_id,
    )

    assert len(res['items']) > 0


def test_delete_integration(apigateway, api_id, resource_id):
    """Attempt to delete a REST API integration

    :testid: apigateway-f-8-3

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str resource_id:
        ID of a resource

    """

    res = apigateway.delete_integration(
        restApiId=api_id,
        resourceId=resource_id,
        httpMethod='GET'
    )

    assert has_status(res, 204), 'Unable to delete integration: {}'.format(res)


def test_delete_method(apigateway, api_id, resource_id):
    """Attempt to delete a REST API method

    :testid: apigateway-f-9-3

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str resource_id:
        ID of a resource

    """

    res = apigateway.delete_method(
        restApiId=api_id,
        resourceId=resource_id,
        httpMethod='GET'
    )

    assert has_status(res, 204), 'Unable to delete method: {}'.format(res)


def test_delete_resource(apigateway, api_id, resource_id):
    """Attempt to delete a REST API resource

    :testid: apigateway-f-6-3

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str resource_id:
        ID of a resource

    """

    res = apigateway.delete_resource(
        restApiId=api_id,
        resourceId=resource_id,
    )

    assert has_status(res, 202), 'Unable to delete resource'


def test_delete_documentation_version(apigateway, api_id, doc_version):
    """Attempt to delete REST API documentation version

    :testid: apigateway-f-7-3

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str doc_version:
        Documentation version

    """

    res = apigateway.delete_documentation_version(
        restApiId=api_id,
        documentationVersion=doc_version,
    )

    assert has_status(res, 202), 'Unable to delete documentation version'


def test_delete_documentation_parts(apigateway, api_id, doc_part_id):
    """Attempt to delete REST API documentation part

    :testid: apigateway-f-4-3

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str doc_part_id:
        ID of documentation parts

    """

    res = apigateway.delete_documentation_part(
        restApiId=api_id,
        documentationPartId=doc_part_id,
    )

    assert has_status(res, 202), 'Unable to delete documentation part'


def test_delete_stage(apigateway, api_id, stage_name):
    """Attempt to delete stages.

    :testid: apigateway-f-5-3

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str stage_name:
        Name of a stage

    """

    res = apigateway.delete_stage(
        restApiId=api_id,
        stageName=stage_name,
    )

    assert has_status(res, 202), 'Unable to delete stage'


def test_delete_deployment(apigateway, api_id, deployment_id):
    """Attempt to delete API deployments.

    :testid: apigateway-f-5-3

    :param APIGateway.Client apigateway:
        A handle to the API Gateway APIs.
    :param str api_id:
        ID of an API
    :param str deployment_id:
        ID of a deployment

    """

    res = apigateway.delete_deployment(
        restApiId=api_id,
        deploymentId=deployment_id,
    )

    assert has_status(res, 202), 'Unable to delete deployment'
