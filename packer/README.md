# Pre-built AMIs

This directory contains [Packer](https://www.packer.io/) templates for creating
custom AMIs that simplify the burden of executing commands within the AWS
environment.

## Building An AMI

The process of building a new AMI is quite simple. The basic steps include:

* Downloading the latest version of [Packer](https://www.packer.io/downloads.html)
* Creating or updating the appropriate template, such as `rds.json`
* Building the image: `AWS_PROFILE=rhedcloud-serviceforge-110 packer build rds.json`

  Note the explicit `AWS_PROFILE` environment variable. Per the documentation
  in the root of this repository, the default AWS Profile for this project
  should be `rhedcloud-serviceforge-100`. If you do not override this environment
  variable, the AMI will be created in the wrong AWS account.
* Updating the code to reference the AMI ID provided by Packer when the build
  finishes successfully. Currently this means just updating `AMI_ID` in
  `rhedcloud-aws-python-testutils/aws_test_functions/plugin/aws_script_runner.py`.

## RDS Testing

The `rds.json` template builds an image that includes the command line clients
for the following RDS engines:

* MySQL/MariaDB (`mysql`)
* PostgreSQL (`psql`)
* Oracle (`sqlplus`)
* SQL Server (`sqlcmd`)
* Telnet(`telnet`)
* Redis(`redis-cli`)

After it installs all of these clients, it sets some environment variables
which come in handy when using EC2 instance created from the AMI. Finally, it
prepares the AMI to be used with cloud-init again.
