"""
================
test_elasticache
================

Verify the ability to utlize ElastiCache services with Redis, and that services are blocked with Memcached(Currently Memcached is not blocked).

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Create a security group with the proper inbound rules for Redis Access(port 6379)
* Launch the Elasticache Service with a Redis Replication Group with Encryption enabled for HIPAA Compliancy
* From an EC2 Instance within the same VPC/Subnet that the Elasticache Group was launched, and proper access controls in place, access the Redis Group
* Test Commands on the Redis Group
* Launch the Elasticache Service with a Memcached Cluster(Should Fail, currently does not)

${testcount:5}

"""

import copy
import pytest

from aws_test_functions import (
    build_command_runner,
    new_security_group,
)


@pytest.fixture(scope='module')
def ec2(session):
    """A shared handle to EC2 APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the EC2 APIs.

    """

    return session.client('ec2')


@pytest.fixture(scope='module')
def elasticache(session):
    """A shared handle to Elasticache APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Elasticache APIs.

    """

    return session.client('elasticache')


@pytest.fixture(scope='module')
def security_group(session, ec2, vpc_id):
    """Creates a Security Group in the given vpc allowing ingress on port 6379

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        The AWS VPC ID to create the security group in
    :returns:
        A string.

    """

    with new_security_group(vpc_id, 'test_elasticache', ec2=ec2) as sg:
        ec2.authorize_security_group_ingress(
            GroupId=sg.group_id,
            IpProtocol='tcp',
            FromPort=6379,
            ToPort=6379,
            CidrIp='0.0.0.0/0',
        )

        yield sg


@pytest.fixture(scope='module')
def create_subnet_group(elasticache, internal_subnet_id, name="TestElastiCacheSubnetGroup"):
    """Create cache subnet group for ElastiCache

    :param ElastiCache.Client elasticache:
        A handle to the ElastiCache APIs.
    :param str internal_subnet_id:
        The ID of the subnet where the subnet group will be deployed.
    :param str name(optional):
        The name to use for the Subnet Group

    """

    resp = {}
    try:
        resp = elasticache.create_cache_subnet_group(CacheSubnetGroupName=name,
                                                     CacheSubnetGroupDescription='AWS Org SCP ElastiCache test subnet group',
                                                     SubnetIds=[internal_subnet_id])
        assert resp['ResponseMetadata']['HTTPStatusCode'] == 200, 'Failed to Create Subnet Group'
        yield resp
    except Exception:
        raise
    finally:
        delete_subnet_group_resp = {}
        if 'ResponseMetadata' in resp and resp['ResponseMetadata']['HTTPStatusCode'] == 200:
            delete_subnet_group_resp = elasticache.delete_cache_subnet_group(CacheSubnetGroupName=name)
        if 'ResponseMetadata' in delete_subnet_group_resp:
            assert delete_subnet_group_resp['ResponseMetadata']['HTTPStatusCode'] == 200, 'Failed to Delete Subnet Group'


@pytest.mark.slowtest
def test_elasticache_redis(session, ec2, elasticache, vpc_id, create_subnet_group, internal_subnet_id, security_group, internal_runner):
    """Create an ElastiCache group utilizing the Redis engine and test the functionality with access from an EC2 Instance

    :testid: db-ec-1-redis

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param ElastiCache.Client elasticache:
        A handle to the ElastiCache APIs.
    :param str vpc_id:
        The AWS VPC ID to create the security group in
    :param dict create_subnet_group:
        A dependency on the fixture that creates the subnet group for ElastiCache
    :param str internal_subnet_id:
        The ID of the subnet where the subnet group will be deployed.
    :param boto3.EC2.SecurityGroup security_group:
        A SecurityGroup to allow communication for Redis.
    :param callable internal_runner:
        A dependency on the fixture that sets up an EC2 instance for running
        commands using SSM.
    """

    create_replication_group_resp = {}
    previous_groups = []
    internal_instance = internal_runner.instance
    for group in internal_instance.security_groups:
        previous_groups.append(group['GroupId'])

    groups = copy.deepcopy(previous_groups)
    groups.append(security_group.id)
    try:
        ec2.modify_instance_attribute(InstanceId=internal_instance.instance_id, Groups=groups)

        create_replication_group_resp = elasticache.create_replication_group(ReplicationGroupId='TestRedisGroup',
                                                                             ReplicationGroupDescription='AWS Org SCP ElastiCache test redis cluster',
                                                                             CacheNodeType='cache.t2.micro',
                                                                             Engine='redis',
                                                                             EngineVersion='3.2.6',
                                                                             CacheSubnetGroupName='TestElastiCacheSubnetGroup',
                                                                             SecurityGroupIds=[security_group.id],
                                                                             Port=6379,
                                                                             TransitEncryptionEnabled=True,
                                                                             AtRestEncryptionEnabled=True,
                                                                             NumCacheClusters=1)
        elasticache.get_waiter('replication_group_available').wait(ReplicationGroupId='TestRedisGroup')
        assert create_replication_group_resp['ResponseMetadata']['HTTPStatusCode'] == 200, 'Failed to Create Redis Cluster'
        assert create_replication_group_resp['ReplicationGroup']['TransitEncryptionEnabled'], 'Failed to enable Transit Encryption'
        assert create_replication_group_resp['ReplicationGroup']['AtRestEncryptionEnabled'], 'Failed to enable At Rest Encryption'

        cluster_desc = elasticache.describe_replication_groups()
        endpoint = cluster_desc['ReplicationGroups'][0]['NodeGroups'][0]['PrimaryEndpoint']['Address']
        port = cluster_desc['ReplicationGroups'][0]['NodeGroups'][0]['PrimaryEndpoint']['Port']
        assert port == 6379, "Replication Group created on incorrect port"

        openssl_script = "#!/bin/bash\n{\nsleep 2\necho SET a 'HelloOpenSSL'\n\
sleep 2\necho GET a\nsleep 2\n\
echo quit\nsleep 2\n} | \
openssl s_client -connect " + endpoint + ":" + str(port) + "\nsleep 2\nexit"

        script_runner = build_command_runner(internal_instance)
        output = script_runner(openssl_script, timeout=60, delay=5, max_attempts=60)
        expected_output1 = "HelloOpenSSL"
        expected_output2 = "+OK"
        expected_output3 = "closed"
        expected_exit = "verify return:1"
        assert expected_output1 in output, "Redis Commands Failed, or OpenSSL connection Failed {}".format(output)
        assert expected_output2 in output, "Redis Commands Failed, or OpenSSL connection Failed {}".format(output)
        assert expected_output3 in output, "Redis Commands Failed, or OpenSSL connection Failed {}".format(output)
        assert expected_exit in output, "OpenSSL connection Failed {}".format(output)
    except Exception:
        raise
    finally:
        # Cleanup in reverse order
        delete_replication_resp = {}
        if 'ResponseMetadata' in create_replication_group_resp and create_replication_group_resp['ResponseMetadata']['HTTPStatusCode'] == 200:
            delete_replication_resp = elasticache.delete_replication_group(ReplicationGroupId='TestRedisGroup')
            elasticache.get_waiter('replication_group_deleted').wait(ReplicationGroupId='TestRedisGroup')

        if 'ResponseMetadata' in delete_replication_resp:
            assert delete_replication_resp['ResponseMetadata']['HTTPStatusCode'] == 200, 'Failed to Delete Redis Cluster'

        ec2.modify_instance_attribute(InstanceId=internal_instance.instance_id, Groups=previous_groups)


@pytest.mark.skip(reason="Proper Security Controls have not yet been put in place for blocking deployment of Memcached clusters")
# FIXME: Do this probably once Security Controls are in place @pytest.mark.raises_access_denied
def test_elasticache_memcached(session, elasticache, create_subnet_group, internal_subnet_id, security_group):
    """Create an ElastiCache group utilizing the Memcached engine and test that one cannot be deployed in this configuration.

    :testid: db-ec-1-mem

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param ElastiCache.Client elasticache:
        A handle to the ElastiCache APIs.
    :param dict create_subnet_group:
        A dependency on the fixture that creates the subnet group for ElastiCache
    :param str internal_subnet_id:
        The ID of the subnet where the subnet group will be deployed.
    :param boto3.EC2.SecurityGroup security_group:
        A SecurityGroup to allow communication for Redis.

    """

    create_cluster_resp = {}
    try:
        create_cluster_resp = elasticache.create_cache_cluster(CacheClusterId='TestMemcachedCluster',
                                                               CacheNodeType='cache.t2.micro',
                                                               Engine='memcached',
                                                               CacheSubnetGroupName='TestElastiCacheSubnetGroup',
                                                               SecurityGroupIds=[security_group.id],
                                                               Port=11211,
                                                               NumCacheNodes=1)
        elasticache.get_waiter('cache_cluster_available').wait(CacheClusterId='TestMemcachedCluster')
        assert False, 'Had permission to deploy Memcached ElasticCache cluster'
    except Exception:
        raise
    finally:
        # Cleanup in reverse order
        delete_cluster_resp = {}
        if 'ResponseMetadata' in create_cluster_resp and create_cluster_resp['ResponseMetadata']['HTTPStatusCode'] == 200:
            delete_cluster_resp = elasticache.delete_cache_cluster(CacheClusterId='TestMemcachedCluster')
            elasticache.get_waiter('cache_cluster_deleted').wait(CacheClusterId='TestMemcachedCluster')
        if 'ResponseMetadata' in delete_cluster_resp:
            assert delete_cluster_resp['ResponseMetadata']['HTTPStatusCode'] == 200, 'Failed to Delete Memcached Cluster'
