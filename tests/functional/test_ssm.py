"""
========
test_ssm
========

Verify ability to invoke AWS EC2 Systems Manager API.

Plan:

* Verify users can run commands
* Verify users can use the state manager
* Verify users can use the parameter store

As of 2018-07-16, SSM is blocked via IAM Policy. As such, it shouldn't be
tested as a fully-blocked service (because those are designed for SCP-based
blocks).

${testcount:22}

"""

import json

import pytest

from aws_test_functions import (
    build_command_runner,
    dict_to_filters,
    has_status,
    ignore_errors,
    unexpected,
    make_identifier,
)

bypass_access_denied = pytest.mark.bypass('raises_access_denied')


@pytest.fixture(scope="module")
def ssm(session):
    """A shared handle to the SSM API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the SSM API.

    """

    return session.client("ssm")


@pytest.fixture(scope="module")
def instance(internal_runner):
    """Return an EC2 instance that is setup for use with SSM.

    :param callable internal_runner:
        A dependency on the fixture that sets up an EC2 instance for running
        commands using SSM.

    """

    return internal_runner.instance


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    return dict(
        doc_name=make_identifier("test-ssm-doc"),

        # this is a copy of AWS-RunShellScript
        doc_content=json.dumps({
            "schemaVersion": "1.2",
            "description": "Run a shell script or specify the commands to run.",
            "parameters": {
                "commands": {
                    "type": "StringList",
                    "description": "(Required) Specify a shell script or a command to run.",
                    "minItems": 1,
                    "displayType": "textarea",
                },
                "workingDirectory": {
                    "type": "String",
                    "default": "",
                    "description": "(Optional) The path to the working directory on your instance.",
                    "maxChars": 4096,
                },
                "executionTimeout": {
                    "type": "String",
                    "default": "3600",
                    "description": "(Optional) The time in seconds for a command to complete before it is considered to have failed. Default is 3600 (1 hour). Maximum is 172800 (48 hours).",
                    "allowedPattern": "([1-9][0-9]{0,4})|(1[0-6][0-9]{4})|(17[0-1][0-9]{3})|(172[0-7][0-9]{2})|(172800)",
                },
            },
            "runtimeConfig": {
                "aws:runShellScript": {
                    "properties": [{
                        "id": "0.aws:runShellScript",
                        "runCommand": "{{ commands }}",
                        "workingDirectory": "{{ workingDirectory }}",
                        "timeoutSeconds": "{{ executionTimeout }}",
                    }]
                }
            },
        }),

        window_name=make_identifier("test-window"),
        window_id='twentytwentytwentytwenty20',

        command_id='thirtysixthirtysixthirtysixthirtysix',
    )


@pytest.mark.raises_access_denied
def test_run_command(ssm, instance):
    """Users should be able to use the EC2 Systems Manager's Run Command within
    the Type 1 VPC.

    :testid: ssm-f-1-1

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param EC2.Instance instance:
        An EC2 instance that is configured for SSM.

    """

    # use the test user's session explicitly
    run = build_command_runner(instance, ssm=ssm)
    output = run("echo teststring")

    assert output["Status"] == "Success", "Failed to execute command: {}".format(output)
    assert output["ExecutionElapsedTime"] is not None, "Unexpected duration: {}".format(
        output
    )
    assert output == "teststring\n", "Unexpected output: {}".format(output)


@pytest.mark.raises_access_denied
def test_state_manager(ssm, instance, stack):
    """Users should be able to use the EC2 System Manager's State Manager.

    :testid: ssm-f-1-2

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param EC2.Instance instance:
        An EC2 instance that is configured for SSM.
    :param contextlib.ExitStack stack:
        A context manager shared across all SSM tests.

    """

    doc = "AWS-RunShellScript"
    inst_id = instance.instance_id
    association_id = None

    res = ssm.create_association(
        Name=doc, InstanceId=inst_id, Parameters=dict(commands=["echo testing"])
    )
    assert "AssociationDescription" in res, unexpected(res)
    association = res["AssociationDescription"]
    association_id = association["AssociationId"]
    print("Created association {} for {}".format(association_id, inst_id))

    # automatically clean up the association
    stack.callback(
        ssm.delete_association,
        Name=doc,
        InstanceId=inst_id,
        AssociationId=association_id,
    )

    # find all associations for the current instance
    res = ssm.list_associations()
    assocs = {
        a["AssociationId"]: a["Name"]
        for a in res["Associations"]
        if a["InstanceId"] == inst_id
    }

    assert len(assocs), "No associations found for {}".format(inst_id)
    assert association_id in assocs, "Association {} not found".format(association_id)
    assert assocs[association_id] == doc, "Unexpected association: {}".format(assocs)


#@pytest.mark.raises_access_denied
def test_parameter_store(ssm, stack):
    """Users should be able to use the EC2 System Manager's Parameter Store.

    :testid: ssm-f-1-3

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param contextlib.ExitStack stack:
        A context manager shared across all SSM tests.

    """

    name = "/testing/param1"

    # create a new parameter
    res = ssm.put_parameter(
        Name=name,
        Value="a test value",
        Type="SecureString",
        # Overwrite=True,
    )
    assert "Version" in res, unexpected(res)

    # delete the parameter upon completion
    stack.callback(ssm.delete_parameter, Name=name)

    # describe parameter metadata
    res = ssm.describe_parameters(
        Filters=dict_to_filters({"Name": name}, key_name="Key")
    )
    assert "Parameters" in res and len(res["Parameters"]), unexpected(res)
    assert res["Parameters"][0]["Type"] == "SecureString", unexpected(res)

    # change parameter value
    res = ssm.put_parameter(
        Name=name, Value="new value", Type="SecureString", Overwrite=True
    )
    assert "Version" in res, unexpected(res)

    # verify changed value
    res = ssm.get_parameters(Names=[name], WithDecryption=True)
    assert "Parameters" in res and len(res["Parameters"]), unexpected(res)
    assert res["Parameters"][0]["Value"] == "new value", unexpected(res)


@pytest.mark.raises_access_denied
def test_create_document(ssm, doc_name, doc_content, stack):
    """Verify access to create documents.

    :testid: ssm-f-2-1

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str doc_name:
        Name of a document.
    :param str doc_content:
        Content of a document, as a JSON string.
    :param contextlib.ExitStack stack:
        A context manager shared across all SSM tests.

    """

    res = ssm.create_document(
        Name=doc_name,
        Content=doc_content,
        DocumentType="Command",
        DocumentFormat="JSON",
    )
    assert has_status(res, 200), unexpected(res)
    assert "DocumentDescription" in res, unexpected(res)

    print("Created document: {}".format(doc_name))

    stack.callback(ignore_errors(test_delete_document), ssm, doc_name)


@pytest.mark.raises_access_denied
def test_list_documents(ssm):
    """Verify access to list documents.

    :testid: ssm-f-2-2

    :param SSM.Client ssm:
        A handle to the SSM API.

    """

    res = ssm.list_documents()
    assert has_status(res, 200), unexpected(res)
    assert "DocumentIdentifiers" in res, unexpected(res)


@pytest.mark.raises_access_denied
def test_list_document_versions(ssm, doc_name):
    """Verify access to list versions of documents.

    :testid: ssm-f-2-3

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str doc_name:
        Name of a document.

    """

    res = ssm.list_document_versions(
        Name=doc_name,
    )
    assert has_status(res, 200), unexpected(res)
    assert "DocumentVersions" in res and len(res["DocumentVersions"]) == 1, \
        unexpected(res)


@pytest.mark.raises_access_denied
def test_describe_document(ssm, doc_name):
    """Verify access to describe a document.

    :testid: ssm-f-2-4

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str doc_name:
        Name of a document.

    """

    res = ssm.describe_document(
        Name=doc_name,
    )
    assert has_status(res, 200), unexpected(res)
    assert "Document" in res and "DocumentVersion" in res["Document"]
    assert res["Document"]["DocumentVersion"] == "1", unexpected(res)


@pytest.mark.raises_access_denied
def test_describe_document_permission(ssm, doc_name):
    """Verify access to describe document permissions.

    :testid: ssm-f-2-5

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str doc_name:
        Name of a document.

    """

    res = ssm.describe_document_permission(
        Name=doc_name,
        PermissionType='Share',
    )
    assert has_status(res, 200), unexpected(res)
    assert 'AccountIds' in res, unexpected(res)


@pytest.mark.raises_access_denied
def test_get_document(ssm, doc_name):
    """Verify access to get information about a document.

    :testid: ssm-f-2-6

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str doc_name:
        Name of a document.

    """

    res = ssm.get_document(
        Name=doc_name,
        DocumentFormat='JSON',
    )
    assert has_status(res, 200), unexpected(res)
    assert 'Content' in res, unexpected(res)
    assert json.loads(res['Content'])


@pytest.mark.raises_access_denied
def test_modify_document_permissions(ssm, doc_name):
    """Verify access to update document permissions.

    :testid: ssm-f-2-7

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str doc_name:
        Name of a document.

    """

    res = ssm.modify_document_permission(
        Name=doc_name,
        PermissionType='Share',
        AccountIdsToAdd=['All'],
    )
    assert has_status(res, 200), unexpected(res)

    # permissions must be reset so we can delete the document
    res = ssm.modify_document_permission(
        Name=doc_name,
        PermissionType='Share',
        AccountIdsToRemove=['All'],
    )
    assert has_status(res, 200), unexpected(res)


@bypass_access_denied
def test_update_document(ssm, doc_name, doc_content):
    """Verify access to update documents.

    :testid: ssm-f-2-8

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str doc_name:
        Name of a document.
    :param str doc_content:
        Content of a document, as a JSON string.

    """

    # commands cannot be updated
    with pytest.raises(Exception, match='InvalidDocument'):
        res = ssm.update_document(
            Name=doc_name,
            Content=doc_content.replace("commands", "magic"),
            DocumentVersion='$LATEST',
        )
        assert not has_status(res, 200), unexpected(res)


@pytest.mark.raises_access_denied
def test_update_document_default_version(ssm, doc_name):
    """Verify access to set the default version of a document.

    :testid: ssm-f-2-9

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str doc_name:
        Name of a document.

    """

    # commands cannot be updated
    # with pytest.raises(Exception, match='InvalidDocumentSchemaVersion'):
    res = ssm.update_document_default_version(
        Name=doc_name,
        DocumentVersion='2',
    )
    assert not has_status(res, 200), unexpected(res)


@pytest.mark.raises_access_denied
def test_delete_document(ssm, doc_name):
    """Verify access to delete documents.

    :testid: ssm-f-2-10

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str doc_name:
        Name of a document.

    """

    print("Deleting document: {}".format(doc_name))
    res = ssm.delete_document(Name=doc_name)
    assert has_status(res, 200), unexpected(res)


@pytest.mark.raises_access_denied
def test_create_maintenance_window(ssm, window_name, stack, shared_vars):
    """Verify access to create maintenance windows.

    :testid: ssm-f-3-1

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str window_name:
        Name of a maintenance window.
    :param contextlib.ExitStack stack:
        A context manager shared across all SSM tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = ssm.create_maintenance_window(
        Name=window_name,
        Schedule='cron(0 0 0 ? * * *)',
        Duration=2,
        Cutoff=1,
        AllowUnassociatedTargets=True,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'WindowId' in res, unexpected(res)

    window_id = shared_vars['window_id'] = res['WindowId']
    print('Created maintenance window: {}'.format(window_id))

    stack.callback(
        ignore_errors(test_delete_maintenance_window),
        ssm, window_id,
    )


@pytest.mark.raises_access_denied
def test_describe_maintenance_windows(ssm):
    """Verify access to describe maintenance windows.

    :testid: ssm-f-3-2

    :param SSM.Client ssm:
        A handle to the SSM API.

    """

    res = ssm.describe_maintenance_windows()
    assert has_status(res, 200), unexpected(res)
    assert 'WindowIdentities' in res, unexpected(res)


@pytest.mark.raises_access_denied
def test_get_maintenance_window(ssm, window_id, window_name):
    """Verify access to describe a maintenance window.

    :testid: ssm-f-3-3

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str window_id:
        ID of a maintenance window.
    :param str window_name:
        Name of a maintenance window.

    """

    res = ssm.get_maintenance_window(
        WindowId=window_id,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'Name' in res, unexpected(res)
    assert res['Name'] == window_name


@pytest.mark.raises_access_denied
def test_update_maintenance_window(ssm, window_id):
    """Verify access to update maintenance windows.

    :testid: ssm-f-3-4

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str window_id:
        ID of a maintenance window.

    """

    res = ssm.update_maintenance_window(
        WindowId=window_id,
        Description='testing',
    )
    assert has_status(res, 200), unexpected(res)
    assert 'Name' in res, unexpected(res)


@pytest.mark.raises_access_denied
def test_delete_maintenance_window(ssm, window_id):
    """Verify access to delete maintenance windows.

    :testid: ssm-f-3-5

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param str window_id:
        ID of a maintenance window.

    """

    print('Deleting maintenance window: {}'.format(window_id))
    res = ssm.delete_maintenance_window(
        WindowId=window_id,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.raises_access_denied
def test_list_commands(ssm):
    """Verify access to list commands.

    :testid: ssm-f-4-1

    :param SSM.Client ssm:
        A handle to the SSM API.

    """

    res = ssm.list_commands()
    assert has_status(res, 200), unexpected(res)
    assert 'Commands' in res, unexpected(res)


@pytest.mark.raises_access_denied
def test_send_command(ssm, instance, shared_vars):
    """Verify access to execute commands via SSM.

    :testid: ssm-f-4-2

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param EC2.Instance instance:
        An EC2 instance that is configured for SSM.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = ssm.send_command(
        InstanceIds=[instance.instance_id],
        DocumentName='AWS-RunShellScript',
        Parameters={
            'commands': ['date'],
            'executionTimeout': ['30'],
        },
    )
    assert has_status(res, 200), unexpected(res)
    assert 'Command' in res, unexpected(res)

    shared_vars['command_id'] = res['Command']['CommandId']


@pytest.mark.raises_access_denied
def test_list_command_invocations(ssm):
    """Verify access to list executed commands.

    :testid: ssm-f-4-3

    :param SSM.Client ssm:
        A handle to the SSM API.

    """

    res = ssm.list_command_invocations()
    assert has_status(res, 200), unexpected(res)
    assert 'CommandInvocations' in res, unexpected(res)


@pytest.mark.raises_access_denied
def test_get_command_invocation(ssm, instance, command_id):
    """Verify access to get information about an executed command.

    :testid: ssm-f-4-4

    :param SSM.Client ssm:
        A handle to the SSM API.
    :param EC2.Instance instance:
        An EC2 instance that is configured for SSM.
    :param str window_id:
        ID of a maintenance window.

    """

    res = ssm.get_command_invocation(
        CommandId=command_id,
        InstanceId=instance.instance_id,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'DocumentName' in res, unexpected(res)
