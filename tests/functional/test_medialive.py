"""
==============
test_medialive
==============

Verify users can utilize AWS MediaLive functionality.

${testcount:9}

* Attempt to create a MediaLive input security group
* Attempt to create a MediaLive input
* Attempt to create and start a MediaLive channel
* Ensure that the MediaLive channel has two healthy pipelines runnings
* Attempt to stop and delete the MediaLive channel
* Attempt to delete the MediaLive input
* Attempt to delete the MediaLive input security group

"""

import pytest

from aws_test_functions import (
    aws_client,
    find_objects_in_bucket,
    has_status,
    new_medialive_channel,
    new_medialive_input,
    new_medialive_input_security_group,
    new_role,
    new_policy,
    retry
)
from aws_test_functions.mediapackage import create_channels_and_endpoints

from tests.functional.test_mediaconvert import create_job

pytestmark = [
    # The AWS Elemental Media suite(MediaConvert, MediaPackage, and MediaLive)
    # require about 10 minutes on average, so they're marked as slow tests.
    # Most of the time is spent on the MediaLive channel startup about 8
    # minutes, though 2 minutes are spent on the conversion as well, so those
    # two tests are marked slow
    pytest.mark.slowtest,

    # This service is blocked in the HIPAA repository
    pytest.mark.scp_check('list_channels')
]

pytest_plugins = [
    'tests.plugins.mediaconvert',
    'tests.plugins.mediapackage'
]


@pytest.fixture(scope='module')
def medialive(session):
    """A shared handle to MediaLive API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the MediaLive API.

    """

    return session.client('medialive')


@pytest.fixture(scope='module')
def s3(session):
    """A shared handle to S3 APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the S3 APIs.

    """

    return session.client('s3')


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'dest_settings': [],

        # The MediaLive input security group ID
        'input_sg_id': '',

        # The MediaLive input ID
        'input_id': '',

        # The MediaLive channel ID
        'channel_id': '',
    }


@aws_client('medialive')
def cleanup_channels(*, medialive=None):
    """Attempt to remove any unused channels and components."""
    res = medialive.list_channels()
    for channel in res['Channels']:
        channel_id = channel['Id']
        status = medialive.describe_channel(ChannelId=channel_id)
        if status['State'] == 'RUNNING':
            medialive.stop_channel(ChannelId=channel_id)
            status = retry(
                medialive.describe_channel,
                kwargs={'ChannelId': channel_id},
                msg='Waiting for MediaLive channel {} to stop'.format(channel_id),
                until=lambda s: s['State'] != 'STOPPING',
                delay=10,
                max_attempts=15
            )

        assert status['State'] == 'IDLE', "Failed to stop MediaLive channel {} with status {}".format(channel_id, status['State'])
        res = medialive.delete_channel(ChannelId=channel_id)
        assert has_status(res, 200, 201)
        status = retry(
            medialive.describe_channel,
            kwargs={'ChannelId': channel_id},
            msg='Waiting for MediaLive channel {} to finish deleting'.format(channel_id),
            until=lambda s: s['State'] == 'DELETED',
            delay=10,
            max_attempts=15
        )
        assert status['State'] == 'DELETED', "Failed to delete MediaLive channel {} with status {}".format(channel_id, status['State'])

    res = medialive.list_inputs()
    for ml_input in res['Inputs']:
        input_id = ml_input['Id']
        res = medialive.delete_input(InputId=input_id)

    res = medialive.list_input_security_groups()
    for input_sg in res['InputSecurityGroups']:
        input_sg_id = input_sg['Id']
        res = medialive.delete_input_security_group(InputSecurityGroupId=input_sg_id)


@pytest.fixture(scope='module')
def medialive_role(stack):
    """Create a new role with necessary permissions for AWS MediaLive.

    :param contextlib.ExitStack stack:
        A context manager shared across all Batch tests.

    :returns:
        An IAM.Role.

    """

    stmt = [{
        "Effect": "Allow",
        "Action": [
            "mediastore:ListContainers",
            "mediastore:PutObject",
            "mediastore:GetObject",
            "mediastore:DeleteObject",
            "mediastore:DescribeObject",
            "s3:ListBucket",
            "s3:PutObject",
            "s3:GetObject",
            "s3:DeleteObject",
            "ssm:Describe*",
            "ssm:Get*",
            "ssm:List*"
        ],
        "Resource": "*"
    }]

    p = stack.enter_context(new_policy('medialive', stmt))
    r = stack.enter_context(new_role('medialive', 'medialive', p.arn))

    yield r


def test_create_input_security_group(shared_vars):
    """A customer should be able to create a MediaLive input security group

    :testid: medialive-f-1-1

    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    with new_medialive_input_security_group(delete=False) as input_sg_id:
        shared_vars['input_sg_id'] = input_sg_id


def test_create_input(session, s3, mediaconvert, mediaconvert_role, convert_bucket, backup_bucket, shared_vars):
    """A customer should be able to create a MediaLive input

    :testid: medialive-f-1-2

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param S3.Client s3:
        A handle to the S3 APIs.
    :param MediaConvert.Client mediaconvert:
        A handle to the MediaConvert APIs.
    :param boto3.IAM.Role mediaconvert_role:
        A Role for running the MediaConvert job.
    :param S3.Bucket convert_bucket:
        A handle to an S3 Bucket owned by the user containing big_buck_bunny.mp4.
    :param S3.Bucket backup_bucket:
        A handle to another S3 Bucket owned by the user(ideally containing big_buck_bunny.mp4 though not necessary).
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    # Ensure we have something to stream
    create_job(session, s3, mediaconvert, None, mediaconvert_role, convert_bucket, backup_bucket)
    assert find_objects_in_bucket(convert_bucket.name, 'big_buck_bunny.m3u8')['big_buck_bunny.m3u8'], \
        'Unable to find primary converted HLS manifest'
    assert find_objects_in_bucket(backup_bucket.name, 'big_buck_bunny.m3u8')['big_buck_bunny.m3u8'], \
        'Unable to find backup converted HLS manifest'

    primary_source = 's3://{}/{}'.format(convert_bucket.name, 'big_buck_bunny.m3u8')
    backup_source = 's3://{}/{}'.format(backup_bucket.name, 'big_buck_bunny.m3u8')
    with new_medialive_input('test_ml_input',
                             input_sg_id=shared_vars['input_sg_id'],
                             hls_url_primary=primary_source,
                             hls_url_backup=backup_source,
                             delete=False) as input_id:
        shared_vars['input_id'] = input_id


def test_create_channels_and_endpoints(mediapackage, stack, shared_vars):
    dest_settings = create_channels_and_endpoints(
        stack,
        mediapackage=mediapackage,
    )

    shared_vars['dest_settings'] = dest_settings


@pytest.mark.xfail(reason='Will not have settings in TEST_ORG when blocked')
def test_create_start_and_stop_channel(medialive_role, input_id, dest_settings, shared_vars):
    """A customer should be able to create a MediaLive channel

    :testid: medialive-f-1-3, medialive-f-1-4

    :param boto3.IAM.Role medialive_role:
        A Role for running MediaLive channels.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    assert len(dest_settings) == 2, 'Two destination settings are required'

    destination = dict(
        Id='TestMLDestination',
        Settings=dest_settings,
    )

    with new_medialive_channel(
        'test_ml_channel',
        destination=destination,
        input_id=input_id,
        role=medialive_role.arn,
        delete=False
    ) as channel_id:
        shared_vars['channel_id'] = channel_id


def test_pipeline_health(medialive, shared_vars):
    """The created MediaLive channel should have two healthy pipelines running

    :testid: medialive-f-1-5

    :param MediaLive.Client medialive:
        A handle to the MediaLive APIs.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    status = retry(
        medialive.describe_channel,
        kwargs={'ChannelId': shared_vars['channel_id']},
        msg='Checking Pipeline Health on Channel {}'.format(shared_vars['channel_id']),
        until=lambda s: s['PipelinesRunningCount'] == 2,
        delay=10,
        max_attempts=15
    )
    assert status['PipelinesRunningCount'] == 2, 'Failed to achieve Pipeline Health on MediaLive channel {}'.format(shared_vars['ChannelId'])


@pytest.fixture(scope='module', autouse=True)
def cleanup(session, medialive, shared_vars):
    """Automatically remove successfully created components.

    :testid: medialive-f-1-6

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param MediaLive.Client medialive:
        A handle to the medialive APIs.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    yield

    # this will be executed after all test functions in this module complete
    if shared_vars['channel_id']:
        channel_id = shared_vars['channel_id']
        status = medialive.describe_channel(ChannelId=channel_id)
        if status['State'] == 'RUNNING':
            medialive.stop_channel(ChannelId=channel_id)
            status = retry(
                medialive.describe_channel,
                kwargs={'ChannelId': channel_id},
                msg='Waiting for MediaLive channel {} to stop'.format(channel_id),
                until=lambda s: s['State'] != 'STOPPING',
                delay=10,
                max_attempts=15
            )
        assert status['State'] == 'IDLE', "Failed to stop MediaLive channel {} with status {}".format(channel_id, status['State'])

        res = medialive.delete_channel(ChannelId=channel_id)
        assert has_status(res, 200, 201)
        status = retry(
            medialive.describe_channel,
            kwargs={'ChannelId': channel_id},
            msg='Waiting for MediaLive channel {} to finish deleting'.format(channel_id),
            until=lambda s: s['State'] == 'DELETED',
            delay=10,
            max_attempts=15
        )
        assert status['State'] == 'DELETED', "Failed to delete MediaLive channel {} with status {}".format(channel_id, status['State'])

    if shared_vars['input_id']:
        res = medialive.delete_input(InputId=shared_vars['input_id'])
        assert has_status(res, 200, 201)

    if shared_vars['input_sg_id']:
        res = medialive.delete_input_security_group(InputSecurityGroupId=shared_vars['input_sg_id'])
        assert has_status(res, 200, 201)
