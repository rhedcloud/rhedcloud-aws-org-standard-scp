"""
========
test_waf
========

Verify that the basic WAF functionality is working

waf-f-1-1
Use WAF to block requests to a sample app

${testcount:8}

"""

import pytest

from aws_test_functions import (
    env,
    get_subnet_id,
    ignore_errors,
    make_identifier,
    new_ec2_instance,
    new_elb,
    retry,
    unexpected,
    wait_for_elb_state,
)
from aws_test_functions.waf import (
    cleanup_waf,
    delete_regex_pattern_set,
    delete_rule,
    delete_web_acl,
    get_token,
)

from tests.functional.test_elb import NGINX_AMI

# treat errors about nonexistent items as access denied errors (because they
# likely originate from an access denied error earlier in the suite).
ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden|WAFNonexistentItemException).*'

# This service is fully blocked
pytestmark = [pytest.mark.scp_check('list_web_acls')]


@pytest.fixture(scope='module')
def waf(session):
    """
    Return an WAF client

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A low-level client representing Amazon WAF

    """

    return session.client('waf-regional')


@pytest.fixture(scope='module', autouse=True)
def cleanup_module(waf):
    """Delete any web acls, match sets, and pattern sets created during
    previous tests.

    :param WAFRegional.Client waf:
        A handle to the WAF Regional API.

    """

    ignore_errors(cleanup_waf)(waf=waf)

    yield


@pytest.fixture(scope='module')
def subnet_ids(vpc_id):
    """Return a list of subnet IDs.

    :param str vpc_id:
        ID of a VPC.

    :returns:
        A list.

    """

    return [
        get_subnet_id(vpc_id, subnet)
        for subnet in env.RHEDCLOUD_DEFAULT_VPC_ENDPOINT_SUBNETS
    ]


@pytest.fixture(scope='module')
def instance(session, subnet_ids, stack):
    def _status(i):
        i.reload()
        return i.state['Name']

    inst = stack.enter_context(new_ec2_instance(
        'waf',
        ami=NGINX_AMI,
        session=session,
        SubnetId=subnet_ids[0]
    ))

    retry(
        _status,
        args=(inst,),
        msg='Waiting for all server to be in "running" state...',
        until=lambda r: r == 'running',
    )

    yield inst


@pytest.fixture(scope='module')
def load_balancer(session, subnet_ids, instance, stack):
    lb = stack.enter_context(new_elb(
        'application', 'internal',
        [instance],
        session=session,
        subnets=subnet_ids,
    ))

    wait_for_elb_state(lb, 'active')

    return lb


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    :param boto3.client A handle to Amazon WAF

    """

    return {
        'acl_id': 'not-created',
        'rule_id': 'not-created',
    }


def test_create_acl(waf, stack, shared_vars):
    """Create a web ACL

    :testid: waf-f-1-1

    :param WAFRegional.Client waf:
        A handle to the WAF Regional API.
    :param contextlib.ExitStack stack:
        A context manager shared across all WAF tests.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    # Create a web ACL, Name/MetricName params too simple for make_identifier()
    acl_response = waf.create_web_acl(
        Name='TestACL',
        MetricName='TestMetric',
        DefaultAction={
            'Type': 'ALLOW'
        },
        ChangeToken=get_token(waf=waf),
    )

    assert 'ChangeToken' in acl_response, 'Unable to create web acl'

    acl_id = shared_vars['acl_id'] = acl_response['WebACL']['WebACLId']
    print('Created Web ACL: {}'.format(acl_id))

    stack.callback(
        ignore_errors(delete_web_acl),
        acl_id,
        waf=waf,
    )


def test_create_rule(waf, acl_id, stack, shared_vars):
    """Create a string match condition for request paths beginning with /BAD
    Create a rule to block requests that match the previous condition

    :testid: waf-f-1-2

    :param WAFRegional.Client waf:
        A handle to the WAF Regional API.
    :param str acl_id:
        ID of an ACL.
    :param contextlib.ExitStack stack:
        A context manager shared across all WAF tests.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    # Create regex match set to use in update_regex call
    match_response = waf.create_regex_match_set(
        Name='TestRegex',
        ChangeToken=get_token(waf=waf)
    )

    # Create regex pattern set to use in update_regex call
    pattern_response = waf.create_regex_pattern_set(
        Name='TestPattern',
        ChangeToken=get_token(waf=waf)
    )

    # identifier results for update_regex_match_set/update_web_acl calls
    regex_id = match_response['RegexMatchSet']['RegexMatchSetId']
    stack.callback(
        ignore_errors(waf.delete_regex_match_set),
        RegexMatchSetId=regex_id,
        ChangeToken=get_token(waf=waf),
    )

    pattern_id = pattern_response['RegexPatternSet']['RegexPatternSetId']
    stack.callback(
        ignore_errors(delete_regex_pattern_set),
        pattern_id,
        waf=waf,
    )

    # Create a string match condition for request paths beginning with /BAD
    update_response = waf.update_regex_match_set(
        RegexMatchSetId=regex_id,
        Updates=[{
            'Action': 'INSERT',
            'RegexMatchTuple': {
                'FieldToMatch': {
                    'Type': 'URI',
                    'Data': '/BAD'
                },
                'TextTransformation': 'NONE',
                'RegexPatternSetId': pattern_id
            }
        }],
        ChangeToken=get_token(waf=waf)
    )

    rule_response = waf.create_rule(
        Name=make_identifier('rule', separator='', max_length=12),
        MetricName=make_identifier('metric', separator='', max_length=12),
        ChangeToken=get_token(waf=waf)
    )

    rule_id = shared_vars['rule_id'] = rule_response['Rule']['RuleId']
    print('Created rule: {}'.format(rule_id))

    stack.callback(
        ignore_errors(delete_rule),
        rule_id,
        waf=waf,
    )

    update_response = waf.update_rule(
        RuleId=rule_id,
        Updates=[{
            'Action': 'INSERT',
            'Predicate': {
                'Negated': False,
                'Type': 'RegexMatch',
                'DataId': regex_id
            }
        }],
        ChangeToken=get_token(waf=waf)
    )

    assert 'ChangeToken' in update_response, 'Unable to update rule'


def test_attach_rule(waf, acl_id, rule_id):
    """Add the rule to the web ACL

    :testid: waf-f-1-3

    :param WAFRegional.Client waf:
        A handle to the WAF Regional API.
    :param str acl_id:
        ID of an ACL.
    :param str rule_id:
        ID of a rule.

    """

    # boto3.set_stream_logger(name='botocore')
    attach_response = waf.update_web_acl(
        WebACLId=acl_id,
        Updates=[{
            'Action': 'INSERT',
            'ActivatedRule': {
                'Priority': 1,
                'RuleId': rule_id,
                'Action': {
                    'Type': 'BLOCK'
                }
            }
        }],
        DefaultAction={
            'Type': 'BLOCK'
        },
        ChangeToken=get_token(waf=waf)
    )

    assert 'ChangeToken' in attach_response, \
        'Unable to update web acl with rule'


def test_assoc_acl(waf, acl_id, load_balancer, stack, shared_vars):
    """Associate the web ACL with the ALB

    :testid: waf-f-1-4

    :param boto3.client A handle to Amazon WAF Regional API
    :param dict shared_vars: dictionary shared between test functions

    """

    arn = load_balancer['LoadBalancerArn']

    res = waf.associate_web_acl(
        WebACLId=acl_id,
        ResourceArn=arn
    )
    assert res, unexpected(res)
    print('Associated Web ACL {} with load balancer {}'.format(acl_id, arn))

    stack.callback(
        ignore_errors(waf.disassociate_web_acl),
        ResourceArn=arn,
    )
