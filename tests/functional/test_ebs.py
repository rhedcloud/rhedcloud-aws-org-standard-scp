"""
========
test_ebs
========

Verify that customers can create volumes using Elastic Block Storage.

Plan:

* Create an encrypted EBS volume in 3 different availability zones
* Create an unencrypted EBS volume in 3 different availability zones

* RHEDCloudSnapshotSharedBlock tests:

    * Create an encrypted EBS volume
    * Create a snapshot of the encrypted EBS volume
    * Confirm access to share the snapshot with another AWS account using the
      following roles:

        * RHEDcloudAuditorRole -- should fail with and without the SCP
        * RHEDcloudAdministratorRole -- should fail with the SCP
        * RHEDcloudCentralAdministratorRole -- should succeed
        * RHEDcloudMaintenanceOperatorRole -- should succeed
        * RHEDcloudSecurityIRRole -- should succeed
        * RHEDcloudSecurityRiskDetectionServiceRole -- should succeed

${testcount:13}

"""

from contextlib import ExitStack
import random
import time

import pytest

from aws_test_functions import (
    aws_client,
    ClientError,
    debug,
    get_or_create_kms_key_arn,
    has_status,
    new_ebs_snapshot,
    new_ebs_volume,
    unexpected,
)

pytestmark = pytest.mark.test_with_and_without_scp


@aws_client('ec2')
def get_azs(*, ec2=None):
    """Return 3 AZ names at random."""

    res = ec2.describe_availability_zones()
    all_azs = [az['ZoneName'] for az in res['AvailabilityZones']]
    random.shuffle(all_azs)

    return all_azs[:3]


@pytest.fixture(scope='module')
def ec2(session):
    return session.client('ec2')


@pytest.fixture(scope="function", params=[
    ("RHEDcloudAuditorRole", False, False),
    ("RHEDcloudAdministratorRole", True, False),
    ("RHEDcloudCentralAdministratorRole", True, True),
    ("RHEDcloudMaintenanceOperatorRole", True, True),
    ("RHEDcloudSecurityIRRole", True, True),
    ("RHEDcloudSecurityRiskDetectionServiceRole", True, True),
])
def role_expect(request, is_scp_active):
    """Return pairings of roles and whether each role is expected to be allowed
    to share snapshots based on whether the Service Control Policy is in
    effect.

    :returns:
        A 2-tuple containing the role name and a boolean.

    """

    name, no_scp, with_scp = request.param
    expect = with_scp if is_scp_active else no_scp

    return (name, expect)


@pytest.fixture
def rate_limit(role_expect):
    """Sleep for a little bit after each permutation of the test in order to
    avoid API rate limit errors.

    """

    yield

    # add a delay only if we're not on the last role passed to ``role_expect``
    if "RiskDetection" not in role_expect[0]:
        debug("Sleeping to avoid API rate limits...")
        time.sleep(60)


@pytest.dict_fixture
def shared_vars():
    return {
        "volume": None,
        "snapshot": None,
    }


@pytest.mark.parametrize('az', get_azs())
@pytest.mark.parametrize('encrypted', (True, False))
def test_volume(ec2, az, encrypted):
    """Customers should be able to create encrypted and unencrypted EBS volumes.

    :testid: ebs-f-1-1, ebs-f-1-2

    """

    with new_ebs_volume('test_ebs', az, encrypted=encrypted, ec2=ec2) as vol:
        assert vol is not None, 'invalid volume'


@pytest.mark.rhedcloudsnapshotshareblock
def test_prepare_snapshot(ec2, shared_vars, stack):
    """Prepare an EBS volume and snapshot for use in the next set of tests.

    :param EC2.Client ec2:
        A handle to the EC2 API.
    :param dict shared_vars:
        A collection used to share data across tests.
    :param ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    az = get_azs()[0]
    key_id = get_or_create_kms_key_arn()

    volume = stack.enter_context(new_ebs_volume(
        "test_ebs",
        az,
        encrypted=True,
        KmsKeyId=key_id,
        ec2=ec2,
    ))

    debug("waiting for volume to be available...")
    waiter = ec2.get_waiter('volume_available')
    waiter.wait(
        VolumeIds=[volume.id],
        DryRun=False,
        WaiterConfig={
            'Delay': 2,
            'MaxAttempts': 40,
        },
    )

    debug("creating snapshot...")
    snapshot = stack.enter_context(new_ebs_snapshot(
        volume_id=volume.id,
        ec2=ec2,
    ))

    shared_vars.update({
        "volume": volume,
        "snapshot": snapshot,
    })


@pytest.mark.rhedcloudsnapshotshareblock
def test_share_snapshot_as_role(
    snapshot,
    role_expect,
    master_account_number,
    assume_role,
    rate_limit,
):
    """Test whether a particular IAM Role has access to share a snapshot of an
    encrypted EBS volume with another AWS account.

    :param EC2.Snapshot snapshot:
        Information about an encrypted EBS volume snapshot.
    :param tuple role_expect:
        A 2-tuple containing a role name and whether that role is expected to
        be permitted to share the snapshot.
    :param str master_account_number:
        The numeric ID of another AWS account.
    :param callable assume_role:
        A function that helps create a boto3 session using a different IAM
        Role.

    """

    role, expect_success = role_expect

    debug("modifying snapshot as {} expecting success: {}...".format(role, expect_success))
    role_session = assume_role("role/rhedcloud/{}".format(role))
    role_ec2 = role_session.client("ec2")

    with ExitStack() as maybe:
        if not expect_success:
            maybe.enter_context(pytest.raises(ClientError, match="UnauthorizedOperation"))

        res = role_ec2.modify_snapshot_attribute(
            SnapshotId=snapshot.id,
            Attribute="createVolumePermission",
            CreateVolumePermission={
                'Add': [{
                    # 'Group': 'all',
                    "UserId": master_account_number,
                }],
            },
            # GroupNames=['all'],
            OperationType='add',
            # UserIds=[
            #     'string',
            # ],
            DryRun=False,
        )
        assert has_status(res, 200), unexpected(res)
