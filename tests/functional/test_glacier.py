"""
============
test_glacier
============

Verify ability to invoke AWS Glacier.

Plan:

* Verify users can store data in Glacier
* Verify users can retrieve data from Glacier
* Verify users can delete data from Glacier

${testcount:5}

"""

import pytest

from aws_test_functions import (
    cleanup_glacier_vaults,
    delete_glacier_archive,
    delete_glacier_vault,
    get_glacier_job_output,
    has_status,
    ignore_errors,
    make_identifier,
    unexpected,
)


pytestmark = [pytest.mark.scp_check('list_vaults')]


@pytest.fixture(scope='module')
def glacier(session):
    """A shared handle to Glacier APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Glacier APIs.

    """

    return session.client('glacier')


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    return {
        "vault_name": make_identifier("test_glacier"),
        "archive_id": "invalid",
    }


@pytest.fixture(scope='module')
def cleanup_vaults(glacier):
    """Attempt to clean up any old Vaults.

    :param Glacier.Client glacier:
        A handle to the Glacier APIs.

    :returns:
        An integer: the number of vaults removed.

    """

    return cleanup_glacier_vaults(glacier=glacier)


def test_create_vault(glacier, vault_name, stack, shared_vars):
    """Users should be able to create Vaults in Glacier.

    :testid: glacier-f-1-1

    :param Glacier.Client glacier:
        A handle to the Glacier APIs.
    :param str vault_name:
        The name of the test vault.
    :param contextlib.ExitStack stack:
        A context manager shared across all Glacier tests.
    :param dict shared_vars:
        Dictionary shared between test functions.

    """

    res = glacier.create_vault(vaultName=vault_name)
    assert has_status(res, 201), unexpected(res)
    assert 'location' in res, unexpected(res)

    print('Created vault: {}'.format(vault_name))

    stack.callback(
        ignore_errors(test_delete_vault),
        glacier,
        vault_name,
    )


def test_upload_to_vault(glacier, vault_name, stack, shared_vars):
    """Users should be able to upload files to Vaults in Glacier.

    :testid: glacier-f-1-1

    :param Glacier.Client glacier:
        A handle to the Glacier APIs.
    :param str vault_name:
        The name of the test vault.
    :param contextlib.ExitStack stack:
        A context manager shared across all Glacier tests.
    :param dict shared_vars:
        Dictionary shared between test functions.

    """

    upload = glacier.upload_archive(
        vaultName=vault_name,
        archiveDescription='Testing Glacier',
        body=b'this is a test',
    )
    assert has_status(upload, 201), unexpected(upload)

    for key in ('location', 'checksum', 'archiveId'):
        assert key in upload, unexpected(upload)

    archive_id = shared_vars["archive_id"] = upload["archiveId"]
    stack.callback(
        ignore_errors(test_delete_from_vault),
        glacier,
        vault_name,
        archive_id,
    )

    res = glacier.describe_vault(vaultName=vault_name)
    assert 'NumberOfArchives' in res, 'Upload not found in vault: {}'.format(res)

    print('Uploaded to vault {}: {} ({})'.format(vault_name, archive_id, upload['checksum']))


@pytest.mark.xfail(reason='Glacier requires minutes or hours to retrieve data')
def test_retrieve_from_vault(glacier, vault_name, archive_id):
    """Users should be able to retrieve files from Vaults in Glacier.

    :testid: glacier-f-1-2

    :param Glacier.Client glacier:
        A handle to the Glacier APIs.
    :param str vault_name:
        The name of the test vault.
    :param str archive_id:
        ID of an uploaded archive.

    """

    job = glacier.initiate_job(
        vaultName=vault_name,
        jobParameters=dict(
            Type='archive-retrieval',
            ArchiveId=archive_id,
        ),
    )
    assert 'jobId' in job, 'Unexpected response: {}'.format(job)
    job_id = job['jobId']

    res = get_glacier_job_output(vault_name, job_id, glacier=glacier)
    assert 'body' in res, 'Unexpected response: {}'.format(res)


@pytest.mark.raises_access_denied_needle(r'ResourceNotFound')
def test_delete_from_vault(glacier, vault_name, archive_id):
    """Users should be able to delete files from Vaults in Glacier.

    :testid: glacier-f-1-3

    :param Glacier.Client glacier:
        A handle to the Glacier APIs.
    :param str vault_name:
        The name of the test vault.
    :param str archive_id:
        ID of an uploaded archive.

    """

    res = delete_glacier_archive(vault_name, archive_id, glacier=glacier)
    assert res['ResponseMetadata']['HTTPStatusCode'] == 204, 'Unexpected status: {}'.format(res)
    print('Deleted from vault {}: {}'.format(vault_name, archive_id))


@pytest.mark.xfail(reason='Glacier complains about the vault being written to recently')
def test_delete_vault(glacier, vault_name):
    """Users should be able to delete Vaults from Glacier.

    :testid: glacier-f-1-3

    :param Glacier.Client glacier:
        A handle to the Glacier APIs.
    :param str vault_name:
        The name of the test vault.

    """

    res = delete_glacier_vault(vault_name, glacier=glacier)
    assert res is not None, 'Unexpected response: {}'.format(res)
