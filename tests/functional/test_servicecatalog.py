"""
===================
test_servicecatalog
===================

Verify access to AWS Service Catalog.

Plan:

* Create an S3 bucket
* Upload an image to the new S3 bucket
* Attempt to detect text, faces, or labels in the image using Rekognition

${testcount:18}

"""

import pytest
import time

from aws_test_functions import (
    new_bucket,
    new_key_pair,
    new_policy,
    new_role,
    retry,
)
from aws_test_functions.servicecatalog import (
    cleanup_portfolios,
    cleanup_products,
    copy_product,
    new_launch_constraint,
    new_portfolio,
    new_portfolio_principal,
    new_portfolio_product,
    new_product,
    new_product_launch,
    new_product_plan,
)

ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden|ResourceNotFound).*'

pytestmark = [pytest.mark.scp_check("list_tag_options")]


@pytest.fixture(scope="module")
def servicecatalog(session):
    """A shared handle to Service Catalog APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Service Catalog APIs.

    """

    return session.client("servicecatalog")


@pytest.fixture(scope="module")
def cloudformation(session):
    """A shared handle to Cloudformation APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Cloudformation APIs.

    """

    return session.client("cloudformation")


@pytest.fixture(scope="module")
def ec2_resource(session):
    """A shared handle to EC2 Resource APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the EC2 Resourece APIs.

    """

    return session.resource("ec2")


@pytest.fixture(scope="module", autouse=True)
def cleanup(servicecatalog):
    """Attempt to clean up any resources lingering from previous tests.

    :param ServiceCatalog.Client servicecatalog:
        A handle to the Service Catalog APIs.

    """

    cleanup_products(servicecatalog=servicecatalog)
    cleanup_portfolios(servicecatalog=servicecatalog)


@pytest.fixture(scope="module")
def bucket(session):
    """Create a new S3 bucket.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :yields:
        An S3 Bucket resource.

    """

    s3 = session.resource("s3")
    with new_bucket("test-servicecatalog", s3=s3) as b:
        yield b


@pytest.fixture(scope="module")
def template(bucket, internal_subnet_id):
    """Create a simple CloudFormation template to spin up a single EC2
    instance.

    :param S3.Bucket bucket:
        An S3 bucket into which the template will be uploaded.
    :param str internal_subnet_id:
        The ID of an internal subnet where the EC2 instance will be created.

    :returns:
        A string: the URL to the uploaded CloudFormation template.

    """

    bucket.upload_file(
        "./tests/resources/servicecatalog-cfn.json",
        "template.json",
        ExtraArgs={"ServerSideEncryption": "AES256"},
    )
    url = "https://s3.amazonaws.com/{}/template.json".format(bucket.name)

    return url


@pytest.fixture(scope="module")
def role(stack):
    """Create a new role with permission to launch a product.

    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.

    :yields:
        An IAM Role resource.

    """

    stmt = [
        {
            "Effect": "Allow",
            "Action": [
                "catalog-user:*",
                "cloudformation:CreateStack",
                "cloudformation:DeleteStack",
                "cloudformation:DescribeStackEvents",
                "cloudformation:DescribeStacks",
                "cloudformation:GetTemplateSummary",
                "cloudformation:SetStackPolicy",
                "cloudformation:ValidateTemplate",
                "cloudformation:UpdateStack",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:CreateSecurityGroup",
                "ec2:DeleteSecurityGroup",
                "ec2:DescribeInstances",
                "ec2:DescribeKeyPairs",
                "ec2:DescribeSecurityGroups",
                "ec2:RunInstances",
                "ec2:TerminateInstances",
                "s3:GetObject",
            ],
            "Resource": "*",
        }
    ]

    p = stack.enter_context(new_policy("service_catalog", stmt))
    r = stack.enter_context(new_role("service_catalog", "servicecatalog", p.arn))

    yield r


@pytest.fixture(scope="module", autouse=True)
def shared_vars(role):
    """Return a dictionary that is used to share information across test
    functions.

    :param IAM.Role role:
        A dependency on the fixture that setups up a new IAM Role.

    :returns:
        A dictionary.

    """

    return {
        # information about the created portfolio
        "portfolio": None,
        # information about the created product
        "product": None,
        # the ID of a provisioning artifact required to launch a product
        "artifact_id": None,
    }


@pytest.fixture(scope="function")
def portfolio(shared_vars):
    """Provide easier access to information about a Service Catalog Portfolio.

    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    :returns:
        A dictionary.

    """

    return shared_vars["portfolio"]


@pytest.fixture(scope="function")
def portfolio_id(portfolio):
    """Provide easier access to the ID of a Service Catalog Portfolio.

    :param dict portfolio:
        Information about a portfolio.

    :returns:
        A string.

    """

    try:
        pid = portfolio["Id"]
    except (TypeError, KeyError):
        pid = "port-1111111111111"

    return pid


@pytest.fixture(scope="function")
def product(shared_vars):
    """Provide easier access to information about a Service Catalog Product.

    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    :returns:
        A dictionary.

    """

    return shared_vars["product"]


@pytest.fixture(scope="function")
def product_id(product):
    """Provide easier access to the ID of a Service Catalog Product.

    :param dict product:
        Information about a product.

    :returns:
        A string.

    """

    try:
        pid = product["ProductViewDetail"]["ProductViewSummary"]["ProductId"]
    except (TypeError, KeyError):
        pid = "prod-1111111111111"

    return pid


@pytest.fixture(scope="function")
def product_arn(product):
    """Provide easier access to the ARN of a Service Catalog Product.

    :param dict product:
        Information about a product.

    :returns:
        A string.

    """

    try:
        arn = product["ProductViewDetail"]["ProductARN"]
    except (TypeError, KeyError):
        arn = "arn:0::::"

    return arn


@pytest.fixture(scope="function")
def artifact_id(shared_vars):
    """Return the ID of the Provisioning Artifact required to launch a product.

    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    :returns:
        A string.

    """

    return shared_vars["artifact_id"]


@pytest.fixture(scope="module")
def key_pair(stack):
    """Create a new EC2 key pair for launching EC2 instances

    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.

    :yields:
        A dictionary with information about the Key Pair.

    """

    k = stack.enter_context(new_key_pair("service_catalog_key"))
    yield k


def test_create_portfolio(servicecatalog, stack, shared_vars):
    """Verify access to create a portfolio.

    :testid: servicecatalog-f-1-1
    :testid: servicecatalog-f-1-7

    :param ServiceCatalog.Client servicecatalog:
        A handle to the Service Catalog API.
    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    portfolio = stack.enter_context(
        new_portfolio("test_servicecatalog", servicecatalog=servicecatalog)
    )
    assert "Id" in portfolio, "Unexpected data: {}".format(portfolio)

    shared_vars["portfolio"] = portfolio


def test_create_product(servicecatalog, template, stack, shared_vars):
    """Verify access to create a product.

    :testid: servicecatalog-f-1-1
    :testid: servicecatalog-f-1-7

    :param ServiceCatalog.Client servicecatalog:
        A handle to the Service Catalog API.
    :param str template:
        URL to a CloudFormation template.
    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    prefix = "test_servicecatalog"
    product = stack.enter_context(
        new_product(prefix, template, servicecatalog=servicecatalog)
    )

    err = "Unexpected data: {}".format(product)
    view_detail = product["ProductViewDetail"]
    assert "ProductViewSummary" in view_detail, err
    assert view_detail["ProductViewSummary"]["Name"].startswith(prefix), err

    shared_vars["product"] = product


def test_associate_product_with_portfolio(
    servicecatalog, portfolio_id, product_id, stack
):
    """Verify access to associate a product with a portfolio.

    :testid: servicecatalog-f-1-1

    :param ServiceCatalog.Client servicecatalog:
        A handle to the Service Catalog API.
    :param str portfolio_id:
        The ID of a portfolio.
    :param str product_id:
        The ID of a product.
    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.

    """

    res = stack.enter_context(
        new_portfolio_product(portfolio_id, product_id, servicecatalog=servicecatalog)
    )
    assert isinstance(
        res, dict
    ), "Unable to associate product with portfolio: {}".format(res)


def test_copy_product(servicecatalog, product_arn, stack):
    """Verify access to copy a product.

    :testid: servicecatalog-f-1-2
    :testid: servicecatalog-f-1-7

    :param ServiceCatalog.Client servicecatalog:
        A handle to the Service Catalog API.
    :param str product_arn:
        The ARN of a product.
    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.

    """

    time.sleep(60)
    product = stack.enter_context(
        copy_product(
            "test-servicecatalog-copy", product_arn, servicecatalog=servicecatalog
        )
    )
    assert product is not None, "Failed to copy product"


def test_associate_role_with_portfolio(servicecatalog, portfolio_id, role, stack):
    """Verify access to associate a role with a portfolio.

    :testid: servicecatalog-f-1-3
    :testid: servicecatalog-f-1-7

    :param ServiceCatalog.Client servicecatalog:
        A handle to the Service Catalog API.
    :param str portfolio_id:
        The ID of a portfolio.
    :param IAM.Role role:
        An IAM Role resource.
    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.

    """

    stack.enter_context(
        new_portfolio_principal(portfolio_id, role.arn, servicecatalog=servicecatalog)
    )


def test_associate_user_with_portfolio(servicecatalog, portfolio_id, user, stack):
    """Verify access to associate a user with a portfolio.

    :testid: servicecatalog-f-1-3
    :testid: servicecatalog-f-1-7

    :param ServiceCatalog.Client servicecatalog:
        A handle to the Service Catalog API.
    :param str portfolio_id:
        The ID of a portfolio.
    :param IAM.User user:
        An IAM User resource.
    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.

    """

    stack.enter_context(
        new_portfolio_principal(portfolio_id, user.arn, servicecatalog=servicecatalog)
    )


def test_create_launch_constraint(
    servicecatalog, portfolio_id, product_id, role, stack
):
    """Verify access to create a launch constraint on a product.

    :testid: servicecatalog-f-1-3
    :testid: servicecatalog-f-1-7

    :param ServiceCatalog.Client servicecatalog:
        A handle to the Service Catalog API.
    :param str portfolio_id:
        The ID of a portfolio.
    :param str product_id:
        The ID of a product.
    :param IAM.Role role:
        An IAM Role resource.
    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.

    """

    res = stack.enter_context(
        new_launch_constraint(
            portfolio_id, product_id, role, servicecatalog=servicecatalog
        )
    )
    assert "ConstraintDetail" in res, "Unable to create launch constraint: {}".format(
        res
    )


def test_create_provisioning_artifact(
    servicecatalog, template, product_id, stack, shared_vars
):
    """Verify access to create a provisioning artifact.

    :testid: servicecatalog-f-1-4
    :testid: servicecatalog-f-1-7

    :param ServiceCatalog.Client servicecatalog:
        A handle to the Service Catalog API.
    :param str template:
        URL to a CloudFormation template.
    :param str product_id:
        The ID of a product.
    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = servicecatalog.create_provisioning_artifact(
        ProductId=product_id,
        Parameters=dict(
            Name="v1",
            Info=dict(LoadTemplateFromURL=template),
            Type="CLOUD_FORMATION_TEMPLATE",
        ),
    )
    assert "ProvisioningArtifactDetail" in res, "Unexpected response: {}".format(res)
    artifact_id = shared_vars["artifact_id"] = res["ProvisioningArtifactDetail"]["Id"]

    retry(
        servicecatalog.describe_provisioning_artifact,
        kwargs=dict(ProvisioningArtifactId=artifact_id, ProductId=product_id),
        msg="Waiting for provisioning artifact {} to become available".format(
            product_id
        ),
        pred=lambda ex: "ResourceNotFoundException" in str(ex),
        until=lambda r: r.get("Status") == "AVAILABLE",
        delay=20,
    )

    print("Created provisioning artifact: {}".format(artifact_id))

    stack.callback(
        servicecatalog.delete_provisioning_artifact,
        ProductId=product_id,
        ProvisioningArtifactId=artifact_id,
    )


def test_create_plan(servicecatalog, product_id, artifact_id, stack):
    """Verify access to create a product provisioning plan.

    :testid: servicecatalog-f-1-4
    :testid: servicecatalog-f-1-7

    :param ServiceCatalog.Client servicecatalog:
        A handle to the Service Catalog API.
    :param str product_id:
        The ID of a product.
    :param str artifact_id:
        The ID of a provisioning artifact.
    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.

    """

    retry(
        servicecatalog.list_launch_paths,
        kwargs=dict(ProductId=product_id),
        msg="Waiting for Product {} to become available".format(product_id),
        pred=lambda ex: "ResourceNotFoundException" in str(ex),
        delay=20,
    )

    plan_id = stack.enter_context(
        new_product_plan(
            "test-servicecatalog",
            product_id,
            artifact_id,
            servicecatalog=servicecatalog,
        )
    )
    assert plan_id, "Unexpected plan ID: {}".format(plan_id)


# This test takes ~3 minutes
@pytest.mark.slowtest
def test_launch_product_with_access(
    servicecatalog,
    cloudformation,
    ec2_resource,
    key_pair,
    internal_subnet_id,
    vpc_id,
    product_id,
    artifact_id,
    stack,
):
    """Verify the ability to provision a product when given proper access

    :testid: servicecatalog-f-1-5
    :testid: servicecatalog-f-1-7

    :param ServiceCatalog.Client servicecatalog:
        A handle to the Service Catalog API.
    :param Cloudformation.Client cloudformation:
        A handle to the Cloudformation API.
    :param EC2.Resource ec2_resource:
        A handle to the EC2 Resource API.
    :param dict key_pair:
        A dictionary of information about an EC2 key pair shared across tests.
    :param str internal_subnet_id:
        The AWS Subnet ID to deploy the instance into.
    :param str vpc_id:
        The AWS VPC ID to deploy the instance into.
    :param str product_id:
        The ID of a product.
    :param str artifact_id:
        The ID of a provisioning artifact.
    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.

    """

    provisioning_params = [
        dict(Key="KeyName", Value=key_pair["KeyName"]),
        dict(Key="SubnetId", Value=internal_subnet_id),
        dict(Key="VpcId", Value=vpc_id),
    ]
    launch_res = stack.enter_context(
        new_product_launch(
            "test_servicecatalog",
            product_id,
            artifact_id,
            provisioning_params,
            servicecatalog=servicecatalog,
        )
    )
    assert "RecordDetail" in launch_res, "Unexpected response: {}".format(launch_res)

    provisioned_id = launch_res["RecordDetail"]["ProvisionedProductId"]
    res = retry(
        servicecatalog.describe_provisioned_product,
        kwargs=dict(Id=provisioned_id),
        until=lambda s: s["ProvisionedProductDetail"]["Status"] in ("AVAILABLE", "ERROR", "TAINTED"),
        msg="Waiting for product {} to finalize launch".format(product_id),
        delay=20,
        max_attempts=10,
    )
    status = res["ProvisionedProductDetail"]["Status"]
    assert (
        status == "AVAILABLE"
    ), "Failed to launch product {} with status {}: {}".format(product_id, status, res)

    record_id = launch_res["RecordDetail"]["RecordId"]
    launch_info = servicecatalog.describe_record(Id=record_id)
    stack_arn = next(
        item["OutputValue"]
        for item in launch_info["RecordOutputs"]
        if item["OutputKey"] == "CloudformationStackARN"
    )
    cloudformation.get_waiter("stack_create_complete").wait(
        StackName=stack_arn
    )

    resource_detail = cloudformation.describe_stack_resource(
        StackName=stack_arn, LogicalResourceId="EC2Instance"
    )
    inst_id = resource_detail["StackResourceDetail"]["PhysicalResourceId"]
    ec2_inst = ec2_resource.Instance(inst_id)
    ec2_inst.reboot()
    ec2_inst.wait_until_running()
    ec2_inst.load()

    assert (
        ec2_inst.subnet_id == internal_subnet_id
    ), "EC2 Instance launched in the incorrect Subnet"
    assert ec2_inst.vpc_id == vpc_id, "EC2 Instance launched in the incorrect VPC"
    assert (
        ec2_inst.state["Name"] == "running"
    ), "Failed to reboot EC2 Instance created by Service Catalog"


# This test takes ~2 minutes
@pytest.mark.slowtest
def test_launch_product_without_access(
    servicecatalog,
    role,
    user,
    portfolio_id,
    product_id,
    artifact_id,
    key_pair,
    internal_subnet_id,
    vpc_id,
    stack,
):
    """Verify the inability to provision a product without the proper access

    :testid: servicecatalog-f-1-6

    :param ServiceCatalog.Client servicecatalog:
        A handle to the Service Catalog API.
    :param IAM.Role role:
        An IAM Role resource associated with the portfolio.
    :param IAM.User user:
        An IAM User resource associated with the portfolio.
    :param str portfolio_id:
        The ID of a portfolio.
    :param str product_id:
        The ID of a product.
    :param str artifact_id:
        The ID of a provisioning artifact.
    :param dict key_pair:
        A dictionary of information about an EC2 key pair shared across tests.
    :param str internal_subnet_id:
        The AWS Subnet ID to deploy the instance into.
    :param str vpc_id:
        The AWS VPC ID to deploy the instance into.
    :param contextlib.ExitStack stack:
        A context manager shared across all Service Catalog tests.

    """

    servicecatalog.disassociate_principal_from_portfolio(
        PortfolioId=portfolio_id, PrincipalARN=role.arn
    )
    servicecatalog.disassociate_principal_from_portfolio(
        PortfolioId=portfolio_id, PrincipalARN=user.arn
    )
    # A bit of delay to allow the disassociation to propagate
    time.sleep(100)

    provisioning_params = [
        dict(Key="KeyName", Value=key_pair["KeyName"]),
        dict(Key="SubnetId", Value=internal_subnet_id),
        dict(Key="VpcId", Value=vpc_id),
    ]

    try:
        stack.enter_context(
            new_product_launch(
                "test_servicecatalog",
                product_id,
                artifact_id,
                provisioning_params,
                servicecatalog=servicecatalog,
            )
        )
    except servicecatalog.exceptions.ResourceNotFoundException:
        # When disassociating roles and users from products, a ResourceNotFoundException is thrown.
        # It is not the expected error but it is the generally expected behavior(would expect access denial of some sort)
        return

    assert False, "Product successfully launched without permissions"
