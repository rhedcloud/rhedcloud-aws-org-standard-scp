"""
========
test_kms
========

Verify that customers can use AWS Key Management Service.

Plan:

* Create test user and attach Administrator permissions
* Try to create a new KMS key
* Try to delete the new KMS key
* Try to list all KMS keys
* Try to describe all KMS keys

${testcount:2}

"""

from datetime import datetime

import pytest


@pytest.fixture(scope='module')
def kms(session):
    return session.client('kms')


def test_create_delete_key(kms):
    """Customers should be able to create and delete KMS keys.

    :testid: kms-f-1-1

    """

    now = datetime.utcnow()
    name = 'test_kms-{}'.format(now)
    descr = 'test key created: {}'.format(now)

    res = kms.create_key(
        Description=descr,
        Tags=[{
            'TagKey': 'Name',
            'TagValue': name,
        }]
    )
    assert 'KeyMetadata' in res, 'failed to create key: {}'.format(res)

    key = res['KeyMetadata']
    assert key['Description'] == descr, 'key incomplete'

    # now schedule the key to be deleted
    kms.schedule_key_deletion(
        KeyId=key['Arn'],
        PendingWindowInDays=7,  # minimum is 7 days, max is 30 days
    )


def test_list_describe_keys(kms):
    """Customers should be able to list and describe KMS keys.

    :testid: kms-f-1-1

    """

    res = kms.list_keys()
    assert 'Keys' in res, 'no keys found'

    for key in res['Keys']:
        desc = kms.describe_key(KeyId=key['KeyArn'])
        assert 'KeyMetadata' in desc
        assert desc['KeyMetadata']['KeyId'] == key['KeyId']
