"""
========
test_ses
========

Verify access to use AWS Simple Email Service

Plan:

* Verify an email address
* Send an email to the verified email address

${testcount:12}

"""

import pytest

from aws_test_functions import (
    get_uri_contents,
    make_identifier,
    retry,
)
from aws_test_functions.ses import (
    IMAP_USER,
    find_identity,
    get_email,
    get_verification_email,
)

# This service is fully blocked
pytestmark = [pytest.mark.scp_check('list_verified_email_addresses')]


@pytest.fixture(scope='module')
def ses(session):
    """A shared handle to SES API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the SES API.

    """

    return session.client('ses')


@pytest.fixture(scope='module')
def address():
    """Email address to use for verification."""

    return IMAP_USER


@pytest.fixture(scope='module')
def subject():
    """Subject to use when sending test email."""

    return make_identifier('test-subject')


@pytest.mark.xfail(reason='Just in case a previous test failed to cleanup')
def test_email_is_unverified(ses, address):
    """Verify that the specified address starts out as unverified for SES.

    :param SES.Client ses:
        A handle to the SES API.
    :param str address:
        Email address to check.

    """

    res = ses.list_verified_email_addresses()
    assert 'VerifiedEmailAddresses' in res, 'Unexpected response: {}'.format(res)
    assert address not in res['VerifiedEmailAddresses'], \
        'Address {} unexpectedly verified: {}'.format(address, res)


def test_validate_email(ses, address, stack):
    """Attempt to verify an email address for use in SES.

    :testid: ses-f-1-1

    :param SES.Client ses:
        A handle to the SES API.
    :param str address:
        Email address to check.
    :param contextlib.ExitStack stack:
        A context manager shared across all SES tests.

    """

    res = ses.verify_email_identity(
        EmailAddress=address,
    )
    assert isinstance(res, dict), 'Unexpected response: {}'.format(res)

    stack.callback(
        ses.delete_verified_email_address,
        EmailAddress=address,
    )


def test_follow_validation_link(ses, address):
    """Complete the verification process for an email address in SES.

    :testid: ses-f-1-1

    :param SES.Client ses:
        A handle to the SES API.
    :param str address:
        Email address to check.

    """

    # trigger "Access Denied" error in TEST_ORG
    find_identity(address, ses=ses)

    msg, url = retry(
        get_verification_email,
        msg='Looking for verification email',
    )

    res = get_uri_contents(url)
    assert 'Verification Successful' in res, 'Unexpected response: {}'.format(res)


def test_email_is_verified(ses, address):
    """Verify that the specified address has been verified in SES.

    :testid: ses-f-1-1

    :param SES.Client ses:
        A handle to the SES API.
    :param str address:
        Email address to check.

    """

    res = ses.list_verified_email_addresses()
    assert 'VerifiedEmailAddresses' in res, 'Unexpected response: {}'.format(res)
    assert address in res['VerifiedEmailAddresses'], \
        'Address {} not verified: {}'.format(address, res)


def test_send_email(ses, address, subject):
    """Attempt to send a test email to the newly-verified email address.

    :testid: ses-f-1-1

    :param SES.Client ses:
        A handle to the SES API.
    :param str address:
        Email address to check.
    :param str subject:
        Email subject line.

    """

    res = ses.send_email(
        Source=address,
        Destination={
            'ToAddresses': [address],
        },
        Message={
            'Subject': {'Data': subject},
            'Body': {
                'Text': {'Data': 'testing 1 2 3'},
            }
        },
    )
    assert 'MessageId' in res, 'Unexpected response: {}'.format(res)


def test_check_email(ses, address, subject):
    """Check that the test email arrived in the inbox of the newly-verified
    email address.

    :testid: ses-f-1-1

    :param SES.Client ses:
        A handle to the SES API.
    :param str address:
        Email address to check.
    :param str subject:
        Email subject line.

    """

    # trigger "Access Denied" error in TEST_ORG
    find_identity(address, ses=ses)

    needle = 'Subject: {}'.format(subject)
    msg = retry(
        get_email,
        msg='Checking email',
        delay=30,
    )
    assert needle in msg, 'Unexpected message: {}'.format(msg)
