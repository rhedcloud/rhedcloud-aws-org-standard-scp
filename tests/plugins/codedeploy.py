"""
===================
CodeDeploy Fixtures
===================

This module contains fixtures for CodeDeploy tests and tests for related
services (like CodePipeline).

"""

from hashlib import sha256
from zipfile import ZipFile, ZipInfo
import io
import json
import os

import pytest

from aws_test_functions import (
    make_identifier,
    new_bucket,
    new_policy,
    new_role,
)

pytest_plugins = [
    'tests.plugins._lambda',
]


@pytest.fixture(scope='module')
def codedeploy(session):
    """A shared handle to CodeDeploy API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the CodeDeploy API.

    """

    return session.client('codedeploy')


@pytest.fixture(scope='module')
def codedeploy_zip_bytes(lambda_code):
    """Return the contents of a Zip file with the Lambda function code.

    :param str lambda_code:
        The code for the Lambda function.

    :returns:
        A bytestring.

    """

    out = io.BytesIO()
    with ZipFile(out, 'w') as fh:
        # give full access to included file
        zinfo = ZipInfo('HelloLambda.py')
        zinfo.external_attr = 0o777 << 16

        fh.writestr(zinfo, lambda_code.replace(
            'Hello from Lambda', 'New version'
        ))

    return out.getvalue()


@pytest.fixture(scope='module')
def codedeploy_bucket(stack):
    """Create a new S3 bucket containing a sample application bundle.

    :param contextlib.ExitStack stack:
        A context manager shared across all CodeDeploy tests.

    :returns:
        An S3 Bucket resource.

    """

    b = stack.enter_context(new_bucket('test-codedeploy'))
    b.Versioning().enable()

    fn = 'SampleCodeDeployApp_Linux.zip'
    b.upload_file(
        os.path.join('./tests/resources', fn),
        fn,
        ExtraArgs={'ServerSideEncryption': 'AES256'},
    )

    return b


@pytest.fixture(scope='module')
def codedeploy_policy(stack):
    """Create a new policy for CodeDeploy.

    :param contextlib.ExitStack stack:
        A context manager shared across all CodeDeploy tests.

    :yields:
        An IAM Policy resource.

    """

    stmt = [{
        "Effect": "Allow",
        "Action": [
            "autoscaling:*",
            "cloudformation:*",
            "codedeploy:*",
            "ec2:*",
            "elasticloadbalancing:*",
            "iam:AddRoleToInstanceProfile",
            "iam:CreateInstanceProfile",
            "iam:CreateRole",
            "iam:DeleteInstanceProfile",
            "iam:DeleteRole",
            "iam:DeleteRolePolicy",
            "iam:GetInstanceProfile",
            "iam:GetRole",
            "iam:GetRolePolicy",
            "iam:ListInstanceProfilesForRole",
            "iam:ListRolePolicies",
            "iam:ListRoles",
            "iam:PassRole",
            "iam:PutRolePolicy",
            "iam:RemoveRoleFromInstanceProfile",
            "lambda:*",
            "s3:*"
        ],
        "Resource": "*"
    }]

    p = stack.enter_context(new_policy('TestCodeDeployPolicy', stmt))

    yield p


@pytest.fixture(scope='module')
def codedeploy_role_policies(codedeploy_policy):
    return (
        codedeploy_policy.arn,
        'AWSLambdaFullAccess',
        'AWSCodeDeployFullAccess',
        'RHEDcloudAdministratorRolePolicy',
        'arn:aws:iam::aws:policy/service-role/AWSCodeDeployRole',
        'arn:aws:iam::aws:policy/service-role/AWSCodeDeployRoleForLambda',
    )


@pytest.fixture(scope='module')
def codedeploy_role(stack, codedeploy_role_policies):
    """Create a new role for CodeDeploy.

    :param contextlib.ExitStack stack:
        A context manager shared across all CodeDeploy tests.
    :param IAM.Policy codedeploy_policy:
        An IAM Policy resource for CodeDeploy.

    :yields:
        An IAM Role resource.

    """

    r = stack.enter_context(new_role(
        'code-deploy-service',
        ['codedeploy.us-east-1', 'lambda'],
        *codedeploy_role_policies,
    ))

    yield r


@pytest.fixture(scope='module')
def revision_info(codedeploy_bucket, lambda_func_name):
    """Return a dictionary containing information about an application revision.

    :param S3.Bucket codedeploy_bucket:
        An S3 Bucket resource.
    :param str lambda_func_name:
        The name of the Lambda function.

    :returns:
        A dictionary.

    """

    content = json.dumps(dict(
        version=0.0,
        Resources=[{
            lambda_func_name: dict(
                Type='AWS::Lambda::Function',
                Properties=dict(
                    Name=lambda_func_name,
                    Alias='HelloLambda',
                    CurrentVersion='1',
                    TargetVersion='2',
                ),
            ),
        }],
    ))

    return {
        'revisionType': 'String',
        'string': {
            'content': content,
            'sha256': sha256(content.encode()).hexdigest(),
        },
    }


@pytest.fixture(scope='module')
def app_name():
    """Generate a new unique name for an application.

    :returns:
        A string.

    """

    return make_identifier('TestCodeDeployApplication')


@pytest.fixture(scope='module')
def group_name():
    """Generate a new unique name for a deployment group.

    :returns:
        A string.

    """

    return make_identifier('TestCodeDeploymentGroup')
