"""
=========
test_logs
=========

Verify access to the Logs API.

Plan:

For each of 3 Logs groups:

* Create a Logs group
* Create a Logs stream
* Put events into Logs streams
* Delete a Logs group
* Delete Logs metric filters

${testcount:15}

"""

import pytest

from aws_test_functions import (
    debug,
    has_status,
    ignore_errors,
    unexpected,
)


pytestmark = [
    pytest.mark.raises_access_denied,
    pytest.mark.parametrize("group_name", (
        "rhedcloudtest",
        "test-FlowLogs",
        "/aws/lambda/rhedcloudtest",
    )),
]


@pytest.fixture
def logs(session):
    return session.client("logs")


def test_create_log_group(logs, group_name, stack):
    """Verify access to create Logs groups.

    :param Logs.Client logs:
        Handle to the Logs API.
    :param str group_name:
        Name of a log group.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    debug("Creating log group: {}".format(group_name))
    res = logs.create_log_group(
        logGroupName=group_name,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Created log group: {}".format(group_name))
    stack.callback(
        ignore_errors(test_delete_log_group),
        logs,
        group_name,
    )


def test_create_log_streams(logs, group_name):
    """Verify access to create Logs streams.

    :param Logs.Client logs:
        Handle to the Logs API.
    :param str group_name:
        Name of a log group.

    """

    debug("Creating log stream: {}".format(group_name))
    res = logs.create_log_stream(
        logGroupName=group_name,
        logStreamName="test",
    )
    assert has_status(res, 200), unexpected(res)


def test_put_log_events(logs, group_name):
    """Verify access to put events into Logs streams.

    :param Logs.Client logs:
        Handle to the Logs API.
    :param str group_name:
        Name of a log group.

    """

    debug("Putting log events: {}".format(group_name))
    res = logs.put_log_events(
        logGroupName=group_name,
        logStreamName="test",
        logEvents=[{
            "timestamp": 123,
            "message": "test",
        }],
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_log_group(logs, group_name):
    """Verify access to delete Logs groups.

    :param Logs.Client logs:
        Handle to the Logs API.
    :param str group_name:
        Name of a log group.

    """

    debug("Deleting log group: {}".format(group_name))
    res = logs.delete_log_group(
        logGroupName=group_name,
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_metric_filter(logs, group_name):
    """Verify access to delete Logs metric filters.

    :param Logs.Client logs:
        Handle to the Logs API.
    :param str group_name:
        Name of a log group.

    """

    debug("Deleting metric filter: {}".format(group_name))
    res = logs.delete_metric_filter(
        logGroupName=group_name,
        filterName="test",
    )
    assert has_status(res, 200), unexpected(res)
