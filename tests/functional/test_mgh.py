"""
========
test_mgh
========

Verify access to use AWS Migration Hub.

Plan:

* Create a progress update stream
* Import a migration task
* Check status of migration task
* List a ProgressUpdateStream
* Delete a ProgressUpdateStream

${testcount:5}

"""

import pytest

from aws_test_functions import (
    aws_client,
    has_status,
    ignore_errors,
    make_identifier,
    unexpected
)


pytestmark = [
    pytest.mark.scp_check('list_migration_tasks'),
]


@pytest.fixture(scope='module')
def mgh(session):
    """A shared handle to Migration Hub API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Migration Hub API.

    """

    return session.client('mgh', region_name='us-west-2')


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'stream_name': make_identifier('test-stream'),
        'task_name': make_identifier('test-task'),
    }


@aws_client('mgh')
def delete_progress_update_stream(name, *, mgh=None):
    """Delete a progress update stream.

    :param str name:
        Name of the progress update stream to delete.

    """

    print('Deleting progress update stream: {}'.format(name))
    res = mgh.delete_progress_update_stream(
        ProgressUpdateStreamName=name,
    )
    assert has_status(res, 200)


def test_create_progress_update_stream(mgh, stream_name, stack, shared_vars):
    """Verify access to create progress update streams.

    :testid: mgh-f-1-1

    :param MigrationHub.Client mgh:
        A handle to the Migration Hub API.
    :param contextlib.ExitStack stack:
        A context manager shared across all Migration Hub tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = mgh.create_progress_update_stream(
        ProgressUpdateStreamName=stream_name,
    )
    assert has_status(res, 200), unexpected(res)

    stack.callback(
        ignore_errors(delete_progress_update_stream),
        stream_name,
        mgh=mgh,
    )


def test_import_migration_task(mgh, stream_name, task_name):
    """Verify access to import migration tasks.

    :testid: mgh-f-1-2

    :param MigrationHub.Client mgh:
        A handle to the Migration Hub API.
    :param str stream_name:
        Name of a progress update stream.
    :param str task_name:
        Name of a migration task.

    """

    res = mgh.import_migration_task(
        ProgressUpdateStream=stream_name,
        MigrationTaskName=task_name,
    )
    assert has_status(res, 200), unexpected(res)


def test_describe_migration_task(mgh, stream_name, task_name):
    """Verify access to describe migration tasks.

    :testid: mgh-f-1-3

    :param MigrationHub.Client mgh:
        A handle to the Migration Hub API.
    :param str stream_name:
        Name of a progress update stream.
    :param str task_name:
        Name of a migration task.

    """

    res = mgh.describe_migration_task(
        ProgressUpdateStream=stream_name,
        MigrationTaskName=task_name,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'MigrationTask' in res, unexpected(res)

    task = res['MigrationTask']
    valid_status = ('NOT_STARTED', 'IN_PROGRESS', 'FAILED', 'COMPLETED')
    assert task['Task']['Status'] in valid_status, unexpected(res)


def test_list_progress_update_streams(mgh, stream_name):
    """Verify access to list progress update streams.

    :testid: mgh-f-1-4

    :param MigrationHub.Client mgh:
        A handle to the Migration Hub API.
    :param str stream_name:
        Name of a progress update stream.

    """

    res = mgh.list_progress_update_streams()
    assert has_status(res, 200), unexpected(res)

    key = 'ProgressUpdateStreamSummaryList'
    assert key in res and len(res[key]) > 0, unexpected(res)

    found = False
    for deets in res[key]:
        if deets['ProgressUpdateStreamName'] == stream_name:
            found = True
            break

    assert found, 'failed to find progress update stream: {}'.format(stream_name)


def test_delete_progress_update_stream(mgh, stream_name):
    """Verify access to delete progress update streams.

    :testid: mgh-f-1-5

    :param MigrationHub.Client mgh:
        A handle to the Migration Hub API.
    :param str stream_name:
        Name of a progress update stream.

    """

    delete_progress_update_stream(stream_name, mgh=mgh)
