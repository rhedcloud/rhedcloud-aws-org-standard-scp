"""
=======
test_mq
=======

Verify access to use AmazonMQ.

Plan:

A user should be able to:
    - mq-f-1 create a configuration, broker, user
    - mq-f-2 describe a configuration, broker, user
    - mq-f-3 list configurations, brokers users
    - mq-f-4 update a configuraiton, broker, or user

${testcount:4}

"""

import pytest

from aws_test_functions import (
    make_identifier,
    unexpected,
    retry
)

pytestmark = [pytest.mark.slowtest]


@pytest.fixture(scope='module')
def mq(session):
    """A shared handle to AmazonMQ API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the AmazonMQ API.

    """

    return session.client('mq')


@pytest.fixture(scope='module')
def ec2(session):
    """A shared handle to EC2 API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the EC2 API.

    """

    return session.client('ec2')


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """
    Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'configuration': None,
        'brokerId': None,
        'username': None
    }


def test_create_configuration(mq, shared_vars):
    """Verify access to create configurations.

    :testid: mq-f-1-1

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    name = make_identifier('test-mq')
    res = mq.create_configuration(
        Name=name,
        EngineType='ACTIVEMQ',
        EngineVersion='5.15.0',
    )

    assert 'Arn' in res, unexpected(res)
    shared_vars['configuration'] = res


def test_create_broker(mq, ec2, shared_vars):
    """Create broker

    :testid: mq-f-1-2

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    list_response = mq.list_brokers()
    brokers = list_response['BrokerSummaries']

    if (len(brokers) > 0):
        brokerId = brokers[0]['BrokerId']
        print("Found existing broker: {}".format(brokerId))
    else:
        describe_security_groups = ec2.describe_security_groups()
        describe_subnets = ec2.describe_subnets()

        security_group = describe_security_groups['SecurityGroups'][0]
        subnet = describe_subnets['Subnets'][0]

        create_response = mq.create_broker(
            AutoMinorVersionUpgrade=True,
            BrokerName=make_identifier('broker', max_length=12),
            Configuration={
                'Id': shared_vars['configuration']['Id']
            },
            DeploymentMode='SINGLE_INSTANCE',
            EngineType='ACTIVEMQ',
            EngineVersion='5.15.0',
            HostInstanceType='mq.t2.micro',
            MaintenanceWindowStartTime={
                'DayOfWeek': 'MONDAY',
                'TimeOfDay': '12:00',
                'TimeZone': 'UTC'
            },
            SecurityGroups=[
                security_group['GroupId']
            ],
            SubnetIds=[
                subnet['SubnetId']
            ],
            Users=[{
                'Username': 'test',
                'Password': make_identifier('pass')
            }]
        )

        assert 'BrokerId' in create_response, 'Unable to create broker'
        brokerId = create_response['BrokerId']
        print("Created broker: {}".format(brokerId))

        status = retry(
            get_broker_state,
            args=[mq, brokerId],
            msg='Waiting for broker...',
            until=lambda b: b == 'RUNNING',
            delay=10,
            max_attempts=99
        )

    assert brokerId is not None
    shared_vars['brokerId'] = brokerId


def get_broker_state(mq, brokerId):
    """Return broker state

    :param MQ.Client mq:
    A handle to the AmazonMQ API.
    :param string brokerId:
    id of broker

    """

    describe_response = mq.describe_broker(
        BrokerId=brokerId
    )

    print('-- current state: %s' % (describe_response['BrokerState']))

    return describe_response['BrokerState']


def test_create_user(mq, shared_vars):
    """Create user

    :testid: mq-f-1-3

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    username = make_identifier('user', max_length=12)
    create_response = mq.create_user(
        BrokerId=shared_vars['brokerId'],
        Username=username,
        Password=make_identifier('pass', max_length=12)
    )

    assert create_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to create user'

    shared_vars['username'] = username


def test_describe_configuration(mq, shared_vars):
    """Describe configuraiton

    :testid: mq-f-2-1

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    describe_response = mq.describe_configuration(
        ConfigurationId=shared_vars['configuration']['Id']
    )

    assert 'Arn' in describe_response, 'Unable to describe configuration'


def test_describe_broker(mq, shared_vars):
    """Describe broker

    :testid: mq-f-2-2

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    describe_response = mq.describe_broker(
        BrokerId=shared_vars['brokerId']
    )

    assert 'BrokerId' in describe_response, 'Unable to describe broker'


def test_describe_user(mq, shared_vars):
    """Describe user

    :testid: mq-f-2-3

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    describe_response = mq.describe_user(
        BrokerId=shared_vars['brokerId'],
        Username=shared_vars['username']
    )

    assert 'Username' in describe_response, 'Unable to describe user'


def test_update_configuration(mq, shared_vars):
    """Update configuration

    :testid: mq-f-3-1

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    update_response = mq.update_configuration(
        ConfigurationId=shared_vars['configuration']['Id'],
        Data='PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8YnJva2VyIHhtbG5zPSJodHRwOi8vYWN0aXZlbXEuYXBhY2hlLm9yZy9zY2hlbWEvY29yZSI+CiAgPGRlc3RpbmF0aW9uUG9saWN5PgogICAgPHBvbGljeU1hcD4KICAgICAgPHBvbGljeUVudHJpZXM+CiAgICAgICAgPHBvbGljeUVudHJ5IHRvcGljPSImZ3Q7Ij4KICAgICAgICAgIDxwZW5kaW5nTWVzc2FnZUxpbWl0U3RyYXRlZ3k+CiAgICAgICAgICAgIDxjb25zdGFudFBlbmRpbmdNZXNzYWdlTGltaXRTdHJhdGVneSBsaW1pdD0iMTAwMCIvPgogICAgICAgICAgPC9wZW5kaW5nTWVzc2FnZUxpbWl0U3RyYXRlZ3k+CiAgICAgICAgPC9wb2xpY3lFbnRyeT4KICAgICAgPC9wb2xpY3lFbnRyaWVzPgogICAgPC9wb2xpY3lNYXA+CiAgPC9kZXN0aW5hdGlvblBvbGljeT4KICA8cGx1Z2lucz4KICA8L3BsdWdpbnM+CjwvYnJva2VyPgo='
    )

    assert 'Arn' in update_response, 'Unable to update configuraiton'


def test_update_broker(mq, shared_vars):
    """Update broker

    :testid: mq-f-3-2

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    update_response = mq.update_broker(
        BrokerId=shared_vars['brokerId'],
        Configuration={
            'Id': shared_vars['configuration']['Id']
        }
    )


def test_update_user(mq, shared_vars):
    """Update user

    :testid: mq-f-3-3

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    update_response = mq.update_user(
        BrokerId=shared_vars['brokerId'],
        Username=shared_vars['username'],
        Password=make_identifier('pass', max_length=12)
    )

    assert update_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to update user'


def test_reboot_broker(mq, shared_vars):
    """Reboot broker

    :testid: mq-f-4-1

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    brokerId = shared_vars['brokerId']
    reboot_response = mq.reboot_broker(
        BrokerId=brokerId
    )

    status = retry(
        get_broker_state,
        args=[mq, brokerId],
        msg='Waiting for broker to reboot..',
        until=lambda b: b == 'RUNNING',
        delay=10,
        max_attempts=99
    )

    assert status == 'RUNNING', 'Unable to reboot broker'


def test_list_configurations(mq, shared_vars):
    """List & Delete users

    :testid: mq-f-5-3
    :testid: mq-f-6-1

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    list_response = mq.list_configurations()
    assert 'Configurations' in list_response, 'Unable to list configurations'


def test_list_and_delete_users(mq, shared_vars):
    """List & Delete users

    :testid: mq-f-5-3
    :testid: mq-f-6-1

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    list_response = mq.list_users(
        BrokerId=shared_vars['brokerId']
    )

    for user in list_response['Users']:
        delete_response = mq.delete_user(
            BrokerId=shared_vars['brokerId'],
            Username=user['Username']
        )

        assert delete_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
            'Unable to delete user'


def test_list_and_delete_brokers(mq, shared_vars):
    """List & Delete brokers

    :testid: mq-f-5-2
    :testid: mq-f-6-2

    :param MQ.Client mq:
        A handle to the AmazonMQ API.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    list_response = mq.list_brokers()
    brokers = list_response['BrokerSummaries']

    assert 'BrokerSummaries' in list_response, 'Unable to list brokers'

    for broker in brokers:
        if broker['BrokerState'] == 'RUNNING':
            delete_response = mq.delete_broker(
                BrokerId=broker['BrokerId']
            )

            assert 'BrokerId' in delete_response, 'Unable to delete broker'
