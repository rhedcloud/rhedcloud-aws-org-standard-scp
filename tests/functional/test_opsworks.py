"""
=============
test_opsworks
=============

Verify access to AWS OpsWorks

Plan:

* Attempt to call various API functions, expecting failure each time.

${testcount:5}

"""

import pytest

from aws_test_functions import (
    get_or_create_role,
    get_uuid,
    retry,
)
from aws_test_functions.opsworks import (
    create_stack,
    delete_instance,
    delete_layer,
    delete_stack,
)

# This service is fully blocked in the HIPAA SCP
pytestmark = [pytest.mark.scp_check('describe_operating_systems')]


@pytest.fixture(scope='module')
def opsworks(session):
    """A shared handle to OpsWorks APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the OpsWorks APIs.

    """

    return session.client('opsworks')


@pytest.fixture(scope='module')
def role(session, stack):
    """Get or create a role for OpsWork to do magic.

    This Role does not get removed automatically because it's required for
    OpsWorks to clean up created stacks after tests finish.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param contextlib.ExitStack stack:
        A context manager shared across all OpsWorks tests.

    :yields:
        An IAM Role resource.

    """

    iam = session.resource('iam')

    return get_or_create_role(
        'RHEDcloudOpsWorksTestRole',
        'arn:aws:iam::aws:policy/service-role/AWSOpsWorksRole',
        'AdministratorAccess',
        service='opsworks',
        iam=iam,
    )


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        # the ID of a stack
        'stack_id': 'not-created',

        # the ID of a layer
        'layer_id': 'not-created',

        # the ID of a instance
        'instance_id': 'not-created',

        # whether the created instance was started successfully
        'instance_started': False,

        # the ID of an app
        'app_id': 'not-created',
    }


@pytest.fixture(scope='function')
def instance_started(shared_vars):
    return shared_vars['instance_started']


def test_create_stack(opsworks, vpc_id, role, script_runner_instance_profile,
                      internal_subnet_id, stack, shared_vars, scp):
    """Verify access to create a stack.

    :testid: opsworks-f-1-1

    :param OpsWorks.Client opsworks:
        A handle to the OpsWorks APIs.
    :param str vpc_id:
        ID of the VPC where the OpsWorks stack should be created.
    :param IAM.Role role:
        An IAM Service Role to use when performing OpsWorks operations.
    :param IAM.InstanceProfile script_runner_instance_profile:
        An IAM Instance Profile to use for instances created within the
        OpsWorks stack.
    :param str internal_subnet_id:
        The ID of a subnet within the customer VPC.
    :param contextlib.ExitStack stack:
        A context manager shared across all OpsWorks tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param scp:
        A dependency on the fixture that waits for the SCP to be in effect.

    """

    name = 'test-opsworks-{}'.format(get_uuid())
    res = retry(
        create_stack,
        args=(
            name,
            vpc_id,
            internal_subnet_id,
            role,
            script_runner_instance_profile,
        ),
        kwargs=dict(
            opsworks=opsworks,
        ),
        msg='Creating stack',
        delay=10,
        show=True,
        pred=lambda ex: 'ValidationException' in str(ex)
    )
    assert 'StackId' in res, 'Unexpected response: {}'.format(res)
    stack_id = shared_vars['stack_id'] = res['StackId']
    print('Created stack: {}'.format(stack_id))

    # cleanup when tests finish
    stack.callback(
        delete_stack,
        stack_id,
        opsworks=opsworks,
    )


def test_create_layer(opsworks, stack_id, stack, shared_vars, scp):
    """Verify access to create a layer.

    :testid: opsworks-f-1-2

    :param OpsWorks.Client opsworks:
        A handle to the OpsWorks APIs.
    :param str stack_id:
        ID of the OpsWorks Stack created above.
    :param contextlib.ExitStack stack:
        A context manager shared across all OpsWorks tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param scp:
        A dependency on the fixture that waits for the SCP to be in effect.

    """

    name = 'layer-{}'.format(get_uuid())[:32]
    res = opsworks.create_layer(
        StackId=stack_id,
        Type='custom',
        Name=name,
        Shortname=name,
    )
    assert 'LayerId' in res, 'Unexpected response: {}'.format(res)
    layer_id = shared_vars['layer_id'] = res['LayerId']
    print('Created layer: {}'.format(layer_id))

    stack.callback(
        delete_layer,
        layer_id,
        opsworks=opsworks,
    )


def test_create_instance(opsworks, stack_id, layer_id, stack, shared_vars, scp):
    """Verify access to create an instance.

    :testid: opsworks-f-1-3

    :param OpsWorks.Client opsworks:
        A handle to the OpsWorks APIs.
    :param str stack_id:
        ID of the OpsWorks Stack created above.
    :param str layer_id:
        ID of the Layer created in the OpsWorks Stack.
    :param contextlib.ExitStack stack:
        A context manager shared across all OpsWorks tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param scp:
        A dependency on the fixture that waits for the SCP to be in effect.

    """

    res = opsworks.create_instance(
        StackId=stack_id,
        LayerIds=[layer_id],
        InstanceType='c3.large',
        Os='Amazon Linux 2017.09',
    )
    assert 'InstanceId' in res, 'Unexpected response: {}'.format(res)
    instance_id = shared_vars['instance_id'] = res['InstanceId']
    print('Created instance: {}'.format(instance_id))

    stack.callback(
        delete_instance,
        instance_id,
        opsworks=opsworks,
    )

    # attempt to start the instance
    opsworks.start_instance(InstanceId=instance_id)
    shared_vars['instance_started'] = True
    print('Started instance: {}'.format(instance_id))


def test_create_app(opsworks, stack_id, instance_started, stack, shared_vars, scp):
    """Verify access to create an app.

    :testid: opsworks-f-1-4

    :param OpsWorks.Client opsworks:
        A handle to the OpsWorks APIs.
    :param str stack_id:
        ID of the OpsWorks Stack created above.
    :param bool instance_started:
        A dependency on the fixture that indicates when an instance has been
        started.
    :param contextlib.ExitStack stack:
        A context manager shared across all OpsWorks tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param scp:
        A dependency on the fixture that waits for the SCP to be in effect.

    """

    name = 'app-{}'.format(get_uuid())[:32]
    res = opsworks.create_app(
        StackId=stack_id,
        Name=name,
        Type='static',
    )
    assert 'AppId' in res, 'Unexpected response: {}'.format(res)
    app_id = shared_vars['app_id'] = res['AppId']
    print('Created app: {}'.format(app_id))

    stack.callback(
        opsworks.delete_app,
        AppId=app_id,
    )


def test_deploy_app(opsworks, stack_id, instance_id, app_id, stack, shared_vars, scp):
    """Verify access to deploy an app.

    :testid: opsworks-f-1-4

    :param OpsWorks.Client opsworks:
        A handle to the OpsWorks APIs.
    :param str stack_id:
        ID of the OpsWorks Stack created above.
    :param str instance_id:
        ID of the instance created in the OpsWork stack.
    :param str app_id:
        ID of the OpsWork App created above.
    :param contextlib.ExitStack stack:
        A context manager shared across all OpsWorks tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param scp:
        A dependency on the fixture that waits for the SCP to be in effect.

    """

    res = opsworks.create_deployment(
        StackId=stack_id,
        AppId=app_id,
        InstanceIds=[instance_id],
        Command=dict(
            Name='start',
        ),
    )
    assert 'DeploymentId' in res, 'Unexpected response: {}'.format(res)
    deployment_id = res['DeploymentId']
    print('Created deployment: {}'.format(deployment_id))
