"""
=============
test_dynamodb
=============

Verify ability to invoke AWS DynamoDB APIs.

Plan:

* Verify users can create, modify, query, and delete a DynamoDB table
* Verify users cannot access DynamoDB using HTTP
* DynamoDB is accessible within the VPC

${testcount:8}

"""

import json

from botocore.auth import SigV4Auth
from botocore.awsrequest import AWSRequest
from botocore.httpsession import URLLib3Session
import pytest

from aws_test_functions import (
    attach_policy,
    get_uuid,
    ignore_errors,
    retry,
)
from aws_test_functions.vpc import (
    create_service_endpoint,
    delete_service_endpoint,
    get_endpoint_for_service,
)
from aws_test_functions.dynamodb import (
    create_table,
    put_item,
    delete_table,
)

pytestmark = [pytest.mark.scp_check("list_tables")]


@pytest.fixture(scope='module')
def dynamodb(session):
    """A shared handle to DynamoDB APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the DynamoDB APIs.

    """

    return session.client('dynamodb')


@pytest.fixture(scope='module', autouse=True)
def vpc_endpoint(vpc_id, maint_session):
    """Create a VPC Endpoint for DynamoDB.

    :param str vpc_id:
        ID of a VPC.
    :param boto3.session.Session maint_session:
        A session belonging to an authenticated IAM User with access to create
        create VPC endpoints.

    """

    create_service_endpoint('dynamodb', vpc_id, ec2=maint_session.client('ec2'))


@pytest.fixture
def query(table):
    """A reusable query.

    :param dict table:
        Information about the DynamoDB table used for testing.

    :returns:
        A dictionary.

    """

    return dict(
        TableName=table['TableName'],
        KeyConditionExpression='test_attr = :value',
        ExpressionAttributeValues={
            ':value': {
                'S': 'new value',
            },
        },
    )


@pytest.fixture(scope='module')
def raw_request(session, dynamodb):
    """Create a function that issues raw HTTP requests.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param DynamoDB.Client dynamodb:
        A handle to the DynamoDB APIs.

    :returns:
        A function.

    """

    url = dynamodb._endpoint.host
    region = dynamodb._client_config.region_name

    def do_request(proto, method, data):
        """Issue an HTTP request to DynamoDB.

        :param str proto:
            The protocol to use for the request (``http`` or ``https``).
        :param str method:
            DynamoDB client method to invoke, such as ``query``.
        :param dict data:
            Body of the request to send.

        """

        local_url = url
        if proto != 'https':
            local_url = '{}{}'.format(proto, local_url[5:])

        headers = {
            'Content-Type': 'application/x-amz-json-1.0',
            'X-Amz-Target': 'DynamoDB_20120810.Query',
        }

        req = AWSRequest(
            url=local_url,
            method='POST',
            headers=headers,
            data=json.dumps(data),
        )
        SigV4Auth(session.get_credentials(), 'dynamodb', region).add_auth(req)

        print('Issuing raw request to: {}'.format(local_url))
        return URLLib3Session().send(req.prepare())

    return do_request


@pytest.fixture(scope='module', autouse=True)
def dynamo_runner(session, script_runner_role, internal_runner):
    """Temporarily provide full access to DynamoDB to the internal script
    runner by way of its IAM Instance Profile.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param IAM.Role script_runner_role:
        The role assigned to the script runner instance profile.
    :param CommandRunner internal_runner:
        A function that makes it easy to run commands within an EC2 instance.

    :yields:
        ``instance_runner``

    """

    with attach_policy(script_runner_role, 'AmazonDynamoDBFullAccess'):
        yield internal_runner


@pytest.fixture
def no_endpoint(vpc_id, maint_session):
    """Temporarily remove the DynamoDB endpoint for the specified VPC.

    :param str vpc_id:
        ID of a VPC.

    """

    ec2 = maint_session.client('ec2')
    svc = 'dynamodb'
    deleted = False

    try:
        endpoint = get_endpoint_for_service(svc, vpc_id, ec2=ec2)
        delete_service_endpoint(endpoint['VpcEndpointId'], ec2=ec2)
        deleted = True

        yield
    except Exception as exc:
        print("Error deleting DynamoDB endpoint: {}".format(exc))
    finally:
        if deleted:
            create_service_endpoint(svc, vpc_id, ec2=ec2)


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        "table": {
            "TableName": "invalid",
        },
    }


def test_create_table(dynamodb, stack, shared_vars):
    """Users should be able to create a DynamoDB table.

    :testid: dynamodb-f-1-1

    :param DynamoDB.Client dynamodb:
        A handle to the DynamoDB APIs.
    :param dict table:
        A dependency on the fixture that creates a new DynamoDB table.

    """

    name = 'test_dynamodb-{}'.format(get_uuid())
    shared_vars["table"] = table = create_table(name, dynamodb=dynamodb)
    assert table['TableStatus'] == 'ACTIVE', 'Table not active: {}'.format(table)

    res = put_item(table['TableName'], test_attr='testing', dynamodb=dynamodb)
    assert 'ConsumedCapacity' in res, 'Unexpected response: {}'.format(res)

    stack.callback(
        ignore_errors(delete_table),
        name,
        dynamodb=dynamodb,
    )


def test_modify_table(dynamodb, table):
    """Users should be able to modify a DynamoDB table.

    :testid: dynamodb-f-1-2

    :param DynamoDB.Client dynamodb:
        A handle to the DynamoDB APIs.
    :param dict table:
        A dependency on the fixture that creates a new DynamoDB table.

    """

    res = put_item(table['TableName'], test_attr='new value', dynamodb=dynamodb)
    assert 'ConsumedCapacity' in res, 'Unexpected response: {}'.format(res)


def test_query_table(dynamodb, query):
    """Users should be able to query a DynamoDB table.

    :testid: dynamodb-f-1-3

    :param DynamoDB.Client dynamodb:
        A handle to the DynamoDB APIs.
    :param dict query:
        The query to issue to DynamoDB.

    """

    res = dynamodb.query(**query)
    assert 'Items' in res and len(res['Items']), 'Unexpected response: {}'.format(res)
    item = res['Items'][0]

    assert 'test_attr' in item, 'Unexpected response: {}'.format(item)


@pytest.mark.parametrize('proto', ('http', 'https'))
def test_query_using_proto(raw_request, proto, query):
    """Users should not be able to access DynamoDB using HTTP.

    :testid: dynamodb-f-2-1, dynamodb-f-2-2

    :param callable raw_request:
        A function that will perform a raw HTTP request.
    :param str proto:
        Protocol to use (HTTP or HTTPS).
    :param dict query:
        The query to issue to DynamoDB.

    """

    res = raw_request(proto, 'query', query)
    assert res is not None and res.status_code == 200

    body = json.loads(res.content.decode())
    assert body['Count'] == 1
    assert body['Items'][0]['test_attr']['S'] == 'new value'


@pytest.mark.skip(reason='Unsure how to disable HTTP access')
def test_query_using_http_disabled():
    """Disable HTTP access to DynamoDB, and verify that querying the DynamoDB
    table using HTTP fails.

    :testid: dynamodb-f-2-3

    """

    assert False


def test_dynamodb_in_vpc(vpc_endpoint, dynamo_runner, table):
    """Issue a DynamoDB query from within the Type 1 VPC to exercise the
    DynamoDB endpoint.

    :testid: dynamodb-f-3-1

    :param callable dynamo_runner:
        A function that will invoke a command in an EC2 instance.
    :param dict table:
        Information about the DynamoDB table to query.

    """

    conditions = {
        'test_attr': {
            'AttributeValueList': [{'S': 'new value'}],
            'ComparisonOperator': 'EQ',
        }
    }

    cmd = """#!/bin/bash
    mkdir -p ~/.aws/
    echo '[default]\nregion = us-east-1' > ~/.aws/config
    echo '{}' > key-conditions.json
    aws dynamodb query \
        --table-name "{}" \
        --key-conditions file://key-conditions.json
    """.format(
        json.dumps(conditions),
        table['TableName'],
    )

    # print('Running command in VPC: {}'.format(cmd))
    output = dynamo_runner(cmd, timeout=20, max_attempts=10)

    assert 'new value' in output, 'Unexpected output: {}'.format(output)

    data = json.loads(str(output))
    assert data['Count'] == 1
    assert data['Items'][0]['test_attr']['S'] == 'new value'


def test_dynamodb_in_vpc_without_endpoint(no_endpoint, dynamo_runner, table):
    """Temporarily remove the DynamoDB endpoint from the VPC and attempt to
    query the table again. The query should timeout because the DynamoDB
    endpoint is missing.

    :testid: dynamodb-f-3-2

    :param no_endpoint:
        A dependency on the fixture which temporarily removes the DynamoDB
        endpoint.
    :param callable dynamo_runner:
        A function that will invoke a command in an EC2 instance.
    :param dict table:
        Information about the DynamoDB table to query.

    """

    with pytest.raises(AssertionError, match='Command TimedOut'):
        test_dynamodb_in_vpc(None, dynamo_runner, table)


def test_delete_table(dynamodb, table):
    """Users should be able to delete a DynamoDB table.

    :testid: dynamodb-f-1-4

    :param DynamoDB.Client dynamodb:
        A handle to the DynamoDB APIs.
    :param dict table:
        A dependency on the fixture that creates a new DynamoDB table.

    """

    retry(
        delete_table,
        args=(table['TableName'],),
        kwargs={
            'dynamodb': dynamodb,
        },
    )
