"""
=============
test_autoscaling
=============

Verify the ability to deploy an autoscaling configuration to a non default vpc

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Create an Autoscaling configuration and group using both the default VPC(should fail - this is not currently tested for, the default VPC will not be available) and RHEDcloud managed VPC(should succeed)
* Launch an EC2 instance in the VPC and attach it to the Autoscaling group.

${testcount:3}

"""

import pytest

from aws_test_functions import (
    attach_instances_to_group,
    new_autoscaling_group,
    new_ec2_instance,
    new_launch_configuration,
)


@pytest.fixture(scope="module")
def autoscaling(session):
    """A shared handle to AutoScaling APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the AutoScaling APIs.

    """

    return session.client("autoscaling")


def test_non_default_auto_scaling(session, autoscaling, internal_subnet_id, stack):
    """Create autoscaling for managed VPC

    :testid: autoscaling-f-1-2

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param Autoscaling.Client autoscaling:
        A handle to the Autoscaling APIs.
    :param str internal_subnet_id:
        The ID of the subnet where the autoscaling group will be deployed.
    :param contextlib.ExitStack stack:
        A context manager shared across all Auto Scaling tests.

    """

    ec2_instance = stack.enter_context(new_ec2_instance(
        "test_auto_scaling",
        session=session,
        SubnetId=internal_subnet_id,
        ImageId="ami-55ef662f",
        InstanceType="t2.micro",
    ))

    config_name = stack.enter_context(new_launch_configuration("test_auto_scaling_config"))
    group_name = stack.enter_context(new_autoscaling_group(
        "test_auto_scaling_group",
        config_name,
        internal_subnet_id,
    ))

    ec2_instance.wait_until_running()

    with attach_instances_to_group(
        [ec2_instance.instance_id], group_name, True
    ):
        pass
