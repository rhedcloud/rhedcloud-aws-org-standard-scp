'''
===========
test_comprehend
===========
A user should be able to use Amazon Comprehend to find insights and
relationships in text

Plan:

* Create test user
* Detect the dominant language
* Detect named entities
* Detect key phrases
* Detect sentiment

${testcount:4}

'''

import pytest

#This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('list_topics_detection_jobs')]

@pytest.fixture(scope='module')
def comprehend(session):
    return session.client('comprehend', region_name='us-east-1')


@pytest.fixture(scope='module')
def raining_today():
    return 'It is raining today in Seattle.'


def test_dominant_language(comprehend, raining_today):
    '''Detect the dominant language

    :testid: comprehend-f-1-1

    '''
    resp = comprehend.detect_dominant_language(
        Text=raining_today,
    )
    assert 'Languages' in resp
    assert len(resp['Languages']) > 0
    assert resp['Languages'][0]['LanguageCode'] == 'en'


def test_named_entities(comprehend, raining_today):
    '''Detect named entities

    :testid: comprehend-f-1-2

    '''
    resp = comprehend.detect_entities(
        Text=raining_today,
        LanguageCode='en',
    )
    assert 'Entities' in resp
    assert len(resp['Entities']) >= 2
    assert resp['Entities'][0]['Text'] == 'today'
    assert resp['Entities'][0]['Type'] == 'DATE'
    assert resp['Entities'][1]['Text'] == 'Seattle'
    assert resp['Entities'][1]['Type'] == 'LOCATION'


def test_key_phrases(comprehend, raining_today):
    '''Detect key phrases

    :testid: comprehend-f-1-3

    '''
    resp = comprehend.detect_key_phrases(
        Text=raining_today,
        LanguageCode='en',
    )
    assert 'KeyPhrases' in resp
    assert len(resp['KeyPhrases']) >= 2
    assert resp['KeyPhrases'][0]['Text'] == 'today'
    assert resp['KeyPhrases'][1]['Text'] == 'Seattle'


def test_sentiment(comprehend, raining_today):
    '''Detect sentiment

    :testid: comprehend-f-1-4

    '''
    resp = comprehend.detect_sentiment(
        Text=raining_today,
        LanguageCode='en',
    )
    assert 'Sentiment' in resp
    assert resp['Sentiment'] == 'NEUTRAL'
