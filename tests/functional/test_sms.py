"""
========
test_sms
========

Verify ability to invoke AWS Server Migration Service.

Plan:

* Create a new test user with RHEDcloudAdministratorRole
* Invoke SMS GetConnectors
* Invoke SMS GetServers

${testcount:2}

"""

import pytest


@pytest.fixture(scope='module')
def sms(session):
    return session.client('sms')


def test_get_connectors(sms):
    """A customer should be able to call GetConnectors.

    :testid: sms-f-1-1

    """

    res = sms.get_connectors()
    assert 'connectorList' in res, 'unexpected response: {}'.format(res)


def test_get_servers(sms):
    """A customer should be able to call GetServers.

    :testid: sms-f-1-1

    """

    res = sms.get_servers()
    assert 'serverList' in res, 'unexpected response: {}'.format(res)
