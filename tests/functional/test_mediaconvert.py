"""
==============
test_mediaconvert
==============

Verify users can utilize AWS MediaConvert functionality.

${testcount:3}

* Attempt to describe MediaConvert endpoints
* Attempt to create and complete a MediaConvert Job

"""

from copy import deepcopy

import pytest

from aws_test_functions import (
    find_objects_in_bucket,
    has_status,
    retry,
)

pytestmark = [
    # The AWS Elemental Media suite(MediaConvert, MediaPackage, and MediaLive)
    # require about 10 minutes on average, so they're marked as slow tests.
    # Most of the time is spent on the MediaLive channel startup about 8
    # minutes, though 2 minutes are spent on the conversion as well, so those
    # two tests are marked slow
    pytest.mark.slowtest,

    # This service is blocked in the HIPAA repository
    pytest.mark.scp_check("list_jobs"),
]

pytest_plugins = ["tests.plugins.mediaconvert"]

M3U8_SETTINGS = dict(
    AudioFramesPerPes=4,
    PcrControl="PCR_EVERY_PES_PACKET",
    PmtPid=480,
    PrivateMetadataPid=503,
    ProgramNumber=1,
    PatInterval=0,
    PmtInterval=0,
    Scte35Source="NONE",
    Scte35Pid=500,
    NielsenId3="NONE",
    TimedMetadata="NONE",
    TimedMetadataPid=502,
    VideoPid=481,
    AudioPids=list(range(482, 493)),
)


H264_SETTINGS = dict(
    InterlaceMode="PROGRESSIVE",
    NumberReferenceFrames=3,
    Syntax="DEFAULT",
    Softness=0,
    GopClosedCadence=1,
    GopSize=90,
    Slices=1,
    GopBReference="DISABLED",
    SlowPal="DISABLED",
    SpatialAdaptiveQuantization="ENABLED",
    TemporalAdaptiveQuantization="ENABLED",
    FlickerAdaptiveQuantization="DISABLED",
    EntropyEncoding="CABAC",
    Bitrate=5000000,
    FramerateControl="INITIALIZE_FROM_SOURCE",
    RateControlMode="CBR",
    CodecProfile="MAIN",
    Telecine="NONE",
    MinIInterval=0,
    AdaptiveQuantization="HIGH",
    CodecLevel="AUTO",
    FieldEncoding="PAFF",
    SceneChangeDetect="ENABLED",
    QualityTuningLevel="SINGLE_PASS",
    FramerateConversionAlgorithm="DUPLICATE_DROP",
    UnregisteredSeiTimecode="DISABLED",
    GopSizeUnits="FRAMES",
    ParControl="INITIALIZE_FROM_SOURCE",
    NumberBFramesBetweenReferenceFrames=2,
    RepeatPps="DISABLED",
)


AUDIO_CODEC_SETTINGS = dict(
    Codec="AAC",
    AacSettings=dict(
        AudioDescriptionBroadcasterMix="NORMAL",
        Bitrate=96000,
        RateControlMode="CBR",
        CodecProfile="LC",
        CodingMode="CODING_MODE_2_0",
        RawFormat="NONE",
        SampleRate=48000,
        Specification="MPEG4",
    ),
)


OUTPUT_SETTINGS = dict(
    HlsSettings=dict(
        AudioGroupId="program_audio",
        AudioRenditionSets="program_audio",
        IFrameOnlyManifest="EXCLUDE",
    ),
)


OUTPUTS = dict(
    NameModifier="_converted",
    ContainerSettings=dict(
        Container="M3U8",
        M3u8Settings=M3U8_SETTINGS,
    ),
    VideoDescription=dict(
        CodecSettings=dict(
            Codec="H_264",
            H264Settings=H264_SETTINGS,
        ),
    ),
    AudioDescriptions=[
        dict(
            AudioTypeControl="FOLLOW_INPUT",
            LanguageCodeControl="FOLLOW_INPUT",
            CodecSettings=AUDIO_CODEC_SETTINGS,
        ),
    ],
    OutputSettings=OUTPUT_SETTINGS,
)


@pytest.fixture(scope="module")
def s3(session):
    """A shared handle to S3 APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the S3 APIs.

    """

    return session.client("s3")


@pytest.fixture(scope="module", autouse=True)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        # The URL of the MediaConvert job endpoint associated with the account
        "endpoint_url": None,
    }


def create_job(
    session,
    s3,
    mediaconvert,
    endpoint_url,
    mediaconvert_role,
    convert_bucket,
    backup_bucket,
):
    """Helper method to ensure that the conversion is only done once per session, outputs the conversion
       to both the convert_bucket and backup_bucket

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param S3.Client s3:
        A handle to the S3 APIs.
    :param MediaConvert.Client mediaconvert:
        A handle to the MediaConvert APIs.
    :param str endpoint_url:
        The account specific endpoint to use for MediaConvert
    :param boto3.IAM.Role mediaconvert_role:
        A Role for running the convert job.
    :param S3.Bucket convert_bucket:
        A handle to an S3 Bucket owned by the user containing big_buck_bunny.mp4.
    :param S3.Bucket backup_bucket:
        A handle to another S3 Bucket owned by the user(ideally containing big_buck_bunny.mp4 though not necessary).

    """

    # Ensure the method has not already been run
    if find_objects_in_bucket(convert_bucket.name, "big_buck_bunny.m3u8")[
        "big_buck_bunny.m3u8"
    ]:
        return

    defined_enpoint = endpoint_url
    if defined_enpoint is None:
        res = mediaconvert.describe_endpoints()
        assert len(res["Endpoints"]), "Unexpected response: {}".format(res)
        defined_enpoint = res["Endpoints"][0]["Url"]

    mediaconvert_with_endpoint = session.client(
        "mediaconvert", endpoint_url=defined_enpoint
    )

    destination = "s3://{}/".format(convert_bucket.name)
    backup_destination = "s3://{}/".format(backup_bucket.name)
    file_input = destination + "big_buck_bunny.mp4"

    inputs = dict(
        FileInput=file_input,
        AudioSelectors={
            "Audio Selector 1": dict(
                Offset=0, DefaultSelection="DEFAULT", ProgramSelection=1
            )
        },
        VideoSelector=dict(ColorSpace="FOLLOW"),
        FilterEnable="AUTO",
        PsiControl="USE_PSI",
        FilterStrength=0,
        DeblockFilter="DISABLED",
        DenoiseFilter="DISABLED",
        TimecodeSource="EMBEDDED",
    )

    hls_group_settings_one = dict(
        ManifestDurationFormat="INTEGER",
        SegmentLength=10,
        TimedMetadataId3Period=10,
        CaptionLanguageSetting="OMIT",
        Destination=destination,
        TimedMetadataId3Frame="PRIV",
        CodecSpecification="RFC_4281",
        OutputSelection="MANIFESTS_AND_SEGMENTS",
        ProgramDateTimePeriod=600,
        MinSegmentLength=0,
        DirectoryStructure="SINGLE_DIRECTORY",
        ProgramDateTime="EXCLUDE",
        SegmentControl="SEGMENTED_FILES",
        ManifestCompression="NONE",
        ClientCache="ENABLED",
        StreamInfResolution="INCLUDE",
    )

    hls_group_settings_two = deepcopy(hls_group_settings_one)
    hls_group_settings_two["Destination"] = backup_destination

    output_group_one = dict(
        CustomName="TestConvertHLSOutputGroupOne",
        Name="Apple HLS One",
        OutputGroupSettings=dict(
            Type="HLS_GROUP_SETTINGS", HlsGroupSettings=hls_group_settings_one
        ),
        Outputs=[OUTPUTS],
    )

    output_group_two = dict(
        CustomName="TestConvertHLSOutputGroupTwo",
        Name="Apple HLS Two",
        OutputGroupSettings=dict(
            Type="HLS_GROUP_SETTINGS", HlsGroupSettings=hls_group_settings_two
        ),
        Outputs=[OUTPUTS],
    )

    settings = dict(Inputs=[inputs], OutputGroups=[output_group_one, output_group_two])

    res = mediaconvert_with_endpoint.create_job(
        Role=mediaconvert_role.arn, Settings=settings
    )
    assert has_status(res, 200, 201)

    job_id = res["Job"]["Id"]
    res = retry(
        mediaconvert_with_endpoint.get_job,
        kwargs={"Id": job_id},
        msg="Checking MediaConvert job {} status".format(job_id),
        until=lambda s: s["Job"]["Status"] in ("ERROR", "CANCELED", "COMPLETE"),
        delay=10,
        max_attempts=15,
    )
    assert (
        res["Job"]["Status"] == "COMPLETE"
    ), "MediaConvert job failed with status {}".format(
        res["Job"]["Status"]
    )


def test_describe_endpoints(mediaconvert, shared_vars):
    """A customer should be able to list mediaconvert endpoints

    :testid: mediaconvert-f-1-1

    :param MediaConvert.Client mediaconvert:
        A handle to the MediaConvert APIs.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    # We may have already retrieved the endpoint depending on the order of tests
    if shared_vars["endpoint_url"] is None:
        res = mediaconvert.describe_endpoints()
        assert len(res["Endpoints"]), "Unexpected response: {}".format(res)
        shared_vars["endpoint_url"] = res["Endpoints"][0]["Url"]


def test_create_job(
    session,
    s3,
    mediaconvert,
    shared_vars,
    mediaconvert_role,
    convert_bucket,
    backup_bucket,
):
    """A customer should be able to create and successfully run a MediaConvert job

    :testid: mediaconvert-f-1-2

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param S3.Client s3:
        A handle to the S3 APIs.
    :param MediaConvert.Client mediaconvert:
        A handle to the MediaConvert APIs.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param boto3.IAM.Role mediaconvert_role:
        A Role for running the convert job.
    :param S3.Bucket convert_bucket:
        A handle to an S3 Bucket owned by the user containing big_buck_bunny.mp4.
    :param S3.Bucket backup_bucket:
        A handle to another S3 Bucket owned by the user(ideally containing big_buck_bunny.mp4 though not necessary).

    """

    create_job(
        session,
        s3,
        mediaconvert,
        shared_vars["endpoint_url"],
        mediaconvert_role,
        convert_bucket,
        backup_bucket,
    )

    assert find_objects_in_bucket(convert_bucket.name, "big_buck_bunny.m3u8")[
        "big_buck_bunny.m3u8"
    ], "Unable to find primary converted HLS manifest"

    assert find_objects_in_bucket(backup_bucket.name, "big_buck_bunny.m3u8")[
        "big_buck_bunny.m3u8"
    ], "Unable to find backup converted HLS manifest"
