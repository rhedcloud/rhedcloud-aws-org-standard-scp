"""
==============
test_iam_users
==============

Customers should not have any IAM permissions for IAM users.

${testcount:5}

"""

import pytest

from aws_test_functions import (
    debug,
    get_policy_arn,
    ignore_errors,
    new_test_user,
)

pytestmark = pytest.mark.raises_access_denied


@pytest.fixture(scope='module')
def iam(session):
    return session.client('iam')


@pytest.fixture
def iam_name():
    return "test_iam_users"


@pytest.fixture
def policy_arn():
    return get_policy_arn("RHEDcloudAdministratorRolePolicy")


@pytest.fixture(scope='function', autouse=True)
def scp(iam, wait_for_scp, user):
    """Fixture that waits for the SCP to take effect before allowing a test to
    proceed.

    """

    wait_for_scp(iam.update_user, kwargs=dict(UserName=user.user_name, NewUserName=user.user_name), delay=30)


def test_create_user(session):
    """A customer should NOT be able to create new Users.

    :testid: iam-f-3-1, iam-f-3-2

    """

    iam = session.resource('iam')
    with new_test_user('test_iam_users', iam=iam):
        print("this should not happen")


def test_modify_user(iam, user):
    """A customer should NOT be able to modify Users.

    :testid: iam-f-3-3

    """

    fakename = 'fredtester'
    iam.update_user(UserName=user.user_name, NewUserName=fakename)

    # if we got here, we need to undo what we just did
    iam.update_user(UserName=fakename, NewUserName=user.user_name)


def test_attach_user_policy(iam, iam_name, policy_arn, stack):
    """Verify access to attach IAM policies to IAM users.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM user.
    :param str policy_arn:
        ARN of an IAM policy.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = iam.attach_user_policy(
        UserName=iam_name,
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Attached policy {} to user {}".format(policy_arn, iam_name))
    stack.callback(
        ignore_errors(test_detach_user_policy),
        iam,
        iam_name,
        policy_arn,
    )


def test_detach_user_policy(iam, iam_name, policy_arn):
    """Verify access to detach IAM policies from IAM users.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param str iam_name:
        Name of an IAM user.
    :param str policy_arn:
        ARN of an IAM policy.

    """

    debug("Detaching policy {} from user {}".format(policy_arn, iam_name))
    res = iam.detach_user_policy(
        UserName=iam_name,
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)


def test_create_rhedcloud_user(iam, iam_name, stack):
    """A customer should NOT be able to create new RHEDcloud Users.

    :testid: iam-f-3-4

    """

    iam.create_user(
        UserName=iam_name,
        Path="/rhedcloud/",
    )

    stack.callback(
        ignore_errors(iam.delete_user),
        UserName=iam_name,
    )


def test_clone_rhedcloud_user(iam):
    """A customer should NOT be able to clone RHEDcloud-created Users.

    :testid: iam-f-3-4

    """

    iam.create_user(UserName='joshv')


def test_delete_rhedcloud_user(iam, user):
    """A customer should NOT be able to delete RHEDcloud-created Users.

    :testid: iam-f-3-5

    """

    iam.delete_user(UserName=user.user_name)


def test_modify_rhedcloud_user(iam, user):
    """A customer should NOT be able to modify RHEDcloud-created Users.

    :testid: iam-f-3-6

    """

    fakename = 'fredtester'
    iam.update_user(UserName='joshv', NewUserName=fakename)

    # if we got here, we need to undo what we just did
    iam.update_user(UserName=fakename, NewUserName='joshv')
