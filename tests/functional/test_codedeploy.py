"""
===============
test_codedeploy
===============

Verify users can utilize AWS CodeDeploy functionality.

${testcount:16}

* Create a new test user with the RHEDcloudAdministratorRole.
* Create a new Lambda function
* Publish a new version of the Lambda function
* Create an application
* Create a deployment group
* Register an application revision
* Deploy a new revision
* Check the status of revision deployment

"""

import pytest

from aws_test_functions import (
    any_in,
    ignore_errors,
    retry,
    unexpected,
)

from tests.functional import test_lambda

# This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('list_applications')]

pytest_plugins = [
    'tests.plugins.codedeploy',
    'tests.plugins._lambda',
]

# Bypass the "Access Denied" expectation for tests that use Lambda (as it is
# not blocked)
# NOTE: this might get more interesting if/when the blocks change
bypass_access_denied = pytest.mark.bypass('raises_access_denied')

# treat errors about nonexistent items as access denied errors (because they
# likely originate from an access denied error earlier in the suite).
ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden|Authorization|DoesNotExist).*'


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'deploy_id': '',
    }


@bypass_access_denied
def test_create_function(
    _lambda, codedeploy_role, lambda_func_name, lambda_code,
    lambda_zip_bytes, stack,
):
    """
    Create a new Lambda function.

    :testid: codedeploy-f-1-1

    :param Lambda.Client _lambda:
        A handle to the Lambda API.
    :param IAM.Role codedeploy_role:
        An IAM Role resource.
    :param str lambda_func_name:
        The name of the Lambda function.
    :param str lambda_code:
        The code for the Lambda function.
    :param bytestring lambda_zip_bytes:
        The byte string of a Zip file containing the Lambda function code.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodeDeploy tests.

    """

    retry(
        test_lambda.test_create_function,
        args=(_lambda, codedeploy_role, lambda_func_name, lambda_zip_bytes, stack),
        msg='Creating Lambda function',
        show=True,
    )


@bypass_access_denied
def test_create_alias(_lambda, lambda_func_name, stack):
    """Verify access to create a Lambda function alias.

    :testid: codedeploy-f-1-1

    :param Lambda.Client _lambda:
        A handle to the Lambda API.
    :param str lambda_func_name:
        The name of the Lambda function.
    :param contextlib.ExitStack stack:
        A context manager shared across all Lambda tests.

    """

    test_lambda.test_create_alias(_lambda, lambda_func_name, stack)


@bypass_access_denied
def test_publish_new_version(_lambda, lambda_func_name, codedeploy_zip_bytes):
    """
    Publish a new version of the Lambda function.

    :testid: codedeploy-f-1-1

    :param Lambda.Client _lambda:
        A handle to the Lambda API.
    :param str lambda_func_name:
        The name of the Lambda function.
    :param bytestring codedeploy_zip_bytes:
        The byte string of a Zip file containing the updated Lambda function code.

    """

    res = _lambda.update_function_code(
        FunctionName=lambda_func_name,
        ZipFile=codedeploy_zip_bytes,
        Publish=True,
    )
    assert 'FunctionArn' in res, unexpected(res)
    print('Published new version of Lambda function: {}'.format(lambda_func_name))


def test_create_application(codedeploy, app_name, stack):
    """A customer should be able to create a CodeDeploy application

    :testid: codedeploy-f-1-1, codedeploy-f-2-1, codedeploy-f-2-7

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application to create.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodeDeploy tests.

    """

    res = codedeploy.create_application(
        applicationName=app_name,
        computePlatform='Lambda',
    )
    assert 'applicationId' in res, unexpected(res)
    print('Created application: {}'.format(app_name))

    stack.callback(
        ignore_errors(codedeploy.delete_application),
        applicationName=app_name,
    )


def test_list_applications(codedeploy, app_name):
    """Verify access to list applications.

    :testid: codedeploy-f-2-2

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.

    """

    res = codedeploy.list_applications()
    assert 'applications' in res, unexpected(res)
    assert app_name in res['applications'], unexpected(res)


def test_get_application(codedeploy, app_name):
    """Verify access to describe applications.

    :testid: codedeploy-f-2-3

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.

    """

    res = codedeploy.get_application(applicationName=app_name)
    assert 'application' in res, unexpected(res)
    assert res['application']['applicationName'] == app_name, unexpected(res)


def test_create_deployment_group(codedeploy, app_name, group_name, codedeploy_role, stack):
    """A customer should be able to create a CodeDeploy Deployment Group

    :testid: codedeploy-f-1-1, codedeploy-f-3-1, codedeploy-f-3-5

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.
    :param str group_name:
        Name of the CodeDeploy group to create.
    :param boto3.IAM.Role codedeploy_role:
        A Role permitted to run CodeDeploy tasks.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodeDeploy tests.

    """

    codedeploy.create_deployment_group(
        applicationName=app_name,
        deploymentGroupName=group_name,
        serviceRoleArn=codedeploy_role.arn,
        deploymentStyle=dict(
            deploymentType='BLUE_GREEN',
            deploymentOption='WITH_TRAFFIC_CONTROL',
        ),
    )
    print('Created deployment group: {}'.format(group_name))

    stack.callback(
        ignore_errors(codedeploy.delete_deployment_group),
        applicationName=app_name,
        deploymentGroupName=group_name,
    )


def test_list_deployment_groups(codedeploy, app_name):
    """Verify access to list deployment groups.

    :testid: codedeploy-f-3-2

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.

    """

    res = codedeploy.list_deployment_groups(
        applicationName=app_name,
    )

    key = 'deploymentGroups'
    assert key in res and len(res[key]), unexpected(res)


def test_get_deployment_group(codedeploy, app_name, group_name):
    """Verify access to list deployment groups.

    :testid: codedeploy-f-3-3

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.
    :param str group_name:
        Name of the CodeDeploy group.

    """

    res = codedeploy.get_deployment_group(
        applicationName=app_name,
        deploymentGroupName=group_name,
    )
    assert 'deploymentGroupInfo' in res, unexpected(res)


def test_update_deployment_group(codedeploy, app_name, group_name):
    """Verify access to update deployment groups.

    :testid: codedeploy-f-3-4

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.
    :param str group_name:
        Name of the CodeDeploy group.

    """

    res = codedeploy.update_deployment_group(
        applicationName=app_name,
        currentDeploymentGroupName=group_name,
        deploymentConfigName='CodeDeployDefault.LambdaAllAtOnce',
    )
    assert res['ResponseMetadata']['HTTPStatusCode'] == 200, unexpected(res)


def test_register_revision(codedeploy, app_name, revision_info):
    """A customer should be able to register a CodeDeploy Application Revisiuon

    :testid: codedeploy-f-1-1, codedeploy-f-2-4

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application to create.
    :param dict revision_info:
        Information about the revision.

    """

    codedeploy.register_application_revision(
        applicationName=app_name,
        revision=revision_info,
    )
    print('Registered application revision:\n{}'.format(revision_info))


def test_list_application_revisions(codedeploy, app_name):
    """Verify access to list application revisions.

    :testid: codedeploy-f-2-5

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.

    """

    res = codedeploy.list_application_revisions(
        applicationName=app_name,
    )
    assert 'revisions' in res and len(res['revisions']), unexpected(res)


def test_get_application_revision(codedeploy, app_name, revision_info):
    """Verify access to describe application revisions.

    :testid: codedeploy-f-2-6

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.
    :param dict revision_info:
        Information about the revision.

    """

    res = codedeploy.get_application_revision(
        applicationName=app_name,
        revision=revision_info,
    )
    assert 'applicationName' in res, unexpected(res)
    assert 'revision' in res, unexpected(res)
    assert 'revisionInfo' in res, unexpected(res)


def test_create_deployment(codedeploy, app_name, group_name, revision_info, shared_vars):
    """A customer should be able to create a CodeDeploy Deployment

    :testid: codedeploy-f-1-1, codedeploy-f-4-1

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.
    :param str group_name:
        Name of the CodeDeploy group.
    :param dict revision_info:
        Information about the revision.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = codedeploy.create_deployment(
        applicationName=app_name,
        deploymentGroupName=group_name,
        revision=revision_info,
    )
    assert 'deploymentId' in res, unexpected(res)

    deploy_id = shared_vars['deploy_id'] = res['deploymentId']
    print('Created deployment: {}'.format(deploy_id))


def test_list_deployments(codedeploy, app_name, group_name):
    """A customer should be able to list deployments.

    :testid: codedeploy-f-4-2

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.
    :param str group_name:
        Name of the CodeDeploy group.

    """

    res = codedeploy.list_deployments(
        applicationName=app_name,
        deploymentGroupName=group_name,
    )
    assert 'deployments' in res, unexpected(res)
    assert len(res['deployments']) >= 1, unexpected(res)


@bypass_access_denied
def test_check_deployment_status(codedeploy, deploy_id):
    """A customer should be able to access the deployments and their statuses

    :testid: codedeploy-f-1-1, codedeploy-f-4-3

    :param CodeDeploy.Client codedeploy:
        A handle to the CodeDeploy APIs.
    :param str app_name:
        Name of the CodeDeploy application.
    :param str group_name:
        Name of the CodeDeploy group.

    """

    if not deploy_id:
        print('Prior AccessDenied error means no deploy_id is available')
        return

    def _test(res):
        return res['deploymentInfo']['status'] in ('Succeeded', 'Failed', 'Stopped', 'Ready')

    res = retry(
        codedeploy.get_deployment,
        kwargs=dict(
            deploymentId=deploy_id,
        ),
        msg='Checking deployment status: {}'.format(deploy_id),
        until=_test,
        pred=lambda ex: any_in(ex, 'DeploymentDoesNotExistException'),
    )
    assert 'deploymentInfo' in res, unexpected(res)
    assert res['deploymentInfo']['status'] == 'Succeeded', unexpected(res)


# For some reason these two functions are executed before the status check
# test, which causes problems. Delete functionality is handled implicitly where
# the deployment group and application are created, as rollback steps, so it's
# not a big deal to comment these out.
#
# def test_delete_deployment_group(codedeploy, app_name, group_name):
#     """Verify access to delete deployment groups.
#
#     :testid: codedeploy-f-3-5
#
#     :param CodeDeploy.Client codedeploy:
#         A handle to the CodeDeploy APIs.
#     :param str app_name:
#         Name of the CodeDeploy application.
#
#     """
#
#     res = codedeploy.delete_deployment_group(
#         applicationName=app_name,
#         deploymentGroupName=group_name,
#     )
#     assert res['ResponseMetadata']['HTTPStatusCode'] == 200, unexpected(res)
#
#
# def test_delete_application(codedeploy, app_name):
#     """Verify access to delete applications.
#
#     :testid: codedeploy-f-2-7
#
#     :param CodeDeploy.Client codedeploy:
#         A handle to the CodeDeploy APIs.
#     :param str app_name:
#         Name of the CodeDeploy application.
#
#     """
#
#     res = codedeploy.delete_application(
#         applicationName=app_name,
#     )
#     assert res['ResponseMetadata']['HTTPStatusCode'] == 200, unexpected(res)
