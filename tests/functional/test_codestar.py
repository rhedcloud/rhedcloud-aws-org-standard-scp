"""
=============
test_codestar
=============

Verify users can utilize AWS CodeStar functionality.

${testcount:12}

* Create a new test user with the RHEDcloudAdministratorRole.
* Create a project
* Describe a project
* Update a project
* Delete a project
* Create a user profile
* Describe a user profile
* Update a user profile
* Delete a user profile
* Associate a user with a project
* List members associated with a project
* Update team members associated with a project
* Disassociate a team member from a project

"""

import pytest

from aws_test_functions import (
    get_or_create_role,
    ignore_errors,
    make_identifier,
    retry,
    unexpected,
)

# This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('list_projects')]


@pytest.fixture(scope='module')
def codestar(session):
    """A shared handle to CodeStar APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the CodeStar APIs.

    """

    return session.client('codestar')


@pytest.fixture(scope='module')
def project_id():
    """Generate a new project ID."""

    return make_identifier('codestar')[:15]


@pytest.fixture(scope='module', autouse=True)
def role(session):
    """Create a service role for CodeStar.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        An IAM Role resource.

    """

    iam = session.resource('iam')
    policy_arn = 'arn:aws:iam::aws:policy/service-role/AWSCodeStarServiceRole'

    return get_or_create_role(
        'aws-codestar-service-role',
        policy_arn,
        service='codestar',
        service_role=True,
        iam=iam,
        delay=15,
    )


def test_create_project(codestar, project_id, stack):
    """Verify access to create a project

    :testid: codestar-f-1-1, codestar-f-1-4

    :param CodeStar.Client codestar:
        A handle to the CodeStar APIs.
    :param str project_id:
        ID of the project to create.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodeStar tests.

    """

    res = retry(
        codestar.create_project,
        kwargs=dict(
            name='TestCodeStarProject',
            id=project_id,
        ),
        msg='Creating CodeStar project',
        pred=lambda ex: 'ProjectAlreadyExistsException' not in str(ex),
        show=True,
    )
    assert 'arn' in res, unexpected(res)

    stack.callback(
        ignore_errors(codestar.delete_project),
        id=project_id,
    )


def test_describe_project(codestar, project_id):
    """Verify access to describe projects.

    :testid: codestar-f-1-3

    :param CodeStar.Client codestar:
        A handle to the CodeStar APIs.
    :param str project_id:
        ID of a project.

    """

    res = codestar.describe_project(
        id=project_id,
    )
    assert 'arn' in res, unexpected(res)


def test_update_project(codestar, project_id):
    """Verify access to describe projects.

    :testid: codestar-f-1-2

    :param CodeStar.Client codestar:
        A handle to the CodeStar APIs.
    :param str project_id:
        ID of a project.

    """

    res = codestar.update_project(
        id=project_id,
        description='testing',
    )
    assert res['ResponseMetadata']['HTTPStatusCode'] == 200, unexpected(res)


def test_create_user_profile(codestar, user, stack):
    """Verify access to create a user profile.

    :testid: codestar-f-2-1, codestar-f-2-4

    :param CodeStar.Client codestar:
        A handle to the CodeStar APIs.
    :param IAM.User user:
        User resource for the current test user.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodeStar tests.

    """

    res = codestar.create_user_profile(
        userArn=user.arn,
        displayName='test user',
        emailAddress='test@test.com',
    )
    assert 'userArn' in res, unexpected(res)
    print('Created user profile for {}'.format(user.arn))

    stack.callback(
        ignore_errors(codestar.delete_user_profile),
        userArn=user.arn,
    )


def test_list_user_profiles(codestar):
    """Verify access to list user profiles.

    :testid: codestar-f-2-2

    :param CodeStar.Client codestar:
        A handle to the CodeStar APIs.

    """

    res = codestar.list_user_profiles()
    key = 'userProfiles'
    assert key in res and len(res[key]), unexpected(res)


def test_update_user_profile(codestar, user):
    """Verify access to update a user profile.

    :testid: codestar-f-2-3

    :param CodeStar.Client codestar:
        A handle to the CodeStar APIs.
    :param IAM.User user:
        User resource for the current test user.

    """

    new_name = 'test user updated'
    res = codestar.update_user_profile(
        userArn=user.arn,
        displayName=new_name,
    )
    assert 'displayName' in res, unexpected(res)
    assert res['displayName'] == new_name, unexpected(res)


def test_delete_user_profile(codestar, user):
    """Verify access to delete a user profile.

    :testid: codestar-f-2-4

    :param CodeStar.Client codestar:
        A handle to the CodeStar APIs.
    :param IAM.User user:
        User resource for the current test user.

    """

    res = codestar.delete_user_profile(
        userArn=user.arn,
    )
    assert res['ResponseMetadata']['HTTPStatusCode'] == 200, unexpected(res)


def test_associate_team_member(codestar, user, project_id, stack):
    """Verify access to associate a user with a project.

    :testid: codestar-f-3-1, codestar-f-3-4

    :param CodeStar.Client codestar:
        A handle to the CodeStar APIs.
    :param IAM.User user:
        User resource for the current test user.
    :param str project_id:
        ID of a project.
    :param contextlib.ExitStack stack:
        A context manager shared across all CodeStar tests.

    """

    res = retry(
        codestar.associate_team_member,
        kwargs=dict(
            projectId=project_id,
            userArn=user.arn,
            projectRole='Owner',
        ),
        msg="Associating team member",
        pred=lambda ex: "ProjectConfigurationException" in str(ex),
    )
    assert res['ResponseMetadata']['HTTPStatusCode'] == 200, unexpected(res)

    stack.callback(
        ignore_errors(codestar.disassociate_team_member),
        projectId=project_id,
        userArn=user.arn,
    )


def test_list_team_members(codestar, project_id):
    """Verify access to list users associated with a project.

    :testid: codestar-f-3-2

    :param CodeStar.Client codestar:
        A handle to the CodeStar APIs.
    :param str project_id:
        ID of a project.

    """

    res = codestar.list_team_members(
        projectId=project_id,
    )
    key = 'teamMembers'
    assert key in res and len(res[key]), unexpected(res)


def test_update_team_member(codestar, user, project_id):
    """Verify access to update users associated with a project.

    :testid: codestar-f-3-3

    :param CodeStar.Client codestar:
        A handle to the CodeStar APIs.
    :param IAM.User user:
        User resource for the current test user.
    :param str project_id:
        ID of a project.

    """

    res = codestar.update_team_member(
        projectId=project_id,
        userArn=user.arn,
        projectRole='Viewer',
    )
    assert 'projectRole' in res, unexpected(res)
    assert res['projectRole'] == 'Viewer', unexpected(res)


def test_disassociate_team_member(codestar, user, project_id):
    """Verify access to disassociate users from a project.

    :testid: codestar-f-3-4

    :param CodeStar.Client codestar:
        A handle to the CodeStar APIs.
    :param IAM.User user:
        User resource for the current test user.
    :param str project_id:
        ID of a project.

    """

    res = codestar.disassociate_team_member(
        projectId=project_id,
        userArn=user.arn,
    )
    assert res['ResponseMetadata']['HTTPStatusCode'] == 200, unexpected(res)


def test_delete_project(codestar, project_id):
    """Verify access to delete a project.

    :testid: codestar-f-1-4

    :param CodeStar.Client codestar:
        A handle to the CodeStar APIs.
    :param str project_id:
        ID of the project to delete.

    """

    codestar.delete_project(id=project_id)
