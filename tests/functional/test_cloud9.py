"""
=============
test_cloud9
=============

Verify access to the AWS Cloud9 API.

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Attempt to create a Cloud9 environment
* Attempt to list the Cloud9 environments and verify the created environment is returned
* Attempt to update the Cloud9 environment
* Attempt to query the Cloud9 environment status
* Attempt to create a Cloud9 membership
* Attempt to describe the Cloud9 memberships and verify the membership was created
* Attempt to update the Cloud9 membership
* Attempt to delete the Cloud9 membership
* Attempt to delete the Cloud9 environment

${testcount:9}

"""

import pytest

from aws_test_functions import (
    aws_client,
    catch,
    has_status,
    ignore_errors,
    in_setup_org,
    make_identifier,
    unexpected,
)

ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden|Validation|NotFound).*'

# This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('list_environments')]

# Create a reusable functor to move the AWS account to the SETUP_ORG before
# attempting to invoke the requested function. This is necessary to ensure that
# we make the best effort to remove resources created during testing (since we
# should not be able to delete with the SCP in effect in the TEST_ORG).
cleaner = in_setup_org('Cloud9 cleanup', one_way=True)


@pytest.fixture(scope='module')
def cloud9(session):
    """A shared handle to Cloud9 APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Cloud9 APIs.

    """

    return session.client('cloud9')


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'environment_id': 'notcreated'
    }


def test_create_environment(cloud9, shared_vars, stack):
    """ Users should be able to create a Cloud9 environment

    :testid: cloud9-f-1-1

    :param Cloud9.Client cloud9:
        A handle to the Cloud9 API.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param contextlib.ExitStack stack:
        A context manager shared across all Cloud9 tests.

    """

    res = cloud9.create_environment_ec2(
        name=make_identifier('cloud9'),
        instanceType='t2.micro',
        clientRequestToken=make_identifier('cloud9')
    )
    assert has_status(res, 200), unexpected(res)
    assert res['environmentId'] is not None, 'Unable to create environment'

    shared_vars['environment_id'] = res['environmentId']
    stack.callback(
        ignore_errors(cleaner(cloud9.delete_environment)),
        environmentId=shared_vars['environment_id']
    )


def test_list_environments(cloud9, environment_id):
    """Users should be able to list Cloud9 environments.

    :testid: cloud9-f-1-2

    :param Cloud9.Client cloud9:
        A handle to the Cloud9 API.
    :param str environment_id:
        The ID of a created Cloud9 Environment

    """

    res = cloud9.list_environments()
    assert has_status(res, 200), unexpected(res)

    found = False
    for env in res['environmentIds']:
        if env == environment_id:
            found = True
            break

    assert found, \
        'Did not find Cloud9 Environment {} in list_environments: {}'.format(
            environment_id,
            res['environmentIds'],
        )


def test_update_environment(cloud9, environment_id):
    """ Users should be able to update a Cloud9 environment

    :testid: cloud9-f-1-3

    :param Cloud9.Client cloud9:
        A handle to the Cloud9 API.
    :param str environment_id:
        The ID of a created Cloud9 Environment

    """

    updated_name = make_identifier('cloud9_updated')

    res = cloud9.update_environment(
        environmentId=environment_id,
        name=updated_name
    )
    assert has_status(res, 200), unexpected(res)

    res = cloud9.describe_environments(
        environmentIds=[environment_id]
    )
    assert has_status(res, 200), unexpected(res)

    environments = res['environments']
    assert len(environments) > 0, 'No environments returned'
    assert environments[0]['name'] == updated_name, 'Environment name not updated'


def test_describe_environment_status(cloud9, environment_id):
    """ Users should be able to retrieve an environment's status

    :testid: cloud9-f-1-4

    :param Cloud9.Client cloud9:
        A handle to the Cloud9 API.
    :param str environment_id:
        The ID of a created Cloud9 Environment

    """

    res = cloud9.describe_environment_status(
        environmentId=environment_id
    )

    assert res['status'] is not None, 'Unable to retrieve environment status'


def test_create_membership(cloud9, user_two, environment_id):
    """ Users should be able to create an environment membership

    :testid: cloud9-f-2-1

    :param Cloud9.Client cloud9:
        A handle to the Cloud9 API.
    :param IAM.User user_two
        A user that does not own the Cloud9 Environment
    :param str environment_id:
        The ID of a created Cloud9 Environment

    """

    res = cloud9.create_environment_membership(
        environmentId=environment_id,
        userArn=user_two.arn,
        permissions='read-write'
    )
    assert has_status(res, 200), 'Unable to create environment membership'


def test_describe_environment_memberships(cloud9, user_two, environment_id):
    """ Users should be able to query environment memberships

    :testid: cloud9-f-2-2

    :param Cloud9.Client cloud9:
        A handle to the Cloud9 API.
    :param IAM.User user_two
        A user that does not own the Cloud9 Environment
    :param str environment_id:
        The ID of a created Cloud9 Environment

    """

    res = cloud9.describe_environment_memberships(
        userArn=user_two.arn,
        environmentId=environment_id
    )
    assert has_status(res, 200), 'Unable to describe environment memberships'
    assert res['memberships'][0]['permissions'] == 'read-write', \
        'Created membership has incorrect permissions: {}'.format(res['memberships'][0])


def test_update_environment_memberships(cloud9, user_two, environment_id):
    """ Users should be able to update an environment membership

    :testid: cloud9-f-2-3

    :param Cloud9.Client cloud9:
        A handle to the Cloud9 API.
    :param IAM.User user_two
        A user that does not own the Cloud9 Environment
    :param str environment_id:
        The ID of a created Cloud9 Environment

    """

    res = cloud9.update_environment_membership(
        userArn=user_two.arn,
        environmentId=environment_id,
        permissions='read-only'
    )
    assert has_status(res, 200), 'Unable to update environment membership'

    res = cloud9.describe_environment_memberships(
        userArn=user_two.arn,
        environmentId=environment_id
    )
    assert has_status(res, 200), 'Unable to describe environment memberships'
    assert res['memberships'][0]['permissions'] == 'read-only', \
        'Updated membership has incorrect permissions: {}'.format(res['memberships'][0])


def test_delete_environment_membership(cloud9, user_two, environment_id):
    """ Users should be able to delete an environment membership

    :testid: cloud9-f-2-4

    :param Cloud9.Client cloud9:
        A handle to the Cloud9 API.
    :param IAM.User user_two
        A user that does not own the Cloud9 Environment
    :param str environment_id:
        The ID of a created Cloud9 Environment

    """

    res = cloud9.delete_environment_membership(
        userArn=user_two.arn,
        environmentId=environment_id
    )
    assert has_status(res, 200), 'Unable to delete environment memberships'

    res = cloud9.describe_environment_memberships(
        environmentId=environment_id
    )
    assert has_status(res, 200), 'Unable to describe environment memberships'

    for member in res['memberships']:
        assert member['userArn'] != user_two.arn, 'Failed to delete environment membership'


def test_delete_environment(cloud9, shared_vars):
    """ Users should be able to delete an environment

    :testid: cloud9-f-1-5

    :param Cloud9.Client cloud9:
        A handle to the Cloud9 API.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = cloud9.delete_environment(
        environmentId=shared_vars['environment_id']
    )

    assert has_status(res, 200), 'Unable to delete environment'
    shared_vars['environment_id'] = 'not-created'


@catch(Exception, msg='Failed to delete cloud9 environment: {ex}')
@aws_client('cloud9')
def cleanup_cloud9(*, cloud9):
    res = cloud9.list_environments()
    for environment_id in res['environmentIds']:
        cloud9.delete_environment(environmentId=environment_id)
