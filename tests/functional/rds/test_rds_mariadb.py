"""
==============
test_rds_maria
==============

Plan:

* Create an instance with "Public accessibility" set to "No" and verify that it
  is not accessible outside of the VPC.
* Use Amazon RDS encryption using AWS Key Management Service to enable
  encryption at rest.
* Use combined CA bundle to encrypt data as it moves to and from the database.
* Verify MariaDB is accessible through standard port.
* Verify that Standard Database User accounts do not have master account level
  privileges.

.. note::

    These tests are used for both MariaDB and MySQL.

.. note::

    These tests makes use of shared utilities in ``test/functional/rds/conftest.py``.

${testcount:4}

"""

import pytest

from .common import (
    test_publicly_accessible,
    test_encryption_at_rest,
    test_encryption_in_motion,
)

assert test_publicly_accessible
assert test_encryption_at_rest
assert test_encryption_in_motion

ENGINE = 'mariadb'
SCRIPT = r"""#!/bin/bash

source ~/.bashrc

set -e
set -x

cat > /usr/local/bin/my <<EOT
#!/bin/bash

username=\$1
shift

mysql \
    --host {hostname} \
    --user \$username \
    --password={password} \
    --connect-timeout=10 \
    --ssl-ca=/opt/rds-combined-ca-bundle.pem \
    --ssl-verify-server-cert \
    "\$@"
EOT
chmod +x /usr/local/bin/my
cat /usr/local/bin/my

export PATH=/usr/local/bin:$PATH

# connect to new RDS instance
my {username} --execute "select now()"

echo Successfully connected.

# create a test user
my {username} --execute "GRANT USAGE ON *.* TO 'test'@'%' IDENTIFIED BY '{password}'"
my {username} --execute "GRANT USAGE ON *.* TO 'test'@'%' REQUIRE SSL"
my test --execute "select now()"

# get a list of privileges for all users
my {username} --skip-column-names --execute "SELECT CONCAT('SHOW GRANTS FOR \'', user, '\'@\'', host, '\';') FROM mysql.user" |& tee /tmp/queries.sql

cat /tmp/queries.sql | xargs -0 -I@ my {username} --execute "@"

"""

MASTER_PRIVS = [
    'GRANT SELECT',
    'INSERT',
    'UPDATE',
    'DELETE',
    'CREATE',
    'DROP',
    'RELOAD',
    'PROCESS',
    'REFERENCES',
    'INDEX',
    'ALTER',
    'SHOW DATABASES',
    'CREATE TEMPORARY TABLES',
    'LOCK TABLES',
    'EXECUTE',
    'REPLICATION SLAVE',
    'REPLICATION CLIENT',
    'CREATE VIEW',
    'SHOW VIEW',
    'CREATE ROUTINE',
    'ALTER ROUTINE',
    'CREATE USER',
    'EVENT',
    'TRIGGER'
]


@pytest.fixture(scope='module')
def engine():
    return ENGINE


@pytest.fixture(scope='module')
def raw_script():
    return SCRIPT


@pytest.fixture(scope='module')
def query_timeout_needle():
    return "Can't connect"


def test_account_privileges(internal_output):
    """Standard database user accounts should not have master account level
    privileges.

    :testid: db-rds-3-maria, db-rds-4-maria, db-rds-3-mysql, db-rds-4-mysql

    """

    all_privs = ', '.join(MASTER_PRIVS)
    assert internal_output.count(all_privs) == 1, 'Unexpected output:\n{}'.format(internal_output)
