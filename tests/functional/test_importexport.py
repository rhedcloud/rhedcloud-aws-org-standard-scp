"""
===============
test_importexport
===============

Verify access to use AWS ImportExport

#FIXME: This test is an incredibly minimal smoke test to ensure
#FIXME: the client is producing acceptable results when blocked 

Plan:

* Attempt to list jobs

${testcount:2}

"""

import pytest

from aws_test_functions import (
    has_status,
    unexpected
)


#This service is fully blocked
pytestmark = [pytest.mark.scp_check('list_jobs')]


@pytest.fixture(scope='module')
def importexport(session):
    """A shared handle to ImportExport API.


    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the ImportExport API.

    """

    return session.client('importexport')


def test_list_jobs(importexport):
    """Verify ability list jobs

    :testid: 

    :param ImportExport.Client importexport:
        A handle to the ImportExport API.

    """


    res = importexport.list_jobs()
    assert has_status(res, 200), unexpected(res)
