"""
=====================
test_rds_db_snapshots
=====================

Verify access to share RDS DB instance snapshots.

Plan:

* Create an RDS DB instance
* Create a snapshot of the RDS DB instance
* Confirm access to share the RDS DB instance snapshot with another AWS account
  using the following roles:

    * RHEDcloudAuditorRole -- should fail with and without the SCP
    * RHEDcloudAdministratorRole -- should fail with the SCP
    * RHEDcloudCentralAdministratorRole -- should succeed
    * RHEDcloudMaintenanceOperatorRole -- should succeed
    * RHEDcloudSecurityIRRole -- should succeed
    * RHEDcloudSecurityRiskDetectionServiceRole -- should succeed

${testcount:7}

"""

from contextlib import ExitStack
import time

import pytest

from aws_test_functions import (
    ClientError,
    debug,
    has_status,
    make_identifier,
    unexpected,
)

pytestmark = [
    pytest.mark.test_with_and_without_scp,
    pytest.mark.rhedcloudsnapshotshareblock,
]


ENGINE = "mariadb"


@pytest.fixture(scope="module")
def engine():
    return ENGINE


@pytest.fixture(scope="module")
def db_id(database):
    # wait for the RDS instance to be available so we have access to
    # information, such as the instance endpoint
    database.wait()

    return database["DBInstanceIdentifier"]


@pytest.dict_fixture
def shared_vars():
    def mkid():
        return make_identifier("rhedcloudsnapshotshareblock")[:63].strip("-")

    return {
        "db_snapshot_id": mkid(),
    }


@pytest.fixture(scope="function", params=[
    ("RHEDcloudAuditorRole", False, False),
    ("RHEDcloudAdministratorRole", True, False),
    ("RHEDcloudCentralAdministratorRole", True, True),
    ("RHEDcloudMaintenanceOperatorRole", True, True),
    ("RHEDcloudSecurityIRRole", True, True),
    ("RHEDcloudSecurityRiskDetectionServiceRole", True, True),
])
def role_expect(request, is_scp_active):
    """Return pairings of roles and whether each role is expected to be allowed
    to share snapshots based on whether the Service Control Policy is in
    effect.

    :returns:
        A 2-tuple containing the role name and a boolean.

    """

    name, no_scp, with_scp = request.param
    expect = with_scp if is_scp_active else no_scp

    return (name, expect)


@pytest.fixture
def rate_limit(role_expect):
    """Sleep for a little bit after each permutation of the test in order to
    avoid API rate limit errors.

    """

    yield

    # add a delay only if we're not on the last role passed to ``role_expect``
    if "RiskDetection" not in role_expect[0]:
        debug("Sleeping to avoid API rate limits...")
        time.sleep(60)


def test_create_db_snapshot(rds, db_id, db_snapshot_id, stack):
    """Create a new RDS DB instance snapshot and wait for it to be available.

    :param RDS.Client rds:
        A handle to the RDS API.
    :param str db_id:
        Identifier for the RDS DB instance.
    :param str db_snapshot_id:
        Identifier for the RDS DB instance snapshot.
    :param ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    debug("Creating DB snapshot...")
    res = rds.create_db_snapshot(
        DBInstanceIdentifier=db_id,
        DBSnapshotIdentifier=db_snapshot_id,
        Tags=[{
            "Key": "CreatedAsRole",
            "Value": "MAINT",
        }]
    )
    assert has_status(res, 200), unexpected(res)
    assert res["DBSnapshot"]["DBSnapshotIdentifier"] == db_snapshot_id

    stack.callback(
        rds.delete_db_snapshot,
        DBSnapshotIdentifier=db_snapshot_id,
    )

    debug("Waiting for snapshot to become available...")
    waiter = rds.get_waiter("db_snapshot_available")
    waiter.wait(
        DBInstanceIdentifier=db_id,
        DBSnapshotIdentifier=db_snapshot_id,
        WaiterConfig={
            "Delay": 10,
            "MaxAttempts": 60,
        },
    )


def test_share_snapshot_as_role(
    db_snapshot_id,
    role_expect,
    master_account_number,
    assume_role,
    rate_limit,
):
    """Verify access to share an RDS DB instance snapshot with another AWS
    account.

    :param str db_snapshot_id:
        Identifier for the RDS DB instance snapshot.
    :param tuple role_expect:
        A 2-tuple containing a role name and whether that role is expected to
        be permitted to share the snapshot.
    :param str master_account_number:
        The numeric ID of another AWS account.
    :param callable assume_role:
        A function that helps create a boto3 session using a different IAM
        Role.

    """

    role, expect_success = role_expect

    debug("modifying DB snapshot as {} expecting success: {}...".format(role, expect_success))
    role_session = assume_role("role/rhedcloud/{}".format(role))
    role_rds = role_session.client("rds")

    with ExitStack() as maybe:
        if not expect_success:
            maybe.enter_context(pytest.raises(ClientError, match="AccessDenied"))

        res = role_rds.modify_db_snapshot_attribute(
            DBSnapshotIdentifier=db_snapshot_id,
            AttributeName="restore",
            ValuesToAdd=[master_account_number],
        )
        assert has_status(res, 200), unexpected(res)
