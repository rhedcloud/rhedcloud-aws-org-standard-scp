"""
==========
test_docdb
==========

Verify access to use AWS DocumentDB APIs.

Plan:

* Create a DocumentDB subnet group
* Create a DocumentDB cluster
* Describe DocumentDB clusters (waiting for the new cluster to be available)
* Verify that the DocumentDB cluster is configured for encryption at rest
* Create a DocumentDB instance in the cluster
* Describe DocumentDB instances (waiting for the new instance to be available)
* Verify that the DocumentDB cluster is configured for encryption in motion
* Describe DocumentDB events
* Verify access to modify cluster settings
* Verify access to modify instance settings
* Attempt to stop the cluster
* Attempt to start the cluster
* Delete the DocumentDB instance
* Delete the DocumentDB cluster
* Delete the DocumentDB subnet group

${testcount:15}

"""

import pytest

from aws_test_functions import (
    env,
    get_subnet_id,
    has_status,
    ignore_errors,
    make_identifier,
    new_security_group,
    retry,
    unexpected,
)


ENGINE = "docdb"
SCRIPT = """#!/bin/bash

mongo --tls --host {hostname}:27017 --tlsCAFile /opt/rds-combined-ca-bundle.pem --username {username} --password {password}

"""


@pytest.fixture(scope="module")
def docdb(session):
    return session.client("docdb")


@pytest.fixture(scope="module")
def engine():
    """This fixture is used by some of the common RDS testing machinery."""

    return ENGINE


@pytest.fixture(scope="module")
def raw_script():
    """This fixture is used by some of the common RDS testing machinery."""

    return SCRIPT


@pytest.fixture
def script(docdb, raw_script, cluster_name, username, password):
    """Replace information in the script to test the database.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str raw_script:
        A shell script to execute to connect to the DocDB cluster.
    :param str cluster_name:
        Name of the DocDB cluster.
    :param str username:
        DocDB cluster username.
    :param str password:
        DocDB cluster password.

    """

    res = test_describe_db_clusters(docdb, cluster_name)
    hostname = res["DBClusters"][0]["Endpoint"]

    return raw_script.format(
        hostname=hostname,
        username=username,
        password=password,
    ).encode()


@pytest.fixture(scope='module')
def query_reached_needle(engine):
    """Needle to search for that indicates the script got to the point of
    executing queries (installed all dependencies, etc).

    """

    return "mongo"


@pytest.fixture(scope="module")
def subnets(ec2, vpc_id):
    return [
        get_subnet_id(vpc_id, subnet, ec2=ec2)
        for subnet in env.RHEDCLOUD_DEFAULT_VPC_ENDPOINT_SUBNETS
    ]


@pytest.fixture(scope="module")
def security_group(ec2, vpc_id):
    """Create a new security group that allows TCP traffic into the DB cluster."""

    with new_security_group(vpc_id, "test_docdb", ec2=ec2) as sg:
        ec2.authorize_security_group_ingress(
            GroupId=sg.group_id,
            IpProtocol="tcp",
            FromPort=27017,
            ToPort=27017,
            CidrIp="0.0.0.0/0",
        )

        yield sg


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return dict(
        subnet_group_name=make_identifier("test-docdb-subnet"),
        cluster_name=make_identifier("test-docdb-cluster"),
        db_name=make_identifier("test-docdb-db"),
        username="docdbadmin",
        password=make_identifier("pw"),
    )


def test_create_db_subnet_group(docdb, cluster_name, subnet_group_name, subnets, stack):
    """Verify access to create DocDB subnet groups.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str cluster_name:
        Name of the DocDB cluster that will use the subnet group.
    :param str subnet_group_name:
        Name of the DocDB subnet group to create.
    :param list(str) subnets:
        A list of subnet IDs to associate with the DocDB subnet group.
    :param ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = docdb.create_db_subnet_group(
        DBSubnetGroupName=subnet_group_name,
        DBSubnetGroupDescription="Testing DocumentDB",
        SubnetIds=subnets,
    )
    assert has_status(res, 200), unexpected(res)

    print("Created DocDB subnet group: {}".format(subnet_group_name))

    stack.callback(
        ignore_errors(test_delete_db_subnet_group),
        docdb,
        cluster_name,
        subnet_group_name,
    )


def test_create_db_cluster(docdb, cluster_name, username, password, subnet_group_name, security_group, stack):
    """Verify access to create DocDB Clusters.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str cluster_name:
        Name of the DocDB cluster to create.
    :param str username:
        DocDB cluster username.
    :param str password:
        DocDB cluster password.
    :param str subnet_group_name:
        Name of the DocDB subnet group to associate with the cluster.
    :param EC2.SecurityGroup security_group:
        The security group to associate with the cluster.
    :param ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = docdb.create_db_cluster(
        DBClusterIdentifier=cluster_name,
        Engine=ENGINE,
        MasterUsername=username,
        MasterUserPassword=password,
        AvailabilityZones=["us-east-1b"],
        StorageEncrypted=True,
        VpcSecurityGroupIds=[security_group.group_id],
        DBSubnetGroupName=subnet_group_name,
    )
    assert has_status(res, 200), unexpected(res)

    print("Created DocDB cluster: {}".format(cluster_name))

    stack.callback(
        ignore_errors(test_delete_db_cluster),
        docdb,
        cluster_name,
    )


def test_describe_db_clusters(docdb, cluster_name, target_state="available", invalid_states=None):
    """Verify access to describe DocDB Clusters.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str cluster_name:
        Name of the DocDB cluster to describe.
    :param str target_state: (optional)
        Name of the state to wait for the cluster to reach.
    :param list(str) invalid_states: (optional)
        Name of one or more states that should be considered invalid for the
        cluster to have.

    """

    def _test(res):
        if not len(res["DBClusters"]):
            return False

        state = res["DBClusters"][0]["Status"]
        if invalid_states and state in invalid_states:
            raise ValueError("Invalid DocDB cluster state: {}".format(state))

        return state == target_state

    res = retry(
        docdb.describe_db_clusters,
        kwargs=dict(
            DBClusterIdentifier=cluster_name,
        ),
        msg="Waiting for DocDB cluster {} to be {}".format(cluster_name, target_state),
        until=_test,
        show=True,
        delay=30,
        max_attempts=30,
    )
    assert has_status(res, 200), unexpected(res)

    return res


def test_encryption_at_rest(docdb, cluster_name):
    """Verify that the DocDB cluster has encryption-at-rest enabled.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str cluster_name:
        Name of the DocDB cluster to create.

    """

    res = test_describe_db_clusters(docdb, cluster_name)
    assert res["DBClusters"][0]["StorageEncrypted"], "Database is not encrypted at rest"


def test_create_db_instance(docdb, cluster_name, db_name, stack):
    """Verify access to create DocDB instances.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str cluster_name:
        Name of the DocDB cluster to create an instance for.
    :param str db_name:
        Name of the DocDB instance to create.
    :param ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = docdb.create_db_instance(
        DBClusterIdentifier=cluster_name,
        DBInstanceIdentifier=db_name,
        DBInstanceClass="db.r5.large",
        Engine=ENGINE,
    )
    assert has_status(res, 200), unexpected(res)

    print("Created DocDB instance: {}".format(db_name))

    stack.callback(
        ignore_errors(test_delete_db_instance),
        docdb,
        db_name,
        wait=False,
    )


def test_describe_db_instances(docdb, db_name, target_state="available", invalid_states=None):
    """Verify access to describe DocDB Instances.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str db_name:
        Name of the DocDB instance to describe.
    :param str target_state: (optional)
        Name of the state to wait for the instance to reach.
    :param list(str) invalid_states: (optional)
        Name of one or more states that should be considered invalid for the
        instance to have.

    """

    def _test(res):
        if not len(res["DBInstances"]):
            return False

        state = res["DBInstances"][0]["DBInstanceStatus"]
        if invalid_states and state in invalid_states:
            raise ValueError("Invalid DocDB instance state: {}".format(state))

        return state == target_state

    res = retry(
        docdb.describe_db_instances,
        kwargs=dict(
            DBInstanceIdentifier=db_name,
        ),
        msg="Waiting for DocDB instance {} to be {}".format(db_name, target_state),
        until=_test,
        pred=lambda ex: not isinstance(ex, ValueError),
        show=True,
        delay=30,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.skip(reason="internal_runner misbehaving")
def test_encryption_in_motion(internal_runner, script):
    """Verify that the DocDB cluster has encryption-in-motion enabled.

    :param callable internal_runner:
        An interface to execute shell scripts inside an EC2 instance.
    :param str script:
        The contents of the shell script to execute.

    """

    output = internal_runner(script)

    assert "command not found" not in output, unexpected(output)
    assert "Connection timed out" not in output, unexpected(output)

    # this indicates that we were able to successfully connect to the database using TLS
    assert "MongoDB server version: 3.6.0" in output, unexpected(output)


def test_describe_events(docdb, db_name):
    """Verify access to describe DocDB events.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str db_name:
        Name of a DocDB instance.

    """

    res = docdb.describe_events(
        SourceIdentifier=db_name,
        SourceType="db-instance",
    )
    assert has_status(res, 200), unexpected(res)


def test_modify_db_cluster(docdb, cluster_name):
    """Verify access to modify DocDB clusters.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str cluster_name:
        Name of a DocDB cluster.

    """

    print("Modifying DB cluster: {}".format(cluster_name))
    res = docdb.modify_db_cluster(
        DBClusterIdentifier=cluster_name,
        ApplyImmediately=True,
        BackupRetentionPeriod=2,
    )
    assert has_status(res, 200), unexpected(res)


def test_modify_db_instance(docdb, db_name):
    """Verify access to modify DocDB instances.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str db_name:
        Name of a DocDB instance.

    """

    print("Modifying DB instance: {}".format(db_name))
    res = docdb.modify_db_instance(
        DBInstanceIdentifier=db_name,
        ApplyImmediately=True,
        PromotionTier=0,
    )
    assert has_status(res, 200), unexpected(res)


def test_stop_db_cluster(docdb, cluster_name):
    """Verify access to stop DocDB clusters.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str cluster_name:
        Name of a DocDB cluster.

    """

    # the cluster must be "available" before we can stop it
    test_describe_db_clusters(docdb, cluster_name)

    print("Stopping DB cluster: {}".format(cluster_name))
    res = docdb.stop_db_cluster(
        DBClusterIdentifier=cluster_name,
    )
    assert has_status(res, 200), unexpected(res)

    # make sure the cluster is stopped
    test_describe_db_clusters(docdb, cluster_name, target_state="stopped")


def test_start_db_cluster(docdb, cluster_name):
    """Verify access to start DocDB clusters.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str cluster_name:
        Name of a DocDB cluster.

    """

    # the cluster must be "stopped" before we can start it
    test_describe_db_clusters(
        docdb,
        cluster_name,
        target_state="stopped",
        invalid_states=["available"],  # the cluster should at least be "stopping"
    )

    print("Starting DB cluster: {}".format(cluster_name))
    res = docdb.start_db_cluster(
        DBClusterIdentifier=cluster_name,
    )
    assert has_status(res, 200), unexpected(res)

    # make sure the cluster is available
    test_describe_db_clusters(docdb, cluster_name)


def test_delete_db_instance(docdb, db_name, wait=True):
    """Verify access to delete DocDB instances.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str db_name:
        Name of a DocDB instance.
    :param bool wait: (optional)
        Whether to wait for the DocDB instance to be available before
        attempting to delete it.

    """

    if wait:
        # the cluster must be available before we can delete an instance
        test_describe_db_instances(
            docdb,
            db_name,
            invalid_states=["stopped", "stopping"],
        )

    res = retry(
        docdb.delete_db_instance,
        kwargs=dict(
            DBInstanceIdentifier=db_name,
        ),
        msg="Deleting DocDB instance: {}".format(db_name),
        pred=lambda ex: "InvalidDBInstanceState" in str(ex),
        delay=30,
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_db_cluster(docdb, cluster_name):
    """Verify access to delete DocDB clusters.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str cluster_name:
        Name of a DocDB cluster.

    """

    res = retry(
        docdb.delete_db_cluster,
        kwargs=dict(
            DBClusterIdentifier=cluster_name,
            SkipFinalSnapshot=True,
        ),
        msg="Deleting DocDB cluster: {}".format(cluster_name),
        pred=lambda ex: "InvalidDBClusterStateFault" in str(ex),
        delay=30,
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_db_subnet_group(docdb, subnet_group_name):
    """Verify access to delete DocDB subnet groups.

    :param DocDB.Client docdb:
        Handle to the DocDB API.
    :param str subnet_group_name:
        Name of a DocDB subnet group.

    """

    res = retry(
        docdb.delete_db_subnet_group,
        kwargs=dict(
            DBSubnetGroupName=subnet_group_name,
        ),
        msg="Deleting DocDB subnet group: {}".format(subnet_group_name),
        pred=lambda ex: "InvalidDB" in str(ex),
        delay=30,
    )
    assert has_status(res, 200), unexpected(res)
