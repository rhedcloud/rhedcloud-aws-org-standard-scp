"""
===========
test_config
===========

Determine whether the user can use the AWS Config service.

Plan:

* Describe delivery channels
* Create a new config delivery channel
* Create a config rule
* Delete a config rule
* Delete a config delivery channel

${testcount:6}

"""

from contextlib import contextmanager

import pytest

from aws_test_functions import (
    # attach_policy,
    get_account_number,
    new_bucket,
    new_policy,
    new_role,
    # new_test_user,
    retry,
)

from tests.functional.test_lambda import (
    create_function as create_lambda,
    delete_function as delete_lambda,
)


# @pytest.fixture(scope='session')
# def user(in_setup_org):
#     """Create a custom test user with permission to access Lambda"""
#
#     stmt = [{
#         "Effect": "Allow",
#         "Action": [
#             "config:Put*"
#         ],
#         "Resource": "*",
#     }, {
#         "Effect": "Allow",
#         "Action": [
#             "lambda:*"
#         ],
#         "Resource": "*",
#     }]
#
#     print('Creating config-specific test user')
#     with new_test_user() as u:
#         with new_policy('test_config_put', stmt) as p:
#             with attach_policy(u, 'RHEDcloudAdministratorRolePolicy', p.arn):
#                 yield u


@pytest.fixture(scope='module')
def config(session):
    """
    A shared handle to Config API

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Config API

    """

    return session.client('config')


@pytest.fixture(scope='function', autouse=True)
def scp(config, wait_for_scp):
    """Fixture that waits for the SCP to take effect before allowing a test to
    proceed.

    :param Config.Client config:
        A handle to the Config API.
    :param callable wait_for_scp:
        A dependency on the fixture helps us know when the SCP should be in
        effect.

    """

    wait_for_scp(config.describe_config_rules, delay=30)


@pytest.fixture(scope='module')
def policy(session, bucket):
    """Create a new policy with all sorts of access to things that seem to be
    required for the Config rules. This probably needs some work if/when the
    Config service is unblocked.

    """

    arn = "arn:aws:s3:::{}".format(bucket.name)
    stmt = [{
        "Effect": "Allow",
        "Action": [
            "s3:PutObject*"
        ],
        "Resource": [
            "{}/AWSLogs/{}/*".format(arn, get_account_number()),
        ],
        "Condition": {
            "StringLike": {
                "s3:x-amz-acl": "bucket-owner-full-control"
            }
        }
    }, {
        "Effect": "Allow",
        "Action": [
            "s3:GetBucketAcl"
        ],
        "Resource": arn,
    }, {
        "Effect": "Allow",
        "Action": [
            "config:Put*"
        ],
        "Resource": "*",
    }, {
        "Effect": "Allow",
        "Action": [
            "lambda:*"
        ],
        "Resource": "*",
    }]

    with new_policy('test_config', stmt) as p:
        yield p


@pytest.fixture(scope='module')
def role(session, policy):
    iam = session.resource('iam')
    with new_role('test_config', 'config', 'arn:aws:iam::aws:policy/service-role/AWSConfigRole', policy.arn, iam=iam) as r:
        yield r


@pytest.fixture(scope='module')
def bucket(session):
    s3 = session.resource('s3')
    with new_bucket('test-config', s3=s3) as b:
        yield b


@contextmanager
def new_config_recorder(config, role):
    """A configuration recorder must be created before a delivery channel can
    be created. Currently, you can specify only one configuration recorder per
    region in your account.

    .. note::

        This is not a pytest fixture because we need it to be executed within a
        test function in order to catch the "Access Denied" error.

    :yields:
        The name of the configuration recorder.

    """

    existing = config.describe_configuration_recorders()
    assert 'ConfigurationRecorders' in existing, 'Unexpected response: {}'.format(existing)
    if len(existing['ConfigurationRecorders']):
        yield existing['ConfigurationRecorders'][0]['name']
    else:
        name = 'default'
        res = None

        try:
            res = config.put_configuration_recorder(
                ConfigurationRecorder={
                    'name': name,
                    'roleARN': role.arn,
                },
            )

            print('Created configuration recorder: {}'.format(name))
            yield name
        finally:
            if res is not None:
                print('Deleting configuration recorder: {}'.format(name))
                config.delete_configuration_recorder(
                    ConfigurationRecorderName=name,
                )


def create_delivery_channel(name, config, bucket):
    """Create a new Config Delivery Channel.

    The call to create/update a delivery channel can fail if the role specified
    on the S3 bucket is still being setup. To work around that, the call is
    retried a few times.

    :param str name:
        Delivery channel name.
    :param config:
        Config client.
    :param bucket:
        S3 Bucket resource.

    :returns:
        True

    """

    retry(
        config.put_delivery_channel,
        kwargs=dict(
            DeliveryChannel=dict(
                name=name,
                s3BucketName=bucket.name,
            ),
        ),
        msg='Attempting to create delivery channel',
        pred=lambda ex: 'InsufficientDeliveryPolicyException' in str(ex),
    )
    print('Created delivery channel: {}'.format(name))

    return True


def test_describe_delivery_channels(config):
    """Attempt to describe Config delivery channels.

    :testid: config-f-1-1

    """

    res = config.describe_delivery_channels()
    assert 'DeliveryChannels' in res, 'Unexpected response: {}'.format(res)


def test_create_delete_delivery_channels(config, bucket, role):
    """Create a Config delivery channel.

    :testid: config-f-1-2, config-f-1-5

    """

    with new_config_recorder(config, role):
        name = 'default'
        created = False

        try:
            # testid: config-f-1-2
            created = create_delivery_channel(name, config, bucket)
        finally:
            if created:
                # testid: config-f-1-5
                print('Deleting delivery channel: {}'.format(name))
                config.delete_delivery_channel(DeliveryChannelName=name)


@pytest.fixture(scope='module')
def lambda_func(session, role):
    """Reuse Lambda test function"""

    res = None

    try:
        res = create_lambda(role, session)
        yield res
    finally:
        if res is not None:
            print('Deleting lambda function...')
            delete_lambda(session)


def test_create_delete_config_rule(config, bucket, role):
    """Create and delete a config rule.

    :testid: config-f-1-3, config-f-1-4

    .. todo::

        Make this work when the Config service is not blocked. Currently I keep
        getting errors like this:

            botocore.errorfactory.InsufficientPermissionsException: An error occurred (InsufficientPermissionsException) when calling the PutConfigRule operation: The AWS Lambda function arn:aws:lambda:us-east-1:462544485824:function:HelloLambda cannot be invoked. Check the specified function ARN, and check the function's permissions.

        For the time being, I have removed the `lambda_func` fixture from this
        test.

    """

    with new_config_recorder(config, role):
        # TODO make this work.
        # # testid: config-f-1-3
        # config.put_config_rule(
        #     ConfigRule=dict(
        #         ConfigRuleName='test_config',
        #         Source=dict(
        #             Owner='CUSTOM_LAMBDA',
        #             SourceIdentifier=lambda_func['FunctionArn'],
        #             SourceDetails=[{
        #                 'EventSource': 'aws.config',
        #                 'MessageType': 'ConfigurationItemChangeNotification',
        #             }],
        #         ),
        #     ))

        # # testid: config-f-1-4
        # res = config.put_config_rule(ConfigRuleName='foo')

        pass
