"""
===============
test_rds_oracle
===============

Plan:

* Create an instance with "Public accessibility" set to "No" and verify that it
  is not accessible outside of the VPC.
* Create an instance with "Enable Encryption" set to "Yes" and verify the
  Storage Encryption parameter.
* Use Oracle NNE to encrypt data as it moves to and from the database.
* Verify Oracle is accessible through standard port.
* Verify that Standard Database User accounts do not have master account level
  privileges.

.. note::

    These tests makes use of shared utilities in ``test/functional/rds/conftest.py``.

${testcount:4}

"""

import pytest

from .common import (
    test_publicly_accessible,
    test_encryption_at_rest,
)

assert test_publicly_accessible
assert test_encryption_at_rest

ENGINE = 'oracle-ee'
SCRIPT = r"""#!/bin/bash

source ~/.bashrc

set -e
set -x

function sql() {{
    sqlplus {username}/{password}@{hostname}/ORCL "$@"
}}

# produce a SQL script to query Oracle
cat > validate.sql <<EOT
SET ECHO ON
SET COLSEP ,
SET PAGESIZE 0
SET TRIMSPOOL ON
SET HEADSEP OFF

SELECT CURRENT_DATE FROM DUAL
/

SELECT NETWORK_SERVICE_BANNER
FROM V\$SESSION_CONNECT_INFO
WHERE SID = SYS_CONTEXT('USERENV','SID')
/

SELECT * FROM DBA_SYS_PRIVS WHERE PRIVILEGE = 'GRANT ANY OBJECT PRIVILEGE'
/

EXIT
EOT

# execute the SQL script
sql @validate.sql

"""


@pytest.fixture(scope='module')
def engine():
    return ENGINE


@pytest.fixture(scope='module')
def raw_script():
    return SCRIPT


@pytest.fixture(scope='module')
def query_reached_needle():
    return "@validate.sql"


@pytest.fixture(scope='module')
def query_timeout_needle():
    return "TNS:Connect timeout occurred"


def test_encryption_in_motion(internal_output, query_timeout_needle):
    """Connect to RDS instance on the standard port with encryption in motion.

    :testid: db-rds-2b-oracle, db-rds-3-oracle

    """

    assert query_timeout_needle not in internal_output, 'Unexpected output:\n{}'.format(internal_output)

    needles = (
        # these two should always be present
        'Encryption service',
        'Crypto-checksumming service',

        # these two are only present when the connection is encrypted
        'AES256 Encryption service',
        'SHA1 Crypto-checksumming service',
    )

    for needle in needles:
        assert needle in internal_output, 'No encryption in motion:\n{}'.format(internal_output)


def test_account_privileges(internal_output):
    """Standard database user accounts should not have master account level
    privileges.

    :testid: db-rds-3-oracle, db-rds-4-oracle

    """

    lines = internal_output.splitlines()
    has_priv = 0
    total_users = 0
    for idx, line in enumerate(lines):
        print("Checking for GRANT ANY OBJECT PRIVILEGE in: {}".format(line))
        if 'GRANT ANY OBJECT PRIVILEGE,' in line:
            total_users += 1
            print("Checking for privileges in: {}".format(line))
            priv, adm, *rest = line.split(',')
            if adm.strip() == 'YES':
                has_priv += 1

    # we expect only 3 users to have the privilege: DBA, RDSADMIN, and the user
    # created for this test
    assert has_priv == 3, 'Unexpected output:\n{}'.format(internal_output)
    assert total_users > 3, 'Regular user not detected:\n{}'.format(internal_output)
