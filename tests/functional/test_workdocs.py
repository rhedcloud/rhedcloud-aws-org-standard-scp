"""
=============
test_workdocs
=============

Verify access to use Amazon WorkDocs.

Plan:

* Attempt to describe users

The API for WorkDocs is rather difficult to work with, so most the tests we
expected to do are not implemented at this time.

* WorkDocs first requires a Simple AD
* Enabling WorkDocs requires manual intervention in the AWS console
* Attempts to create WorkDocs users via the API haven't worked yet
* Most API functions require an authentication token, for which, apparently,
  IAM session tokens are not valid

${testcount:2}

"""

import pytest

from aws_test_functions import aws_client

# This service is fully blocked
pytestmark = [
    pytest.mark.scp_check(
        "describe_users",
        kwargs=dict(OrganizationId="None"),
        needle="UnauthorizedResourceAccessException",
    )
]


@pytest.fixture(scope="module")
def workdocs(session):
    """A shared handle to WorkDocs API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the WorkDocs API.

    """

    return session.client("workdocs")


@aws_client("workdocs")
def get_org_id(session, *, workdocs=None):
    """Retrieve the ID of a Simple AD that has been setup for use with
    WorkDocs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A string.

    """

    ds = session.client("ds")

    # this is the call that normally raises the AccessDenied error caught by
    # the scp fixture (at least while the directoryservice is blocked)
    res = ds.describe_directories()

    for data in res["DirectoryDescriptions"]:
        try:
            dir_id = data["DirectoryId"]
            workdocs.describe_users(OrganizationId=dir_id)

            return dir_id
        except Exception as ex:
            if "AccessDenied" in str(ex) or "UnauthorizedResource" in str(ex):
                raise


def test_describe_users(workdocs, session):
    """Verify access to describe users.

    :testid: workdocs-f-1-1

    :param WorkDocs.Client workdocs:
        A handle to the WorkDocs API.

    """

    # get_org_id relies on workdocs.describe_users. Yes this is redundant, but
    # it's the only API call that seems to work.
    get_org_id(session, workdocs=workdocs)
