"""
===========
test_athena
===========

A user should be able create an encrypted S3 bucket, upload data to the S3
bucket, and then analyze the data with Athena.

Plan:

athena-f-1-1
Upload data to be used by Athena and create an encrypted S3
folder for the query results (see AWS Athena.docx instructions)

* Create S3 bucket with encryption turned on not publicly accessible.
* Upload the Sample Test File (see link above this table)
* Create an encrypted folder in the S3 bucket for the query results

athena-f-1-2
Configure Athena to query test data (see AWS Athena.docx instructions)

* Selected the query results folder created in test above
* Create a new database to store the tables into
* Add a table to the database to query against

athena-f-1-3
Query test data (see AWS Athena.docx instructions)

* Run a query against table

${testcount:1}

"""

import pytest

from aws_test_functions import (
    new_bucket,
    retry
)

pytestmark = [pytest.mark.scp_check('list_named_queries')]


if not pytest.is_blocked('athena') and pytest.is_blocked('glue'):
    pytestmark.append(pytest.mark.skip(reason='Athena requires Glue'))


@pytest.fixture(scope='module')
def s3(session):
    """
    Return an S3 client

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A low-level client representing Amazon S3

    """

    return session.client('s3')


@pytest.fixture(scope='module')
def s3res(session):
    """
    Return an S3 resource

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        An S3 resource handle

    """

    return session.resource('s3')


@pytest.fixture(scope='module')
def bucket(s3res):
    """
    Return an S3 bucket resource

    :param boto3.s3.resource

    :returns:
        An S3 bucket resource

    """

    created = False
    with new_bucket('athena', s3=s3res) as b:
        created = True
        yield b

    assert created, 'failed to create bucket'


@pytest.fixture(scope='module')
def athena(session):
    """
    Return an Athena client

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A low-level client representing Amazon Athena:

    """

    return session.client('athena')


@pytest.dict_fixture
def shared_vars():
    """
    Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'database_created': False,
        'database_name': 'athena_database',
        'datasource': './tests/resources/Departments.txt',
        'destination': 'athena_database/Departments.txt',
        'table_name': 'Departments',
    }


@pytest.mark.bypass("raises_access_denied")
def test_encrypt_and_upload(s3, bucket, datasource, destination):
    """
    Upload data to be used by Athena and create an encrypted S3 folder for the
    query results (see AWS Athena.docx instructions)

    :testid: athena-f-1-1

    :param boto3.client s3:
        A handle to the AWS S3 API.
    :param S3.Bucket bucket:
        A handle to a S3 bucket resource.
    :param str datasource:
        Path to a file to upload to S3.
    :param str destination:
        Path of the key to create in S3.

    """

    bucket.upload_file(
        datasource,
        destination,
        ExtraArgs={
            'ServerSideEncryption': 'AES256',
        },
    )

    resource = s3.head_object(
        Bucket=bucket.name,
        Key=destination,
    )

    sse = resource.get('ServerSideEncryption')
    assert sse == 'AES256', 'file is unexpectedly not encrypted'


def test_create_data(athena, bucket, shared_vars):
    """
    Create database/tables to query with Athena

    :testid: athena-f-1-2

    :param boto3.client athena:
        A handle to the AWS Athena API.
    :param S3.Bucket bucket:
        A handle to a S3 bucket resource.
    :param dict shared_vars:
        Dictionary shared between test functions

    """

    # init variables from shared_vars
    database_name = shared_vars['database_name']
    table_name = shared_vars['table_name']
    table_location = 's3://%s/%s' % (bucket.name, database_name)

    # query to create database
    create_query = 'CREATE DATABASE IF NOT EXISTS %s;' % (database_name)

    # query to create table
    table_query = """
        CREATE EXTERNAL TABLE IF NOT EXISTS %s.%s (
            ID STRING,
            Department STRING
        )
        ROW FORMAT SERDE 'org.openx.data.jsonserde.JsonSerDe'
        WITH SERDEPROPERTIES ('serialization.format' = '1')
        LOCATION '%s';
    """ % (database_name, table_name, table_location)

    # execute each query
    for query in [create_query, table_query]:
        query_response = run_query(athena, query, database_name, bucket.name)
        status = wait_for_status(athena, query_response)
        assert status == 'SUCCEEDED', 'Query {} failed: \n{}'.format(query, query_response)

    shared_vars['database_created'] = True


def test_query_data(athena, bucket, shared_vars):
    """
    Perform a query using Athena

    :testid: athena-f-1-3

    :param boto3.client athena:
        A handle to the AWS Athena API.
    :param S3.Bucket bucket:
        A handle to a S3 bucket resource.
    :param dict shared_vars:
        Dictionary shared between test functions.

    """

    # init variables from shared_vars
    database_name = shared_vars['database_name']
    table_name = shared_vars['table_name']

    # query to retrieve data from table
    query = 'SELECT * FROM %s.%s' % (database_name, table_name)

    # execute query to retrieve data
    query_response = run_query(athena, query, database_name, bucket.name)
    status = wait_for_status(athena, query_response)
    assert status == 'SUCCEEDED', 'Query {} failed: \n{}'.format(query, query_response)

    res = athena.get_query_results(
        QueryExecutionId=query_response['QueryExecutionId'],
    )
    assert res


def get_query_status(athena, query_id):
    """
    Retrieve the status of a query give it's query id

    :param boto3.client athena:
        A handle the AWS Athena API.
    :param str query_id:
        Query identifier.

    """

    status_response = athena.get_query_execution(
        QueryExecutionId=query_id
    )

    return status_response['QueryExecution']['Status']['State']


def run_query(athena, query, database, output):
    """
    Use Athena to run a query against a database

    :param boto3.client athena:
        A handle the AWS Athena API.
    :param str query:
        SQL to execute.
    :param str database:
        Database in which to execute query.
    :param str output:
        S3 location of query results.

    """

    return athena.start_query_execution(
        QueryString=query,
        ResultConfiguration={
            'OutputLocation': 's3://%s' % (output),
            'EncryptionConfiguration': {'EncryptionOption': 'SSE_S3'}
        }
    )


@pytest.fixture(scope='module', autouse=True)
def cleanup_module(athena, bucket, shared_vars):
    """
    Delete any available s3 buckets and their contents and delete any named
    queries created

    :param boto3.client athena:
        A handle to the AWS Athena API.
    :param S3.Bucket bucket:
        A handle to a S3 bucket resource.
    :param dict shared_vars:
        Dictionary shared between test functions.

    """

    yield

    # Athena cleanup
    if shared_vars['database_created']:
        drop_database(athena, shared_vars['database_name'], bucket.name)


def drop_database(athena, database, bucket):
    """
    Drop database used for Athena queries

    :param boto3.client athena:
        A handle to the AWS Athena API.
    :param str database:
        Database to drop.
    :param str bucket:
        S3 bucket identifier to output to.

    """

    query = 'DROP DATABASE %s CASCADE' % (database)
    query_response = run_query(athena, query, database, bucket)
    status = wait_for_status(athena, query_response)
    assert status == 'SUCCEEDED', 'Query {} failed: \n{}'.format(query, query_response)


def wait_for_status(athena, query_response):
    status = retry(
        get_query_status,
        args=(athena, query_response['QueryExecutionId'],),
        msg='Checking query status',
        until=lambda r: r not in ['QUEUED', 'RUNNING'],
    )

    return status
