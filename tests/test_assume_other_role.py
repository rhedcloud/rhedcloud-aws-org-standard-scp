"""
======================
test_assume_other_role
======================

This is a demonstration for how to use the "assume_role" fixture to switch to
another role in the target account.

"""

import pytest

from aws_test_functions import get_account_info


@pytest.fixture(scope="module")
def ir_session(assume_role):
    """Assume the RHEDcloudSecurityIRRole in the target account.

    :param callable assume_role:
        A function that abstracts away the details of assuming a role in the
        target account.

    :returns:
        A boto3 session for the RHEDcloudSecurityIRRole role in the target
        account.

    """

    return assume_role("role/rhedcloud/RHEDcloudSecurityIRRole")


@pytest.fixture(scope="module")
def sts(ir_session):
    """Create a handle to the STS API as the RHEDcloudSecurityIRRole in the
    target account.

    :param boto3.session.Session ir_session:
        A session using the RHEDcloudSecurityIRRole.

    :returns:
        A handle to the STS API.

    """

    return ir_session.client("sts")


def test_as_ir(sts):
    """Confirm that the STS handle is using the RHEDcloudSecurityIRRole"""

    info = get_account_info(sts=sts)
    assert "RHEDcloudSecurityIRRole/TEST-" in info["Arn"]
