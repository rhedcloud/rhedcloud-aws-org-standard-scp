"""
========
test_acm
========

Verify that customers can use AWS Certificate Manager.

Plan:

* Setup a subdomain of the test domain
* Request a certificate for the new subdomain using DNS validation
* Create necessary DNS record for validation
* Wait for certificate to be ISSUED
* Retrive certificate and validate its domain

${testcount:4}

"""

from subprocess import check_output
import os
import re

import boto3
import pytest

from aws_test_functions import aws_client, get_uuid, orgs, retry
from aws_test_functions.route53 import new_record_set


# This it the domain that is used for testing certificates. It lives in the
# rhedcloud-serviceforge-100 account since everyone should already have access to
# that account. This allows us to not have to register a domain in each AWS
# account that is used for testing.
TLD = 'tjc3cstmfhkwnrxadvwc.com'

pytestmark = [
    pytest.mark.scp_check('list_certificates')
]

# skip these tests by default, but allow them to be enabled by setting SKIP_ACM=0
if os.getenv("SKIP_ACM", "1") == "1":
    pytestmark.append(
        pytest.mark.skip(reason='Maximum of 20 certs per year'),
    )


@pytest.fixture(scope='module')
def acm(session):
    """A shared handle to ACM APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the ACM APIs.

    """

    return session.client('acm')


@pytest.fixture(scope='module')
def subdomain():
    """Generate a random subdomain.

    :returns:
        A string.

    """

    return get_uuid()[:6]


@pytest.fixture(scope='module')
def full_domain(subdomain):
    """Combine the random subdomain with the shared top-level domain.

    :param str subdomain:
        A subdomain name.

    :returns:
        A string.

    """

    return '{}.{}'.format(subdomain, TLD)


@pytest.fixture(scope='module', autouse=True)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        # the ARN of the certificate that is requested for testing
        'arn': None,

        # DNS validation information
        'validation': None,

        # whether or not the DNS validation succeeded
        'validated': False,
    }


@pytest.fixture(scope='module')
def route53():
    """A shared handle to Route53 APIs using the :envvar:`RHEDCLOUD_SETUP_PROFILE`
    AWS account. The testing domain was registered in that account so as to be
    accessible by multiple testers.

    .. note::

        This fixture does not require access to the test user's session, as it
        uses an entirely different profile.

    :returns:
        A handle to the Route53 APIs.

    """

    client = None

    try:
        # switch to the rhedcloud-serviceforge-100 account
        orgs.switch_profile(orgs.SETUP_PROFILE)

        print('Getting Route53 client for test domain...')
        client = boto3.client('route53')
    finally:
        # switch back to the correct account for the rest of the testing
        orgs.switch_profile(orgs.TEST_PROFILE)

    # provide access to the route53 client that was created using the different
    # AWS profile AFTER switching back to the normal testing account
    return client


@pytest.fixture(scope='module')
def hosted_zone(route53):
    """Return information about an existing Hosted Zone in Route53.

    :param Route53.Client route53:
        A handle to the Route53 APIs.

    :returns:
        A dictionary.

    """

    res = route53.list_hosted_zones_by_name(
        DNSName='{}.'.format(TLD),
    )
    assert 'HostedZones' in res and len(res['HostedZones']), 'Unexpected response: {}'.format(res)

    return res['HostedZones'][0]


@pytest.fixture(scope='module')
def dns_validation(route53, shared_vars, hosted_zone):
    """Create a new record set for DNS validation required when issuing a new
    certificate. Once the tests finish, this record set is automatically
    removed.

    :param Route53.Client route53:
        A handle to the Route53 APIs.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param dict hosted_zone:
        Information about the Hosted Zone which will be used when creating the
        new record set.

    """

    zone_id = hosted_zone['Id']
    validation = shared_vars['validation']['ResourceRecord']

    params = (
        # ID of the Hosted Zone for the test domain
        zone_id,

        # name of the DNS record to create
        validation['Name'],

        # type of DNS record to create (CNAME)
        validation['Type'],

        # value to set for the new DNS record
        validation['Value'],
    )

    with new_record_set(*params, route53=route53):
        yield


@aws_client('acm')
def describe_certificate(arn, *, acm=None):
    """Return information about a Certificate issued by ACM.

    :param str arn:
        The ARN of the certificate to retrieve.

    :returns:
        A dictionary.

    """

    res = acm.describe_certificate(CertificateArn=arn)
    assert 'Certificate' in res, 'Unexpected response: {}'.format(res)

    return res['Certificate']


@pytest.fixture(scope='module', autouse=True)
def cleanup(acm, shared_vars):
    """Automatically remove the certificate if it was successfully created.

    :param ACM.Client acm:
        A handle to the ACM APIs.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    yield

    # this will be executed after all test functions in this module complete
    if shared_vars['arn']:
        print('Deleting certificate: {}'.format(shared_vars['arn']))
        acm.delete_certificate(
            CertificateArn=shared_vars['arn'],
        )


def test_request_certificate(acm, shared_vars, subdomain, full_domain):
    """Customers should be able to request certificates using ACM.

    :testid: acm-f-1-1

    :param ACM.Client acm:
        A handle to the ACM APIs.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param str subdomain:
        The subdomain to request a certificate for.
    :param str full_domain:
        The full domain to request a certificate for.

    """

    res = acm.request_certificate(
        DomainName=full_domain,
        ValidationMethod='DNS',
        IdempotencyToken=subdomain,
    )
    assert 'CertificateArn' in res, 'Unexpected response: {}'.format(res)

    # track the new certificate's ARN for use in other tests
    shared_vars['arn'] = arn = res['CertificateArn']
    print('Requested certificate: {}'.format(arn))


def test_get_dns_validation(acm, shared_vars):
    """Customers should be able to describe requested certificates to retrieve
    validation details.

    The validation information is not always immediately available, so this
    test must attempt to retrieve the data multiple times.

    :testid: acm-f-1-2

    :param ACM.Client acm:
        A handle to the ACM APIs.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    # there's no point moving forward if we haven't requested a cert
    assert shared_vars['arn'], 'Certificate not requested'

    cert = retry(
        describe_certificate,
        args=[shared_vars['arn']],
        kwargs=dict(acm=acm),
        msg='Checking DNS validation...',
        until=lambda s: 'DomainValidationOptions' in s and len(s['DomainValidationOptions']) and 'ResourceRecord' in s['DomainValidationOptions'][0],
        delay=10,
        max_attempts=30
    )

    shared_vars['validation'] = cert['DomainValidationOptions'][0]
    assert shared_vars['validation'], 'Domain validation information not found: {}'.format(cert)


def test_domain_validated(acm, shared_vars, dns_validation):
    """Customers should be able to check whether the DNS validation for a
    certificate has completed successfully.

    It can take several minutes for DNS validation to complete, so the status
    of the certificate request must be checked many times.

    :testid: acm-f-1-3

    :param ACM.Client acm:
        A handle to the ACM APIs.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param dns_validation:
        A dependency on the fixture that sets up the DNS record required for
        DNS validation.

    """

    # there's no point moving forward if we don't have DNS validation
    # information yet
    assert shared_vars['validation'], 'DNS validation not retrieved'

    cert = retry(
        describe_certificate,
        args=[shared_vars['arn']],
        kwargs=dict(acm=acm),
        msg='Checking domain validation...',
        until=lambda s: s['Status'] == 'ISSUED',
        delay=20,
        max_attempts=30
    )

    assert cert['Status'] == 'ISSUED', 'Failed to verify domain: {}'.format(cert)
    shared_vars['validated'] = True


def test_cert_domain(acm, shared_vars, full_domain):
    """Customers should be able to retrieve the requested certificate, and its
    domain should match what was originally requested.

    :testid: acm-f-1-4

    :param ACM.Client acm:
        A handle to the ACM APIs.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param str full_domain:
        The full domain name expected to appear in the certificate's Common
        Name field.

    """

    # there's no point moving forward if we haven't completed the DNS
    # validation yet
    assert shared_vars['validated'], 'DNS validation not performed'

    res = acm.get_certificate(CertificateArn=shared_vars['arn'])
    assert 'Certificate' in res, 'Certificate not found: {}'.format(res)
    assert 'CertificateChain' in res, 'Certificate chain not found: {}'.format(res)

    # call out to the openssl binary for simplicity
    data = check_output(
        args=['openssl', 'x509', '-text', '-noout'],
        input=res['Certificate'].encode(),
    ).decode()

    match = re.search(r'Subject: .+\b{}'.format(full_domain), data)
    assert match, 'Bad certificate: {}'.format(data)
