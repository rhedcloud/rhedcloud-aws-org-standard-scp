"""
=============
test_workmail
=============

Verify access to use Amazon WorkMail.

Plan:

* Attempt to list organizations, users, and groups
* Attempt to describe a group and a user
* Attempt to create a group and a user

${testcount:14}

"""

import pytest

from aws_test_functions import (
    make_identifier,
)
# from aws_test_functions.directoryservice import (
#     new_simple_directory,
# )

# Use a custom regular expression when handling "access denied" errors. The
# test descriptions state that messages such as "organization id is invalid"
# should be considered successful, but the validation happens before the
# permissions are checked (just like many/most AWS APIs).
ACCESS_DENIED_NEEDLE = r'(AccessDenied|Validation|OrganizationNotFound)'

# This service is fully blocked
pytestmark = [pytest.mark.scp_check('list_organizations')]
# pytestmark = pytest.mark.skip(reason='Appears to require manual intervention in order to setup a Simple AD for WorkMail.')


@pytest.fixture(scope='module')
def workmail(session):
    """A shared handle to WorkMail API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the WorkMail API.

    """

    return session.client('workmail')


# @pytest.fixture(scope='module')
# @catch(Exception, msg='Error creating directory: {ex}')
# def directory_id(session, stack, vpc_id):
#     """Create a new Simple AD to setup with WorkMail.
#
#     :param boto3.session.Session session:
#         A session belonging to an authenticated IAM User.
#     :param contextlib.ExitStack stack:
#         A context manager shared across all WorkMail tests.
#     :param str vpc_id:
#         ID of the VPC where the directory should be created.
#
#     """
#
#     ds = session.client('ds')
#
#     did = stack.enter_context(
#         new_simple_directory('test.workmail.com', vpc_id, ds=ds)
#     )
#
#     alias = make_identifier('workmail')
#     res = ds.create_alias(
#         DirectoryId=did,
#         Alias=alias,
#     )
#     assert 'Alias' in res, 'Unexpected response: {}'.format(res)
#
#     yield did
#
#
# @pytest.fixture(scope='module')
# def setup_fixtures(directory_id):
#     yield


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'org_id': 'm-0123456789abcdef0123456789abcdef',
        'user_id': 'nope-not-created',
        'group_id': 'nope-not-created',
    }


def test_list_organizations(workmail, shared_vars):
    """Verify access to list organizations.

    :testid: workmail-f-1-2

    :param WorkMail.Client workmail:
        A handle to the WorkMail API.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = workmail.list_organizations()
    assert 'OrganizationSummaries' in res, 'Unexpected response: {}'.format(res)

    orgs = res['OrganizationSummaries']
    assert len(orgs), 'No organizations found: {}'.format(res)

    active_orgs = [o for o in orgs if o['State'] == 'Active']
    assert len(active_orgs), 'No active organizations found: {}'.format(res)

    org = active_orgs[0]
    shared_vars['org_id'] = org['OrganizationId']


def test_create_user(workmail, org_id, stack, shared_vars):
    """Verify access to create users.

    :testid: workmail-f-3-2

    :param WorkMail.Client workmail:
        A handle to the WorkMail API.
    :param str org_id:
        ID of a WorkMail Organization.
    :param contextlib.ExitStack stack:
        A context manager shared across all WorkMail tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = workmail.create_user(
        OrganizationId=org_id,
        Name='testuser',
        DisplayName='Test User',
        Password='CredentialRedactedAndRotated%',
    )
    assert 'UserId' in res, 'Unexpected response: {}'.format(res)
    user_id = shared_vars['user_id'] = res['UserId']
    print('Created user: {}'.format(user_id))

    stack.callback(
        workmail.delete_user,
        OrganizationId=org_id,
        UserId=user_id,
    )


def test_list_users(workmail, org_id, user_id):
    """Verify access to list users.

    :testid: workmail-f-1-1

    :param WorkMail.Client workmail:
        A handle to the WorkMail API.
    :param str org_id:
        ID of a WorkMail Organization.
    :param str user_id:
        ID of a WorkMail User.

    """

    res = workmail.list_users(
        OrganizationId=org_id,
    )
    assert 'Users' in res, 'Unexpected response: {}'.format(res)

    users = res['Users']
    assert len(users), 'No users found: {}'.format(res)

    found = False
    for user in users:
        if user['Id'] == user_id:
            found = True
            break

    assert found, 'User {} not found: {}'.format(user_id, res)


def test_create_group(workmail, org_id, stack, shared_vars):
    """Verify access to create groups.

    :testid: workmail-f-3-1

    :param WorkMail.Client workmail:
        A handle to the WorkMail API.
    :param str org_id:
        ID of a WorkMail Organization.
    :param contextlib.ExitStack stack:
        A context manager shared across all WorkMail tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-group')
    res = workmail.create_group(
        OrganizationId=org_id,
        Name=name,
    )
    assert 'GroupId' in res, 'Unexpected response: {}'.format(res)
    group_id = shared_vars['group_id'] = res['GroupId']
    print('Created group: {}'.format(group_id))

    stack.callback(
        workmail.delete_group,
        OrganizationId=org_id,
        GroupId=group_id,
    )


def test_list_groups(workmail, org_id, group_id):
    """Verify access to list groups.

    :testid: workmail-f-1-3

    :param WorkMail.Client workmail:
        A handle to the WorkMail API.
    :param str org_id:
        ID of a WorkMail Organization.
    :param str group_id:
        ID of a WorkMail Group.

    """

    res = workmail.list_groups(
        OrganizationId=org_id,
    )
    assert 'Groups' in res, 'Unexpected response: {}'.format(res)

    groups = res['Groups']
    assert len(groups), 'No groups found: {}'.format(res)

    found = False
    for grp in groups:
        if grp['Id'] == group_id:
            found = True
            break

    assert found, 'Group {} not found: {}'.format(group_id, res)


def test_describe_group(workmail, org_id, group_id):
    """Verify access to describe groups.

    :testid: workmail-f-2-1

    :param WorkMail.Client workmail:
        A handle to the WorkMail API.
    :param str org_id:
        ID of a WorkMail Organization.
    :param str group_id:
        ID of a WorkMail Group.

    """

    res = workmail.describe_group(
        OrganizationId=org_id,
        GroupId=group_id,
    )
    assert 'GroupId' in res, 'Unexpected response: {}'.format(res)


def test_describe_user(workmail, org_id, user_id):
    """Verify access to describe users.

    :testid: workmail-f-2-2

    :param WorkMail.Client workmail:
        A handle to the WorkMail API.
    :param str org_id:
        ID of a WorkMail Organization.
    :param str user_id:
        ID of a WorkMail User.

    """

    res = workmail.describe_user(
        OrganizationId=org_id,
        UserId=user_id,
    )
    assert 'UserId' in res, 'Unexpected response: {}'.format(res)
