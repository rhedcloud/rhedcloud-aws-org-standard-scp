"""
=================
test_rds_postgres
=================

Plan:

* Create an instance with "Public accessibility" set to "No" and verify that it
  is not accessible outside of the VPC.
* Create an instance with "Enable Encryption" set to "Yes" and verify the
  Storage Encryption parameter.
* Use combined CA bundle to encrypt data as it moves to and from the database.
* Verify PostgreSQL is accessible through standard port.
* Verify that Standard Database User accounts do not have master account level
  privileges.

.. note::

    These tests makes use of shared utilities in ``test/functional/rds/conftest.py``.

${testcount:4}

"""

import pytest

from .common import (
    test_publicly_accessible,
    test_encryption_at_rest,
    test_encryption_in_motion,
)

assert test_publicly_accessible
assert test_encryption_at_rest
assert test_encryption_in_motion

ENGINE = 'postgres'
SCRIPT = r"""#!/bin/bash

source ~/.bashrc

set -e
set -x

export PGHOST={hostname}
export PGDATABASE=test_rds
export PGUSER={username}
export PGPASSWORD={password}
export PGCONNECT_TIMEOUT=10
export PGSSLROOTCERT=/opt/rds-combined-ca-bundle.pem
export PGSSLMODE=verify-full

# connect to new RDS instance
psql --command "select now()"

echo Successfully connected.

# create a test user
createuser -e test
psql --command "ALTER USER test WITH PASSWORD 'password'"
PGUSER=test PGPASSWORD=password psql --command "select now()"

# get a list of privileges for all users
psql --tuples-only --command "SELECT row_to_json(t) FROM (SELECT r.rolname, r.rolsuper, r.rolinherit, r.rolcreaterole, r.rolcreatedb, r.rolcanlogin, r.rolconnlimit, r.rolvaliduntil, ARRAY(SELECT b.rolname FROM pg_catalog.pg_auth_members m JOIN pg_catalog.pg_roles b ON (m.roleid = b.oid) WHERE m.member = r.oid) AS memberof, r.rolreplication, r.rolbypassrls FROM pg_catalog.pg_roles r ORDER BY 1) t"

"""


@pytest.fixture(scope='module')
def engine():
    return ENGINE


@pytest.fixture(scope='module')
def raw_script():
    return SCRIPT


@pytest.fixture(scope='module')
def query_timeout_needle():
    return "timeout expired"


def test_account_privileges(internal_output):
    """Standard database user accounts should not have master account level
    privileges.

    :testid: db-rds-3-postgresql, db-rds-4-postgresql

    """

    needle = '"memberof":["rds_superuser"]'
    assert internal_output.count(needle) == 1, 'Unexpected output:\n{}'.format(internal_output)
