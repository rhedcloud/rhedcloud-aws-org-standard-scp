"""
=========================
test_elasticsearchservice
=========================

Verify access to utilize AWS ElasticsearchService functionality.

${testcount:10}

* Create a new test user with the RHEDcloudAdministratorRole.
* Attempt to create and delete a domain

"""

import json

from botocore.exceptions import ClientError
import pytest

from aws_test_functions import (
    has_status,
    ignore_errors,
    make_identifier,
    retry,
    unexpected,
)

pytestmark = [
    # This service is fully blocked in the HIPAA SCP
    pytest.mark.scp_check("list_domain_names", client_fixture="elasticsearch")
]


@pytest.fixture(scope="module")
def elasticsearch(session):
    """A shared handle to ElasticsearchService API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the ElasticsearchService API.

    """

    return session.client("es")


@pytest.fixture(scope="module", autouse=True)
def role(session):
    """Get or create a role for Elasticsearch Service to do magic.

    This Role does not get removed automatically because it's required for
    Elasticsearch Service to work when tests are not running.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        An IAM Role resource.

    """

    iam = session.resource("iam")
    name = "AWSServiceRoleForAmazonElasticsearchService"

    try:
        r = iam.Role(name)
        r.load()

        print("Found existing role: {}".format(r.arn))
    except ClientError:
        res = iam.meta.client.create_service_linked_role(
            AWSServiceName="es.amazonaws.com"
        )
        assert has_status(res, 200), unexpected(res)

        r = iam.Role(name)

    return r


@pytest.dict_fixture
def shared_vars():
    name = make_identifier("testessdomain")[:27]

    return {
        "domain_name": name,
        "domain_version": "6.2",
        "instance_type": "m4.large.elasticsearch",
        "access_policy": json.dumps(
            {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Effect": "Allow",
                        "Principal": {"AWS": ["*"]},
                        "Action": ["es:*"],
                        "Resource": "*",
                    }
                ],
            }
        ),
    }


@pytest.fixture
def cluster_config(
    role, domain_name, domain_version, instance_type, internal_subnet_id, access_policy
):
    """Common configuration values for an Elasticsearch domain.

    :param IAM.Role role:
        A dependency on the service-linked role that is required to create an
        Elasticsearch domain in the type 1 VPC.
    :param str domain_name:
        Name of an Elasticsearch domain.
    :param str domain_version:
        Version of Elasticsearch domain.
    :param str instance_type:
        EC2 instance type to use.
    :param str internal_subnet_id:
        ID of an internal subnet.
    :param str access_policy:
        A JSON document describing access to an Elasticsearch domain.

    :returns:
        A dictionary.

    """

    return dict(
        DomainName=domain_name,
        ElasticsearchClusterConfig=dict(InstanceType=instance_type),
        VPCOptions=dict(SubnetIds=[internal_subnet_id]),
        AccessPolicies=access_policy,
        EBSOptions=dict(EBSEnabled=True, VolumeType='standard', VolumeSize=10),
    )


def test_list_elasticsearch_versions(elasticsearch, domain_version):
    """Verify access to list available Elasticsearch versions.

    :testid: ess-f-3-1

    :param ElasticsearchService.Client elasticsearch:
        A handle to the ElasticsearchService API.
    :param str domain_version:
        Version of Elasticsearch domain.

    """

    res = elasticsearch.list_elasticsearch_versions()
    assert has_status(res, 200), unexpected(res)
    assert domain_version in res["ElasticsearchVersions"], unexpected(res)


def test_list_elasticsearch_instance_types(elasticsearch, domain_version, instance_type):
    """Verify access to list available Elasticsearch instance types.

    :testid: ess-f-3-2

    :param ElasticsearchService.Client elasticsearch:
        A handle to the ElasticsearchService API.
    :param str domain_version:
        Version of Elasticsearch domain.
    :param str instance_type:
        EC2 instance type to use.

    """

    res = elasticsearch.list_elasticsearch_instance_types(
        ElasticsearchVersion=domain_version,
    )
    assert has_status(res, 200), unexpected(res)
    assert "ElasticsearchInstanceTypes" in res, unexpected(res)
    assert instance_type in res["ElasticsearchInstanceTypes"]


def test_create_domain(
    elasticsearch, domain_name, domain_version, cluster_config, stack
):
    """Verify access to create Elasticsearch domains.

    :testid: ess-f-1-1

    :param ElasticsearchService.Client elasticsearch:
        A handle to the ElasticsearchService API.
    :param str domain_name:
        Name of an Elasticsearch domain.
    :param str domain_version:
        Version of Elasticsearch domain.
    :param dict cluster_config:
        Configuration values for the new Elasticsearch domain.
    :param contextlib.ExitStack stack:
        A context manager shared across all Elasticsearch Service tests.

    """

    kwargs = dict(ElasticsearchVersion=domain_version, **cluster_config)

    res = retry(
        elasticsearch.create_elasticsearch_domain,
        kwargs=kwargs,
        msg="Attempting to create Elasticsearch cluster",
        show=True,
        pred=lambda exc: 'AccessDenied' not in str(exc),
    )
    assert has_status(res, 200) and "DomainStatus" in res, unexpected(res)
    print("Created Elasticsearch domain: {}".format(domain_name))

    stack.callback(ignore_errors(test_delete_domain), elasticsearch, domain_name)


def test_list_domain_names(elasticsearch, domain_name):
    """Verify access to list existing Elasticsearch domains.

    :testid: ess-f-1-2

    :param ElasticsearchService.Client elasticsearch:
        A handle to the ElasticsearchService API.
    :param str domain_name:
        Name of an Elasticsearch domain.

    """

    res = elasticsearch.list_domain_names()
    assert has_status(res, 200) and "DomainNames" in res, unexpected(res)

    found = False
    for info in res["DomainNames"]:
        if info["DomainName"] == domain_name:
            found = True
            break

    assert found, "did not find domain {}".format(domain_name)


def test_describe_domains(elasticsearch, domain_name):
    """Verify access to describe existing Elasticsearch domains.

    :testid: ess-f-1-3

    :param ElasticsearchService.Client elasticsearch:
        A handle to the ElasticsearchService API.
    :param str domain_name:
        Name of an Elasticsearch domain.

    """

    res = elasticsearch.describe_elasticsearch_domains(
        DomainNames=[domain_name],
    )
    assert has_status(res, 200) and "DomainStatusList" in res, unexpected(res)


def test_describe_domain(elasticsearch, domain_name):
    """Verify access to describe an existing Elasticsearch domain.

    :testid: ess-f-1-4

    :param ElasticsearchService.Client elasticsearch:
        A handle to the ElasticsearchService API.
    :param str domain_name:
        Name of an Elasticsearch domain.

    """

    res = elasticsearch.describe_elasticsearch_domain(
        DomainName=domain_name,
    )
    assert has_status(res, 200) and "DomainStatus" in res, unexpected(res)


def test_describe_domain_config(elasticsearch, domain_name):
    """Verify access to describe an Elasticsearch domain's configuration.

    :testid: ess-f-2-1

    :param ElasticsearchService.Client elasticsearch:
        A handle to the ElasticsearchService API.
    :param str domain_name:
        Name of an Elasticsearch domain.

    """

    res = elasticsearch.describe_elasticsearch_domain_config(
        DomainName=domain_name,
    )
    assert has_status(res, 200) and "DomainConfig" in res, unexpected(res)


def test_update_domain_config(elasticsearch, cluster_config):
    """Verify access to update an Elasticsearch domain's configuration.

    :testid: ess-f-2-2

    :param ElasticsearchService.Client elasticsearch:
        A handle to the ElasticsearchService API.
    :param dict cluster_config:
        Configuration values for the new Elasticsearch domain.

    """

    res = elasticsearch.update_elasticsearch_domain_config(**cluster_config)
    assert has_status(res, 200) and "DomainConfig" in res, unexpected(res)


# It can take close to a half hour for the Elasticsearch domain to be ready and
# responding to curl requests.
@pytest.mark.slowtest
def test_query_domain(elasticsearch, domain_name, internal_runner):
    """Verify access to query Elasticsearch domains.

    :testid: ess-f-4-1

    :param ElasticsearchService.Client elasticsearch:
        A handle to the ElasticsearchService API.
    :param str domain_name:
        Name of an Elasticsearch domain.
    :param callable internal_runner:
        A function that makes it easy to run commands within an EC2 instance.

    """

    def _test(res):
        # print("Domain status:", res)
        return \
            has_status(res, 200) \
            and "DomainStatus" in res \
            and "Endpoints" in res["DomainStatus"] \
            and "vpc" in res["DomainStatus"]["Endpoints"] \
            and res["DomainStatus"]["Endpoints"]["vpc"]

    res = retry(
        elasticsearch.describe_elasticsearch_domain,
        kwargs=dict(DomainName=domain_name),
        msg="Looking for endpoint URL...",
        until=_test,
        delay=90,
        max_attempts=30,
    )
    assert _test(res), "No endpoint found: {}".format(res)

    def _test2(out):
        # print("Cluster status:", out)
        return '"green"' in out

    url = res["DomainStatus"]["Endpoints"]["vpc"]
    out = retry(
        internal_runner,
        args=["curl {}/_cluster/health".format(url)],
        msg="Checking cluster health",
        until=_test2,
        delay=90,
        max_attempts=15,
    )
    assert _test2(out), out


def test_delete_domain(elasticsearch, domain_name):
    """Verify access to delete Elasticsearch domains.

    :testid: ess-f-1-5

    :param ElasticsearchService.Client elasticsearch:
        A handle to the ElasticsearchService API.
    :param str domain_name:
        Name of an Elasticsearch domain.

    """

    print("Deleting Elasticsearch domain: {}".format(domain_name))
    res = elasticsearch.delete_elasticsearch_domain(
        DomainName=domain_name,
    )
    assert has_status(res, 200), unexpected(res)
