"""
==================
test_rds_sqlserver
==================

Plan:

* Create an instance with "Public accessibility" set to "No" and verify that it
  is not accessible outside of the VPC.
* Create an instance with "Enable Encryption" set to "Yes" and verify the
  Storage Encryption parameter.
* Use combined CA bundle to encrypt data as it moves to and from the database.
* Verify SQL Server is accessible through standard port.
* Verify that Standard Database User accounts do not have master account level
  privileges.

.. note::

    These tests makes use of shared utilities in ``test/functional/rds/conftest.py``.

${testcount:4}

"""

import pytest

from .common import (
    test_publicly_accessible,
    test_encryption_at_rest,
)

assert test_publicly_accessible
assert test_encryption_at_rest

ENGINE = 'sqlserver-web'
SCRIPT = r"""#!/bin/bash

source ~/.bashrc

set -e
set -x

# see https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-encrypted-connections
cp /opt/rds-combined-ca-bundle.pem /etc/pki/ca-trust/source/anchors/rds-combined-ca-bundle.crt
update-ca-trust

# see https://docs.microsoft.com/en-us/sql/relational-databases/scripting/sqlcmd-use-with-scripting-variables
export SQLCMDSERVER={hostname}
export SQLCMDUSER={username}
export SQLCMDPASSWORD={password}
export SQLCMDLOGINTIMEOUT=8

# connect to new RDS instance
sqlcmd -N -Q "SELECT getutcdate()"

echo Successfully connected.

# check encryption in motion
sqlcmd -N -Q "SELECT ENCRYPT_OPTION FROM SYS.DM_EXEC_CONNECTIONS WHERE SESSION_ID = @@SPID"

# get a list of privileges for all users
sqlcmd -N -Q "SELECT l.name as grantee_name, p.state_desc, p.permission_name FROM sys.server_permissions AS p JOIN sys.server_principals AS l ON   p.grantee_principal_id = l.principal_id WHERE permission_name = 'VIEW ANY DATABASE'"

"""


@pytest.fixture(scope='module')
def engine():
    """SQL Server Express Edition does not support encryption"""

    return ENGINE


@pytest.fixture(scope='module')
def raw_script():
    return SCRIPT


@pytest.fixture(scope='module')
def query_reached_needle(engine):
    return "SELECT getutcdate()"


@pytest.fixture(scope='module')
def query_timeout_needle():
    return "not found or not accessible"


def test_encryption_in_motion(internal_output):
    """Connect to RDS instance on the standard port with encryption in motion.

    :testid: db-rds-2b-sqlserver, db-rds-3-sqlserver

    """

    assert 'ENCRYPT_OPTION' in internal_output, 'Failed to execute query:\n{}'.format(internal_output)
    lines = internal_output.splitlines()
    for idx, line in enumerate(lines):
        if line.startswith('ENCRYPT_OPTION'):
            assert lines[idx + 2].strip() == 'TRUE', 'Encryption in motion not detected:\n{}'.format(internal_output)


def test_account_privileges(internal_output):
    """Standard database user accounts should not have master account level
    privileges.

    :testid: db-rds-3-sqlserver, db-rds-4-sqlserver

    """

    perm_needle = 'VIEW ANY DATABASE'
    grant_needle = 'GRANT_WITH_GRANT_OPTION'
    users = 0
    with_grant = 0

    for line in internal_output.splitlines():
        if line.strip().endswith(perm_needle):
            users += 1
            if grant_needle in line:
                with_grant += 1

    # we expect to find `public` and the master user created with the RDS instance
    assert users == 2, 'Unexpected number of users can {}:\n{}'.format(perm_needle, internal_output)

    # only the master user should be able to view all databases and grant that
    # privilege to other users
    assert with_grant == 1, 'Unexpected number of users have {}:\n{}'.format(grant_needle, internal_output)
