"""
========
test_sqs
========

Verify the ability to to use Amazon SQS(Simple Queuing Service)

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Create a queue, using both the standard configuration and FIFO configuration
* Send a message to the queue
* Consume the sent message from the queue
* Delete the message
* Delete the queue

${testcount:14}

"""

import pytest

from aws_test_functions import (
    debug,
    get_account_number,
    has_status,
    ignore_errors,
    unexpected,
)

pytestmark = pytest.mark.parametrize("mode", ("fifo", "standard"))


@pytest.fixture
def sqs(session):
    return session.client("sqs")


@pytest.fixture
def queue_name(mode):
    if mode == "fifo":
        return "Test_FIFO_Queue.fifo"
    else:
        return "Test_Standard_Queue"


@pytest.fixture
def queue_url(mode, shared_vars):
    return shared_vars["{}_queue_url".format(mode)]


@pytest.fixture
def message_body(mode):
    return "test {} message".format(mode)


@pytest.fixture
def receipt_handle(mode, shared_vars):
    return shared_vars["{}_receipt_handle".format(mode)]


@pytest.dict_fixture
def shared_vars():
    return {
        "fifo_queue_url": None,
        "standard_queue_url": None,
        "fifo_receipt_handle": None,
        "standard_receipt_handle": None,
    }


def test_create_queue(sqs, queue_name, mode, stack, shared_vars):
    """Verify ability to use the SQS service for both Standard LIFO and FIFO Queues.

    :testid: sqs-f-1-1, sqs-f-1-2

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param str queue_name:
        Name of the queue.
    :param str mode:
        Queue type.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    is_fifo = mode == "fifo"
    params = dict(
        QueueName=queue_name,
    )

    if is_fifo:
        params["Attributes"] = {
            "FifoQueue": "true",
            "ContentBasedDeduplication": "true",
        }

    res = sqs.create_queue(**params)
    assert has_status(res, 200), unexpected(res)

    key = "{}_queue_url".format(mode)
    shared_vars[key] = queue_url = res["QueueUrl"]

    stack.callback(
        ignore_errors(test_delete_queue),
        sqs,
        queue_url,
        mode,
    )


def test_add_permission(sqs, queue_url, mode):
    """Verify access to add permissions to SQS queues.

    :testid: sqs-f-1-1, sqs-f-1-2

    :param SQS.Client sqs:
        A handle to the SQS API.
    :param str queue_url:
        URL of the SQS queue to modify.
    :param str mode:
        Queue type.

    """

    debug("Attempting to add permissions to queue: {} / {}".format(queue_url, mode))
    res = sqs.add_permission(
        QueueUrl=queue_url,
        Label=mode,
        AWSAccountIds=[get_account_number()],
        Actions=["GetQueueAttributes"],
    )
    assert has_status(res, 200), unexpected(res)


def test_remove_permission(sqs, queue_url, mode):
    """Verify access to remove permissions from SQS queues.

    :testid: sqs-f-1-1, sqs-f-1-2

    :param SQS.Client sqs:
        A handle to the SQS API.
    :param str queue_url:
        URL of the SQS queue to modify.
    :param str mode:
        Queue type.

    """

    debug("Attempting to remove permissions from queue: {} / {}".format(queue_url, mode))
    res = sqs.remove_permission(
        QueueUrl=queue_url,
        Label=mode,
    )
    assert has_status(res, 200), unexpected(res)


def test_send_message(sqs, queue_url, message_body, mode):
    """Verify access to send SQS messages.

    :testid: sqs-f-1-1, sqs-f-1-2

    :param SQS.Client sqs:
        A handle to the SQS API.
    :param str queue_url:
        URL of the SQS queue to modify.
    :param str message_body:
        Body of the message to send.
    :param str mode:
        Queue type.

    """

    params = {}
    if mode == "fifo":
        params["MessageGroupId"] = "TestFIFOMessageGroup"

    res = sqs.send_message(
        QueueUrl=queue_url,
        MessageBody=message_body,
        DelaySeconds=0,
        **params,
    )
    assert has_status(res, 200), unexpected(res)


def test_receive_messages(sqs, queue_url, message_body, mode, shared_vars):
    """Verify access to receive SQS messages.

    :testid: sqs-f-1-1, sqs-f-1-2

    :param SQS.Client sqs:
        A handle to the SQS API.
    :param str queue_url:
        URL of the SQS queue to modify.
    :param str message_body:
        Body of the message to send.
    :param str mode:
        Queue type.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = sqs.receive_message(
        QueueUrl=queue_url,
        WaitTimeSeconds=20,
    )
    assert has_status(res, 200) and "Messages" in res, unexpected(res)

    messages = res["Messages"]
    assert len(messages) > 0, "Failed to receive sent message in 30 seconds"

    msg = messages[0]
    assert msg["Body"] == message_body, "Unexpected message body: {}".format(msg["Body"])

    shared_vars["{}_receipt_handle".format(mode)] = msg["ReceiptHandle"]


def test_delete_message(sqs, queue_url, receipt_handle, mode):
    """Verify access to delete SQS messages.

    :testid: sqs-f-1-1, sqs-f-1-2

    :param SQS.Client sqs:
        A handle to the SQS API.
    :param str queue_url:
        URL of the SQS queue to modify.
    :param str receipt_handle:
        ID of the message to delete.
    :param str mode:
        Queue type.

    """

    debug("Deleting SQS message: {} / {}".format(queue_url, receipt_handle))
    res = sqs.delete_message(
        QueueUrl=queue_url,
        ReceiptHandle=receipt_handle,
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_queue(sqs, queue_url, mode):
    """Verify access to delete SQS queues.

    :testid: sqs-f-1-1, sqs-f-1-2

    :param SQS.Client sqs:
        A handle to the SQS API.
    :param str queue_url:
        URL of the SQS queue to modify.
    :param str mode:
        Queue type.

    """

    debug("Deleting SQS queue: {}".format(queue_url))
    res = sqs.delete_queue(
        QueueUrl=queue_url,
    )
    assert has_status(res, 200), unexpected(res)
