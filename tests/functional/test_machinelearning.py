"""
===========
test_machinelearning
===========
A user should be able use AWS Machine Learning to build and train predictive
models.

Plan:

* Create a Training Data Source from S3
* Check the status of a Machine Learning model
* Generate a batch prediction

${testcount:3}

"""

import json

from botocore.client import BaseClient
from botocore.exceptions import ClientError, WaiterError
import boto3
import botocore
import pytest

from aws_test_functions import make_identifier, new_bucket, retry

pytestmark = [
    # This test takes ~15 minutes
    pytest.mark.slowtest,

    #This service is blocked in the HIPAA repository
    pytest.mark.scp_check('describe_evaluations', client_fixture='ml')
]

@pytest.fixture(scope='module')
def ml(session: boto3.session.Session) -> BaseClient:
    """Machine Learning client fixture.

    Decorators:
        pytest.fixture

    Arguments:
        session {boto3.session.Session} -- Session.

    Returns:
        boto3.client.BaseClient -- Machine Learning client.
    """
    return session.client('machinelearning')


@pytest.fixture(scope='module')
def bucket(session: boto3.session.Session):
    """S3 bucket.

    Decorators:
        pytest.fixture

    Arguments:
        session: boto3.session.Session {session} -- Session.

    Returns:
        boto3.client.BaseClient -- S3 client.
    """

    with new_bucket('aml-sample-data') as b:
        arn = "arn:aws:s3:::{}".format(b.name)
        policy = json.dumps({
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "machinelearning.amazonaws.com"
                    },
                    "Action": "s3:ListBucket",
                    "Resource": arn,
                },
                {
                    "Effect": "Allow",
                    "Principal": {
                        "Service": "machinelearning.amazonaws.com"
                    },
                    "Action": ['s3:GetObject',
                               's3:PutObject',
                               's3:PutObjectAcl'],
                    "Resource": "{}/*".format(arn),
                },
            ]
        })
        b.meta.client.put_bucket_policy(
            Bucket=b.name,
            Policy=policy,
        )

        # reload bucket with policy change
        b.load()

        b.upload_file('./tests/resources/banking.csv', 'banking.csv', ExtraArgs={'ServerSideEncryption': 'AES256'})
        b.upload_file('./tests/resources/banking-batch.csv',
                      'banking-batch.csv',
                      ExtraArgs={'ServerSideEncryption': 'AES256'})

        yield b


DATA_SOURCE_ID = make_identifier('bankingdata')
DATA_SOURCE_NAME = 'BankingData'


def waiter_helper(waiter, *args, **kwargs):
    """
    Calls .wait() and catches WaiterError. On exception it prints
    the last response and then raises the same exception. On WaiterError
    exceptions, this should give better insight to the error.

    Arguments:
        waiter {boto3.resources.model.Waiter} -- Waiter instance.

        args {list} -- Arguments.

    Keyword Arguments:
        kwargs {dict} -- keyword arguments.

    Returns:
        None
    """
    try:
        waiter.wait(*args, **kwargs)
    except WaiterError as e:
        print(e.last_response)
        raise e


def test_create_data_source(ml: BaseClient, bucket):
    """A user should be able to create a Training Data Source from S3.

    Arguments:
        ml {boto3.client.BaseClient} -- Machine Learning client.

    :testid: machinelearning-f-1-1
    """

    def d(name: str, type: str):
        return dict(fieldName=name, fieldType=type)

    def dn(name: str):
        return d(name, 'NUMERIC')

    def dc(name: str):
        return d(name, 'CATEGORICAL')

    def dt(name: str):
        return d(name, 'TEXT')

    def dwis(name: str):
        return d(name, 'WEIGHTED_INT_SEQUENCE')

    def dwss(name: str):
        return d(name, 'WEIGHTED_STRING_SEQUENCE')

    def db(name: str):
        return d(name, 'BINARY')

    ml.create_data_source_from_s3(
        ComputeStatistics=True,
        DataSourceId=DATA_SOURCE_ID,
        DataSourceName=DATA_SOURCE_NAME,
        DataSpec=dict(
            DataLocationS3='s3://{}/banking.csv'.format(bucket.name),
            DataSchema=json.dumps(dict(
                version='1.0',
                targetAttributeName='y',
                dataFormat='CSV',
                dataFileContainsHeader=True,
                attributes=[
                    dn('age'),
                    dc('job'),
                    dc('marital'),
                    dc('education'),
                    db('default'),
                    dc('housing'),
                    db('loan'),
                    dc('contact'),
                    dc('month'),
                    dc('day_of_week'),
                    dn('duration'),
                    dc('campaign'),
                    dn('pdays'),
                    dn('previous'),
                    dn('poutcome'),
                    dn('emp_var_rate'),
                    dn('cons_price_idx'),
                    dn('cons_conf_idx'),
                    dn('euribor3m'),
                    dn('nr_employed'),
                    db('y'),
                ]
            )),
        )
    )
    waiter = ml.get_waiter('data_source_available')
    waiter_helper(waiter,
                  FilterVariable='Name',
                  EQ=DATA_SOURCE_NAME)

    '''
    A user should be able to create and check the status of a Machine Learning
    model.

    :testid: machinelearning-f-1-2
    '''
    model_name = 'ML model: Banking Data 1'
    model_id = ml.create_ml_model(MLModelId=make_identifier('mbd1'),
                                  MLModelName=model_name,
                                  MLModelType='BINARY',
                                  TrainingDataSourceId=DATA_SOURCE_ID,
                                  )['MLModelId']
    waiter = ml.get_waiter('ml_model_available')
    waiter_helper(waiter,
                  FilterVariable='Name',
                  EQ=model_name)

    def safe_get_first_index(o, default=None):
        return next(iter(o), default)

    status = safe_get_first_index(
        ml.describe_ml_models(FilterVariable='Name', EQ=model_name).get(
            'Results', []), {'Status': 'failed'}).get('Status', 'failed')
    assert status == 'COMPLETED', 'Expected completed status'

    eval_name = 'Evalution: Banking Data 1'
    eval_id = ml.create_evaluation(EvaluationId=make_identifier('EvalId'),
                                   EvaluationName=eval_name,
                                   MLModelId=model_id,
                                   EvaluationDataSourceId=DATA_SOURCE_ID,
                                   )['EvaluationId']
    waiter = ml.get_waiter('evaluation_available')
    waiter_helper(waiter,
                  FilterVariable='Name',
                  EQ=eval_name)
    resp = ml.get_evaluation(EvaluationId=eval_id)

    properties = resp.get(
        'PerformanceMetrics', dict(Properties=dict())).get('Properties', {})
    assert 'BinaryAUC' in properties, 'Expected binary AUC key'

    '''
    A user should be able to generate a batch prediction.

    :testid: machinelearning-f-1-3
    '''
    try:
        batch_pred_id = make_identifier('Bankingdata1bp')
        batch_pred_name = 'Batch Prediction: Banking Data 1'
        ml.create_batch_prediction(
            BatchPredictionId=batch_pred_id,
            BatchPredictionName=batch_pred_name,
            MLModelId=model_id,
            BatchPredictionDataSourceId=DATA_SOURCE_ID,
            OutputUri='s3://{}/'.format(bucket.name)
        )

        waiter = ml.get_waiter('batch_prediction_available')
        waiter_helper(waiter,
                      FilterVariable='Name',
                      EQ=batch_pred_name)

        status = safe_get_first_index(
            ml.describe_batch_predictions(
                FilterVariable='Name', EQ=batch_pred_name).get('Results', []),
            {'Status': 'failed'}).get('Status', 'failed')
        assert status == 'COMPLETED', \
            'Expected COMPLETED status'
    except ml.exceptions.InvalidInputException:
        pytest.xfail("Cannot create batch predictions which output to a Bucket with SSE, SCP requires all S3 buckets to have SSE")
