"""
===============
test_greengrass
===============

Verify access to use AWS Greengrass.

Plan:

* Create a core definition
* Create a device definition
* Create a function definition
* Create a logger definition
* Create a resource definition
* Create a subscription definition
* Create a core group
* Create a group certificate authority
* Get information about the group certificate authority

${testcount:20}

"""

import pytest

from aws_test_functions import (
    make_identifier,
    unexpected,
)

# This service is fully blocked
pytestmark = [pytest.mark.scp_check('list_core_definitions')]


@pytest.fixture(scope='module')
def greengrass(session):
    """A shared handle to Greengrass API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Greengrass API.

    """

    return session.client('greengrass')


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'core_arn': 'not-created',
        'device_arn': 'not-created',
        'function_arn': 'not-created',
        'logger_arn': 'not-created',
        'resource_arn': 'not-created',
        'subscription_arn': 'not-created',
        'group_id': 'not-created',
        'group_ca_id': 'not-created',
    }


def test_create_core_definition(greengrass, stack, shared_vars):
    """Verify access to create core definitions.

    :testid: greengrass-f-1-1

    :param Greengrass.Client greengrass:
        A handle to the Greengrass API.
    :param contextlib.ExitStack stack:
        A context manager shared across all Greengrass tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-core-def')
    res = greengrass.create_core_definition(
        Name=name,
        InitialVersion=dict(
            Cores=[],
        ),
    )
    assert 'Arn' in res, unexpected(res)
    print('Created core definition: {}'.format(res['Arn']))
    shared_vars['core_arn'] = res['LatestVersionArn']

    stack.callback(
        greengrass.delete_core_definition,
        CoreDefinitionId=res['Id'],
    )


def test_create_device_definition(greengrass, stack, shared_vars):
    """Verify access to create device definitions.

    :testid: greengrass-f-1-1

    :param Greengrass.Client greengrass:
        A handle to the Greengrass API.
    :param contextlib.ExitStack stack:
        A context manager shared across all Greengrass tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-device-def')
    res = greengrass.create_device_definition(
        Name=name,
        InitialVersion=dict(
            Devices=[],
        ),
    )
    assert 'Arn' in res, unexpected(res)
    print('Created device definition: {}'.format(res['Arn']))
    shared_vars['device_arn'] = res['LatestVersionArn']

    stack.callback(
        greengrass.delete_device_definition,
        DeviceDefinitionId=res['Id'],
    )


def test_create_function_definition(greengrass, stack, shared_vars):
    """Verify access to create function definitions.

    :testid: greengrass-f-1-1

    :param Greengrass.Client greengrass:
        A handle to the Greengrass API.
    :param contextlib.ExitStack stack:
        A context manager shared across all Greengrass tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-func-def')
    res = greengrass.create_function_definition(
        Name=name,
        InitialVersion=dict(
            Functions=[],
        ),
    )
    assert 'Arn' in res, unexpected(res)
    print('Created function definition: {}'.format(name))
    shared_vars['function_arn'] = res['LatestVersionArn']

    stack.callback(
        greengrass.delete_function_definition,
        FunctionDefinitionId=res['Id'],
    )


def test_create_logger_definition(greengrass, stack, shared_vars):
    """Verify access to create logger definitions.

    :testid: greengrass-f-1-1

    :param Greengrass.Client greengrass:
        A handle to the Greengrass API.
    :param contextlib.ExitStack stack:
        A context manager shared across all Greengrass tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-logger-def')
    res = greengrass.create_logger_definition(
        Name=name,
        InitialVersion=dict(
            Loggers=[dict(
                Component='GreengrassSystem',
                Id='01234567-89ab-cdef-0123-456789abcdef',
                Level='INFO',
                Space=123,
                Type='FileSystem',
            )],
        ),
    )
    assert 'Arn' in res, unexpected(res)
    print('Created logger definition: {}'.format(name))
    shared_vars['logger_arn'] = res['LatestVersionArn']

    stack.callback(
        greengrass.delete_logger_definition,
        LoggerDefinitionId=res['Id'],
    )


def test_create_resource_definition(greengrass, stack, shared_vars):
    """Verify access to create resource definitions.

    :testid: greengrass-f-1-1

    :param Greengrass.Client greengrass:
        A handle to the Greengrass API.
    :param contextlib.ExitStack stack:
        A context manager shared across all Greengrass tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-res-def')
    res = greengrass.create_resource_definition(
        Name=name,
        InitialVersion=dict(
            Resources=[],
        ),
    )
    assert 'Arn' in res, unexpected(res)
    print('Created resource definition: {}'.format(name))
    shared_vars['resource_arn'] = res['LatestVersionArn']

    stack.callback(
        greengrass.delete_resource_definition,
        ResourceDefinitionId=res['Id'],
    )


def test_create_subscription_definition(greengrass, stack, shared_vars):
    """Verify access to create subscription definitions.

    :testid: greengrass-f-1-1

    :param Greengrass.Client greengrass:
        A handle to the Greengrass API.
    :param contextlib.ExitStack stack:
        A context manager shared across all Greengrass tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-sub-def')
    res = greengrass.create_subscription_definition(
        Name=name,
        InitialVersion=dict(
            Subscriptions=[],
        ),
    )
    assert 'Arn' in res, unexpected(res)
    print('Created subscription definition: {}'.format(name))
    shared_vars['subscription_arn'] = res['LatestVersionArn']

    stack.callback(
        greengrass.delete_subscription_definition,
        SubscriptionDefinitionId=res['Id'],
    )


def test_create_group(greengrass, stack, shared_vars):
    """Verify access to create groups.

    :testid: greengrass-f-1-2

    :param Greengrass.Client greengrass:
        A handle to the Greengrass API.
    :param contextlib.ExitStack stack:
        A context manager shared across all Greengrass tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-group')
    params = dict(
        Name=name,
        InitialVersion=dict(
            CoreDefinitionVersionArn=shared_vars['core_arn'],
            DeviceDefinitionVersionArn=shared_vars['device_arn'],
            FunctionDefinitionVersionArn=shared_vars['function_arn'],
            LoggerDefinitionVersionArn=shared_vars['logger_arn'],
            ResourceDefinitionVersionArn=shared_vars['resource_arn'],
            SubscriptionDefinitionVersionArn=shared_vars['subscription_arn'],
        ),
    )
    res = greengrass.create_group(**params)
    assert 'Arn' in res, unexpected(res)
    print('Created group: {}'.format(name))
    group_id = shared_vars['group_id'] = res['Id']

    stack.callback(
        greengrass.delete_group,
        GroupId=group_id,
    )


def test_create_group_ca(greengrass, group_id, shared_vars):
    """Verify access to create group certificate authorities.

    :testid: greengrass-f-1-3

    :param Greengrass.Client greengrass:
        A handle to the Greengrass API.
    :param str group_id:
        ID of a Greengrass Group.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = greengrass.create_group_certificate_authority(
        GroupId=group_id,
    )
    assert 'GroupCertificateAuthorityArn' in res, unexpected(res)
    ca_arn = shared_vars['group_ca_arn'] = res['GroupCertificateAuthorityArn']
    print('Created group CA: {}'.format(ca_arn))


def test_list_group_cas(greengrass, group_id, shared_vars):
    """Verify access to list group certificate authorities.

    :testid: greengrass-f-1-3

    :param Greengrass.Client greengrass:
        A handle to the Greengrass API.
    :param str group_id:
        ID of a Greengrass Group.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = greengrass.list_group_certificate_authorities(
        GroupId=group_id,
    )
    assert 'GroupCertificateAuthorities' in res, unexpected(res)

    group_cas = res['GroupCertificateAuthorities']
    assert len(group_cas), unexpected(res)

    ca = group_cas[0]
    assert 'GroupCertificateAuthorityId' in ca, unexpected(res)

    ca_id = shared_vars['group_ca_id'] = ca['GroupCertificateAuthorityId']
    print('Created group CA: {}'.format(ca_id))


def test_get_group_ca(greengrass, group_ca_id, group_id):
    """Verify access to get information about group certificate authorities.

    :testid: greengrass-f-1-3

    :param Greengrass.Client greengrass:
        A handle to the Greengrass API.
    :param str group_ca_id:
        ID of a Greengrass Group Certificate Authority.
    :param str group_id:
        ID of a Greengrass Group.

    """

    res = greengrass.get_group_certificate_authority(
        CertificateAuthorityId=group_ca_id,
        GroupId=group_id,
    )
    assert 'PemEncodedCertificate' in res, unexpected(res)
