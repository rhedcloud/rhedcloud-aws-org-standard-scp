"""
===========
test_events
===========

Verify access to the Events API.

Plan:

* Create an Events rule
* Delete an Events rule
* Disable an Events rule
* Enable an Events rule

${testcount:4}

"""

import json

import pytest

from aws_test_functions import (
    debug,
    has_status,
    ignore_errors,
    unexpected,
)

pytestmark = pytest.mark.raises_access_denied


@pytest.fixture
def events(session):
    return session.client("events")


@pytest.fixture
def rule_name():
    return "rhedcloudtest_rhedcloud_block_write_statement"


def test_put_rule(events, rule_name, stack):
    """Verify access to create Events rules.

    :param Events.Client events:
        A handle to the Events API.
    :param str rule_name:
        Name of a rule.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = events.put_rule(
        Name=rule_name,
        ScheduleExpression="cron(0 20 * * ? *)",
        EventPattern=json.dumps({
            "source": ["aws.ec2"],
            "detail-type": ["EC2 Instance State-change Notification"],
            "detail": {
                "state": ["running"]
            }
        }),
        State="DISABLED",
        Description=rule_name,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Created rule: {}".format(rule_name))
    stack.callback(
        ignore_errors(test_delete_rule),
        events,
        rule_name,
    )


def test_delete_rule(events, rule_name):
    """Verify access to delete Events rules.

    :param Events.Client events:
        A handle to the Events API.
    :param str rule_name:
        Name of a rule.

    """

    debug("Deleting rule: {}".format(rule_name))
    res = events.delete_rule(
        Name=rule_name,
    )
    assert has_status(res, 200), unexpected(res)


def test_disable_rule(events, rule_name):
    """Verify access to disable Events rules.

    :param Events.Client events:
        A handle to the Events API.
    :param str rule_name:
        Name of a rule.

    """

    debug("Disabling rule: {}".format(rule_name))
    res = events.disable_rule(
        Name=rule_name,
    )
    assert has_status(res, 200), unexpected(res)


def test_enable_rule(events, rule_name):
    """Verify access to enable Events rules.

    :param Events.Client events:
        A handle to the Events API.
    :param str rule_name:
        Name of a rule.

    """

    debug("Enabling rule: {}".format(rule_name))
    res = events.enable_rule(
        Name=rule_name,
    )
    assert has_status(res, 200), unexpected(res)
