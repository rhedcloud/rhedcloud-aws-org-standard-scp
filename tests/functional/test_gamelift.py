"""
=============
test_gamelift
=============

Verify access to use Amazon GameLift.

Plan:

* List, create, describe, update, and delete builds
* List, create, describe, update, and delete fleets
* List, create, describe, update, and delete aliases

${testcount:30}

"""

import pytest

from aws_test_functions import (
    TEST_PROFILE,
    create_bucket,
    ignore_errors,
    make_identifier,
    new_role,
    retry,
    unexpected,
)
from aws_test_functions.gamelift import (
    cleanup,
    delete_alias,
    delete_build,
    delete_fleet,
)

# This service is fully blocked
pytestmark = [pytest.mark.scp_check('list_aliases')]

# the S3 key for the uploaded application
APP_KEY = 'app.zip'


@pytest.fixture(scope='module')
def gamelift(session):
    """A shared handle to GameLift API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the GameLift API.

    """

    return session.client('gamelift')


@pytest.fixture(scope='module', autouse=True)
def do_cleanup(gamelift):
    """Clean up any old resources before proceeding with any tests.

    """

    cleanup(gamelift=gamelift)


@pytest.fixture(scope='module')
def bucket(session):
    """Get or create an S3 bucket where a sample application can be uploaded.

    In order to speed things along, if the S3 bucket already exists, it is
    assumed that the sample application is already uploaded and present inside
    that bucket. Another attempt will not be made to upload the sample
    application.

    If the S3 bucket does not exist, it will be created and the sample
    application will be uploaded into it.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :yields:
        An S3 Bucket resource.

    """

    s3 = session.resource('s3')
    name = '{}-gamelift'.format(TEST_PROFILE)

    b = s3.Bucket(name)
    b.load()
    if not b.creation_date:
        b = create_bucket(name, wait=True, s3=s3)
        get_sample_app(b, APP_KEY)

    return b


def get_sample_app(bucket, key):
    """Download a sample application and immediately upload it into the
    specified S3 bucket.

    :param S3.Bucket bucket:
        An S3 Bucket resource.
    :param str key:
        Key for the uploaded zip file.

    """

    # url = 'https://dhvs6pbgc0r1n.cloudfront.net/1.13.0.0/lumberyard-1.13-570429-pc-vrproject.zip'

    # print('Downloading sample app from {}'.format(url))
    # data = BytesIO(get_uri_contents(url, binary=True))

    # print('Uploading sample app to S3: {}'.format(bucket.name))
    # bucket.upload_fileobj(data, key)

    with open('./tests/resources/HelloLambda.zip', 'rb') as fh:
        print('Uploading sample app to S3: {}'.format(bucket.name))
        bucket.upload_fileobj(fh, key, ExtraArgs={'ServerSideEncryption': 'AES256'})


@pytest.fixture(scope='module')
def role(session, stack):
    """Create a new IAM Role for GameLift to access our S3 Bucket where the
    sample application lives.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param contextlib.ExitStack stack:
        A context manager shared across all GameLift tests.

    :yields:
        An IAM Role resource.

    """

    iam = session.resource('iam')
    r = stack.enter_context(new_role(
        'test-gamelift',
        'gamelift',
        'AmazonS3FullAccess',
        iam=iam,
    ))

    yield r


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'build_id': 'build-not-created',
        'fleet_id': 'fleet-not-created',
        'alias_id': 'alias-not-created',
        'instance_id': 'not-created',
    }


def test_list_aliases(gamelift, stack):
    """Verify access to list aliases.

    :testid: gamelift-f-1-1

    :param GameLift.Client gamelift:
        A handle to the GameLift API.

    """

    res = gamelift.list_aliases()
    assert 'Aliases' in res, unexpected(res)


def test_list_builds(gamelift):
    """Verify access to list builds.

    :testid: gamelift-f-1-2

    :param GameLift.Client gamelift:
        A handle to the GameLift API.

    """

    res = gamelift.list_builds()
    assert 'Builds' in res, unexpected(res)


def test_list_fleets(gamelift):
    """Verify access to list fleets.

    :testid: gamelift-f-1-3

    :param GameLift.Client gamelift:
        A handle to the GameLift API.

    """

    res = gamelift.list_fleets()
    assert 'FleetIds' in res, unexpected(res)


def test_create_build(gamelift, bucket, role, stack, shared_vars):
    """Verify access to create builds.

    :testid: gamelift-f-3-4

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param S3.Bucket bucket:
        S3 Bucket resource where a sample application lives.
    :param IAM.Role role:
        IAM Role resource that GameLift will use to access the S3 bucket.
    :param contextlib.ExitStack stack:
        A context manager shared across all GameLift tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-build')
    res = gamelift.create_build(
        Name=name,
        OperatingSystem='AMAZON_LINUX',
        StorageLocation=dict(
            Bucket=bucket.name,
            Key=APP_KEY,
            RoleArn=role.arn,
        ),
    )
    assert 'Build' in res, unexpected(res)

    build = res['Build']
    build_id = shared_vars['build_id'] = build['BuildId']
    print('Created build: {}'.format(build_id))

    stack.callback(
        ignore_errors(delete_build),
        build_id,
        gamelift=gamelift,
    )


def test_describe_build(gamelift, build_id):
    """Verify access to describe builds.

    :testid: gamelift-f-2-2

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str build_id:
        ID of a build.

    """

    def _test(res):
        return 'Build' in res and res['Build']['Status'] == 'READY'

    res = retry(
        gamelift.describe_build,
        kwargs={
            'BuildId': build_id,
        },
        msg='Checking build status',
        until=_test,
        delay=60,
        show=True,

        # don't retry when there's an AccessDenied error
        pred=lambda ex: 'AccessDenied' not in str(ex),
    )
    assert _test(res), unexpected(res)


def test_update_build(gamelift, build_id):
    """Verify access to update builds.

    :testid: gamelift-f-3-6

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str build_id:
        ID of a build.

    """

    res = gamelift.update_build(
        BuildId=build_id,
        Name='updated name',
    )
    assert 'Build' in res, unexpected(res)


def test_create_fleet(gamelift, build_id, stack, shared_vars):
    """Verify access to create fleets.

    :testid: gamelift-f-3-7

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str build_id:
        ID of a build.
    :param contextlib.ExitStack stack:
        A context manager shared across all GameLift tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-fleet')
    res = gamelift.create_fleet(
        Name=name,
        BuildId=build_id,
        EC2InstanceType='t2.micro',
        RuntimeConfiguration=dict(
            ServerProcesses=[dict(
                LaunchPath='/local/game/MyGame/server.exe',
                ConcurrentExecutions=1,
            )],
        ),
    )
    assert 'FleetAttributes' in res, unexpected(res)

    fleet = res['FleetAttributes']
    fleet_id = shared_vars['fleet_id'] = fleet['FleetId']
    print('Created fleet: {}'.format(fleet_id))

    stack.callback(
        ignore_errors(delete_fleet),
        fleet_id,
        gamelift=gamelift,
    )


@pytest.mark.slowtest
def test_describe_fleet_attributes_slow(gamelift, fleet_id):
    """Verify access to describe fleet attributes.

    :testid: gamelift-f-2-3

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str fleet_id:
        ID of a fleet.

    """

    def _test(res):
        return 'FleetAttributes' in res and res['FleetAttributes'][0]['Status'] in ('ACTIVE', 'ERROR')

    print('Describing fleet: {}'.format(fleet_id))
    res = retry(
        gamelift.describe_fleet_attributes,
        kwargs={
            'FleetIds': [fleet_id],
        },
        msg='Checking fleet status',
        until=_test,
        delay=600,
        max_attempts=20,
        show=True,
    )
    assert _test(res), unexpected(res)


def test_describe_fleet_attributes(gamelift, fleet_id):
    """Verify access to describe fleet attributes.

    :testid: gamelift-f-2-3

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str fleet_id:
        ID of a fleet.

    """

    res = gamelift.describe_fleet_attributes(
        FleetIds=[fleet_id],
    )
    assert 'FleetAttributes' in res and len(res['FleetAttributes']), unexpected(res)


def test_update_fleet_attributes(gamelift, fleet_id):
    """Verify access to update fleet attributes.

    :testid: gamelift-f-3-9

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str fleet_id:
        ID of a fleet.

    """

    res = gamelift.update_fleet_attributes(
        FleetId=fleet_id,
        Name='updated name',
    )
    assert 'FleetId' in res, unexpected(res)


def test_create_alias(gamelift, fleet_id, stack, shared_vars):
    """Verify access to create fleet aliases.

    :testid: gamelift-f-3-1

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str fleet_id:
        ID of a fleet.
    :param contextlib.ExitStack stack:
        A context manager shared across all GameLift tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-alias')
    res = gamelift.create_alias(
        Name=name,
        RoutingStrategy=dict(
            Type='SIMPLE',
            FleetId=fleet_id,
        ),
    )
    assert 'Alias' in res, unexpected(res)

    alias = res['Alias']
    alias_id = shared_vars['alias_id'] = alias['AliasId']
    print('Created alias: {}'.format(alias_id))

    stack.callback(
        ignore_errors(delete_alias),
        alias_id,
        gamelift=gamelift,
    )


def test_describe_alias(gamelift, alias_id):
    """Verify access to describe fleet aliases.

    :testid: gamelift-f-2-1

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str alias_id:
        ID of a fleet alias.

    """

    res = gamelift.describe_alias(
        AliasId=alias_id,
    )
    assert 'Alias' in res, unexpected(res)


def test_update_alias(gamelift, alias_id):
    """Verify access to update fleet aliases.

    :testid: gamelift-f-3-3

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str alias_id:
        ID of a fleet alias.

    """

    res = gamelift.update_alias(
        AliasId=alias_id,
        Name='updated alias',
    )
    assert 'Alias' in res, unexpected(res)


@pytest.mark.skip(reason='Fleet activation takes too long')
def test_describe_instances(gamelift, fleet_id, shared_vars):
    """Verify access to describe fleet instances.

    :testid: gamelift-f-2-4

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str fleet_id:
        ID of a fleet.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = gamelift.describe_instances(
        FleetId=fleet_id,
    )
    assert 'Instances' in res, unexpected(res)

    instances = res['Instances']
    assert len(instances), unexpected(res)

    instance = instances[0]
    instance_id = shared_vars['instance_id'] = instance['InstanceId']
    print('Found fleet instance: {}'.format(instance_id))


@pytest.mark.skip(reason='Fleet activation takes too long')
def test_get_instance_access(gamelift, fleet_id, instance_id):
    """Verify access to get access to a fleet instance.

    :testid: gamelift-f-2-4

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str fleet_id:
        ID of a fleet.
    :param str instance_id:
        ID of an EC2 instance within the fleet.

    """

    res = gamelift.get_instance_access(
        FleetId=fleet_id,
        InstanceId=instance_id,
    )
    assert 'InstanceAccess' in res, unexpected(res)


def test_delete_alias(gamelift, alias_id):
    """Verify access to delete a fleet alias.

    :testid: gamelift-f-3-2

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str alias_id:
        ID of the fleet alias to delete.

    """

    delete_alias(alias_id, gamelift=gamelift)


def test_delete_fleet(gamelift, fleet_id):
    """Verify access to delete fleets.

    :testid: gamelift-f-3-8

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str fleet_id:
        ID of the fleet to delete.

    """

    try:
        delete_fleet(fleet_id, gamelift=gamelift)
    except Exception as ex:
        if 'InvalidRequestException' not in str(ex):
            raise


def test_delete_build(gamelift, build_id):
    """Verify access to delete fleets.

    :testid: gamelift-f-3-5

    :param GameLift.Client gamelift:
        A handle to the GameLift API.
    :param str build_id:
        ID of the build to delete.

    """

    delete_build(build_id, gamelift=gamelift)
