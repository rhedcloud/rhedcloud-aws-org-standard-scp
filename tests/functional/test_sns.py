"""
========
test_sns
========

A customer should be able to use Amazon Simple Notification System (SNS) to
create topics, set filters, and publish messages.

Plan:

* Create SNS topic
* Setup SNS subscription filters
* Publish message to SNS topic

${testcount:12}

"""

from contextlib import contextmanager
from functools import partial
import operator
import json

import pytest

from aws_test_functions import (
    aws_client,
    aws_resource,
    debug,
    has_status,
    ignore_errors,
    make_identifier,
    retry,
    unexpected,
)

# treat errors about nonexistent items as access denied errors (because they
# likely originate from an access denied error earlier in the suite).
ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden|Authorization|InvalidParameter).*'

# This service is fully blocked
pytestmark = [
    pytest.mark.scp_check('list_platform_applications', needle='AuthorizationError'),
]


@pytest.fixture(scope='module')
def sns(session):
    """A shared handle to SNS API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the SNS API.

    """

    yield session.client('sns')


@pytest.fixture(scope='module')
def sqs(session):
    """A shared handle to SQS Service Resource API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the SQS Service Resource API.

    """

    yield session.resource('sqs')


@pytest.fixture(scope='module', autouse=True)
def do_cleanup(sns, sqs):
    """Clean up resources from previous tests."""

    ignore_errors(cleanup_queues)(sqs=sqs)
    ignore_errors(cleanup_topics)(sns=sns)


@pytest.fixture(scope='module')
def messages():
    """Return some messages to publish to the SNS Topic.

    :returns:
        A tuple of dictionaries with information about the messages to publish.

    """

    return (
        dict(
            Subject='Insurance Quote Request #1',
            Message='2017 Volvo S60, Montreal',
            MessageAttributes={
                'insurance_type': {
                    'DataType': 'String',
                    'StringValue': 'car',
                },
            },
        ), dict(
            Subject='Insurance Quote Request #2',
            Message='Male, 33 years old, Vancouver',
            MessageAttributes={
                'insurance_type': {
                    'DataType': 'String',
                    'StringValue': 'life',
                },
            },
        ), dict(
            Subject='Insurance Quote Request #3',
            Message='Townhouse, 1500 sq ft, Toronto',
            MessageAttributes={
                'insurance_type': {
                    'DataType': 'String',
                    'StringValue': 'home',
                },
            },
        ),
    )


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        # SNS Topic ARN
        'topic_arn': 'not-created',

        # Dictionary of SQS Queue Resources, keyed on queue name
        'queues': {},

        # Dictionary of Subscription ARNs for queues that subscribe to the SNS
        # Topic, keyed on queue name
        'subscriptions': {},

        # A list of message IDs that were published to the SNS Topic
        'message_ids': [],
    }


@contextmanager
@aws_resource('sqs')
def create_queues(*names, sqs=None, **kwargs):
    """Context manager to create some SQS Queues.

    :param str names:
        One or more queue names.

    :yields:
        A dictionary of SQS Queue Resources, keyed on queue name.

    """

    queues = {}

    try:
        for name in names:
            name = make_identifier(name)
            queue = sqs.create_queue(
                QueueName=name,
                Attributes=kwargs,
            )
            print('Created queue: {}'.format(queue.url))
            queues[name] = queue

            # This seems like it should work, but it does not. That's why
            # there's a policy (below) that allows any principal to send/receive
            # messages.
            # queue.add_permission(
            #     Label=make_identifier('queue-perm'),
            #     AWSAccountIds=[get_account_number()],
            #     Actions=['*'],
            # )

            # allow anyone to send/receive messages using this queue
            queue.set_attributes(
                Attributes=dict(
                    Policy=json.dumps(dict(
                        Version='2008-10-17',
                        Statement=[dict(
                            Effect='Allow',
                            Principal='*',
                            Action=[
                                'SQS:SendMessage',
                                'SQS:ReceiveMessage',
                            ],
                            Resource=queue.attributes['QueueArn'],
                        )],
                    )),
                ),
            )

        yield queues
    finally:
        for queue_name, queue in queues.items():
            print('Deleting queue: {}'.format(queue_name))
            ignore_errors(queue.delete)()


@aws_client('sns')
def delete_topic(arn, *, sns=None):
    """Delete a SNS Topic.

    :param str arn:
        ARN of a SNS Topic.

    """

    print('Deleting topic: {}'.format(arn))
    sns.delete_topic(TopicArn=arn)


@aws_client('sns')
def cleanup_topics(*, sns=None):
    """Remove SNS Topics from previous tests."""

    res = sns.list_topics()
    for topic in res['Topics']:
        ignore_errors(delete_topic)(
            topic['TopicArn'],
            sns=sns,
        )


@aws_resource('sqs')
def cleanup_queues(*, sqs=None):
    """Remove SQS Queues from previous tests."""

    for queue in sqs.queues.all():
        print('Deleting queue: {}'.format(queue.url))
        ignore_errors(queue.delete)()


def test_create_topic(sns, stack, shared_vars):
    """Verify access to create topics.

    :testid: sns-f-1-1

    :param SNS.Client sns:
        A handle to the SNS API.
    :param contextlib.ExitStack stack:
        A context manager shared across all SNS tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-topic')
    res = sns.create_topic(
        Name=name,
    )

    assert 'TopicArn' in res, unexpected(res)
    topic_arn = shared_vars['topic_arn'] = res['TopicArn']
    print('Created topic: {}'.format(topic_arn))

    stack.callback(
        ignore_errors(delete_topic),
        topic_arn,
        sns=sns,
    )


@pytest.mark.bypass('raises_access_denied')
def test_create_queues(sqs, stack, shared_vars):
    """Verify access to create queues.

    Note that this test bypasses "Access Denied" checks because it uses SQS,
    not SNS.

    :testid: sns-f-1-2

    :param SQS.ServiceResource sqs:
        A handle to the SNS Service Resource API.
    :param contextlib.ExitStack stack:
        A context manager shared across all SNS tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    queues = stack.enter_context(create_queues(
        'Vehicle-Insurance-Quotes',
        'Life-Insurance-Quotes',
        'All-Quotes',
        ReceiveMessageWaitTimeSeconds='10',
        VisibilityTimeout='3600',
        sqs=sqs,
    ))
    shared_vars['queues'] = queues


def test_subscribe_to_topic(sns, topic_arn, queues, stack, shared_vars):
    """Verify access to subscribe to topics.

    :testid: sns-f-1-2

    :param SNS.Client sns:
        A handle to the SNS API.
    :param str topic_arn:
        ARN of the SNS Topic to subscribe to.
    :param dict queues:
        A dictionary of queues that will subscribe to the specified topic.
    :param contextlib.ExitStack stack:
        A context manager shared across all SNS tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    subscriptions = {}

    for queue_name, queue in queues.items():
        res = sns.subscribe(
            TopicArn=topic_arn,
            Protocol='sqs',
            Endpoint=queue.attributes['QueueArn'],
        )
        assert 'SubscriptionArn' in res, unexpected(res)

        arn = res['SubscriptionArn']
        print('Subscribed {} to topic {} => {}'.format(queue_name, topic_arn, queue.url))
        subscriptions[queue_name] = arn

        stack.callback(
            ignore_errors(sns.unsubscribe),
            SubscriptionArn=arn,
        )

    shared_vars['subscriptions'] = subscriptions


permitted = partial(pytest.param, marks=pytest.mark.permitted)
blocked = partial(pytest.param, marks=pytest.mark.raises_access_denied)


@pytest.mark.parametrize("proto,endpoint", (
    permitted("https", "https://www.google.com"),
    permitted("sqs", "see-queue-arn-fixture"),
    blocked("http", "http://www.google.com"),
    permitted("email", "aws-admin-16@emory.edu"),
    permitted("email-json", "aws-admin-16@emory.edu"),
    blocked("sms", "4047277882"),
))
def test_subscribe(sns, topic_arn, queues, proto, endpoint):
    """Verify access to subscribe to SNS endpoints.

    :param SNS.Client sns:
        Handle to the SNS API.
    :param str topic_arn:
        ARN of the SNS Topic to subscribe to.
    :param dict queues:
        A dictionary of queues that will subscribe to the specified topic.
    :param str proto:
        Topic protocol.
    :param str endpoint:
        Topic endpoint.

    """

    if proto == "sqs":
        queue_name, queue = tuple(queues.items())[0]
        endpoint = queue.attributes["QueueArn"]

    debug("Attempting to subscribe: {}; {}".format(proto, endpoint))
    res = sns.subscribe(
        TopicArn=topic_arn,
        Protocol=proto,
        Endpoint=endpoint,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.bypass('raises_access_denied')
def test_set_subscription_attributes(sns, subscriptions):
    """Verify access to alter filter policies for subscriptions.

    :testid: sns-f-1-2

    :param SNS.Client sns:
        A handle to the SNS API.
    :param dict subscriptions:
        A dictionary of subscription ARNs keyed on queue name.

    """

    for queue_name, sub_arn in subscriptions.items():
        if 'Vehicle' in queue_name:
            insurance_type = ['car', 'boat']
        elif 'Life' in queue_name:
            insurance_type = ['life']
        else:
            continue

        sns.set_subscription_attributes(
            SubscriptionArn=sub_arn,
            AttributeName='FilterPolicy',
            AttributeValue=json.dumps({
                'insurance_type': insurance_type,
            }),
        )
        print('Updated subscription: {}'.format(sub_arn))


def test_publish(sns, topic_arn, messages, shared_vars):
    """Verify access to publish messages to topics.

    :testid: sns-f-1-3

    :param SNS.Client sns:
        A handle to the SNS API.
    :param str topic_arn:
        ARN of the SNS Topic to subscribe to.
    :param iterable messages:
        An iterable containing information about messages to publish to the
        specified topic.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    message_ids = []

    for msg in messages:
        res = sns.publish(TopicArn=topic_arn, **msg)
        assert 'MessageId' in res, unexpected(res)

        msg_id = res['MessageId']
        print('Published message: {}'.format(msg_id))
        message_ids.append(msg_id)

    shared_vars['message_ids'] = message_ids


@pytest.mark.bypass('raises_access_denied')
def test_receive_messages(sns, queues, message_ids, is_blocked_service):
    """Verify access to receive messages published to topics.

    :testid: sns-f-1-3

    :param SNS.Client sns:
        A handle to the SNS API.
    :param dict queues:
        A dictionary of queues that will subscribe to the specified topic.
    :param list message_ids:
        A list of IDs for messages that were published to the SNS topic.
    :param callable is_blocked_service:
        A callable to determine whether SNS is a blocked service.

    """

    is_blocked = is_blocked_service()
    messages = {}

    def _test(res):
        """Determine whether an API response appears to be valid.

        :param list res:
            A list of messages retrieved from the API.

        :returns:
            A boolean.

        """

        if is_blocked:
            # while in the TEST_ORG, expect the number of messages to be 0
            # (because there shouldn't be a topic to publish to)
            check = operator.eq
        else:
            # while NOT in the TEST_ORG, expect the number of messages to be
            # greater than 0
            check = operator.gt

        return check(len(res), 0)

    for queue in queues.values():
        res = retry(
            queue.receive_messages,
            kwargs=dict(
                MaxNumberOfMessages=10,
            ),
            msg='Checking queue {} for messages'.format(queue.url),
            until=_test,
            max_attempts=3,
            show=True,
        )
        assert _test(res), unexpected(res)

        if is_blocked:
            # no need to check message IDs in the TEST_ORG
            continue

        messages[queue] = msg = json.loads(res[0].body)
        assert msg['MessageId'] in message_ids, \
            'Unexpected message found: {} not in {}'.format(msg, message_ids)

    assert len(messages) == len(message_ids)
