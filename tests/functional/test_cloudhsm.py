"""
=============
test_cloudhsm
=============

This is a fully blocked service. Attempts to describe, create or delete
clusters should be blocked by a SCP enabled account.

${testcount:3}

"""

import pytest

from aws_test_functions import (
    aws_client,
    catch,
    debug,
    env,
    get_subnet_id,
    has_status,
    ignore_errors,
    unexpected,
)

# This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('describe_clusters')]


@pytest.fixture(scope='module')
def cloudhsm(session):
    """
    A shared handle to Amazon CloudHSM

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to Amazon CloudHSM

    """

    return session.client('cloudhsmv2')


@pytest.fixture
def subnet_id(vpc_id):
    return get_subnet_id(vpc_id, env.RHEDCLOUD_INTERNAL_SUBNET)


@pytest.dict_fixture
def shared_vars():
    """
    Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'cluster_id': 'cluster-aaaaaaaaaaaa',
        'cluster_created': False
    }


@catch(Exception, msg='Failed to delete cluster: {ex}')
@aws_client('cloudhsmv2')
def cleanup_clusters(*, cloudhsmv2=None):
    """Attempt to remove any unused clusters."""

    res = cloudhsmv2.describe_clusters()
    for cluster in res['Clusters']:
        ignore_errors(test_delete_cluster)(cloudhsmv2, cluster["ClusterId"])


def test_describe_clusters(cloudhsm):
    describe_response = cloudhsm.describe_clusters()

    assert 'Clusters' in describe_response, 'Clusters not able to be described'


def test_create_cluster(cloudhsm, subnet_id, stack, shared_vars):
    """
    Create a CloudHSM cluster

    :param boto3.client cloudhsm:
        A handle to the AWS CloudHSM API.
    :param str subnet_id:
        ID of a subnet.
    :param contextlib.ExitStack stack:
        A context manager shared across all CloudHSM tests.
    :param dict shared_vars:
        Dictionary shared between test functions

    """

    create_response = cloudhsm.create_cluster(
        SubnetIds=[subnet_id],
        HsmType='hsm1.medium'
    )

    assert 'Cluster' in create_response, 'Unable to create cluster'

    cluster_id = shared_vars['cluster_id'] = create_response['Cluster']['ClusterId']
    shared_vars['cluster_created'] = True

    stack.callback(
        ignore_errors(test_delete_cluster),
        cloudhsm,
        cluster_id,
    )


def test_delete_cluster(cloudhsm, cluster_id):
    """
    Delete the cluster created in test

    :param boto3.client cloudhsm:
        A handle to the AWS CloudHSM API.
    :param str cluster_id:
        ID of the cluster to delete.

    """

    debug('Deleting cluster: {}'.format(cluster_id))
    res = cloudhsm.delete_cluster(
        ClusterId=cluster_id,
    )

    assert has_status(res, 200), unexpected(res)
    assert 'Cluster' in res, 'Unable to delete cluster'
