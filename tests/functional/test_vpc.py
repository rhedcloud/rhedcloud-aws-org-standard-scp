"""
========
test_vpc
========

Verify the ability to use Amazon VPC APIs.

Plan:

* Verify that users cannot create or delete VPCs
* Verify that users cannot create, modify, or delete subnets
* Verify that users cannot create, modify, or delete route tables
* Verify that users cannot create Internet Gateways
* Verify that users cannot attach Elastic IPs to EC2 instances
* Verify that users cannot create, modify, or delete VPC endpoints
* Verify that users cannot create NAT Gateways
* Verify that users cannot setup VPC peering
* Verify that users cannot create Customer Gateways
* Verify that users cannot create, modify, or delete virtual private gateways
* Verify that users cannot create VPN connections

${testcount:20}

"""

import pytest

from aws_test_functions import (
    ClientError,
    create_internet_gateway,
    delete_service_endpoint,
    dict_to_filters,
    get_full_service_name,
    new_ec2_instance,
    service_endpoint,
)

pytestmark = [pytest.mark.scp_check("describe_vpc_endpoints", client_fixture="ec2")]


# this mark behaves similarly to the raises_access_denied mark, but for
# "UnauthorizedOperation" errors
unauthorized = pytest.mark.raises(ClientError, match='UnauthorizedOperation')


@pytest.fixture(scope='module')
def ec2(session):
    """A shared handle to EC2 APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the EC2 APIs.

    """

    return session.client('ec2')


@pytest.fixture(scope='module')
def cidr():
    """A reusable CIDR value.

    :returns:
        A string.

    """

    return '192.168.0.0/24'


@pytest.fixture(scope='module')
def route_table(ec2, vpc_id):
    """Retrieve information about an existing route table.

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        ID of a VPC.

    :returns:
        A dictionary.

    """

    filters = dict_to_filters({
        'vpc-id': vpc_id,
    })

    res = ec2.describe_route_tables(Filters=filters)
    for table in res['RouteTables']:
        # we require a route table that is associated with a subnet
        if not table['Associations']:
            continue

        return table


def get_elastic_ip(ec2):
    """Attempt to allocate an Elastic IP address. This should always fail in
    Type 1 VPCs.

    .. note::

        This is not a fixture because fixtures are setup before test functions
        are executed, and we need this operation to be handled by the
        ``@unauthorized`` mark.

    :param EC2.Client ec2:
        A handle to the EC2 APIs.

    :returns:
        A dictionary.

    """

    res = ec2.allocate_address(DryRun=True)
    assert False, 'Had permission to create Elastic IP'

    return res


@pytest.fixture(scope='module')
def s3_endpoint(ec2, vpc_id):
    """Retrieve information about the existing S3 Service Endpoint.

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        ID of a VPC.

    :returns:
        A dictionary.

    """

    filters = dict_to_filters({
        'service-name': get_full_service_name('s3'),
        'vpc-id': vpc_id,
    })

    res = ec2.describe_vpc_endpoints(Filters=filters)
    assert 'VpcEndpoints' in res, 'Unexpected response: {}'.format(res)

    return res['VpcEndpoints'][0]


def create_customer_gateway(ec2):
    """Attempt to create a Customer Gateway. This should always fail in Type 1
    VPCs.

    .. note::

        This is not a fixture because fixtures are setup before test functions
        are executed, and we need this operation to be handled by the
        ``@unauthorized`` mark.

    :param EC2.Client ec2:
        A handle to the EC2 APIs.

    :returns:
        A dictionary.

    """

    return ec2.create_customer_gateway(
        BgpAsn=65000,
        PublicIp='8.8.8.8',
        Type='ipsec.1',
        DryRun=True,
    )['CustomerGateway']


@pytest.fixture(scope='module')
def vgw(ec2):
    """Retrieve information about an existing Virtual Private Gateway.

    :param EC2.Client ec2:
        A handle to the EC2 APIs.

    :returns:
        A dictionary.

    """

    try:
        res = ec2.describe_vpn_gateways()
        assert 'VpnGateways' in res and len(res['VpnGateways']), 'Unexpected response: {}'.format(res)

        return res['VpnGateways'][0]
    except (IndexError, AssertionError):
        # Hosting VPC's don't have VGWs
        pass


@unauthorized
def test_create_vpc(ec2, cidr):
    """Users should not be able to create VPCs.

    :testid: vpc-f-1-1

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str cidr:
        An IPv4 CIDR string.

    """

    ec2.create_vpc(
        CidrBlock=cidr,
        DryRun=True,
    )
    assert False, 'Had permission to create VPC'


@unauthorized
def test_delete_vpc(ec2, vpc_id):
    """Users should not be able to delete VPCs.

    :testid: vpc-f-1-2

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        ID of a VPC.

    """

    ec2.delete_vpc(
        VpcId=vpc_id,
        DryRun=True,
    )
    assert False, 'Had permission to delete VPC'


@unauthorized
def test_create_subnet(ec2, vpc_id, cidr):
    """Users should not be able to create subnets.

    :testid: vpc-f-2-1

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        ID of a VPC.
    :param str cidr:
        An IPv4 CIDR string.

    """

    res = ec2.create_subnet(
        CidrBlock=cidr,
        VpcId=vpc_id,
        DryRun=True,
    )
    print('Created subnet: {}'.format(res))
    assert False, 'Had permission to create subnet'


@unauthorized
def test_modify_subnet(ec2, internal_subnet_id):
    """Users should not be able to modify subnets.

    :testid: vpc-f-2-2

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str internal_subnet_id:
        ID of a subnet in a Type 1 VPC.

    """

    filters = dict_to_filters({
        'subnet-id': internal_subnet_id,
    })
    subnets_res = ec2.describe_subnets(Filters=filters)
    assert 'Subnets' in subnets_res, 'Unexpected response: {}'.format(subnets_res)
    assert len(subnets_res['Subnets']) == 1, 'Unexpected response: {}'.format(subnets_res)
    current_value = subnets_res['Subnets'][0]['MapPublicIpOnLaunch']

    res = ec2.modify_subnet_attribute(
        SubnetId=internal_subnet_id,
        MapPublicIpOnLaunch={'Value': not current_value},
    )
    print('Modified subnet: {}'.format(res))

    # if we get to this point, undo the change we just made (we should never
    # make it here if things are configured correctly)
    res = ec2.modify_subnet_attribute(
        SubnetId=internal_subnet_id,
        MapPublicIpOnLaunch={'Value': current_value},
    )


@unauthorized
def test_delete_subnet(ec2, internal_subnet_id):
    """Users should not be able to delete subnets.

    :testid: vpc-f-2-3

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str internal_subnet_id:
        ID of a subnet in a Type 1 VPC.

    """

    res = ec2.delete_subnet(
        SubnetId=internal_subnet_id,
        DryRun=True,
    )
    print('Deleted subnet: {}'.format(res))
    assert False, 'Had permission to delete subnet'


@unauthorized
def test_create_route_table(ec2, vpc_id):
    """Users should not be able to create route tables.

    :testid: vpc-f-3-1

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        ID of a VPC.

    """

    ec2.create_route_table(
        VpcId=vpc_id,
        DryRun=True,
    )
    assert False, 'Had permission to create route table'


@unauthorized
def test_modify_route_table(ec2, route_table):
    """Users should not be able to modify route tables.

    :testid: vpc-f-3-2

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param dict route_table:
        A dependency on the fixture which returns information about an existing
        route table.

    """

    ec2.disassociate_route_table(
        AssociationId=route_table['Associations'][0]['RouteTableAssociationId'],
        DryRun=True,
    )
    assert False, 'Had permission to modify route table'


@unauthorized
def test_delete_route_table(ec2, route_table):
    """Users should not be able to delete route tables.

    :testid: vpc-f-3-3

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param dict route_table:
        A dependency on the fixture which returns information about an existing
        route table.

    """

    ec2.delete_route_table(
        RouteTableId=route_table['RouteTableId'],
        DryRun=True,
    )
    assert False, 'Had permission to delete route table'


@unauthorized
def test_create_internet_gateway(ec2):
    """Users should not be able to create Internet Gateways.

    :testid: vpc-f-4-1

    :param EC2.Client ec2:
        A handle to the EC2 APIs.

    """

    create_internet_gateway(dry_run=True, ec2=ec2)
    assert False, 'Had permission to create Internet Gateway'


@unauthorized
def test_attach_elastic_ip(ec2, internal_subnet_id):
    """Users should not be able to attach Elastic IPs to EC2 instances.

    :testid: vpc-f-5-1

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str internal_subnet_id:
        ID of a subnet in a Type 1 VPC.

    """

    elastic_ip = get_elastic_ip(ec2)
    with new_ec2_instance('test_vpc', SubnetId=internal_subnet_id) as inst:
        ec2.associate_address(
            InstanceId=inst.instance_id,
            PublicIp=elastic_ip['PublicIp'],
            DryRun=True,
        )
        assert False, 'Had permission to attach Elastic IP'


@unauthorized
def test_create_kinesis_streams_endpoint(ec2, vpc_id):
    """Users should not be able to create a VPC endpoint for Kinesis Streams.

    :testid: vpc-f-6-1

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        ID of a VPC.

    """

    with service_endpoint('kinesis-streams', vpc_id, ec2=ec2):
        assert True


@unauthorized
def test_modify_vpc_endpoint(ec2, s3_endpoint):
    """Users should not be able to modify a VPC endpoint.

    :testid: vpc-f-6-2

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param dict s3_endpoint:
        A dependency on the fixture which returns information about the
        existing S3 Service Endpoint.

    """

    ec2.modify_vpc_endpoint(
        VpcEndpointId=s3_endpoint['VpcEndpointId'],
        PolicyDocument=s3_endpoint['PolicyDocument'],
        DryRun=True,
    )
    assert False, 'Had permission to modify VPC endpoint'


@unauthorized
def test_delete_vpc_endpoint(ec2, s3_endpoint):
    """Users should not be able to delete a VPC endpoint.

    :testid: vpc-f-6-3

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param dict s3_endpoint:
        A dependency on the fixture which returns information about the
        existing S3 Service Endpoint.

    """

    delete_service_endpoint(s3_endpoint['VpcEndpointId'], DryRun=True, ec2=ec2)
    assert False, 'Had permission to delete VPC endpoint'


@unauthorized
def test_create_nat_gateway(ec2, internal_subnet_id):
    """Users should not be able to create a NAT Gateway.

    :testid: vpc-f-7-1

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str internal_subnet_id:
        ID of a subnet in a Type 1 VPC.

    """

    elastic_ip = get_elastic_ip(ec2)
    ec2.create_nat_gateway(
        AllocationId=elastic_ip['AllocationId'],
        SubnetId=internal_subnet_id,
    )
    assert False, 'Had permission to create NAT Gateway'


@unauthorized
def test_create_vpc_peering_connection(ec2, vpc_id):
    """Users should not be able to create a VPC Peering connection.

    :testid: vpc-f-8-1

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        ID of a VPC.

    """

    filters = dict_to_filters({
        'isDefault': 'true',
    })

    res = ec2.describe_vpcs(Filters=filters)
    assert 'Vpcs' in res, 'Unexpected response: {}'.format(res)

    ec2.create_vpc_peering_connection(
        VpcId=vpc_id,
        PeerVpcId=res['Vpcs'][0]['VpcId'],
        DryRun=True,
    )
    assert False, 'Had permission to create VPC Peering Connection'


@unauthorized
def test_create_customer_gateway(ec2, vpc_id):
    """Users should not be able to create Customer Gateway.

    :testid: vpc-f-9-1

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        ID of a VPC.

    """

    create_customer_gateway(ec2)
    assert False, 'Had permission to create Customer Gateway'


@unauthorized
def test_create_vgw(ec2, vpc_id):
    """Users should not be able to create Virtual Private Gateways.

    :testid: vpc-f-10-1

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        ID of a VPC.

    """

    ec2.create_vpn_gateway(
        Type='ipsec.1',
        DryRun=True,
    )
    assert False, 'Had permission to create Virtual Private Gateway'


@unauthorized
def test_modify_vgw(ec2, vpc_id, vgw):
    """Users should not be able to modify Virtual Private Gateways.

    :testid: vpc-f-10-2

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param str vpc_id:
        ID of a VPC.
    :param dict vgw:
        A dependency on the fixture which returns information about the
        existing Virtual Private Gateway.

    """

    ec2.detach_vpn_gateway(
        VpcId=vpc_id,
        VpnGatewayId=vgw['VpnGatewayId'],
        DryRun=True,
    )
    assert False, 'Had permission to detach Virtual Private Gateway'


@unauthorized
def test_delete_vgw(ec2, vgw):
    """Users should not be able to delete Virtual Private Gateways.

    :testid: vpc-f-10-3

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param dict vgw:
        A dependency on the fixture which returns information about the
        existing Virtual Private Gateway.

    """

    ec2.delete_vpn_gateway(
        VpnGatewayId=vgw['VpnGatewayId'],
        DryRun=True,
    )
    assert False, 'Had permission to delete Virtual Private Gateway'


@unauthorized
def test_create_vpn_connection(ec2, vgw):
    """Users should not be able to create VPN connections.

    :testid: vpc-f-11-1

    :param EC2.Client ec2:
        A handle to the EC2 APIs.
    :param dict vgw:
        A dependency on the fixture which returns information about the
        existing Virtual Private Gateway.

    """

    customer_gateway = create_customer_gateway(ec2)
    ec2.create_vpn_connection(
        CustomerGatewayId=customer_gateway['CustomerGatewayId'],
        Type='ipsec.1',
        VpnGatewayId=vgw['VpnGatewayId'],
        DryRun=True,
    )
    assert False, 'Had permission to create VPN connection'
