'''
==========
test_polly
==========

A customer can use Polly

Plan:

* Autoprovision a type 1 VPC by invoking the RHEDcloud ESB or web service
* Create a new user with Administrator privileges
* Describe Polly voices using the new user
* Synthesize speech using the new user

${testcount:1}

'''

import re

import pytest

# This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('list_lexicons')]


@pytest.fixture(scope='module')
def polly(session):
    return session.client('polly', region_name='us-east-1')


def test_polly_usage(polly):
    """Synthesize speech using AWS Polly

    :testid: polly-f-1-1

    """

    voices = polly.describe_voices()['Voices']
    for voice in voices:
        assert 'Gender' in voice, 'Expected Gender key in response'
        assert bool(re.match(r'Female|Male', voice['Gender'])), \
            'Unexepcted Gender value'
        assert 'Id' in voice, 'Expected Id key in response'
        assert bool(re.match(r'[A-Z][a-z]', voice['Id'])), \
            'Expected name in response'
        assert 'LanguageCode' in voice and voice['LanguageCode'], \
            'Expected language code key in response'
        assert bool(re.match(r'([a-z]{2,3}\-[A-Z]{2}|arb)', voice['LanguageCode'])), \
            'Expected valid language code in response (got {LanguageCode})'.format(**voice)
        assert 'LanguageName' in voice, 'Expected language name in response'
        assert 'Name' in voice, 'Expected key "Name" in response'

    sample_rate = 8000
    resp = polly.synthesize_speech(
        Text='Hi. My name is Joanna',
        OutputFormat='pcm',
        VoiceId='Joanna',
        SampleRate=str(sample_rate)
    )
    assert resp['ContentType'] == 'audio/pcm', \
        'Expected audio/pcm content type'
    assert int(resp['RequestCharacters']) == 21, \
        'Expected request characters to be 21'
    assert len(resp['AudioStream'].read()) > 0, \
        'Expected length greater than zero bytes'
