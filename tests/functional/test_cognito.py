"""
============
test_cognito
============

This is a fully blocked service. Attempting to create a user pool should
result in failure.

cognito-f-1-1 Verify ability to create user pools
cognito-f-1-1 Verify ability to update user pools

Test count set to 4 due to running as blocked service in pytest.ini
${testcount:4}

"""

import pytest

from aws_test_functions import (
    make_identifier,
)

# This service is fully blocked
pytestmark = [pytest.mark.scp_check('list_user_pools', kwargs=dict(MaxResults=1))]


@pytest.fixture(scope='module')
def cognito(session):
    """
    A shared handle to Cognito Identity Provider API

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Cognito Identity Provider API

    """

    return session.client('cognito-idp')


@pytest.fixture(scope='module', autouse=True)
def cleanup_pool(cognito, shared_vars):
    """
    Delete the user pool created in the test

    :param boto3.client A handle to the AWS Cognito Identity Provider API
    :param dict shared_vars: dictionary shared between test functions

    """

    yield

    if shared_vars['user_pool_created']:
        cognito.delete_user_pool(
            UserPoolId=shared_vars['user_pool_id']
        )


@pytest.dict_fixture
def shared_vars():
    """
    Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'user_pool_id': 'not-created',
        'user_pool_created': False,
    }


def test_create_pool(cognito, shared_vars):
    """
    Verify ability to create user pools

    :testid cognito-f-1-1

    :param boto3.client cognito
        A handle to the AWS Cognito Identity Provider API
    :param dict shared_vars
        dictionary shared between test functions

    """

    create_response = cognito.create_user_pool(
        PoolName=make_identifier('pool')
    )

    assert 'UserPool' in create_response, 'Unable to create user pool'

    shared_vars['user_pool_created'] = True
    shared_vars['user_pool_id'] = create_response['UserPool']['Id']


def test_update_pool(cognito, shared_vars):
    """
    Verify ability to create user pools

    :testid cognito-f-1-2

    :param boto3.client cognito
        A handle to the AWS Cognito Identity Provider API
    :param dict shared_vars
        dictionary shared between test functions

    """

    update_response = cognito.update_user_pool(
        UserPoolId=shared_vars['user_pool_id'],
        Policies={
            'PasswordPolicy': {
                'MinimumLength': 12
            }
        }
    )

    assert update_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to update user pool'
