"""
==================
test_stepfunctions
==================

A user should be able to create Step Functions to create, execute, update, and
delete application workflow.

Plan:

* Create a state machine
* Start a new execution
* Update a state machine
* Delete state machine

${testcount:14}

"""

import json

import pytest

from aws_test_functions import (
    aws_client,
    get_or_create_role,
    has_status,
    ignore_errors,
    make_identifier,
    retry,
    unexpected,
)

# treat errors about invalid ARNs as access denied errors (because they likely
# originate from an access denied error earlier in the suite).
ACCESS_DENIED_NEEDLE = r'.*(AccessDenied|Unauthorized|Forbidden|InvalidArn).*'

# This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('list_state_machines', client_fixture='sf')]


@pytest.fixture(scope='module')
def sf(session):
    """A shared handle to Step Functions API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Step Functions API.

    """

    return session.client('stepfunctions')


def code_dict(output="Hello World!"):
    return {
        "Comment": ("A Hello World example of the Amazon States Language "
                    "using a Pass state"),
        "StartAt": "HelloWorld",
        "States": {
            "HelloWorld": {
                "Type": "Pass",
                "Result": output,
                "End": True
            }
        }
    }.copy()


@pytest.fixture(scope='module')
def code():
    """Produce some code for Step Functions.

    :returns:
        A JSON string.

    """

    return json.dumps(code_dict())


@pytest.fixture(scope='module')
def updated_code():
    """Produce some updated code for Step Functions.

    :returns:
        A JSON string.

    """

    return json.dumps(code_dict("Hello World has been updated!"))


@pytest.fixture(scope='module')
def role(session):
    """Create a new IAM Role for Step Functions to work correctly.

    :returns:
        An IAM Role resource.

    """

    return get_or_create_role(
        'StatesExecutionRole-us-east-1',
        'AWSStepFunctionsFullAccess',
        service='states',
        iam=session.resource('iam'),
    )


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'machine_arn': 'not-created',
        'execution_arn': 'not-created',
    }


@aws_client('stepfunctions')
def delete_state_machine(arn, *, stepfunctions=None):
    """Delete a state machine.

    :param str arn:
        ARN of the state machine.

    """

    print('Deleting state machine: {}'.format(arn))
    stepfunctions.delete_state_machine(
        stateMachineArn=arn,
    )


def test_create_state_machine(sf, code, role, stack, shared_vars):
    """Verify access to create state machines.

    :testid: stepfunctions-f-1-1

    :param SFN.Client sf:
        A handle to the Step Functions API.
    :param str code:
        Code to execute with Step Functions.
    :param IAM.Role role:
        IAM Role to assign to the state machine.
    :param contextlib.ExitStack stack:
        A context manager shared across all Step Functions tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('HelloWorld')
    res = sf.create_state_machine(
        name=name,
        definition=code,
        roleArn=role.arn,
    )
    assert 'stateMachineArn' in res, unexpected(res)
    machine_arn = shared_vars['machine_arn'] = res['stateMachineArn']
    print('Created state machine: {}'.format(machine_arn))

    stack.callback(
        ignore_errors(delete_state_machine),
        machine_arn,
        stepfunctions=sf,
    )


def test_start_execution(sf, machine_arn, stack, shared_vars):
    """Verify access to start executions.

    :testid: stepfunctions-f-1-2

    :param SFN.Client sf:
        A handle to the Step Functions API.
    :param str machine_arn:
        ARN of the state machine to execute.
    :param contextlib.ExitStack stack:
        A context manager shared across all Step Functions tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = sf.start_execution(
        stateMachineArn=machine_arn,
        input='{}',
    )
    assert 'executionArn' in res, unexpected(res)
    execution_arn = shared_vars['execution_arn'] = res['executionArn']
    print('Started execution: {}'.format(execution_arn))

    stack.callback(
        ignore_errors(sf.stop_execution),
        executionArn=execution_arn,
    )


def test_describe_execution(sf, execution_arn, expected_output='"Hello World!"'):
    """Verify access to describe executions.

    :testid: stepfunctions-f-1-2

    :param SFN.Client sf:
        A handle to the Step Functions API.
    :param str execution_arn:
        ARN of the execution to describe.
    :param str expected_output: (optional)
        Output to expect from state machine execution.

    """

    def _test(res):
        return 'status' in res and res['status'] == 'SUCCEEDED'

    res = retry(
        sf.describe_execution,
        kwargs={
            'executionArn': execution_arn,
        },
        msg='Checking execution status',
        until=_test,
        pred=lambda ex: 'InvalidArn' not in str(ex),
    )
    assert _test(res), unexpected(res)
    assert 'output' in res, unexpected(res)
    assert res['output'] == expected_output


def test_update_state_machine(sf, machine_arn, updated_code):
    """Verify access to update state machines.

    :testid: stepfunctions-f-1-3

    :param SFN.Client sf:
        A handle to the Step Functions API.
    :param str machine_arn:
        ARN of the state machine to update.
    :param str updated_code:
        Updated code to execute.

    """

    res = sf.update_state_machine(
        stateMachineArn=machine_arn,
        definition=updated_code,
    )
    assert 'updateDate' in res, unexpected(res)
    print('Updated state machine: {}'.format(machine_arn))


def test_describe_state_machine(sf, machine_arn):
    """Verify that the state machine has been updated.

    :testid: stepfunctions-f-1-3

    :param SFN.Client sf:
        A handle to the Step Functions API.
    :param str machine_arn:
        ARN of the state machine to update.

    """

    res = retry(
        sf.describe_state_machine,
        kwargs=dict(
            stateMachineArn=machine_arn,
        ),
        msg="Checking state machine definition",
        until=lambda res: "been updated" in res["definition"]
    )
    assert has_status(res, 200), unexpected(res)


def test_start_updated_execution(sf, machine_arn, stack, shared_vars):
    """Verify access to start updated state machines.

    :testid: stepfunctions-f-1-3

    :param SFN.Client sf:
        A handle to the Step Functions API.
    :param str machine_arn:
        ARN of the state machine to execute.
    :param contextlib.ExitStack stack:
        A context manager shared across all Step Functions tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    test_start_execution(sf, machine_arn, stack, shared_vars)


def test_describe_updated_execution(sf, execution_arn):
    """Verify access to describe executions.

    :testid: stepfunctions-f-1-3

    :param SFN.Client sf:
        A handle to the Step Functions API.
    :param str execution_arn:
        ARN of the execution to describe.

    """

    test_describe_execution(sf, execution_arn, '"Hello World has been updated!"')


def test_delete_state_machine(sf, machine_arn):
    """Verify access to delete state machines.

    :testid: stepfunctions-f-1-4

    :param SFN.Client sf:
        A handle to the Step Functions API.
    :param str machine_arn:
        ARN of the state machine to delete.

    """

    delete_state_machine(machine_arn, stepfunctions=sf)
