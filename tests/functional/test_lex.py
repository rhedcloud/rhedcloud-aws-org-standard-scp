'''
===========
test_lex
===========
A customer can use Lex to create a chat bot

Plan:

* Create an Amazon Lex bot
* Test newly created bot

${testcount:2}

'''

import pytest
import time

from aws_test_functions import retry

#This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('get_intents', 
                                    client_fixture='lex_models')]


@pytest.fixture(scope='module')
def flowers():
    return dict(
        name='FlowerTypesTwo',
        description='Types of flowers to pick up (delete me)',
        enumerationValues=[
            dict(value='tulips'),
            dict(value='lilies'),
            dict(value='roses'),
        ]
    )


@pytest.fixture(scope='module')
def order_flowers():
    return {
        'confirmationPrompt': {
            'maxAttempts': 2,
            'messages': [
                {
                    'content': ('Okay, your {FlowerType} will be ready for '
                                'pickup by {PickupTime} on {PickupDate}. Does '
                                'this sound okay?'),
                    'contentType': 'PlainText'
                }
            ]
        },
        'name': 'OrderFlowersTwo',
        'rejectionStatement': {
            'messages': [
                {
                    'content': 'Okay, I will not place your order.',
                    'contentType': 'PlainText'
                }
            ]
        },
        'sampleUtterances': [
            'I would like to pick up flowers',
            'I would like to order some flowers'
        ],
        'slots': [
            {
                'slotType': 'FlowerTypesTwo',
                'name': 'FlowerType',
                'slotConstraint': 'Required',
                'valueElicitationPrompt': {
                    'maxAttempts': 2,
                    'messages': [
                        {
                            'content': ('What type of flowers would you like'
                                        ' to order?'),
                            'contentType': 'PlainText'
                        }
                    ]
                },
                'priority': 1,
                'slotTypeVersion': '$LATEST',
                'sampleUtterances': [
                    'I would like to order {FlowerType}'
                ],
                'description': 'The type of flowers to pick up'
            },
            {
                'slotType': 'AMAZON.DATE',
                'name': 'PickupDate',
                'slotConstraint': 'Required',
                'valueElicitationPrompt': {
                    'maxAttempts': 2,
                    'messages': [
                        {
                            'content': ('What day do you want the {FlowerType}'
                                        ' to be picked up?'),
                            'contentType': 'PlainText'
                        }
                    ]
                },
                'priority': 2,
                'description': 'The date to pick up the flowers'
            },
            {
                'slotType': 'AMAZON.TIME',
                'name': 'PickupTime',
                'slotConstraint': 'Required',
                'valueElicitationPrompt': {
                    'maxAttempts': 2,
                    'messages': [
                        {
                            'content': ('Pick up the {FlowerType} at what time'
                                        ' on {PickupDate}?'),
                            'contentType': 'PlainText'
                        }
                    ]
                },
                'priority': 3,
                'description': 'The time to pick up the flowers'
            }
        ],
        'fulfillmentActivity': {
            'type': 'ReturnIntent'
        },
        'description': 'Delete me'
    }


@pytest.fixture(scope='module')
def order_flowers_bot():
    return {
        'intents': [
            {
                'intentVersion': '$LATEST',
                'intentName': 'OrderFlowersTwo'
            }
        ],
        'name': 'OrderFlowersBot',
        'locale': 'en-US',
        'abortStatement': {
            'messages': [
                {
                    'content': 'Sorry, I\'m not able to assist at this time',
                    'contentType': 'PlainText'
                }
            ]
        },
        'clarificationPrompt': {
            'maxAttempts': 2,
            'messages': [
                {
                    'content': ('I didn\'t understand you, what would you like'
                                ' to do?'),
                    'contentType': 'PlainText'
                }
            ]
        },
        'voiceId': 'Salli',
        'childDirected': False,
        'idleSessionTTLInSeconds': 600,
        'description': 'Delete me'
    }


@pytest.fixture(scope='module')
def lex_models(session):
    return session.client('lex-models')


@pytest.fixture(scope='module')
def lex_runtime(session):
    return session.client('lex-runtime')


@pytest.fixture(scope='module')
def iam(session):
    return session.client('iam')


def test_create_bot(lex_models,
                    lex_runtime,
                    iam,
                    flowers,
                    order_flowers,
                    order_flowers_bot):
    '''Create an Amazon Lex bot
    Test newly created bot

    :testid: lex-f-1-1
    :testid: lex-f-1-2

    '''

    def delete_intent():
        retry(
            lex_models.delete_intent,
            kwargs={'name': order_flowers['name']},
            exceptions=lex_models.exceptions.ConflictException,
            msg='Deleting lex intent {}'.format(order_flowers['name']),
            delay=15,
            max_attempts=10,
        )

    def delete_slot():
        retry(
            lex_models.delete_slot_type,
            kwargs={'name': flowers['name']},
            exceptions=lex_models.exceptions.ConflictException,
            msg='Deleting lex slot type {}'.format(flowers['name']),
            delay=15,
            max_attempts=10,
        )

    # Initial clean up
    try:
        lex_models.delete_bot(name=order_flowers_bot['name'])
    except lex_models.exceptions.NotFoundException:
        pass

    try:
        delete_intent()
    except lex_models.exceptions.NotFoundException:
        pass

    try:
        delete_slot()
    except lex_models.exceptions.NotFoundException:
        pass

    try:
        iam.delete_service_linked_role(
            RoleName='AWSServiceRoleForLexBotsDeleteMe')
    except iam.exceptions.NoSuchEntityException:
        pass

    # Create an IAM client from session and create a service linked role for
    # lex
    slr = None
    try:
        slr = iam.create_service_linked_role(
            AWSServiceName='lex.amazonaws.com',
            Description='Delete me',
            CustomSuffix='DeleteMe',
        )
    except iam.exceptions.InvalidInputException:
        # Sometimes the delete_service_linked_role() fails
        pass

    # Use lex-models client to create a custom slot type using the
    # 'put_slot_type' API and information from example
    try:
        lex_models.put_slot_type(**flowers)
    except lex_models.exceptions.PreconditionFailedException:
        pass

    # Use lex-models client to create an intent using data from the example
    # and the 'put_intent' API call
    try:
        lex_models.put_intent(**order_flowers)
    except lex_models.exceptions.PreconditionFailedException:
        pass

    # Use lex-models client to create a bot using data from the example and the
    # 'put_bot' API call
    try:
        response = lex_models.put_bot(**order_flowers_bot)
    except lex_models.exceptions.PreconditionFailedException:
        pass

    bot_name = response['name']
    response = retry(
        lex_models.get_bot,
        kwargs={'name': bot_name,
                'versionOrAlias': '$LATEST'},
        msg='Checking bot {} readiness status'.format(bot_name),
        until=lambda s: s['status'] == 'READY',
        delay=15,
        max_attempts=10,
    )
    assert response['status'] == 'READY', 'Unexpected failure in bot creation'

    response = lex_runtime.post_text(botName=bot_name,
                                     botAlias='$LATEST',
                                     userId='UserOne',
                                     inputText='i would like to order flowers')
    expected_message = 'What type of flowers would you like to order?'
    assert response['message'] == expected_message, \
        'Did not get expected message back'

    # Clean up
    lex_models.delete_bot(name=bot_name)
    delete_intent()
    delete_slot()

    if slr:
        iam.delete_service_linked_role(RoleName=slr['Role']['RoleName'])
