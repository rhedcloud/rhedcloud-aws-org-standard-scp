"""
===============
test_iam_groups
===============

Verify that customers should not have any IAM permissions for IAM groups.

Plan:

* Create test user
* Attach RHEDcloud restriction policies
* Attempt to create an IAM group as test user

${testcount:7}

"""

import pytest

from aws_test_functions import (
    aws_client,
    debug,
    get_policy_arn,
    make_identifier,
)

GROUP = make_identifier('test_iam_groups')

pytestmark = [
    pytest.mark.raises_access_denied,
]


@aws_client('iam')
def create_group(name=GROUP, *, iam=None):
    debug('Creating group: {}'.format(name))
    iam.create_group(GroupName=name)


@aws_client('iam')
def delete_group(name=GROUP, *, iam=None):
    debug('Deleting group: {}'.format(name))
    iam.delete_group(GroupName=name)


@aws_client('iam')
def update_group(name=GROUP, *, iam=None):
    debug('Updating group: {}'.format(name))
    iam.update_group(GroupName=name, NewGroupName=name)


@pytest.fixture(scope='module')
def iam(session):
    return session.client('iam')


@pytest.fixture(scope='function', autouse=True)
def scp(iam, wait_for_scp, user):
    """Fixture that waits for the SCP to take effect before allowing a test to
    proceed.

    """

    wait_for_scp(iam.update_user, kwargs=dict(UserName=user.user_name, NewUserName=user.user_name), delay=30)


def test_create_iam_group(iam, stack):
    """A customer should NOT be able to create IAM groups.

    :testid: iam-f-1-1

    """

    create_group(iam=iam)

    # if we got here, we need to clean up after ourselves
    stack.callback(
        delete_group,
        iam=iam,
    )


def test_delete_iam_group(iam):
    """A customer should NOT be able to delete IAM groups.

    :testid: iam-f-1-2

    """

    delete_group(iam=iam)

    # if we got here, we need to re-create the group
    create_group(iam=iam)


def test_modify_iam_group(iam):
    """A customer should NOT be able to modify IAM groups.

    :testid: iam-f-1-3

    """

    update_group(iam=iam)


def test_modify_rhedcloud_iam_group(iam):
    """A customer should NOT be able to modify RHEDcloud-created IAM groups.

    :testid: iam-f-1-5

    """

    iam.update_group(GroupName='it-arch', NewGroupName='please-dont')

    # if we got here, we need to undo what we just did
    iam.update_group(GroupName='please-dont', NewGroupName='it-arch')


def test_clone_rhedcloud_iam_group(iam):
    """A customer should NOT be able to clone an RHEDcloud-created group.

    :testid: iam-f-1-6

    """

    iam.create_group(GroupName='it-arch')


def test_detach_rhedcloud_policy_from_iam_group(iam):
    """A customer should NOT be able to detach RHEDcloud restrictions from IAM
    groups.

    :testid: iam-f-1-7

    """

    arn = get_policy_arn('AdministratorAccess')
    iam.detach_group_policy(GroupName='it-arch', PolicyArn=arn)

    # if we got here, we need to undo what we just did
    iam.attach_group_policy(GroupName='it-arch', PolicyArn=arn)


def test_delete_rhedcloud_iam_group(iam):
    """A customer should NOT be able to delete RHEDcloud-created IAM groups.

    :testid: iam-f-1-4

    """

    iam.delete_group(GroupName='it-arch')


def test_attach_group_policy(iam, stack):
    """Verify access to attach IAM policies to IAM groups.

    :param IAM.Client iam:
        Handle to the IAM API.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    policy_arn = get_policy_arn('AdministratorAccess')
    res = iam.attach_group_policy(
        GroupName=GROUP,
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)

    debug("Attached policy {} to group {}".format(policy_arn, GROUP))
    stack.callback(
        ignore_errors(test_detach_group_policy),
        iam,
        GROUP,
        policy_arn,
    )


def test_detach_group_policy(iam):
    """Verify access to detach IAM policies from IAM groups.

    :param IAM.Client iam:
        Handle to the IAM API.

    """

    policy_arn = get_policy_arn('AdministratorAccess')

    debug("Detaching policy {} from group {}".format(policy_arn, GROUP))
    res = iam.detach_group_policy(
        GroupName=GROUP,
        PolicyArn=policy_arn,
    )
    assert has_status(res, 200), unexpected(res)
