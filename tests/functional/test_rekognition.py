"""
================
test_rekognition
================

Verify access to AWS Rekognition.

Plan:

* Create an S3 bucket
* Upload an image to the new S3 bucket
* Attempt to detect text, faces, or labels in the image using Rekognition

${testcount:1}

"""

import pytest

from aws_test_functions import new_bucket

# This service is not fully blocked, but this check is here for posterity
pytestmark = [pytest.mark.scp_check('list_collections')]


@pytest.fixture(scope='module')
def rekognition(session):
    """A shared handle to Rekognition APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Rekognition APIs.

    """

    return session.client('rekognition')


@pytest.fixture(scope='module')
def bucket(session):
    """Create a new S3 bucket to hold an image for Rekognition to process.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :yields:
        An S3 Bucket resource.

    """

    s3 = session.resource('s3')
    with new_bucket('test-rekognition', s3=s3) as b:
        yield b


@pytest.fixture(scope='module')
def image(bucket):
    """Upload an image to an S3 bucket.

    :param S3.Bucket bucket:
        The S3 Bucket resource to use to upload the image.

    :returns:
        A string: the image's key in the bucket.

    """

    key = 'rekognition.jpg'
    bucket.upload_file('./tests/resources/rekognition.jpg', key, ExtraArgs={'ServerSideEncryption': 'AES256'})

    return key


def test_detection(rekognition, bucket, image):
    """Verify the ability for customers to use AWS Rekognition.

    :testid: rekognition-f-1-1

    :param Rekognition.Client rekognition:
        A handle to the Rekognition APIs.
    :param S3.Bucket bucket:
        The S3 Bucket resource to use to upload the image.
    :param str image:
        S3 Path to the image to process.

    """

    res = rekognition.detect_faces(
        Image=dict(
            S3Object=dict(
                Bucket=bucket.name,
                Name=image,
            ),
        ),
    )
    err = 'Unexpected response: {}'.format(res)
    assert 'FaceDetails' in res and len(res['FaceDetails']) == 1, err

    details = res['FaceDetails'][0]
    err = 'Unexpected details: {}'.format(details)
    assert 'BoundingBox' in details, err
    assert 'Landmarks' in details, err
    assert 'Pose' in details, err
    assert 'Quality' in details, err
    assert 'Confidence' in details, err
