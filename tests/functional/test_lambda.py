"""
===========
test_lambda
===========

Verify the ability to use the Lambda Service

Plan:

* Create a lambda function based on the hello-world-python blueprint
* Create an alias for the function
* Manually invoke the lambda function
* Check the execution result of the invocation

${testcount:4}

"""

from aws_test_functions import (
    aws_client,
    ignore_errors,
    retry,
    unexpected,
)


pytest_plugins = [
    'tests.plugins._lambda',
]


lambda_client = aws_client('_lambda', 'lambda')


@lambda_client
def create_function(name, role, zip_bytes, *, _lambda=None):
    """Create a Lambda function.

    :param str name:
        Name of the Lambda function.
    :param IAM.Role role:
        An IAM Role resource to use when executing the Lambda function.
    :param bytestring zip_bytes:
        The binary contents of a Zip file containing the Lambda function.

    """

    kwargs = dict(
        FunctionName=name,
        Runtime='python3.6',
        Role=role.arn,
        Handler="HelloLambda.lambda_handler",
        Code=dict(
            ZipFile=zip_bytes,
        ),
        Publish=True,
    )

    return retry(
        _lambda.create_function,
        kwargs=kwargs,
        msg='Creating Lambda Function: {}'.format(name),
        pred=lambda ex: 'already exist' not in str(ex),
        max_attempts=5,
        show=True,
    )


@lambda_client
def invoke_function(name, *, _lambda=None):
    """Manually invoke the lambda function.

    :param str name:
        Name of the Lambda function.

    """

    kwargs = dict(
        FunctionName=name,
    )

    return retry(
        _lambda.invoke,
        kwargs=kwargs,
        msg='Invoking Lambda Function: {}'.format(name),
        max_attempts=5,
        show=True,
    )


@lambda_client
def delete_function(name, *, _lambda=None):
    """Delete the lambda function

    :param str name:
        Name of the Lambda function.

    """

    print('Deleting lambda function: {}'.format(name))
    return _lambda.delete_function(FunctionName=name)


def test_create_function(
    _lambda, lambda_role, lambda_func_name, lambda_zip_bytes, stack
):
    """Verify access to create a Lambda function.

    :testid: lambda-f-1-1

    :param Lambda.Client _lambda:
        A handle to the Lambda API.
    :param IAM.Role lambda_role:
        An IAM Role resource.
    :param str lambda_func_name:
        The name of the Lambda function.
    :param bytestring lambda_zip_bytes:
        The byte string of a Zip file containing the Lambda function code.
    :param contextlib.ExitStack stack:
        A context manager shared across all Lambda tests.

    """

    res = create_function(
        lambda_func_name,
        lambda_role,
        lambda_zip_bytes,
        _lambda=_lambda,
    )
    assert res['FunctionName'] == lambda_func_name, unexpected(res)
    assert res['Role'] == lambda_role.arn, unexpected(res)
    print('Created Lambda function: {}'.format(lambda_func_name))

    stack.callback(
        ignore_errors(delete_function),
        lambda_func_name,
        _lambda=_lambda,
    )


def test_create_alias(_lambda, lambda_func_name, stack):
    """Verify access to create a Lambda function alias.

    :param Lambda.Client _lambda:
        A handle to the Lambda API.
    :param str lambda_func_name:
        The name of the Lambda function.
    :param contextlib.ExitStack stack:
        A context manager shared across all Lambda tests.

    """

    name = 'HelloLambda'
    res = _lambda.create_alias(
        FunctionName=lambda_func_name,
        Name=name,
        FunctionVersion='1',
    )
    assert 'AliasArn' in res, unexpected(res)
    print('Created function alias: {}'.format(res['AliasArn']))

    stack.callback(
        ignore_errors(_lambda.delete_alias),
        FunctionName=lambda_func_name,
        Name=name,
    )


def test_invoke_function(_lambda, lambda_func_name):
    """Verify access to invoke a Lambda function.

    :testid: lambda-f-1-1

    :param Lambda.Client _lambda:
        A handle to the Lambda API.
    :param str lambda_func_name:
        The name of the Lambda function.

    """

    res = invoke_function(lambda_func_name, _lambda=_lambda)
    assert res['ResponseMetadata']['HTTPStatusCode'] == 200, unexpected(res)


def test_delete_function(_lambda, lambda_func_name):
    """Verify access to delete a Lambda function.

    :testid: lambda-f-1-1

    :param Lambda.Client _lambda:
        A handle to the Lambda API.
    :param str lambda_func_name:
        The name of the Lambda function.

    """

    res = delete_function(lambda_func_name, _lambda=_lambda)
    assert res['ResponseMetadata']['HTTPStatusCode'] == 204, unexpected(res)
