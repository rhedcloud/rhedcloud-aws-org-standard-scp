"""
===========
test_sagemaker
===========
A user should not be able to use the SageMaker service.

Plan:

* Attempt to create a SageMaker Notebook Instance

${testcount:1}

"""

import json

from botocore.exceptions import ClientError
import pytest

from aws_test_functions import new_policy, new_role
from tests.functional.test_elb import NGINX_AMI

TEST_NOTEBOOK_NAME = 'testnotebook4'

pytestmark = [
    pytest.mark.slowtest,

    # This service is blocked in the HIPAA repository
    pytest.mark.scp_check('list_models')
]


@pytest.fixture(scope='module')
def sagemaker(session):
    """A shared handle to SageMaker APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the SageMaker APIs.

    """

    return session.client('sagemaker')


@pytest.fixture(scope='module')
def role(session):
    iam = session.resource('iam')
    iam_client = session.client('iam')
    stmt = [
        {
            "Effect": "Allow",
            "Action": [
                "sagemaker:*",
                "ecr:GetAuthorizationToken",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "ecr:BatchCheckLayerAvailability",
                "cloudwatch:PutMetricData",
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:DescribeLogStreams",
                "logs:PutLogEvents",
                "logs:GetLogEvents",
                "s3:CreateBucket",
                "s3:ListBucket",
                "s3:GetBucketLocation",
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "iam:PassRole"
            ],
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "iam:PassedToService": "sagemaker.amazonaws.com"
                }
            }
        }
    ]
    trust_policy_stmt = [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "sagemaker.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
    doc = json.dumps({
        'Version': '2012-10-17',
        'Statement': trust_policy_stmt,
    }, indent=2)

    with new_policy('sagemaker_service', stmt) as p:
        with new_role('testsagemaker', 'sagemaker', p.arn, iam=iam) as r:
            iam_client.update_assume_role_policy(RoleName=r.role_name,
                                                 PolicyDocument=doc)
            yield r


def test_create_notebook(sagemaker, role):
    '''A user should be able to create a SageMaker Notebook instance

    :testid: sagemaker-f-1-1

    '''
    created = False
    name_kwargs = dict(NotebookInstanceName=TEST_NOTEBOOK_NAME)

    deleted_waiter = sagemaker.get_waiter('notebook_instance_deleted')
    stopped_waiter = sagemaker.get_waiter('notebook_instance_stopped')
    created_waiter = sagemaker.get_waiter('notebook_instance_in_service')

    resp = None
    try:
        resp = sagemaker.describe_notebook_instance(**name_kwargs)
    except ClientError:
        pass

    if resp and resp['NotebookInstanceStatus'] in ('Pending', 'InService',):
        sagemaker.stop_notebook_instance(**name_kwargs)
        stopped_waiter.wait(**name_kwargs)
    if resp:
        sagemaker.delete_notebook_instance(**name_kwargs)
        deleted_waiter.wait(**name_kwargs)

    try:
        create_response = sagemaker.create_notebook_instance(
            NotebookInstanceName=TEST_NOTEBOOK_NAME,
            InstanceType='ml.t2.medium',
            RoleArn=role.arn)
        print(create_response)
        created = True
    finally:
        if created:
            created_waiter.wait(**name_kwargs)
            sagemaker.stop_notebook_instance(**name_kwargs)
            stopped_waiter.wait(**name_kwargs)
            sagemaker.delete_notebook_instance(**name_kwargs)
            deleted_waiter.wait(**name_kwargs)


@pytest.mark.xfail(reason='Unable to create model due to improper syntax')
def test_sagemaker_create_model(sagemaker, user):
    create_response = sagemaker.create_model(
        ModelName="testmodel",
        PrimaryContainer={
            'Image': NGINX_AMI
        },
        ExecutionRoleArn=user.arn,
        Tags=[{
            'Key': 'TestKey',
            'Value': 'TestValue'
        }],
        VpcConfig={
            'SecurityGroupIds': ['123'],
            'Subnets': ['123']
        }
    )


def test_sagemaker_list_models(sagemaker):
    """A user should be able to list Sagemaker models

    :param sagemaker: A handle to the SageMaker APIs.

    :testid: sagemaker-f-3-1

    """

    list_response = sagemaker.list_models()
    print(list_response)
    assert list_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to list models'


def test_sagemaker_list_endpoints(sagemaker):
    """A user should be able to list Sagemaker endpoints

    :param sagemaker: A handle to the SageMaker APIs.

    :testid: sagemaker-f-3-2

    """

    list_response = sagemaker.list_endpoints()
    print(list_response)
    assert list_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to list endpoints'


def test_sagemaker_list_endpoint_configs(sagemaker):
    """A user should be able to list Sagemaker endpoint configs

    :param sagemaker: A handle to the SageMaker APIs.

    :testid: sagemaker-f-3-3

    """

    list_response = sagemaker.list_endpoint_configs()
    print(list_response)
    assert list_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to list endpoint configs'


def test_sagemaker_list_training_jobs(sagemaker):
    """A user should be able to list Sagemaker training jobs

    :param sagemaker: A handle to the SageMaker APIs.

    :testid: sagemaker-f-3-4

    """

    list_response = sagemaker.list_training_jobs()
    print(list_response)
    assert list_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to list training jobs'


@pytest.mark.xfail(reason='No training jobs to stop')
def test_sagemaker_stop_training_job(sagemaker):
    """A user should be able to stop a Sagemaker training job

    :param sagemaker: A handle to the SageMaker APIs.

    :testid: sagemaker-f-4-1

    """

    list_response = sagemaker.list_training_jobs()
    assert len(list_response['TrainingJobSummaries']) > 0, \
        'No training jobs to stop'

    training_job = list_response['TrainingJobSummaries'][0]
    stop_response = sagemaker.stop_training_job(
        training_job['TrainingJobName']
    )

    assert delete_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to delete model'

@pytest.mark.xfail(reason='No models to delete')
def test_sagemaker_delete_model(sagemaker):
    """A user should be able to delete a Sagemaker model

    :param sagemaker: A handle to the SageMaker APIs.

    :testid: sagemaker-f-4-2

    """

    list_response = sagemaker.list_models()
    assert len(list_response['Models']) > 0, 'No models to delete'

    model = list_response['Models'][0]
    delete_response = sagemaker.delete_model(
        ModelName=model['ModelName']
    )

    assert delete_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to delete model'


@pytest.mark.xfail(reason='No endpoints created to delete')
def test_sagemaker_delete_endpoint(sagemaker):
    """A user should be able to delete a Sagemaker endpoint

    :param sagemaker: A handle to the SageMaker APIs.

    :testid: sagemaker-f-4-3

    """

    list_response = sagemaker.list_endpoints()
    assert len(list_response['Endpoints']) > 0, 'No endpoints to delete'

    endpoint = list_response['Endpoints'][0]
    delete_response = sagemaker.delete_endpoint(
        ModelName=endpoint['Endpoint']
    )

    assert delete_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to delete endpoint'


@pytest.mark.xfail(reason='No endpoint configs created to delete')
def test_sagemaker_delete_endpoint_config(sagemaker):
    """A user should be able to delete a Sagemaker endpoint config

    :param sagemaker: A handle to the SageMaker APIs.

    :testid: sagemaker-f-4-4

    """

    list_response = sagemaker.list_endpoint_configs()
    assert len(list_response['EndpointConfigs']) > 0, 'No configs to delete'
    config = list_response['EndpointConfigs'][0]

    delete_response = sagemaker.delete_endpoint(
        ModelName=config['EndpointConfig']
    )

    assert delete_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
        'Unable to delete endpoint config'
