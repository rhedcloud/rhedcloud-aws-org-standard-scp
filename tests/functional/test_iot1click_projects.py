"""
===============
test_iot1click_projects
===============

Verify access to use AWS IOT 1-Click Projects

#FIXME: This test is an incredibly minimal smoke test to ensure
#FIXME: the client is producing acceptable results when blocked 

Plan:

* Attempt to list available projects

${testcount:4}

"""

import pytest

from aws_test_functions import (
    has_status,
    unexpected
)


#This service is fully blocked
pytestmark = [pytest.mark.scp_check('list_projects', client_fixture='iot_projects')]


@pytest.fixture(scope='module')
def iot_projects(session):
    """A shared handle to Iot 1 Click Projects API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Iot 1 Click Projects API.

    """

    return session.client('iot1click-projects')


def test_list_projects(iot_projects):
    """Verify ability list projects

    :testid: 

    :param Iot1ClickProjects.Client iot_projects:
        A handle to the Iot 1 Click Projects API.

    """


    res = iot_projects.list_projects()
    assert has_status(res, 200), unexpected(res)
