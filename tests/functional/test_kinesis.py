"""
============
test_kinesis
============

Verify restrictions to the AWS Kinesis API. This is a fully blocked service,
and as such, aws_blocked_service.py functionality is utilized for testing,
as opposed to marking each test with pytest.mark.raises_access_denied

Plan:

* Create a new test user with the RHEDcloudAdministratorRole
* Attempt to create and delete a delivery stream using Firehose and S3
* Attempt to use Kinesis Analytics to analyze and aggregate the primary stream created

${testcount:6}

"""

import pytest

from aws_test_functions import (
    build_command_runner,
    debug,
    has_status,
    ignore_errors,
    new_bucket,
    new_firehose_delivery_stream,
    new_kinesis_analytics_application,
    new_policy,
    new_role,
    retry,
    unexpected,
)


AGENT_CONFIG = '''{{
 "awsAccessKeyId": "{}",
 "awsSecretAccessKey": "{}",
 "flows": [
  {{
   "filePattern": "/var/log/scp_test_access_*.log",
   "deliveryStream": "{}",
   "initialPosition": "START_OF_FILE",
   "dataProcessingOptions": [
    {{
     "optionName": "LOGTOJSON",
     "logFormat": "COMBINEDAPACHELOG"
    }}
   ]
  }}
 ]
}}'''


KINESIS_START_SCRIPT = '''#!/bin/bash
sleep 2\nsudo echo '{}' > '/etc/aws-kinesis/agent.json'
sleep 2\nsudo chmod ugo+rwx /var/run/aws-kinesis-agent/
sleep 2\nsudo service aws-kinesis-agent start
sleep 2\nexit'''


KINESIS_STOP_SCRIPT = '''#!/bin/bash
sleep 2\nsudo service aws-kinesis-agent stop
sleep 2\nexit'''


@pytest.fixture(scope="module")
def kinesis(session):
    """A shared handle to the Kinesis APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Kinesis APIs.

    """

    return session.client("kinesis")


@pytest.fixture(scope='module')
def kinesis_analytics(session):
    """A shared handle to the Kinesis Analytics APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Kinesis Analytics APIs.

    """

    return session.client('kinesisanalytics')


@pytest.fixture(scope='module')
def firehose(session):
    """A shared handle to the Firehose APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Firehose APIs.

    """

    return session.client('firehose')


@pytest.fixture(scope='module')
def primary_stream_bucket(session):
    """A handle to an S3 Bucket.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A boto3.S3.Bucket owned by the given user

    """

    s3 = session.resource('s3')
    with new_bucket('kinesisprimary', s3=s3, ACL='public-read-write') as b:
        yield b


@pytest.fixture(scope='module')
def analytics_bucket(session):
    """A handle to an S3 Bucket.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A boto3.S3.Bucket owned by the given user

    """

    s3 = session.resource('s3')
    with new_bucket('kinesisanalytics', s3=s3, ACL='public-read-write') as b:
        yield b


@pytest.fixture(scope='module')
def firehose_role(stack):
    """Create a new role with necessary permissions for AWS Firehose Service.

    :param contextlib.ExitStack stack:
        A context manager shared across all Kinesis tests.

    :returns:
        An IAM.Role.

    """

    stmt = [{
        "Effect": "Allow",
        "Action": [
            "firehose:*",
            "logs:PutLogEvents",
            "s3:AbortMultipartUpload",
            "s3:GetBucketLocation",
            "s3:GetObject",
            "s3:ListBucket",
            "s3:ListBucketMultipartUploads",
            "s3:PutObject",
            "s3:PutObjectAcl",
        ],
        "Resource": "*"
    }]

    p = stack.enter_context(new_policy('firehose_trust', stmt))
    r = stack.enter_context(new_role('firehose_service', 'firehose', p.arn))

    yield r


@pytest.fixture(scope='module')
def kanalytics_role(stack):
    """Create a new role with necessary permissions for AWS KinesisAnalytics Service.

    :param contextlib.ExitStack stack:
        A context manager shared across all Kinesis tests.

    :returns:
        An IAM.Role.

    """

    stmt = [{
        "Effect": "Allow",
        "Action": [
            "firehose:*",
        ],
        "Resource": "*"
    }]

    p = stack.enter_context(new_policy('kanalytics_trust', stmt))
    r = stack.enter_context(new_role('kanalytics_service', 'kinesisanalytics', p.arn))

    yield r


# FIXME: See discussion on JIRA Topic AISCP-56 concerning endpoint usage
# @pytest.fixture(scope='module')
# def kinesis_vpc_endpoint_setup(vpc_id):
#     """Create the Kinesis-Streams endpoint for the specified VPC.

#     :param str vpc_id:
#         ID of a VPC.

#     """

#     svc = 'kinesis-streams'
#     created = False

#     try:
#         with in_setup_org(detect=True):
#             create_service_endpoint(svc, vpc_id)
#             endpoint = get_endpoint_for_service(svc, vpc_id)
#             while True:
#                 if endpoint['State'] == 'available':
#                     break
#                 time.sleep(10)
#                 endpoint = get_endpoint_for_service(svc, vpc_id)

#             created = True
#         yield
#     finally:
#         if created:
#             with in_setup_org(detect=True):
#                 endpoint = get_endpoint_for_service(svc, vpc_id)
#                 delete_service_endpoint(endpoint['VpcEndpointId'])
# :param fixture kinesis_vpc_endpoint_setup:
#         A dependency on the fixture that sets up an endpoint for kinesis


def get_bucket_size(bucket):
    """Retrieve the number of objects(keys) in a bucket.

    :param S3.Bucket bucket:
        The Bucket

    :returns:
        An int.

    """

    bucket.load()
    return sum(1 for _ in bucket.objects.all())


@pytest.mark.slowtest  # This test takes ~5 minutes
@pytest.mark.scp_check('list_delivery_streams', client_fixture='firehose') # This service is fully blocked
def test_delivery_stream(session, firehose, firehose_role, kanalytics_role, primary_stream_bucket, analytics_bucket, external_runner):
    """Users should not be able to create or delete a Kinesis Delivery Stream.
       Users should not be able to use kinesis analytics

    :testid: kinesis-f-1-1

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.
    :param Firehose.Client firehose:
        A handle to the Firehose APIs.
    :param boto3.IAM.Role firehose_role:
        A Role permitted to run Firehose tasks.
    :param boto3.IAM.Role firehose_role:
        A Role permitted to run Kinesis Analytics tasks.
    :param S3.Bucket primary_stream_bucket:
        A bucket to output the primary firehose stream data
    :param S3.Bucket analytics_bucket:
        A bucket to output the analytics firehose stream data
    :param callable external_runner:
        A dependency on the fixture that sets up an EC2 instance for running
        commands using SSM.

    """

    script_runner = build_command_runner(external_runner.instance)
    with new_firehose_delivery_stream("TestPrimaryDeliveryStream", firehose_role.arn, primary_stream_bucket.name) as primary_stream_name:
        stream_desc = firehose.describe_delivery_stream(DeliveryStreamName=primary_stream_name)
        primary_stream_arn = stream_desc['DeliveryStreamDescription']['DeliveryStreamARN']
        with new_firehose_delivery_stream("TestAnalyticsStream", firehose_role.arn, analytics_bucket.name) as analysis_stream_name:
            stream_desc = firehose.describe_delivery_stream(DeliveryStreamName=analysis_stream_name)
            analysis_stream_arn = stream_desc['DeliveryStreamDescription']['DeliveryStreamARN']
            with new_kinesis_analytics_application("TestAnalyticsApp", primary_stream_arn, analysis_stream_arn, kanalytics_role.arn, True):
                try:
                    creds = session.get_credentials()
                    frozen_creds = creds.get_frozen_credentials()
                    access_key = frozen_creds.access_key
                    secret_key = frozen_creds.secret_key

                    config = AGENT_CONFIG.format(access_key, secret_key, primary_stream_name)
                    start_script = KINESIS_START_SCRIPT.format(config)
                    script_runner(start_script, timeout=60, delay=5, max_attempts=60)

                    primary_size = retry(
                        get_bucket_size,
                        args=[primary_stream_bucket],
                        msg='Checking delivery stream {} status'.format(primary_stream_name),
                        until=lambda s: s > 0,
                        delay=30,
                        max_attempts=5,
                    )
                    assert primary_size > 0, "Firehose failed to stream primary source data to s3"

                    analytics_size = retry(
                        get_bucket_size,
                        args=[analytics_bucket],
                        msg='Checking delivery stream {} status'.format(analysis_stream_name),
                        until=lambda s: s > 0,
                        max_attempts=30,
                    )
                    assert analytics_size > 0, "Kinesis Analytics failed to stream aggregated data to s3"
                finally:
                    script_runner(KINESIS_STOP_SCRIPT, timeout=60, delay=5, max_attempts=60)


@pytest.fixture
def stream_name():
    return "test-kinesis-stream"


@pytest.mark.from_rs_account
def test_create_stream(kinesis, stream_name, stack):
    """Verify access to create Kinesis streams.

    :param Kinesis.Client kinesis:
        A handle to the Kinesis API.
    :param str stream_name:
        Name of the stream to create.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = kinesis.create_stream(
        StreamName=stream_name,
        ShardCount=1
    )
    assert has_status(res, 200), unexpected(res)

    debug("Created Stream: {}".format(stream_name))

    stack.callback(
        ignore_errors(test_delete_stream),
        kinesis,
        stream_name,
    )


@pytest.mark.from_rs_account
def test_describe_stream(kinesis, stream_name):
    """Verify access to describe Kinesis streams.

    :param Kinesis.Client kinesis:
        A handle to the Kinesis API.
    :param str stream_name:
        Name of the stream to describe.

    """

    res = kinesis.describe_stream(
        StreamName=stream_name,
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.from_rs_account
def test_delete_stream(kinesis, stream_name):
    """Verify access to delete Kinesis streams.

    :param Kinesis.Client kinesis:
        A handle to the Kinesis API.
    :param str stream_name:
        Name of the stream to delete.

    """

    res = kinesis.delete_stream(
        StreamName=stream_name,
    )
    assert has_status(res, 200), unexpected(res)
