"""
=================
test_datapipeline
=================

Verify access to AWS DataPipeline functionality.

${testcount:9}

* Create a new test user with the RHEDcloudAdministratorRole.
* Attempt to create and delete a pipeline
* Attempt to define a pipeline with a pipeline definition
* Attempt to activate a pipeline
* Attempt to list the pipelines
* Attempt to validate pipeline object definitions
* Attempt to query pipeline objects
* Attempt to evaluate an expression that uses pipeline objects

"""

import pytest

from aws_test_functions import (
    create_pipeline,
    delete_pipeline,
    has_status,
    ignore_errors,
    make_identifier,
    new_data_pipeline,
    retry,
    unexpected,
)

# This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('list_pipelines')]


@pytest.fixture(scope='module')
def datapipeline(session):
    """A shared handle to DataPipeline APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the DataPipeline APIs.

    """

    return session.client('datapipeline')


@pytest.fixture(scope='module', autouse=True)
def pipeline_setup_id(maint_session):
    """Returns a pipeline ID properly configured in the setup organization

    :param DataPipeline.Client datapipeline:
        A handle to the DataPipeline APIs.

    :returns:
        A string.

    """

    datapipeline = maint_session.client('datapipeline')
    with new_data_pipeline("SetupPipe", datapipeline=datapipeline) as pipe_id:
        assert pipe_id is not None, "Failed to create Setup Pipeline"
        yield pipe_id


@pytest.dict_fixture(with_assertion=False)
def shared_vars(pipeline_setup_id=None):
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'pipeline_name': make_identifier('test-datapipeline'),
        'pipeline_id': pipeline_setup_id or 'invalid',
        'pipeline_objects': [{
            'id': 'TestPLObjID',
            'name': 'TestPLObjName',
            'fields': [{
                'key': 'workerGroup',
                'stringValue': 'TestPlObjVal'
            }]
        }],
    }


def test_create_pipeline(datapipeline, pipeline_name, stack, shared_vars):
    """Verify access to create DataPipelines

    :testid: datapipeline-f-1-1

    :param DataPipeline.Client datapipeline:
        A handle to the DataPipeline APIs.
    :param str pipeline_name:
        Name of the pipeline to create.
    :param contextlib.ExitStack stack:
        A context manager shared across all Data Pipeline tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    pipeline_id = create_pipeline(pipeline_name, datapipeline=datapipeline)
    assert pipeline_id is not None, "Failed to create Test Pipeline"

    shared_vars['pipeline_id'] = pipeline_id

    stack.callback(
        ignore_errors(delete_pipeline),
        pipeline_id,
        datapipeline=datapipeline,
    )


def test_define_pipeline(datapipeline, pipeline_id, pipeline_objects):
    """Verify access to define pipelines.

    :testid: datapipeline-f-1-2

    :param DataPipeline.Client datapipeline:
        A handle to the DataPipeline APIs.
    :param str pipeline_id:
        A Pipeline ID.
    :param list(dict) pipeline_objects:
        Pipeline objects.

    """

    res = datapipeline.put_pipeline_definition(
        pipelineId=pipeline_id,
        pipelineObjects=pipeline_objects,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'errored' in res and res['errored'] is not True, unexpected(res)


def test_activate_pipeline(datapipeline, pipeline_id):
    """Verify access to activate pipelines.

    :testid: datapipeline-f-1-3

    :param DataPipeline.Client datapipeline:
        A handle to the DataPipeline APIs.
    :param str pipeline_id:
        An inactive Pipeline ID.

    """

    res = datapipeline.activate_pipeline(pipelineId=pipeline_id)
    assert has_status(res, 200), unexpected(res)


def test_list_pipelines(datapipeline, pipeline_id):
    """Verify access to list pipelines.

    :testid: datapipeline-f-1-4

    :param DataPipeline.Client datapipeline:
        A handle to the DataPipeline APIs.
    :param str pipeline_id:
        A Pipeline ID.

    """

    res = datapipeline.list_pipelines()
    assert has_status(res, 200), unexpected(res)

    pipe_id_list = res['pipelineIdList']
    assert len(pipe_id_list) > 0, 'Pipeline ID List Empty'

    found = False
    for pipeline in pipe_id_list:
        if pipeline_id == pipeline['id']:
            found = True
            break

    assert found, 'Test Pipeline Not Found'


def test_validate_pipeline_definition(datapipeline, pipeline_id, pipeline_objects):
    """Verify access to validate pipelines definitions.

    :testid: datapipeline-f-2-1

    :param DataPipeline.Client datapipeline:
        A handle to the DataPipeline APIs.
    :param str pipeline_id:
        A Pipeline ID.
    :param list(dict) pipeline_objects:
        Pipeline objects.

    """

    res = datapipeline.validate_pipeline_definition(
        pipelineId=pipeline_id,
        pipelineObjects=pipeline_objects,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'errored' in res and not res['errored'], unexpected(res)


def test_validate_pipeline_definition_invalid(datapipeline, pipeline_id, pipeline_objects):
    """Verify pipelines definition validation will fail with invalid objects.

    :testid: datapipeline-f-2-2

    :param DataPipeline.Client datapipeline:
        A handle to the DataPipeline APIs.
    :param str pipeline_id:
        A Pipeline ID.
    :param list(dict) pipeline_objects:
        Pipeline objects.

    """

    spec = pipeline_objects[0].copy()
    spec['id'] = 'invalid!@#$!@#$'

    res = datapipeline.validate_pipeline_definition(
        pipelineId=pipeline_id,
        pipelineObjects=[spec],
    )
    assert has_status(res, 200), unexpected(res)
    assert 'errored' in res and res['errored'], unexpected(res)


def test_query_objects(datapipeline, pipeline_id):
    """Verify access to query pipeline objects.

    :testid: datapipeline-f-3-1

    :param DataPipeline.Client datapipeline:
        A handle to the DataPipeline APIs.
    :param str pipeline_id:
        A Pipeline ID.

    """

    res = retry(
        datapipeline.query_objects,
        kwargs=dict(
            pipelineId=pipeline_id,
            sphere='COMPONENT',
            query=dict(
                selectors=[dict(
                    fieldName='name',
                    operator={
                        'type': 'EQ',
                        'values': ['TestPLObjName'],
                    },
                )],
            ),
        ),
        msg="Querying objects",
        until=lambda res: len(res.get("ids", [])) > 0,
        max_attempts=3,
    )

    assert has_status(res, 200), unexpected(res)
    assert 'ids' in res, unexpected(res)
    assert len(res['ids']) > 0, unexpected(res)


def test_evaluate_expression(datapipeline, pipeline_id):
    """Verify access to evaluate expressions using pipeline objects.

    :testid: datapipeline-f-4-1

    :param DataPipeline.Client datapipeline:
        A handle to the DataPipeline APIs.
    :param str pipeline_id:
        A Pipeline ID.

    """

    expr_in = 'Test expression with #{workerGroup} in it'
    res = datapipeline.evaluate_expression(
        pipelineId=pipeline_id,
        objectId='TestPLObjID',
        expression=expr_in,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'evaluatedExpression' in res, unexpected(res)

    expr_out = res['evaluatedExpression']
    print('Input expression:', expr_in)
    print('Output expression:', expr_out)
    assert expr_out.endswith('TestPlObjVal in it'), unexpected(res)


def test_delete_pipeline(datapipeline, pipeline_id):
    """Verify access to delete pipelines.

    :testid: datapipeline-f-1-5

    :param DataPipeline.Client datapipeline:
        A handle to the DataPipeline APIs.
    :param str pipeline_id:
        A currently deployed pipeline id

    """

    delete_pipeline(pipeline_id, datapipeline=datapipeline)
