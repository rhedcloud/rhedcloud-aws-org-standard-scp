"""
========
test_eks
========

Verify access to use AWS Elastic Container Service for Kubernetes(EKS)

#FIXME: This test is an incredibly minimal smoke test to ensure
#FIXME: the client is producing acceptable results when blocked

Plan:

* Attempt to list available EKS Clusters

${testcount:2}

"""

import pytest

from aws_test_functions import (
    has_status,
    ignore_errors,
    in_setup_org,
    make_identifier,
    new_policy,
    new_role,
    retry,
    unexpected,
)


pytestmark = [
    # This service is fully blocked
    pytest.mark.scp_check('list_clusters'),

    pytest.mark.xfail(reason="Multiple Subnets in multiple AZs are required in cluster creation")
]

# Create a reusable functor to move the AWS account to the SETUP_ORG before
# attempting to invoke the requested function. This is necessary to ensure that
# we make the best effort to remove resources created during testing (since we
# should not be able to delete with the SCP in effect in the TEST_ORG).
cleaner = in_setup_org('EKS cleanup', one_way=True)


@pytest.fixture(scope='module')
def eks(session):
    """A shared handle to EKS API.


    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the EKS API.

    """

    return session.client('eks')


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'cluster_name': 'not-created',
    }


@pytest.fixture(scope='module')
def role(stack):
    """Create a role for DAX to create clusters

    :param contextlib.ExitStack stack:
        A context manager shared across all DAX tests.

    :yields:
        An IAM Role resource.

    """

    stmt = [dict(
        Effect='Allow',
        Action=[
            "autoscaling:DescribeAutoScalingGroups",
            "autoscaling:UpdateAutoScalingGroup",
            "ec2:AttachNetworkInterface",
            "ec2:AttachVolume",
            "ec2:AuthorizeSecurityGroupIngress",
            "ec2:CreateNetworkInterface",
            "ec2:CreateNetworkInterfacePermission",
            "ec2:CreateRoute",
            "ec2:CreateSecurityGroup",
            "ec2:CreateTags",
            "ec2:CreateVolume",
            "ec2:DeleteNetworkInterface",
            "ec2:DeleteRoute",
            "ec2:DeleteSecurityGroup",
            "ec2:DeleteVolume",
            "ec2:Describe*",
            "ec2:DetachNetworkInterface",
            "ec2:DeleteNetworkInterfacePermission",
            "ec2:DetachVolume",
            "ec2:ModifyInstanceAttribute",
            "ec2:ModifyVolume",
            "ec2:RevokeSecurityGroupIngress",
            "elasticloadbalancing:AddTags",
            "elasticloadbalancing:ApplySecurityGroupsToLoadBalancer",
            "elasticloadbalancing:AttachLoadBalancerToSubnets",
            "elasticloadbalancing:ConfigureHealthCheck",
            "elasticloadbalancing:CreateListener",
            "elasticloadbalancing:CreateLoadBalancer",
            "elasticloadbalancing:CreateLoadBalancerListeners",
            "elasticloadbalancing:CreateLoadBalancerPolicy",
            "elasticloadbalancing:CreateTargetGroup",
            "elasticloadbalancing:DeleteListener",
            "elasticloadbalancing:DeleteLoadBalancer",
            "elasticloadbalancing:DeleteLoadBalancerListeners",
            "elasticloadbalancing:DeleteTargetGroup",
            "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
            "elasticloadbalancing:DeregisterTargets",
            "elasticloadbalancing:Describe*",
            "elasticloadbalancing:DetachLoadBalancerFromSubnets",
            "elasticloadbalancing:ModifyListener",
            "elasticloadbalancing:ModifyLoadBalancerAttributes",
            "elasticloadbalancing:ModifyTargetGroup",
            "elasticloadbalancing:ModifyTargetGroupAttributes",
            "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
            "elasticloadbalancing:RegisterTargets",
            "elasticloadbalancing:SetLoadBalancerPoliciesForBackendServer",
            "elasticloadbalancing:SetLoadBalancerPoliciesOfListener",
            "kms:DescribeKey",
            "route53:ChangeResourceRecordSets",
            "route53:CreateHealthCheck",
            "route53:DeleteHealthCheck",
            "route53:Get*",
            "route53:List*",
            "route53:UpdateHealthCheck",
            "servicediscovery:DeregisterInstance",
            "servicediscovery:Get*",
            "servicediscovery:List*",
            "servicediscovery:RegisterInstance",
            "servicediscovery:UpdateInstanceCustomHealthStatus"
        ],
        Resource=['*'],
    )]

    p = stack.enter_context(new_policy('test_eks', stmt))
    r = stack.enter_context(new_role(
        'test_eks', 'eks', p.arn,
        'arn:aws:iam::aws:policy/AmazonEKSClusterPolicy',
        'arn:aws:iam::aws:policy/AmazonEKSServicePolicy',
    ))

    yield r


def test_create_cluster(eks, role, internal_subnet_id, shared_vars, stack):
    """Verify ability to create an EKS cluster

    :testid:

    :param EKS.Client eks:
        A handle to the EKS API.
    :param IAM.Role role:
        A role configured for proper usage with EKS
    :param str internal_subnet_id:
        The ID of the subnet to use for the cluster
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param contextlib.ExitStack stack:
        A context manager shared across all EKS tests

    """

    name = make_identifier('EKSCluster')
    res = eks.create_cluster(name=name,
                             resourcesVpcConfig=dict(subnetIds=[internal_subnet_id]),
                             roleArn=role.arn)
    assert has_status(res, 200), unexpected(res)

    shared_vars['cluster_name'] = name
    stack.callback(
        cleaner(ignore_errors(eks.delete_cluster)),
        name=name
    )


def test_list_clusters(eks, cluster_name):
    """Verify ability list EKS clusters

    :testid:

    :param EKS.Client eks:
        A handle to the EKS API.

    """

    res = eks.list_clusters()
    assert has_status(res, 200), unexpected(res)
    if cluster_name != 'not-created':
        assert cluster_name in res['clusters'], 'Failed to find cluster {} in list_clusters'.format(cluster_name)


def test_describe_cluster(eks, cluster_name):
    """Verify ability to describe an EKS cluster

    :testid:

    :param EKS.Client eks:
        A handle to the EKS API.
    :param str cluster_name:
        The name of a created or creating EKS cluster

    """

    res = retry(
        eks.describe_cluster,
        kwargs=dict(name=cluster_name),
        msg='Waiting for EKS cluster {} to finalize creation'.format(cluster_name),
        until=lambda r: r['cluster']['status'] != 'CREATING',
        pred=lambda ex: False,
        delay=60,
        max_attempts=20
    )
    assert has_status(res, 200), unexpected(res)
    assert res['cluster']['status'] == 'ACTIVE', 'Failed to create cluster: {}'.format(res)


def test_delete_cluster(eks, cluster_name, shared_vars):
    """Verify ability to delete a EKS cluster

    :testid:

    :param EKS.Client eks:
        A handle to the EKS API.
    :param str cluster_name:
        The name of a created and available EKS cluster
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    def _test(res):
        return has_status(res, 200) and (
            'cluster' not in res or
            'status' not in res['cluster'] or
            res['cluster']['status'] != 'DELETING'
        )

    # Delete the cluster and ensure that describe_cluster eventually
    # throws a ClusterNotFoundFault or no clusters are listed or
    # no status is listed for the cluster.
    res = eks.delete_cluster(name=cluster_name)
    assert has_status(res, 200), unexpected(res)

    deleted = False
    try:
        res = retry(
            eks.describe_cluster,
            kwargs=dict(name=[cluster_name]),
            msg='Waiting for EKS cluster {} to finalize deletion'.format(cluster_name),
            until=lambda r: _test(r),
            pred=lambda ex: False,
            delay=60,
            max_attempts=20,
        )

        if _test(res):
            deleted = True
    except Exception:
        deleted = True

    assert deleted, 'Failed to delete EKS Cluster {}'.format(cluster_name)
    shared_vars['cluster_name'] = 'not-created'
