"""
===============
test_s3_objects
===============

"Type": "functional",
"Name": "test_s3_objects",
"Description": "Verify that the BlockRHEDcloudBucketsObjects statement within the RHEDcloudAdministratorRolePolicy policy is performing as expected against the specified resources.",
"Plan": "Attach *:* and this policy, test each statement and expect AccessDenied errors for each blocked call.",
"ExpectedResult": "Failure"

This test verifies that the blockRHEDcloudBucketsObjects statement is
disallowing users from performing the following actions on the
appropriate resources and allowing on other resources:

"s3:DeleteBucket",
"s3:DeleteBucketPolicy",
"s3:DeleteBucketWebsite",
"s3:PutBucketAcl",
"s3:PutBucketCORS",
"s3:PutBucketLogging",
"s3:PutBucketNotification",
"s3:PutBucketPolicy",
"s3:PutBucketRequestPayment",
"s3:PutBucketTagging",
"s3:PutBucketVersioning",
"s3:PutBucketWebsite",
"s3:PutInventoryConfiguration",
"s3:PutLifecycleConfiguration",
"s3:PutMetricsConfiguration",
"s3:PutReplicationConfiguration"
"s3:DeleteObject",
"s3:DeleteObjectTagging",
"s3:DeleteObjectVersion",
"s3:DeleteObjectVersionTagging",
"s3:PutObject",
"s3:PutObjectTagging",
"s3:PutObjectVersionAcl",
"s3:PutObjectVersionTagging",
"s3:RestoreObject",
"s3:PutObjectAcl"

${testcount:58}

"""

import json
import os

from botocore.exceptions import ClientError
import boto3
import pytest

from aws_test_functions import (
    empty_bucket,
    get_account_number,
    has_status,
    ignore_errors,
    unexpected,
)

BUCKET_NAME = "testbucket-{}".format(get_account_number())
CLOUDTRAIL_BUCKET_NAME = os.environ["CLOUDTRAIL_BUCKET_NAME"]


def is_cloudtrail(pyfuncitem):
    """Determine whether a test is working with the CloudTrail bucket.

    :param pytest.Function pyfuncitem:
        The internal representation of a test function, from which the test
        bucket name will be determined.

        Alternatively, a string may be passed, in which case it is treated as
        the bucket name directly.

    :returns:
        A boolean.

    """

    try:
        bucket_name = pyfuncitem.funcargs["bucket_name"]
    except AttributeError:
        bucket_name = pyfuncitem

    return bucket_name == CLOUDTRAIL_BUCKET_NAME


def not_cloudtrail(pyfuncitem):
    """Determine whether we are working with the test bucket.

    :param pytest.Function pyfuncitem:
        The internal representation of a test function, from which the test
        bucket name will be determined.

        Alternatively, a string may be passed, in which case it is treated as
        the bucket name directly.

    :returns:
        A boolean.

    """

    return not is_cloudtrail(pyfuncitem)


# This allows us to run all test functions in this module twice: once with an
# empty test bucket and a second time using the CloudTrail bucket created by
# CloudFormation. When working with the CloudTrail bucket, most tests are
# expected to raise AccessDenied errors.
pytestmark = [
    pytest.mark.parametrize("bucket_name", [
        pytest.param(
            BUCKET_NAME,
            marks=pytest.mark.permitted,
        ),
        pytest.param(
            CLOUDTRAIL_BUCKET_NAME,
            marks=pytest.mark.raises_access_denied(
                needle=r'.*(AccessDenied|Unauthorized|Forbidden|NoSuchKey).*',
                when=is_cloudtrail,
            ),
        ),
    ]),
]


# simplify access to common markers
bypass_access_denied = pytest.mark.bypass("raises_access_denied")
raises_invalid = pytest.mark.raises(ClientError, match=r"Invalid(Argument|Request)", when=not_cloudtrail)
raises_both_invalid = pytest.mark.raises(ClientError, match=r"Invalid(Argument|Request)")


@pytest.fixture(scope="module")
def s3(session):
    """Return a handle to the S3 API using a temporary user's session.

    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    :yields:
        An S3.Client object.

    """

    yield session.client("s3")


@pytest.fixture(scope="module")
def bucket_acl(s3):
    """Create a sample bucket ACL.

    :param S3.Client s3:
        A handle to the S3 API.

    :returns:
        A dictionary.

    """

    res = s3.list_buckets()
    owner_id = res["Owner"]["ID"]
    display_name = res["Owner"]["DisplayName"]

    return {
        "Grants": [{
            "Grantee": {
                "ID": owner_id,
                "DisplayName": display_name,
                "Type": "CanonicalUser",
            },
            "Permission": "FULL_CONTROL",
        }],
        "Owner": {
            "ID": owner_id,
            "DisplayName": display_name,
        },
    }


@pytest.fixture
def object_acl(s3):
    """Create a sample object ACL.

    :param S3.Client s3:
        A handle to the S3 API.

    :returns:
        A dictionary.

    """

    res = s3.list_buckets()
    owner_id = res["Owner"]["ID"]
    display_name = res["Owner"]["DisplayName"]

    return {
        'Owner': {
            'DisplayName': display_name,
            'ID': owner_id,
        },
        'Grants': [{
            'Grantee': {
                'DisplayName': display_name,
                'ID': owner_id,
                'Type': 'CanonicalUser',
            },
            'Permission': 'FULL_CONTROL',
        }],
    }


@bypass_access_denied
def test_create_bucket(s3, bucket_name, stack):
    """Verify access to create a new S3 bucket.

    We do not attempt to create the S3 bucket when working with the CloudTrail
    bucket, as it's expected to already exist from CloudFormation. As such, we
    must bypass the "AccessDenied" expectation in that case.

    This does also clean up after itself when it's able to create an S3 bucket.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    if is_cloudtrail(bucket_name):
        return

    res = s3.create_bucket(
        Bucket=bucket_name,
        ACL="private",
    )
    assert has_status(res, 200), unexpected(res)

    stack.callback(
        ignore_errors(test_delete_bucket),
        s3,
        bucket_name,
    )

    stack.callback(
        empty_bucket,
        bucket_name,
        s3=s3,
    )


def test_put_bucket_acl(s3, bucket_name, bucket_acl):
    """Verify access to set a bucket ACL.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.
    :param dict bucket_acl:
        A sample bucket ACL.

    """

    res = s3.put_bucket_acl(
        Bucket=bucket_name,
        AccessControlPolicy=bucket_acl,
    )
    assert has_status(res, 200), unexpected(res)


def test_put_bucket_cors(s3, bucket_name, stack):
    """Verify access to set bucket CORS rules.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.
    :param contextlib.ExitStack stack:
        An existing context manager that will help clean up after ourselves.

    """

    res = s3.put_bucket_cors(
        Bucket=bucket_name,
        CORSConfiguration={
            "CORSRules": [
                {
                    "AllowedHeaders": ["Authorization"],
                    "AllowedMethods": ["GET"],
                    "AllowedOrigins": ["*"],
                    "MaxAgeSeconds": 12,
                }
            ]
        },
    )
    assert has_status(res, 200), unexpected(res)

    stack.callback(
        ignore_errors(s3.delete_bucket_cors),
        Bucket=bucket_name,
    )


def test_put_bucket_logging(s3, bucket_name):
    """Verify access to configure bucket logging.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_bucket_logging(
        Bucket=bucket_name,
        BucketLoggingStatus={},
    )
    assert has_status(res, 200), unexpected(res)


def test_put_bucket_notification_configurtion(s3, bucket_name):
    """Verify access to configure bucket notifications.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_bucket_notification_configuration(
        Bucket=bucket_name,
        NotificationConfiguration={},
    )
    assert has_status(res, 200), unexpected(res)


def test_put_bucket_policy(s3, bucket_name):
    """Verify access to configure bucket policies.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_bucket_policy(
        Bucket=bucket_name,
        Policy=json.dumps({
            "Version": "2012-10-17",
            "Statement": [{
                "Sid": "AWSCloudTrailAclCheck",
                "Effect": "Allow",
                "Principal": {"Service": "cloudtrail.amazonaws.com"},
                "Action": "s3:*",
                "Resource": "arn:aws:s3:::{}".format(bucket_name),
            }],
        }),
    )
    assert has_status(res, 204), unexpected(res)


def test_put_bucket_request_payment(s3, bucket_name):
    """Verify access to configure bucket request payments.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_bucket_request_payment(
        Bucket=bucket_name,
        RequestPaymentConfiguration={
            "Payer": "BucketOwner",
        },
    )
    assert has_status(res, 200), unexpected(res)


def test_put_bucket_tagging(s3, bucket_name):
    """Verify access to configure bucket tagging.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_bucket_tagging(
        Bucket=bucket_name,
        Tagging={
            "TagSet": [{
                "Key": "string",
                "Value": "string",
            }],
        },
    )
    assert has_status(res, 204), unexpected(res)


def test_put_bucket_versioning(s3, bucket_name):
    """Verify access to configure bucket versioning.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_bucket_versioning(
        Bucket=bucket_name,
        VersioningConfiguration={
            "Status": "Suspended",
        },
    )
    assert has_status(res, 200), unexpected(res)


def test_put_bucket_website(s3, bucket_name):
    """Verify access to configure bucket websites.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_bucket_website(
        Bucket=bucket_name,
        WebsiteConfiguration={
            "IndexDocument": {
                "Suffix": "index.html",
            },
        },
    )
    assert has_status(res, 200), unexpected(res)


def test_put_bucket_inventory_configuration(s3, bucket_name):
    """Verify access to configure bucket inventory.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_bucket_inventory_configuration(
        Bucket=bucket_name,
        Id="test",
        InventoryConfiguration={
            "Schedule": {
                "Frequency": "Weekly",
            },
            "IsEnabled": False,
            "Destination": {
                "S3BucketDestination": {
                    "Bucket": "arn:aws:s3:::{}".format(bucket_name),
                    "Format": "CSV",
                },
            },
            "IncludedObjectVersions": "Current",
            "Id": "test",
        },
    )
    assert has_status(res, 204), unexpected(res)


def test_put_bucket_lifecycle_configuration(s3, bucket_name):
    """Verify access to configure bucket lifecycle.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_bucket_lifecycle_configuration(
        Bucket=bucket_name,
        LifecycleConfiguration={
            "Rules": [{
                "Filter": {
                    "Prefix": "test",
                },
                "Status": "Disabled",
                "Expiration": {
                    "ExpiredObjectDeleteMarker": False,
                },
                "ID": "test",
            }],
        },
    )
    assert has_status(res, 200), unexpected(res)


def test_put_bucket_metrics_configuration(s3, bucket_name):
    """Verify access to configure bucket metrics.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_bucket_metrics_configuration(
        Bucket=bucket_name,
        Id="test",
        MetricsConfiguration={
            "Filter": {
                "Prefix": "test",
            },
            "Id": "test",
        },
    )
    assert has_status(res, 204), unexpected(res)


@raises_invalid
def test_put_bucket_replication(s3, bucket_name):
    """Verify access to configure bucket replication.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_bucket_replication(
        Bucket=bucket_name,
        ReplicationConfiguration={
            "Role": "arn:aws:iam::{}:role/test".format(get_account_number()),
            "Rules": [{
                "ID": "mytestid",
                "Prefix": "",
                "Status": "Disabled",
                "Destination": {
                    "Bucket": "arn:aws:s3:::{}".format(bucket_name),
                    "StorageClass": "STANDARD",
                },
            }],
        },
    )

    # we shouldn't actually get here
    assert has_status(res, 200), unexpected(res)


def test_put_object(s3, bucket_name):
    """Verify access to upload objects into bucket.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_object(
        Bucket=bucket_name,
        Body=b"testing",
        Key="test.txt",
        ServerSideEncryption="AES256",
        ACL="authenticated-read",
    )
    assert has_status(res, 200), unexpected(res)


def test_put_object_tagging(s3, bucket_name):
    """Verify access to set object tags.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_object_tagging(
        Bucket=bucket_name,
        Key="test.txt",
        Tagging={
            "TagSet": [{
                "Key": "string",
                "Value": "string",
            }],
        },
    )
    assert has_status(res, 200), unexpected(res)


@bypass_access_denied
@raises_both_invalid
def test_put_object_version_tagging(s3, bucket_name):
    """Verify access to set object tags with object version.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_object_tagging(
        Bucket=bucket_name,
        Key="test.txt",
        VersionId="randomversionid",
        Tagging={
            "TagSet": [{
                "Key": "string",
                "Value": "string",
            }],
        },
    )

    # we shouldn't actually get here
    assert has_status(res, 200), unexpected(res)


def test_put_object_acl(s3, bucket_name, object_acl):
    """Verify access to set object ACL.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_object_acl(
        Bucket=bucket_name,
        Key="test.txt",
        AccessControlPolicy=object_acl,
    )
    assert has_status(res, 200), unexpected(res)


@bypass_access_denied
@raises_both_invalid
def test_put_object_version_acl(s3, bucket_name, object_acl):
    """Verify access to set object ACL with object version.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.put_object_acl(
        Bucket=bucket_name,
        Key="test.txt",
        VersionId="randomversionid",
        AccessControlPolicy=object_acl,
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_object_tagging(s3, bucket_name):
    """Verify access to delete object tags.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.delete_object_tagging(
        Bucket=bucket_name,
        Key="test.txt",
    )
    assert has_status(res, 204), unexpected(res)


@bypass_access_denied
@raises_both_invalid
def test_delete_object_version_tagging(s3, bucket_name):
    """Verify access to delete object tags with object version.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.delete_object_tagging(
        Bucket=bucket_name,
        Key="test.txt",
        VersionId="randomversionid",
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_object(s3, bucket_name):
    """Verify access to delete object.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.delete_object(
        Bucket=bucket_name,
        Key="test.txt",
    )
    assert has_status(res, 204), unexpected(res)


@bypass_access_denied
@raises_both_invalid
def test_delete_object_version(s3, bucket_name):
    """Verify access to delete object with object version.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.delete_object(
        Bucket=bucket_name,
        Key="test.txt",
        VersionId="randomversionid",
    )
    assert has_status(res, 200), unexpected(res)


def test_delete_bucket_website(s3, bucket_name):
    """Verify access to delete bucket websites.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.delete_bucket_website(
        Bucket=bucket_name,
    )
    assert has_status(res, 204), unexpected(res)


def test_delete_bucket_policy(s3, bucket_name):
    """Verify access to delete bucket policies.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.delete_bucket_policy(
        Bucket=bucket_name,
    )
    assert has_status(res, 204), unexpected(res)


@pytest.mark.raises(ClientError, match=r"NoSuchKey", when=not_cloudtrail)
def test_restore_object(s3, bucket_name):
    """Verify access to restore objects.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.restore_object(
        Bucket=bucket_name,
        Key="thisismyrandomkey",
    )
    assert has_status(res, 200), unexpected(res)


@pytest.mark.raises(ClientError, match=r"BucketNotEmpty", when=not_cloudtrail)
def test_delete_bucket(s3, bucket_name):
    """Verify access to delete buckets.

    :param S3.Client s3:
        A handle to the S3 API.
    :param str bucket_name:
        Name of the S3 bucket to create.

    """

    res = s3.delete_bucket(Bucket=bucket_name)
    assert has_status(res, 204), unexpected(res)
