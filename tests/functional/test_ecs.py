"""
========
test_ecs
========

Verify the ability to create and use the Elastic Container Service(ECS)

Plan:

* Create a new test user with the RHEDcloudAdministratorRole.
* Create and delete a new cluster.
* Create and delete a new task definition.
* Create and delete a new service definition.

${testcount:6}

"""

import time

import pytest

from aws_test_functions import (
    aws_client,
    catch,
    new_ecs_cluster,
    new_ecs_service,
    new_ecs_task_definition,
)

pytest_plugins = ["tests.plugins.ecs"]


ECS_COMMAND = '''/bin/sh -c "echo '<html> <head>
 <title>Amazon ECS Sample App</title>
 <style>body {margin-top: 40px; background-color: #333;} </style>
 </head><body>
 <div style=color:white;text-align:center>
 <h1>Amazon ECS Sample App</h1>
 <h2>Congratulations!</h2>
 <p>Your application is now running on a container in Amazon ECS.</p> </div></body></html>' >
 /usr/local/apache2/htdocs/index.html && httpd-foreground"'''.replace(
    "\n", ""
)


@catch(Exception, msg="Failed to delete service: {ex}")
@aws_client("ecs")
def cleanup_services(cluster_name, *, ecs=None):
    """Attempt to remove any unused services."""

    res = ecs.list_services(cluster=cluster_name)
    res = ecs.describe_services(cluster=cluster_name, services=res["serviceArns"])
    for service in res["services"]:
        name = service["Name"]
        if service["desiredCount"] > 0:
            network_config = service["networkConfiguration"]
            ecs.update_service(
                cluster=cluster_name,
                service=name,
                desiredCount=0,
                networkConfiguration=network_config,
            )

        print("Deleting service: {}".format(name))
        ecs.delete_service(cluster=cluster_name, service=name)


@catch(Exception, msg="Failed to delete cluster: {ex}")
@aws_client("ecs")
def cleanup_clusters(*, ecs=None):
    """Attempt to remove any unused clusters."""

    res = ecs.describe_clusters()
    for cluster in res["clusters"]:
        name = cluster["clusterName"]
        cleanup_services(name)
        print("Deleting cluster: {}".format(name))
        ecs.delete_cluster(cluster=name)


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    return {
        "cluster_name": "",
        "task_def_name": "",
    }


def test_create_cluster(ecs, stack, shared_vars):
    """Verify access to create an ECS cluster.

    :testid: ecs-f-1-1

    :param ECS.Client ecs:
        A handle to the ECS API.
    :param contextlib.ExitStack stack:
        A context manager shared across all ECS tests.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    cluster_name = stack.enter_context(new_ecs_cluster("TestECSCluster", ecs=ecs))
    shared_vars["cluster_name"] = cluster_name


def test_create_task_definition(ecs, ecs_task_role, stack, shared_vars):
    """Verify access to create an ECS task definitions.

    :testid: ecs-f-1-1

    :param ECS.Client ecs:
        A handle to the ECS API.
    :param boto3.IAM.Role ecs_task_role:
        A Role permitted to run ECS tasks.
    :param contextlib.ExitStack stack:
        A context manager shared across all ECS tests.
    :param dict shared_vars:
        dictionary shared between test functions

    """

    task_def_name = stack.enter_context(
        new_ecs_task_definition("TestECSFamily", ecs_task_role, ECS_COMMAND, ecs=ecs)
    )
    shared_vars["task_def_name"] = task_def_name


def test_create_service(
    ecs, cluster_name, task_def_name, security_group, internal_subnet_id, stack
):
    """Verify ability to use the ECS service

    :testid: ecs-f-1-1

    :param ECS.Client ecs:
        A handle to the ECS API.
    :param str cluster_name:
        Name of an ECS cluster.
    :param str task_def_name:
        Name of a task definition.
    :param boto3.EC2.SecurityGroup security_group:
        A SecurityGroup for the compute environment instances.
    :param str internal_subnet_id:
        The ID of the subnet where the compute environment will be deployed.
    :param contextlib.ExitStack stack:
        A context manager shared across all ECS tests.

    """

    stack.enter_context(
        new_ecs_service(
            "TestECSService",
            cluster_name,
            task_def_name,
            security_group.group_id,
            internal_subnet_id,
            ecs=ecs,
        )
    )
    time.sleep(5)
    assert True, "Failed to Create ECS Stack"
