"""
=============
test_pinpoint
=============

Verify access to AWS Pinpoint

Plan:

* Attempt to create an app
* Attempt to create a segment
* Attempt to create a campaign
* Attempt to get the app
* Attempt to get the campaign
* Attempt to get the segment

${testcount:12}

"""

from datetime import datetime, timedelta

import pytest

from aws_test_functions import (
    make_identifier,
)
from aws_test_functions.pinpoint import (
    delete_app,
    delete_campaign,
    delete_segment,
)


pytestmark = [pytest.mark.scp_check('get_apps')]


@pytest.fixture(scope='module')
def pinpoint(session):
    """A shared handle to Pinpoint API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the Pinpoint API.

    """

    return session.client('pinpoint')


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'app_id': 'not-created',
        'campaign_id': 'not-created',
        'segment_id': 'not-created',
    }


def test_create_app(pinpoint, stack, shared_vars):
    """Verify access to create an app.

    :testid: pp-f-1-1

    :param Pinpoint.Client pinpoint:
        A handle to the Pinpoint API.
    :param contextlib.ExitStack stack:
        A context manager shared across all Pinpoint tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-pinpoint')
    res = pinpoint.create_app(
        CreateApplicationRequest=dict(
            Name=name,
        ),
    )
    assert 'ApplicationResponse' in res, 'Unexpected response: {}'.format(res)

    app_id = shared_vars['app_id'] = res['ApplicationResponse']['Id']
    print('Created app: {}'.format(app_id))

    stack.callback(
        delete_app,
        app_id,
        pinpoint=pinpoint,
    )


def test_create_segment(pinpoint, app_id, stack, shared_vars):
    """Verify access to create a segment.

    :testid: pp-f-1-1

    :param Pinpoint.Client pinpoint:
        A handle to the Pinpoint API.
    :param str app_id:
        The ID of an app.
    :param contextlib.ExitStack stack:
        A context manager shared across all Pinpoint tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = pinpoint.create_segment(
        ApplicationId=app_id,
        WriteSegmentRequest=dict(),
    )
    assert 'SegmentResponse' in res, 'Unexpected response: {}'.format(res)

    segment_id = shared_vars['segment_id'] = res['SegmentResponse']['Id']
    print('Created segment: {}'.format(segment_id))

    stack.callback(
        delete_segment,
        app_id,
        segment_id,
        pinpoint=pinpoint,
    )


def test_create_campaign(pinpoint, app_id, segment_id, stack, shared_vars):
    """Verify access to create a campaign.

    :testid: pp-f-1-1

    :param Pinpoint.Client pinpoint:
        A handle to the Pinpoint API.
    :param str app_id:
        The ID of an app.
    :param str segment_id:
        The ID of a segment.
    :param contextlib.ExitStack stack:
        A context manager shared across all Pinpoint tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    name = make_identifier('test-campaign')
    now = datetime.utcnow()

    res = pinpoint.create_campaign(
        ApplicationId=app_id,
        WriteCampaignRequest=dict(
            Name=name,
            SegmentId=segment_id,
            Schedule=dict(
                StartTime=(now + timedelta(days=1)).isoformat(),
                EndTime=(now + timedelta(days=2)).isoformat(),
                Frequency='ONCE',
            ),
            MessageConfiguration=dict(
                EmailMessage=dict(
                    FromAddress='testing@rhedcloudtesting.edu',
                    Title='testing',
                    Body='message',
                ),
            ),
        ),
    )
    assert 'CampaignResponse' in res, 'Unexpected response: {}'.format(res)

    campaign_id = shared_vars['campaign_id'] = res['CampaignResponse']['Id']
    print('Created campaign: {}'.format(campaign_id))

    stack.callback(
        delete_campaign,
        app_id,
        campaign_id,
        pinpoint=pinpoint,
    )


def test_get_app(pinpoint, app_id):
    """Verify access to retrieve information about an app.

    :testid: pp-f-1-2

    :param Pinpoint.Client pinpoint:
        A handle to the Pinpoint API.
    :param str app_id:
        The ID of an app.

    """

    res = pinpoint.get_app(ApplicationId=app_id)
    assert 'ApplicationResponse' in res, 'Unexpected response: {}'.format(res)


def test_get_campaign(pinpoint, app_id, campaign_id):
    """Verify access to retrieve information about an app.

    :testid: pp-f-1-2

    :param Pinpoint.Client pinpoint:
        A handle to the Pinpoint API.
    :param str app_id:
        The ID of an app.
    :param str campaign_id:
        The ID of a campaign.

    """

    res = pinpoint.get_campaign(
        ApplicationId=app_id,
        CampaignId=campaign_id,
    )
    assert 'CampaignResponse' in res, 'Unexpected response: {}'.format(res)


def test_get_segment(pinpoint, app_id, segment_id):
    """Verify access to retrieve information about an app.

    :testid: pp-f-1-2

    :param Pinpoint.Client pinpoint:
        A handle to the Pinpoint API.
    :param str app_id:
        The ID of an app.
    :param str segment_id:
        The ID of a segment.

    """

    res = pinpoint.get_segment(
        ApplicationId=app_id,
        SegmentId=segment_id,
    )
    assert 'SegmentResponse' in res, 'Unexpected response: {}'.format(res)
