import pytest


@pytest.fixture(scope="module")
def ec2(session):
    """A shared handle to EC2 APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the EC2 APIs.

    """

    return session.client("ec2")
