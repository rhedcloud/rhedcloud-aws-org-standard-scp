"""
=============
test_cloudwatch
=============

A user should be able to create alarms, check status of alarms, and delete alarms

Plan:

* Create a new test user
* Create a CloudWatch client (using user session)
* Create a billing alarm using the put_metric_alarm api call
* - AlarmName = "BillingAlarm"
* - ComparisonOperator = "GreaterThanThreshold"
* - EvaluationPeriods = "1"
* - MetricName = "EstimatedCharges"
* - Period = "21600"
* - Statistic = "Maximum"
* - Threshold = "2500.0"
* - Dimensions = [{ "Name": "Currency", "Value": "USD" }]
* Find "BillingAlarm" alarm and chec status (OK | ALARM | INSUFFICIENT DATA)
* Delete "BillingAlarm" alarm
* Assert that a user can create, modify, and delete a self managed CloudTrail trail
* Assert that a user cannot send a customer managed CloudTrail trail to  an RHEDcloud managed CloudTrail bucket

${testcount:3}

"""

import pytest

@pytest.fixture(scope='module')
def cloudwatch(session):
    """A shared handle to CloudWatch API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the CloudWatch API.

    """
    return session.client('cloudwatch')

def test_create_cloudwatch_alarm(cloudwatch):
    """Create a CloudWatch alarm named 'BillingAlarm'

    :testid: cloudwatch-f-1-1

    :param A handle to the CloudWatch API

    """
    create_alarm_resp = cloudwatch.put_metric_alarm(
        AlarmName='BillingAlarm',
        ComparisonOperator='GreaterThanThreshold',
        EvaluationPeriods=1,
        MetricName='EstimatedCharges',
        Namespace='AWS/Billing',
        Period=21600,
        Statistic='Maximum',
        Threshold=2500.0,
        Dimensions=[{'Name': 'Currency', 'Value': 'USD'}]
    )
    
    assert create_alarm_resp['ResponseMetadata']['HTTPStatusCode'] == 200, 'Failed to create CloudWatch alarm'

def test_check_cloudwatch_alarm(cloudwatch):
    """Check status of CloudWatch alarm named 'BillingAlarm'

    :testid: cloudwatch-f-1-2

    :param A handle to the CloudWatch API
    
    """
    alarms_resp = cloudwatch.describe_alarms(
        AlarmNames=['BillingAlarm']
    )

    assert len(alarms_resp['MetricAlarms']) > 0, 'CloudWatch alarm not found'

def test_delete_cloudwatch_alarm(cloudwatch):
    """Delete the CloudWatch alarm named 'BillingAlarm'

    :testid: cloudwatch-f-1-3

    :param A handle to the CloudWatch API
    
    """
    delete_resp = cloudwatch.delete_alarms(
        AlarmNames=['BillingAlarm']
    )

    assert delete_resp['ResponseMetadata']['HTTPStatusCode'] == 200, 'Failed to delete CloudWatch alarm'
