"""
==============
test_guardduty
==============

guardduty-f-1
Users should not be able to turn on or configure GuardDuty

guardduty-f-1-1	Create a GuardDuty detector
guardduty-f-1-2	Create a GuardDuty member
guardduty-f-1-3	Modify GuardDuty Detector configuration
guardduty-f-1-4	Stop GuardDuty monitoring

${testcount:6}

"""

import pytest

from aws_test_functions import (
    aws_client,
    catch,
    make_identifier,
    new_bucket
)

pytestmark = [
    # This service is blocked in the HIPAA repository
    pytest.mark.scp_check('list_invitations'),

    pytest.mark.xfail(reason='The SCP blocks creating the GuardDuty service linked role, which inadvertently blocks the service')
]


@pytest.fixture(scope='module')
def guardduty(session):
    """
    A shared handle to Amazon GuardDuty

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to Amazon GuardDuty

    """

    return session.client('guardduty')


@pytest.fixture(scope='module')
def s3res(session):
    """
    Return an S3 resource

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        An S3 resource handle

    """

    return session.resource('s3')


@pytest.fixture(scope='module')
def bucket(s3res):
    """
    Return an S3 bucket resource

    :param boto3.s3.resource

    :returns:
        An S3 bucket resource

    """

    created = False
    with new_bucket('guardduty', s3=s3res) as b:
        created = True
        yield b

    assert created, 'failed to create bucket'


@pytest.fixture(scope='module', autouse=True)
def shared_vars():
    """
    Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'detector_created': False,
        'detector_id': 'not-created',
        'member_id': '123456789012'
    }


def test_create_detector(guardduty, shared_vars):
    """
    Create a GuardDuty detector

    :param boto3.client A handle to Amazon GuardDuty
    :param dict shared_vars: dictionary shared between test functions

    :testid: guardduty-f-1-1

    """

    create_response = guardduty.create_detector()

    assert 'DetectorId' in create_response, 'Detector not created'
    shared_vars['detector_id'] = create_response['DetectorId']
    shared_vars['detector_created'] = True


@pytest.mark.skip(reason='Unable to invite/accept invitations')
def test_create_member(guardduty, shared_vars):
    """
    Create a GuardDuty member

    :param boto3.client A handle to Amazon GuardDuty
    :param dict shared_vars: dictionary shared between test functions

    :testid: guardduty-f-1-2

    """

    detector_id = shared_vars['detector_id']
    member_id = shared_vars['member_id']

    master_response = guardduty.get_master_account(
        DetectorId=detector_id
    )

    # print(master_response)

    # assert 'Master' in master_response, 'Unable to get master account'

    create_response = guardduty.create_members(
        AccountDetails=[{
            'AccountId': member_id,
            'Email': 'don@donboots.com'
        }],
        DetectorId=detector_id
    )

    assert len(create_response['UnprocessedAccounts']) == 0, \
        'Unable to create Guard Duty member'

    invite_response = guardduty.invite_members(
        AccountIds=[member_id],
        DetectorId=detector_id,
        Message='This is an invite'
    )

    # print(invite_response)
    assert len(invite_response['UnprocessedAccounts']) == 0, \
        'Unable to invite member'

    list_response = guardduty.list_invitations()

    # print(list_response)


def test_modify_detector(guardduty, shared_vars, bucket):
    """
    Modify a GuardDuty detector

    :param boto3.client A handle to Amazon GuardDuty
    :param dict shared_vars: dictionary shared between test functions

    :testid: guardduty-f-1-3

    """

    threat_location = 's3://%s/%s' % \
        (bucket.name, make_identifier('threat_set'))

    try:
        # create threat intel set
        create_response = guardduty.create_threat_intel_set(
            DetectorId=shared_vars['detector_id'],
            Name=make_identifier('threat'),
            Location=threat_location,
            Format='TXT'
        )

        assert 'ThreatIntelSetId' in create_response, \
            'Unable to create threat intel set'

        threatIntel_set_id = create_response['ThreatIntelSetId']

        # modify detector with created threat intel set
        modify_response = guardduty.update_threat_intel_set(
            DetectorId=shared_vars['detector_id'],
            ThreatIntelSetId=threatIntel_set_id
        )

        assert modify_response['ResponseMetadata']['HTTPStatusCode'] == 200, \
            'Unable to modify Guard Duty detector'
    except guardduty.exceptions.InternalServerErrorException:
        pytest.xfail("Internal Server Errors occur intermittently")


def test_stop_monitoring(guardduty, shared_vars):
    """
    Stop GuardDuty monitoring

    :param boto3.client A handle to Amazon GuardDuty
    :param dict shared_vars: dictionary shared between test functions

    :testid: guardduty-f-1-4

    """

    stop_response = guardduty.stop_monitoring_members(
        DetectorId=shared_vars['detector_id'],
        AccountIds=[shared_vars['member_id']]
    )

    # print(stop_response)


@catch(Exception, msg='Failed to delete detector: {ex}')
@aws_client('guardduty')
def cleanup_detectors(*, guardduty):
    """ Delete any detectors created during tests """

    list_response = guardduty.list_detectors()
    for detector in list_response['DetectorIds']:
        res = guardduty.list_members(DetectorId=detector)
        account_ids = []
        for member in res['Members']:
            account_ids.append(member['AccountId'])

        if len(account_ids):
            print('Deleting members {}'.format(', '.join(account_ids)))
            guardduty.delete_members(AccountIds=account_ids,
                                     DetectorId=detector)

        print('Deleting detector %s' % (detector))
        guardduty.delete_detector(DetectorId=detector)
