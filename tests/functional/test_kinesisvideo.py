"""
=================
test_kinesisvideo
=================

Verify access for users to utilize AWS KinesisVideo functionality.

${testcount:6}

* Create a Kinesis Video stream
* Wait for stream to be active
* Update the stream
* List streams
* Get a data endpoint for a video stream
* Delete the KinesisVideo stream

"""

import pytest

from aws_test_functions import (
    aws_client,
    ignore_errors,
    catch,
    make_identifier,
    unexpected,
    retry,
    has_status,
)

# This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('list_streams')]


@pytest.fixture(scope='module')
def kinesisvideo(session):
    """A shared handle to KinesisVideo API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the KinesisVideo API.

    """

    return session.client('kinesisvideo')


@catch(Exception, msg='Failed to delete stream: {ex}')
@aws_client('kinesisvideo')
def cleanup_streams(*, kinesisvideo=None):
    """Attempt to remove any unused streams."""

    res = kinesisvideo.list_streams()
    for stream in res['StreamInfoList']:
        name = stream['StreamName']
        kv_arn = stream['StreamARN']
        print('Deleting stream: {}'.format(name))
        kinesisvideo.delete_stream(StreamARN=kv_arn)


@pytest.fixture(scope='module')
def stream_name():
    """Generate a unique name for a Kinesis Video Stream.

    :returns:
        A string.

    """

    return make_identifier('test-kinesis-video')


@pytest.dict_fixture(with_assertion=False)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'stream_arn': "arn:aws:kinesisvideo:invalid:111:invalid/invalid/111",
        'stream_version': "0",
    }


def test_create_stream(kinesisvideo, stream_name, stack, shared_vars):
    """Verify access to create a Kinesis Video stream.

    :testid: kinesisvideo-f-1-1

    :param KinesisVideo.Client kinesisvideo:
        A handle to the KinesisVideo API.
    :param str stream_name:
        Name of the Kinesis Video Stream.
    :param contextlib.ExitStack stack:
        A context manager shared across all Kinesis Video tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = kinesisvideo.create_stream(
        StreamName=stream_name,
        MediaType='video/h264',
    )
    assert has_status(res, 200), unexpected(res)
    assert 'StreamARN' in res, unexpected(res)

    stream_arn = shared_vars['stream_arn'] = res['StreamARN']

    stack.callback(
        ignore_errors(kinesisvideo.delete_stream),
        StreamARN=stream_arn,
    )


def test_wait_for_active_stream(kinesisvideo, stream_arn, shared_vars):
    """Verify access to describe a Kinesis Video stream.

    :testid: kinesisvideo-f-1-2

    :param KinesisVideo.Client kinesisvideo:
        A handle to the KinesisVideo API.
    :param str stream_arn:
        ARN of the Kinesis Video Stream.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    def _test(res):
        status = res['StreamInfo']['Status']
        print('Current stream status: {}'.format(status))
        return status == 'ACTIVE'

    res = retry(
        kinesisvideo.describe_stream,
        kwargs=dict(
            StreamARN=stream_arn,
        ),
        until=_test,
        msg='Checking Kinesis Video Stream status: {}'.format(stream_arn),
        delay=15,
    )
    assert _test(res), unexpected(res)

    shared_vars['stream_version'] = res['StreamInfo']['Version']


def test_update_stream(kinesisvideo, stream_arn, stream_version):
    """Verify access to update a Kinesis Video stream.

    :testid: kinesisvideo-f-1-3

    :param KinesisVideo.Client kinesisvideo:
        A handle to the KinesisVideo API.
    :param str stream_arn:
        ARN of the Kinesis Video Stream.
    :param str stream_version:
        Version of the Kinesis Video Stream to update.

    """

    res = kinesisvideo.update_stream(
        StreamARN=stream_arn,
        CurrentVersion=stream_version,
        MediaType='video/h265',
    )
    assert has_status(res, 200), unexpected(res)


def test_list_streams(kinesisvideo):
    """Verify access to list Kinesis Video streams.

    :testid: kinesisvideo-f-1-4

    :param KinesisVideo.Client kinesisvideo:
        A handle to the KinesisVideo API.

    """

    res = kinesisvideo.list_streams()
    assert 'StreamInfoList' in res, unexpected(res)
    assert len(res['StreamInfoList']) > 0, unexpected(res)


def test_get_data_endpoint(kinesisvideo, stream_arn):
    """Verify access to get a data endpoint for a Kinesis Video stream.

    :testid: kinesisvideo-f-1-5

    :param KinesisVideo.Client kinesisvideo:
        A handle to the KinesisVideo API.
    :param str stream_arn:
        ARN of the Kinesis Video Stream.

    """

    res = kinesisvideo.get_data_endpoint(
        StreamARN=stream_arn,
        APIName='GET_MEDIA',
    )
    assert 'DataEndpoint' in res, unexpected(res)


def test_delete_stream(kinesisvideo, stream_arn):
    """Verify access to delete a Kinesis Video stream.

    :testid: kinesisvideo-f-1-6

    :param KinesisVideo.Client kinesisvideo:
        A handle to the KinesisVideo API.
    :param str stream_arn:
        ARN of the Kinesis Video Stream.

    """

    res = kinesisvideo.delete_stream(
        StreamARN=stream_arn,
    )
    assert has_status(res, 200), unexpected(res)
