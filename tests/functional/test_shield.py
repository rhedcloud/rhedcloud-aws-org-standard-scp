"""
===========
test_shield
===========

shield-f-1
Customers should be able to make "list" calls within the AWS Shield service

shield-f-1-1	ListAttacks
shield-f-1-2    ListProtections

shield-f-2
Customers should be able to make "read" calls within the AWS Shield service

shield-f-2-1	DescribeAttack
shield-f-2-2	DescribeProtection
shield-f-2-3	DescribeSubscription

shield-f-3
Customers should be able to make "write" calls within the AWS Shield service

shield-f-3-1	CreateProtection
shield-f-3-2	CreateSubscription
shield-f-3-3	DeleteProtection
shield-f-3-4	DeleteSubscription

${testcount:18}

"""

import pytest

from aws_test_functions import (
    make_identifier,
    get_subnet_id,
    new_ec2_instance,
    retry,
    get_account_number,
)

pytestmark = pytest.mark.skip(reason="This service has significant monetary impact")


@pytest.fixture(scope="module")
def shield(session):
    """A shared handle to Amazon Shield

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to Amazon Shield

    """

    return session.client("shield")


@pytest.fixture(scope="module")
def ec2(session):
    """A shared handle to Amazon EC2

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to Amazon EC2

    """

    return session.client("ec2")


@pytest.fixture(scope="module")
def instance(session, vpc_id, stack):
    def _status(i):
        i.reload()
        return i.state["Name"]

    env = "Private Subnet 1"
    subnet_id = get_subnet_id(vpc_id, env)

    inst = stack.enter_context(
        new_ec2_instance(env, session=session, SubnetId=subnet_id)
    )

    retry(
        _status,
        args=(inst,),
        msg='Waiting for instance to be in "running" state...',
        until=lambda r: r == "running",
    )

    yield inst


def test_list_attacks(shield):
    """
    Attempt to call "ListAttacks"

    :param boto3.client A handle to Amazon Shield

    :testid shield-f-1-1

    """

    list_response = shield.list_attacks()
    assert "AttackSummaries" in list_response, "Unable to list attacks"


def test_create_protection(shield, ec2, vpc_id, instance):
    allocate_response = ec2.allocate_address(Domain="vpc")

    """
    create_response = ec2.create_internet_gateway()

    gateway_id = create_response['InternetGateway']['InternetGatewayId']

    attach_response = ec2.attach_internet_gateway(
        InternetGatewayId=gateway_id,
        VpcId=vpc_id
    )
    """

    ec2.associate_address(
        AllocationId=allocate_response["AllocationId"], InstanceId=instance.instance_id
    )

    resource_arn = "arn:aws:ec2:us-east-1:%s:eip-allocation/%s" % (
        get_account_number(),
        allocate_response["AllocationId"],
    )
    shield.create_protection(Name=make_identifier("shield"), ResourceArn=resource_arn)


def test_list_protections(shield):
    """
    Attempt to call "ListProtections"

    :param boto3.client A handle to Amazon Shield

    :testid shield-f-1-2

    """

    list_response = shield.list_protections()
    assert "Protections" in list_response, "Unable to list protections"


def test_describe_attacks(shield):
    """
    Attempt to call "DescribeAttack" against an existing or
    non-existing attack.

    :param boto3.client A handle to Amazon Shield

    :testid shield-f-2-1

    """

    describe_response = shield.describe_attack(AttackId=make_identifier("attack"))

    print(describe_response)


@pytest.fixture(scope="module", autouse=True)
def cleanup(shield, ec2):
    """Release any IP addresses
    Detach and delete any internet gateways

    :param boto3.client A handle to Amazon Shield
    :param boto3.client A handle to Amazon EC2

    """

    describe_response = ec2.describe_addresses()

    for address in describe_response["Addresses"]:
        print(address)
        ec2.release_address(AllocationId=address["AllocationId"])

    describe_response = ec2.describe_hosts()

    for host in describe_response["Hosts"]:
        print("releasing host %s" % (host))
        ec2.release_hosts(host["HostId"])

    describe_response = ec2.describe_internet_gateways()

    """
    for gateway in describe_response['InternetGateways']:
        gateway_id = gateway['InternetGatewayId']

        print(gateway)

        for attachment in gateway['Attachments']:
            vpc_id = attachment['VpcId']

            print(attachment)

            ec2.detach_internet_gateway(
                InternetGatewayId=gateway_id,
                VpcId=vpc_id
            )

        ec2.delete_internet_gateway(
            InternetGatewayId=gateway_id
        )
    """
    yield
