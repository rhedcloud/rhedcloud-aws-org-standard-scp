"""
========
test_dms
========

Verify that customers can use AWS Database Migration Service.

Plan:

* Create a replication instance
* Create source and target endpoints
* Test connectivity to endpoints
* Create replication task
* Start replication task
* Monitor progress of replication task
* Clean everything up

${testcount:15}

"""

import pytest

from aws_test_functions import (
    get_subnet_id,
    new_role,
    retry,
)
from aws_test_functions.dms import (
    cleanup_endpoints,
    cleanup_replication_instances,
    cleanup_replication_subnet_groups,
    cleanup_replication_tasks,
    get_connection_status,
    get_replication_instance_status,
    get_replication_task_status,
    new_endpoint,
    new_replication_instance,
    new_replication_subnet_group,
    new_replication_task,
)
from aws_test_functions.rds import (
    new_db_instance_async,
)

pytestmark = pytest.mark.slowtest


@pytest.fixture(scope='module')
def dms(session):
    """A shared handle to DMS APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the DMS APIs.

    """

    return session.client('dms')


@pytest.fixture(scope='module')
def rds(session):
    """A shared handle to RDS APIs.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the RDS APIs.

    """

    return session.client('rds')


@pytest.fixture(scope='module')
def subnets(vpc_id):
    """Return the IDs of subnets for the replication subnet group.

    :param str vpc_id:
        ID of a VPC.

    :returns:
        A list of strings.

    """

    return [
        get_subnet_id(vpc_id, 'Private Subnet 1'),
        get_subnet_id(vpc_id, 'Private Subnet 2'),
    ]


@pytest.fixture(scope='module')
def cleanup(dms):
    """Perform a series of clean up tasks before running any tests.

    :param DMS.Client dms:
        A handle to the DMS APIs.

    """

    cleanup_replication_tasks(dms=dms)
    cleanup_endpoints(dms=dms)
    cleanup_replication_instances(dms=dms)
    cleanup_replication_subnet_groups(dms=dms)


@pytest.fixture(scope='module')
def role():
    """Create a new IAM Role required by DMS.

    :yields:
        An IAM.Role resource.

    """

    arn = 'arn:aws:iam::aws:policy/service-role/AmazonDMSVPCManagementRole'
    with new_role('dms-vpc-role', 'dms', arn, exact_name=True) as r:
        yield r


@pytest.fixture(params=('source', 'target'))
def endpoint_type(request):
    """The current type of endpoint instance to create."""

    return request.param


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        # subnet group to use for the replication instance
        'subnet_group': None,

        # the replication instance to use for the test
        'replicator': None,

        # a mapping of each RDS instance used by the tests
        'mysql': {},

        # information about the source RDS instance
        'source_db': None,

        # information about the target RDS instance
        'target_db': None,

        # a mapping of each replication endpoint used by the tests
        'endpoints': {},

        # information about the source replication endpoint
        'source_endpoint': None,

        # information about the target replication endpoint
        'target_endpoint': None,

        # the ARN of the replication task created by the tests
        'replication_task': None,

        # whether or not the replication task was started successfully
        'task_started': False,
    }


def test_create_replication_subnet_group(dms, role, subnets, stack, shared_vars):
    """A customer should be able to create a replication subnet group for DMS.

    :testid: dms-f-1-1

    :param DMS.Client dms:
        A handle to the DMS APIs.
    :param IAM.Role role:
        A dependency on the IAM Role required for DMS.
    :param list subnets:
        A list of subnet IDs to use when creating the replication subnet group.
    :param contextlib.ExitStack stack:
        A context manager shared across all DMS tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    sng = shared_vars['subnet_group'] = stack.enter_context(
        new_replication_subnet_group('test-dms', subnets, dms=dms)
    )

    assert sng, 'replication subnet group not created'


def test_create_replication_instance(dms, subnet_group, stack, shared_vars):
    """A customer should be able to create a DMS replication instance.

    :testid: dms-f-1-1

    :param DMS.Client dms:
        A handle to the DMS APIs.
    :param dict subnet_group:
        Information about a replication subnet group.
    :param contextlib.ExitStack stack:
        A context manager shared across all DMS tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    subnet_group_id = subnet_group['ReplicationSubnetGroupIdentifier']
    replicator = shared_vars['replicator'] = stack.enter_context(
        new_replication_instance('test-dms', subnet_group_id, dms=dms),
    )

    assert replicator['ReplicationInstanceClass'] == 'dms.t2.micro', \
        'Unexpected replication instance: {}'.format(replicator)


def test_create_database(dms, rds, vpc_id, endpoint_type, stack, shared_vars):
    """A customer should be able to create RDS instances for use in DMS.

    :testid: dms-f-1-2

    :param DMS.Client dms:
        A handle to the DMS APIs.
    :param RDS.Client rds:
        A handle to the RDS APIs.
    :param str vpc_id:
        ID of a VPC.
    :param str endpoint_type:
        The type of RDS instance being created.
    :param contextlib.ExitStack stack:
        A context manager shared across all DMS tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    key = '{}_db'.format(endpoint_type)
    db = shared_vars[key] = new_db_instance_async(
        'test-dms-{}'.format(endpoint_type),
        'mysql',
        vpc_id,
        rds=rds,
    )

    assert db, '{} database not created'.format(endpoint_type)
    shared_vars['mysql'][endpoint_type] = db


def test_create_endpoint(dms, mysql, endpoint_type, stack, shared_vars):
    """A customer should be able to create DMS replication endpoints.

    :testid: dms-f-1-2

    :param DMS.Client dms:
        A handle to the DMS APIs.
    :param dict mysql:
        A dependency on the fixture that holds information about each RDS
        instance.
    :param str endpoint_type:
        The type of replication endpoint being created.
    :param contextlib.ExitStack stack:
        A context manager shared across all DMS tests.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    db = mysql[endpoint_type]
    db.wait()

    key = '{}_endpoint'.format(endpoint_type)
    endpoint = shared_vars[key] = stack.enter_context(
        new_endpoint(
            'test-dms-{}'.format(endpoint_type),
            endpoint_type,
            'mysql',
            db,
            dms=dms,
        ),
    )

    assert endpoint, '{} endpoint not created'.format(endpoint_type)
    shared_vars['endpoints'][endpoint_type] = endpoint


def test_replication_instance_is_active(dms, replicator):
    """A customer should be able to determine when a replication instance is
    active.

    :testid: dms-f-1-3

    :param DMS.Client dms:
        A handle to the DMS APIs.
    :param dict replicator:
        Information about the replication instance to use.

    """

    status = retry(
        get_replication_instance_status,
        args=(replicator['ReplicationInstanceArn'],),
        msg='Checking replication instance status',
        until=lambda r: r == 'available',
        delay=10,
    )

    assert status == 'available', 'Replication instance {} not available'.format(replicator)


def test_endpoint_exists(dms, mysql, endpoints, endpoint_type):
    """A customer should be able to determine whether a replication endpoint
    exists.

    :testid: dms-f-1-3

    :param DMS.Client dms:
        A handle to the DMS APIs.
    :param dict endpoints:
        Information about the source and target endpoints.
    :param str endpoint_type:
        The type of endpoint being checked.

    """

    endpoint = endpoints[endpoint_type]
    found = False

    res = dms.describe_endpoints()
    for ep in res['Endpoints']:
        if ep['EndpointArn'] == endpoint['EndpointArn']:
            found = True
            break

    assert found, '{} endpoint not found: {}'.format(endpoint_type, res)


def test_connectivity(dms, replicator, endpoints, endpoint_type):
    """A customer should be able to determine whether a replication endpoint
    can connect with a replication instance.

    :testid: dms-f-1-3

    :param DMS.Client dms:
        A handle to the DMS APIs.
    :param dict replicator:
        Information about the replication instance to use.
    :param dict endpoints:
        Information about the source and target endpoints.
    :param str endpoint_type:
        The type of endpoint being checked.

    """

    replicator_arn = replicator['ReplicationInstanceArn']
    endpoint_arn = endpoints[endpoint_type]['EndpointArn']

    res = dms.test_connection(
        ReplicationInstanceArn=replicator_arn,
        EndpointArn=endpoint_arn,
    )
    assert 'Connection' in res, 'Unexpected response: {}'.format(res)
    conn = res['Connection']

    print('Connection: {}'.format(conn))
    status = retry(
        get_connection_status,
        args=(endpoint_arn,),
        msg='Checking {} endpoint connection status'.format(endpoint_type),
        until=lambda r: r == 'successful',
        delay=10,
    )

    assert status == 'successful', 'Failed to establish connection between {} and {}: {}'.format(
        replicator_arn, endpoint_arn, conn)


def test_replication_task_ready(dms, replicator, source_endpoint,
                                target_endpoint, stack, shared_vars):
    """A customer should be able to create a replication task and determine
    when it is ready for use.

    :testid: dms-f-1-4

    :param DMS.Client dms:
        A handle to the DMS APIs.
    :param str replication_task:
        ARN of a replication task.

    """

    arn = shared_vars['replication_task'] = stack.enter_context(
        new_replication_task(
            'test-dms-task',
            replicator['ReplicationInstanceArn'],
            source_endpoint['EndpointArn'],
            target_endpoint['EndpointArn'],
            dms=dms
        ))

    status = retry(
        get_replication_task_status,
        args=(arn,),
        msg='Checking replication task status',
        until=lambda r: r == 'ready',
        delay=10,
    )

    assert status == 'ready', 'Replication task status is not ready'


def test_start_replication_task(dms, replication_task, shared_vars):
    """A customer should be able to start a replication task.

    :testid: dms-f-1-5

    :param DMS.Client dms:
        A handle to the DMS APIs.
    :param str replication_task:
        ARN of a replication task.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = dms.start_replication_task(
        ReplicationTaskArn=replication_task,
        StartReplicationTaskType='reload-target',
    )
    assert 'ReplicationTask' in res, 'Unexpected response: {}'.format(res)
    shared_vars['task_started'] = True


def test_check_replication_task(dms, replication_task, task_started):
    """A customer should be able to monitor the status of a replication task.

    :testid: dms-f-1-6

    :param DMS.Client dms:
        A handle to the DMS APIs.
    :param str replication_task:
        ARN of a replication task.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    for desired in ('running', 'stopped'):
        status = retry(
            get_replication_task_status,
            args=(replication_task,),
            msg='Checking replication task "{}" status'.format(desired),
            until=lambda r: r == desired,
            delay=10,
        )

        assert status == desired, 'Unexpected replication task status'


def test_cleanup_resources(mysql):
    """Remove the RDS instances required for these tests.

    :testid: dms-f-1-7

    :param dict mysql:
        A dictionary of RDS instances used by the tests.

    """

    for db in mysql.values():
        db.cleanup()
