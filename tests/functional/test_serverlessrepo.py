"""
===============
test_serverlessrepo
===============

Verify access to use AWS Serverless Application Repository

Plan:

* Attempt to create an application
* Attempt to list the available applications and verify the application is returned
* Attempt to update the application
* Attempt to delete the application

${testcount:8}

"""

import pytest

from aws_test_functions import (
    has_status,
    ignore_errors,
    in_setup_org,
    make_identifier,
    retry,
    unexpected
)

# This service is fully blocked - As of 7/2/2020 this is no longer blocked (ppeters)
#pytestmark = [pytest.mark.scp_check('list_applications')]

# Create a reusable functor to move the AWS account to the SETUP_ORG before
# attempting to invoke the requested function. This is necessary to ensure that
# we make the best effort to remove resources created during testing (since we
# should not be able to delete with the SCP in effect in the TEST_ORG).
cleaner = in_setup_org('ServerlessRepo cleanup', one_way=True)


@pytest.fixture(scope='module')
def serverlessrepo(session):
    """A shared handle to ServerlessApplicationRepository API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the ServerlessApplicationRepository API.

    """

    return session.client('serverlessrepo')


@pytest.dict_fixture
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'application_id': 'not-created',
    }


def test_create_application(serverlessrepo, shared_vars, stack):
    """Verify ability to create an application

    :testid:

    :param ServerlessApplicationRepository.Client serverlessrepo:
        A handle to the ServerlessApplicationRepository API.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.
    :param contextlib.ExitStack stack:
        A context manager shared across all ServelessRepo tests.

    """

    name = make_identifier('TestSARApplication')
    res = serverlessrepo.create_application(Name=name,
                                            Author='Test App Author',
                                            Description='Testing Serveless App Repo')
    assert has_status(res, 201), unexpected(res)

    app_id = shared_vars['application_id'] = res['ApplicationId']
    stack.callback(
        ignore_errors(cleaner(serverlessrepo.delete_application)),
        ApplicationId=app_id
    )


def test_list_applications(serverlessrepo, application_id):
    """Verify ability to list applications

    :testid:

    :param ServerlessApplicationRepository.Client serverlessrepo:
        A handle to the ServerlessApplicationRepository API.
    :param str application_id:
        The name of a created application

    """

    def _test(res):
        found = False
        for app in res['Applications']:
            if app['ApplicationId'] == application_id:
                found = True
                break
        return found and has_status(res, 200)

    res = retry(
        serverlessrepo.list_applications,
        until=_test,
        msg='Checking application creation status',
        pred=lambda ex: False
    )
    assert has_status(res, 200), unexpected(res)
    assert _test(res), 'Did not find domain {} in list_applications: {}'.format(application_id, res['Applications'])


def test_update_application(serverlessrepo, application_id):
    """Verify ability to update an application

    :testid:

    :param ServerlessApplicationRepository.Client serverlessrepo:
        A handle to the ServerlessApplicationRepository API.
    :param str application_id:
        The name of a created application

    """

    updated_description = 'Updated Serverless Repo App description'

    def _test(res):
        success = False
        for app in res['Applications']:
            if app['ApplicationId'] == application_id:
                success = app['Description'] == updated_description
                break
        return success and has_status(res, 200)

    res = serverlessrepo.update_application(ApplicationId=application_id,
                                            Description=updated_description)
    assert has_status(res, 200), unexpected(res)

    res = retry(
        serverlessrepo.list_applications,
        until=_test,
        msg='Checking application update status',
        pred=lambda ex: False
    )
    assert has_status(res, 200), unexpected(res)
    assert _test(res), 'Failed to update {}: {}'.format(application_id, res)


def test_delete_application(serverlessrepo, shared_vars):
    """Verify ability to delete an application

    :testid:

    :param ServerlessApplicationRepository.Client serverlessrepo:
        A handle to the ServerlessApplicationRepository API.
    :param dict shared_vars:
        A dictionary of information that is shared between test functions.

    """

    res = serverlessrepo.delete_application(ApplicationId=shared_vars['application_id'])
    assert has_status(res, 200, 204), unexpected(res)

    shared_vars['application_id'] = 'not-created'
