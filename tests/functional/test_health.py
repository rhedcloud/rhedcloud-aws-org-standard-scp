"""
=============
test_health
=============

Verify that a user is able to use Health

Plan:
phd-f-1-1 Describe Health events
phd-f-1-2 Describe Heatlh affected entities
phd-f-1-3 Describe Health entity aggregates
phd-f-1-4 Describe Health event types
phd-f-1-5 Describe Health event details
phd-f-1-6 Describe Health event aggregates

${testcount:6}

"""

import pytest


@pytest.fixture(scope='module')
def health(session):
    """A shared handle to AWS Support API.

    :param boto3.session.Session session:
        A session belonging to an authenticated IAM User.

    :returns:
        A handle to the AWS Health API.

    """
    return session.client('health')


@pytest.fixture(scope='module', autouse=True)
def shared_vars():
    """Return a dictionary that is used to share information across test
    functions.

    """

    return {
        'eventId': None
    }


@pytest.mark.premium_account_required
def test_describe_events(health, shared_vars):
    """Describe Health events

    :testid: phd-f-1-1

    :param
        A handle to the AWS Health API
    :param dict shared_vars:
        dictionary shared between test functions

    """
    health_events_resp = health.describe_events()

    events = health_events_resp['events']

    assert len(events) > 0, \
        'No events found in Health check'

    shared_vars['eventId'] = events[0]['arn']


@pytest.mark.premium_account_required
def test_describe_affected_entities(health, shared_vars):
    """Describe Health affected entities

    :testid: phd-f-2-1

    :param
        A handle to the AWS Health API
    :param dict shared_vars:
        dictionary shared between test functions

    """
    health_events_resp = health.describe_affected_entities(
        filter={
            'eventArns': [shared_vars['eventId']]
        }
    )

    assert len(health_events_resp['entities']) > 0, \
        'No entities found in Health check'


@pytest.mark.premium_account_required
def test_describe_entity_aggregates(health, shared_vars):
    """Describe Health entity aggregates

    :testid: phd-f-3-1

    :param
        A handle to the AWS Health API
    :param dict shared_vars:
        dictionary shared between test functions

    """
    health_events_resp = health.describe_entity_aggregates()

    assert 'entityAggregates' in health_events_resp, \
        'No entity aggregates found in Health check'


@pytest.mark.premium_account_required
def test_describe_event_types(health, shared_vars):
    """Describe Health event types

    :testid: phd-f-4-1

    :param
        A handle to the AWS Health API
    :param dict shared_vars:
        dictionary shared between test functions

    """
    health_events_resp = health.describe_event_types()

    assert len(health_events_resp['eventTypes']) > 0, \
        'No event types found in Health check'


@pytest.mark.premium_account_required
def test_describe_event_details(health, shared_vars):
    """Describe Health event details

    :testid: phd-f-5-1

    :param
        A handle to the AWS Health API
    :param dict shared_vars:
        dictionary shared between test functions

    """
    health_events_resp = health.describe_event_details(
        eventArns=[shared_vars['eventId']]
    )

    assert 'successfulSet' in health_events_resp, \
        'No event details found in Health check'


@pytest.mark.premium_account_required
def test_describe_event_aggregates(health, shared_vars):
    """Describe Health event aggregates

    :testid: phd-f-6-1

    :param
        A handle to the AWS Health API
    :param dict shared_vars:
        dictionary shared between test functions

    """
    health_events_resp = health.describe_event_aggregates(
        filter={
            'eventArns': [shared_vars['eventId']]
        },
        aggregateField='eventTypeCategory'
    )

    assert 'eventAggregates' in health_events_resp, \
        'No event aggregates found in Health check'
