"""
============
test_route53
============

Verify ability to invoke AWS Route 53 APIs.

Plan:

* Verify users cannot check domain status
* Verify users cannot view domain billing reports
* Verify users cannot register new domains
* Verify users can create private hosted zones
* Verify users can update private DNS entries to point to EC2 instances

${testcount:5}

"""

import pytest

from aws_test_functions import get_uuid
from aws_test_functions.route53 import new_record_set


@pytest.fixture(scope='module')
def domains(session):
    """A handle to the Route 53 Domains APIs."""

    return session.client('route53domains')


@pytest.fixture(scope='module')
def route53(session):
    """A handle to the Route 53 APIs."""

    return session.client('route53')


@pytest.fixture(scope='module')
def private_zone(route53, vpc_id):
    """Create a private hosted zone and clean it up when we're done.

    :param Route53.Client domains:
        A handle to the Route 53 APIs.
    :param str vpc_id:
        The IP of a Type 1 VPC.

    :yields:
        A dictionary containing information about the new hosted zone.

    """

    call_id = get_uuid()
    zone_id = None

    try:
        res = route53.create_hosted_zone(
            Name='standard-scp.test.rhedcloud.com',
            VPC=dict(
                VPCId=vpc_id,
                VPCRegion='us-east-1',
            ),
            CallerReference=call_id,
            HostedZoneConfig=dict(
                PrivateZone=True,
                Comment='Testing private zone',
            ),
        )
        assert 'HostedZone' in res, 'Unexpected response: {}'.format(res)
        zone = res['HostedZone']
        zone_id = zone['Id']
        print('Created hosted zone: {}'.format(zone_id))

        yield zone
    finally:
        if zone_id is not None:
            print('Deleting hosted zone: {}'.format(zone_id))
            route53.delete_hosted_zone(Id=zone_id)


@pytest.fixture(scope='module')
def instance_ip(internal_runner):
    """Return the IP address of an EC2 instance within the Type 1 VPC.

    :param EC2.Instance instance:
        An EC2 Instance resource.

    :returns:
        A string.

    """

    return internal_runner.instance.private_ip_address


@pytest.mark.raises_access_denied
def test_domain_status(domains):
    """Customers should not be able to check domain availability.

    :testid: route53-f-1-1

    :param Route53Domains.Client domains:
        A handle to the Route 53 Domains APIs.

    """

    res = domains.check_domain_availability(
        DomainName='rhedcloud.com',
    )
    assert 'Availability' in res, 'Unexpected response: {}'.format(res)


@pytest.mark.raises_access_denied
def test_view_billing_report(domains):
    """Customers should not be able to view domain billing reports.

    :testid: route53-f-1-2

    :param Route53Domains.Client domains:
        A handle to the Route 53 Domains APIs.

    """

    res = domains.view_billing()
    assert 'BillingRecords' in res, 'Unexpected response: {}'.format(res)


@pytest.mark.raises_access_denied
def test_register_domain(domains):
    """Customers should not be able to register new domains.

    :testid: route53-f-1-3

    :param Route53Domains.Client domains:
        A handle to the Route 53 Domains APIs.

    """

    contact = dict(
        FirstName='RHEDcloud',
        LastName='RHEDcloud',
        ContactType='PERSON',
        CountryCode='US',
        PhoneNumber='+999.12345678',
        Email='invalid@invalid.com',
    )

    res = domains.register_domain(
        DomainName='rhedcloud.com',
        DurationInYears=1,
        AdminContact=contact,
        RegistrantContact=contact,
        TechContact=contact,
    )
    assert 'OperationId' in res, 'Unexpected response: {}'.format(res)


def test_private_hosted_zone(private_zone):
    """Customers should be able to create private hosted zones.

    :testid: route53-f-2-1

    :param dict private_zone:
        Information about a private zone created in a fixture.

    """

    assert private_zone['Config']['PrivateZone'] is True, 'Not a private zone: {}'.format(private_zone)


def test_update_record_set(route53, private_zone, instance_ip):
    """Customers should be able to update DNS entries for a private hosted
    zone.

    :testid: route53-f-2-2

    :param Route53.Client domains:
        A handle to the Route 53 APIs.
    :param dict private_zone:
        Information about a private zone created in a fixture.
    :param str instance_ip:
        The internal IP address of an EC2 instance.

    """

    zone_id = private_zone['Id']
    name = 'internal_runner.{}'.format(private_zone['Name'])
    found = False

    with new_record_set(zone_id, name, 'A', instance_ip, route53=route53) as records:
        for record in records:
            if record['Name'] == name:
                assert record['ResourceRecords'][0]['Value'] == instance_ip
                found = True
                break

        assert found, 'Record set for {} was not found'.format(name)
