"""
=================
test_mediapackage
=================

Verify users can utilize AWS MediaPackage functionality.

${testcount:4}

* Attempt to create two MediaPackage channels
* Attempt to create a MediaPackage origin endpoint on each of the channels
* Attempt to delete the origin endpoint
* Attempt to delete the channel

"""

import pytest

from aws_test_functions.mediapackage import (
    cleanup,
    create_channels_and_endpoints,
    ignore_errors,
)

# This service is blocked in the HIPAA repository
pytestmark = [pytest.mark.scp_check('list_channels')]

pytest_plugins = [
    'tests.plugins.mediapackage'
]


cleanup = ignore_errors(cleanup)


@pytest.fixture(scope='module', autouse=True)
def setup_fixtures(in_setup_org, admin_session):
    client = admin_session.client('mediapackage')
    cleanup(mediapackage=client)

    yield


def test_create_channel_and_endpoint(mediapackage, stack):
    """Verify access to create channels and endpoints.

    :testid: mediapackage-f-1-1, mediapackage-f-1-2

    """

    create_channels_and_endpoints(
        stack,
        mediapackage=mediapackage,
    )


def test_list_channels(mediapackage):
    """Verify access to list channels.

    :testid: mediapackage-f-1-1

    """

    res = mediapackage.list_channels()
    assert len(res['Channels']) > 1, 'Channels not created'


def test_list_origin_endpoints(mediapackage):
    """Verify access to list endpoints.

    :testid: mediapackage-f-1-2

    """

    res = mediapackage.list_origin_endpoints()
    assert len(res['OriginEndpoints']) > 1, 'Endpoints not created'
